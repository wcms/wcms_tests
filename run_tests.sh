rm -rf tests/_output
rm -rf tests/_data

codecoverage=0
stepper=0
debugger=0
testname=""

cp tests/acceptance_norecorder.suite.yml tests/acceptance.suite.yml

while getopts 'crsdt:' OPTION; do
  case "$OPTION" in
    r)
      cp tests/acceptance_recorder.suite.yml tests/acceptance.suite.yml
      ;;
    c)
      codecoverage=1
      ;;
    s)
      stepper=1
      ;;
    d)
      debugger=1
      ;;
    t)
      testname=$OPTARG
      ;;
    ?)
      echo "script usage: [-c] [-r] [-s] [-d] [-t] <TestName>" >&2
      exit 1
      ;;
  esac
done

if [ $stepper == 0 ] && [ $codecoverage == 0 ]; then
  if [ $debugger == 1 ]; then
    if [ $testname ]; then
      php vendor/bin/codecept run acceptance $testname --fail-fast --debug
    else
      php vendor/bin/codecept run acceptance --fail-fast --debug
    fi
  else
    if [ $testname ]; then
      php vendor/bin/codecept run acceptance $testname --fail-fast
    else
      php vendor/bin/codecept run acceptance --fail-fast
    fi
  fi
fi

if [ $stepper == 1 ] && [ $codecoverage == 1 ]; then
  if [ $debugger == 1 ]; then
    if [ $testname ]; then
      php vendor/bin/codecept run acceptance $testname --steps --coverage-html --fail-fast --debug
    else
      php vendor/bin/codecept run acceptance --steps --coverage-html --fail-fast --debug
    fi
  else
    if [ $testname ]; then
      php vendor/bin/codecept run acceptance $testname --steps --coverage-html --fail-fast
    else
      php vendor/bin/codecept run acceptance --steps --coverage-html --fail-fast
    fi
  fi
else
  if [ $stepper == 1 ]; then
    if [ $debugger == 1 ]; then
      if [ $testname ]; then
        php vendor/bin/codecept run acceptance $testname --steps --fail-fast --debug
      else
        php vendor/bin/codecept run acceptance --steps --fail-fast --debug
      fi
    else
      if [ $testname ]; then
        php vendor/bin/codecept run acceptance $testname --steps --fail-fast
      else
        php vendor/bin/codecept run acceptance --steps --fail-fast
      fi
    fi
  fi
  if [ $codecoverage == 1 ]; then
    if [ $debugger == 1 ]; then
      if [ $testname ]; then
        php vendor/bin/codecept run acceptance $testname --coverage-html --debug
      else
        php vendor/bin/codecept run acceptance --coverage-html --debug
      fi
    else
      if [ $testname ]; then
        php vendor/bin/codecept run acceptance $testname --coverage-html --fail-fast
      else
        php vendor/bin/codecept run acceptance --coverage-html --fail-fast
      fi
    fi
  fi
fi

files=(tests/_output/*.fail.html)

if [ -e "${files[0]}" ]; then
  sed -i -- 's/link rel\=\"stylesheet\" media\=\"all\" href\=\"\/core/link rel\=\"stylesheet\" media\=\"all\" href\=\"\.\.\/\.\.\/web\/core/g' tests/_output/*.fail.html
  sed -i -- 's/link rel\=\"stylesheet\" media\=\"all\" href\=\"\/profiles/link rel\=\"stylesheet\" media\=\"all\" href\=\"\.\.\/\.\.\/web\/profiles/g' tests/_output/*.fail.html
fi
