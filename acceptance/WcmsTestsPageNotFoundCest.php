<?php

/**
 * Class PageNotFoundCest.
 *
 * Tests for page not found.
 */
class WcmsTestsPageNotFoundCest {

  /**
   * Tests for page not found.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testPageNotFound(AcceptanceTester $i) {

    // The message to look for.
    $message = 'Page not found';

    // The roles to test.
    $roles = [
      'anonymous',
      'authenticated',
      'administrator',
      'uw_role_site_owner',
      'uw_role_site_manager',
      'uw_role_content_editor',
      'uw_role_content_author',
      'uw_role_form_editor',
      'uw_role_form_results_access',
    ];

    // Check 'Page not found' for each role.
    foreach ($roles as $role) {

      // Visit a page that doesn't exist.
      $i->amOnPage('user/logout');
      $i->logInWithRole($role);
      $i->amOnPage('/asdf');

      // Check if the page is a 404 and contains the 'Page not found' message.
      $i->see('The requested page could not be found. ');
      $i->see($message);
    }

  }

}
