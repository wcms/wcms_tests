<?php

use Codeception\Util\Locator;

/**
 * Class NodeRevisionDeleteCest.
 *
 * Tests for node revision delete.
 */
class WcmsTestsNodeRevisionDeleteCest {

  /**
   * Tests for diff headers.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testNodeRevisionDelete(AcceptanceTester $i) {

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');

    // Check node revision delete config form.
    $i->amOnPage('admin/config/content/node_revision_delete');
    $i->see('Node Revision Delete');

    // Ensures that the current configuration table is found.
    $table_rows = $i->grabMultiple('#edit-current-configuration tbody tr');
    $i->assertGreaterThan(0, count($table_rows), 'Node revison table not null');

    // Ensures that the expected table headers count are found.
    $headers = $i->grabMultiple('thead th');
    $i->assertEquals(8, count($headers));

    // List of expect headers.
    $expected_headers = [
      'Content type',
      'Machine name',
      'Minimum to keep',
      'Minimum age',
      'When to delete',
      'Candidate nodes',
      'Candidate revisions',
      'Operations',
    ];

    // Steo through each of the headers and ensure that it exists.
    foreach ($expected_headers as $header) {
      $i->seeElement(Locator::contains('#edit-current-configuration th', $header));
    }

    // Ensures that all the layouts are listed in the table.
    $content_types = [
      'uw_ct_blog' => 'Blog post',
      'uw_ct_catalog_item' => 'Catalog item',
      'uw_ct_contact' => 'Contact',
      'uw_ct_event' => 'Event',
      'uw_ct_expand_collapse_group' => 'Expand/collapse group',
      'uw_ct_news_item' => 'News item',
      'uw_ct_opportunity' => 'Opportunity',
      'uw_ct_profile' => 'Profile',
      'uw_ct_project' => 'Project',
      'uw_ct_service' => 'Service',
      'uw_ct_sidebar' => 'Sidebar',
      'uw_ct_site_footer' => 'Site footer',
      'uw_ct_web_page' => 'Web page',
    ];
    $i->assertEquals(count($content_types), count($table_rows));

    // Keep track of the index.
    $index = 0;

    // Step through each of the content types and ensure row
    // entries are correct in the table.
    foreach ($content_types as $machine_name => $content_type) {

      // Test for content type.
      $i->assertTrue(str_contains($table_rows[$index], $content_type), $content_type . ' found in table at row ' . ($index + 1));

      // Test for machine name.
      $i->assertTrue(str_contains($table_rows[$index], $machine_name), $machine_name . ' found in table at row ' . ($index + 1));

      // Test for 50 revisions minimum.
      $i->assertTrue(str_contains($table_rows[$index], '50'), '50 minimum to keep found for ' . $content_type . ' at row ' . ($index + 1));

      // Test for minimum age none.
      $i->assertTrue(str_contains($table_rows[$index], 'None'), 'Minimum age none found for ' . $content_type . ' at row ' . ($index + 1));

      // Test for Always delete when found.
      $i->assertTrue(str_contains($table_rows[$index], 'Always delete'), 'When to delete always found for ' . $content_type . ' at row ' . ($index + 1));

      // Test fo 0 canidate nodes and revisions.
      $i->assertTrue(str_contains($table_rows[$index], '0 0'), '0 candidate nodes and revisions found for ' . $content_type . ' at row ' . ($index + 1));

      // Increment the index so we are accessing correct table row.
      $index++;
    }

    // Fields to test for.
    $fields = [
      'edit-node-revision-delete-cron' => '50',
      'edit-node-revision-delete-minimum-age-to-delete-time-max-number' => '12',
      'edit-node-revision-delete-when-to-delete-time-max-number' => '12',
    ];

    // Step through each field and ensure that it exists.
    foreach ($fields as $id => $value) {
      $i->seeElement('input[id="' . $id . '"][value="' . $value . '"]');
    }

    // Select fields to test for.
    $fields = [
      '#edit-node-revision-delete-time' => 'Everyday',
      '#edit-node-revision-delete-minimum-age-to-delete-time-time' => 'Months',
      '#edit-node-revision-delete-when-to-delete-time-time' => 'Months',
    ];

    // Step through each field and ensure that it exists.
    foreach ($fields as $id => $value) {
      $i->seeOptionIsSelected($id, $value);
    }
  }

}
