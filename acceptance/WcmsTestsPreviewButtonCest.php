<?php

/**
 * Class PreviewButtonCest.
 *
 * Tests for preview buttons.
 */
class WcmsTestsPreviewButtonCest {

  /**
   * Tests for preview button.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testPreviewButton(AcceptanceTester $i) {

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');

    // Add new content type.
    $i->amOnPage('admin/structure/types/add');

    // Ensure correct settings.
    $i->seeCheckboxIsChecked('#edit-preview-mode-0');
    $i->dontSeeCheckboxIsChecked('#edit-preview-mode-1');
    $i->dontSeeCheckboxIsChecked('#edit-preview-mode-2');

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Get the UW content types.
    $types = $i->getContentTypes();

    // Step through each type and check there is no
    // preview button.
    foreach ($types as $type) {

      // Load the node-add page.
      $i->amOnPage('node/add/' . $type);

      // Preview button doesn't exist.
      $i->dontSeeElement('#edit-preview');
    }
  }

}
