<?php

use Drupal\webform\Entity\Webform;

/**
 * Class WebformAccessCest.
 *
 * Tests for web forms.
 */
class WcmsTestsWebformAccessCest {

  /**
   * The web form entity.
   *
   * @var \Drupal\webform\Entity\Webform
   */
  private $webform;

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _before(AcceptanceTester $i) {
    // phpcs:enable

    // Get any web forms with title Test Form.
    $webforms = \Drupal::entityTypeManager()
      ->getStorage('webform')
      ->loadByProperties(['title' => 'Test Form']);

    // If there are any web forms delete them.
    if (count($webforms) > 0) {
      foreach ($webforms as $webform) {
        $webform->delete();
      }
    }

    // Create a web form.
    $this->webform = Webform::create([
      'id' => 'test_form',
      'title' => 'Test Form',
    ]);

    // Save the newly created web form.
    $this->webform->save();
  }

  /**
   * Test for web form access.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   *
   * @throws Exception
   */
  public function testAccess(AcceptanceTester $i) {

    // Logout of the site, so we can get correct
    // role when we login next line.
    $i->amOnPage('/user/logout');
    $i->logInWithRole('administrator');

    // Save the access page. If a form is altered by unsetting parts of it
    // instead of setting #access, empty access values will be saved leading to
    // access issues later. Save the page here to verify that no access values
    // get changed.
    $edit = [];
    $i->amOnPage('admin/structure/webform/manage/test_form/access');
    $i->submitForm('#edit-submit', $edit);

    // The variable $test_paths stores Drupal paths and permission information.
    // In this array, the keys are paths. If the value is TRUE, everyone has
    // access. Otherwise, the value must be an array of roles that have access.
    // Other roles do not have access.
    $test_paths = $this->getTestPaths();

    // Test these paths with each user.
    foreach ($i->getRoles() as $role) {

      // Admin always has access so there is no point testing.
      if ($role === 'administrator') {
        continue;
      }

      // Logout of the site, so we can get correct
      // role when we login next line.
      $i->amOnPage('/user/logout');

      // Login with the role.
      $i->logInWithRole($role);

      // Paths that have login message.
      $login_message_paths = $this->getLoginMessagePaths();

      // Step through each path and test the access.
      foreach ($test_paths as $path => $permissions) {

        // Get the expected HTTP response code.
        $expected_code = ($permissions === TRUE || in_array($role, $permissions, TRUE)) ? 200 : 403;

        // Go to the path.
        $i->amOnPage($path);

        // Depending on the code, check for messages to see if
        // we can or can not access.
        if ($expected_code == 403) {
          if (!in_array($path, $login_message_paths)) {
            $i->see('You are not authorized to access this page.');
          }
          else {
            $i->see('You do not appear to have access to this form.');
          }
        }
        else {
          if (!in_array($path, $login_message_paths)) {
            $i->dontSee('You are not authorized to access this page.');
          }
          else {
            $i->dontSee('You do not appear to have access to this form.');
          }
        }
      }
    }
  }

  /**
   * Function to test web form access mode.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testAccessMode(AcceptanceTester $i) {

    // Test access by access mode. Default mode 'all' is tested above.
    $access_mode_tests = [
      // Access for authenticated, no access for anonymous.
      'auth' => [
        'authenticated' => 200,
        'anonymous' => 403,
      ],
      // Access for anonymous, no access for authenticated.
      'anon' => [
        'authenticated' => 403,
        'anonymous' => 200,
      ],
    ];

    // Step through each of the access modes and test the access.
    foreach ($access_mode_tests as $access_mode => $tests) {

      // Logout of the site, so we can get correct
      // role when we login next line.
      $i->amOnPage('/user/logout');

      // Set the form access mode.
      $i->logInWithRole('uw_role_form_editor');

      // Go to the access web form page.
      $i->amOnPage('admin/structure/webform/manage/test_form/access');

      // Select the access control method.
      $selector = 'label[for="edit-access-create-uw-access-control-method-' . $access_mode . '"]';
      $i->click($selector);

      // Submit the form.
      $i->click('#edit-submit');

      // Form editor always has access.
      $i->amOnPage('form/test-form');
      $i->see('Test Form');

      // Test access as various roles.
      foreach ($tests as $role => $status) {

        // Logout of the site, so we can get correct
        // role when we login next line.
        $i->amOnPage('/user/logout');

        if ($role == 'authenticated') {
          // Login with the role.
          $i->logInWithRole($role);
        }

        // Visit the form.
        $i->amOnPage('form/test-form');

        // Check the access.
        if ($status === 403) {
          $i->see('Access Denied');
        }
        else {
          $i->see('Test Form');
        }
      }
    }
  }

  /**
   * Function to get the paths that have a login message.
   *
   * @return string[]
   *   Array of paths that have a login message.
   */
  private function getLoginMessagePaths(): array {

    return [
      'admin/structure/webform/manage/test_form',
      'admin/structure/webform/manage/test_form/results/submissions',
      'admin/structure/webform/manage/test_form/settings',
      'webform/test_form/test',
    ];
  }

  /**
   * Function to get the paths to test.
   *
   * @return array
   *   Array of paths to test.
   */
  private function getTestPaths(): array {

    return [
      'admin' => [
        'uw_role_site_owner',
        'uw_role_site_manager',
        'uw_role_content_editor',
        'uw_role_content_author',
      ],

      // Webform.
      'admin/structure/webform' => [
        'uw_role_form_editor',
        'uw_role_form_results_access',
      ],
      'admin/structure/webform/add' => ['uw_role_form_editor'],
      'admin/structure/webform/addons' => ['uw_role_form_editor'],
      'admin/structure/webform/config' => ['uw_role_form_editor'],
      'admin/structure/webform/config/advanced' => ['uw_role_form_editor'],
      'admin/structure/webform/config/elements' => ['uw_role_form_editor'],
      'admin/structure/webform/config/exporters' => ['uw_role_form_editor'],
      'admin/structure/webform/config/handlers' => ['uw_role_form_editor'],
      'admin/structure/webform/config/libraries' => ['uw_role_form_editor'],
      'admin/structure/webform/options/manage' => ['uw_role_form_editor'],
      'admin/structure/webform/config/submissions' => ['uw_role_form_editor'],
      'admin/structure/webform/config/variants' => ['uw_role_form_editor'],
      'admin/structure/webform/help' => [
        'uw_role_form_editor',
        'uw_role_form_results_access',
      ],
      'admin/structure/webform/submissions/manage' => [
        'uw_role_form_editor',
        'uw_role_form_results_access',
      ],
      // Test Webform.
      'admin/structure/webform/manage/test_form' => ['uw_role_form_editor'],
      'admin/structure/webform/manage/test_form/results/submissions' => [
        'uw_role_form_editor',
        'uw_role_form_results_access',
      ],
      'admin/structure/webform/manage/test_form/settings' => ['uw_role_form_editor'],
      'webform/test_form/test' => ['uw_role_form_editor'],
    ];
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _passed(AcceptanceTester $i) {

    $this->webform->delete();
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i) {
    // phpcs:enable

    $this->webform->delete();
  }

}
