<?php

use Codeception\Util\Locator;
use Drupal\taxonomy\Entity\Term;
use Step\Acceptance\ContentType;
use Step\Acceptance\TaxonomyTest;

/**
 * Class WcmsTestsContentTypeProjectCest.
 *
 * Tests for project content type.
 */
class WcmsTestsContentTypeProjectCest {

  /**
   * Array used for any tids that we created.
   *
   * @var array
   */
  private $tidsUsed = [];

  /**
   * The content type.
   *
   * @var string
   */
  private string $contentType = 'uw_ct_project';

  /**
   * Array of nodes used.
   *
   * @var array
   */
  private array $nodesUsed = [];

  /**
   * Tests for profiles.
   *
   * @param Step\Acceptance\ContentType $i
   *   Acceptance test variable.
   */
  public function testProjectContentType(ContentType $i): void {

    // Test the content type.
    $i->testContentType(
      'Project',
      $i->getContentTypeFields($this->contentType)
    );

    // Test the content type edits.
    $i->testContentTypeEdits(
      $this->contentType,
      $i->getContentTypeEdits($this->contentType)
    );
  }

  /**
   * Function to test taxonomies.
   *
   * @param Step\Acceptance\TaxonomyTest $i
   *   The acceptance tester.
   */
  public function testProjectTaxonomies(TaxonomyTest $i): void {

    // Test the taxonomy terms.
    $i->testTaxonomies(
      $this->contentType,
      'project',
      ['uw_vocab_audience']
    );
  }

  /**
   * Tests for project taxonomy terms.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testProjectTaxonomyTerms(AcceptanceTester $i) {

    // Create a project role term.
    $this->tidsUsed['project_role'] = $i->createTerm(
      'uw_vocab_project_role',
      $i->uwRandomString()
    );

    // Create a project topic term.
    $this->tidsUsed['project_topic'] = $i->createTerm(
      'uw_vocab_project_topic',
      $i->uwRandomString()
    );

    // Login as administrator.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');

    // Check for project role term.
    $i->amOnPage('admin/structure/taxonomy/manage/uw_vocab_project_role/overview');
    $i->see('Project role');
    $i->see(Term::load($this->tidsUsed['project_role'])->getName());

    // Check for project topic term.
    $i->amOnPage('admin/structure/taxonomy/manage/uw_vocab_project_topic/overview');
    $i->see('Project topic');
    $i->see(Term::load($this->tidsUsed['project_topic'])->getName());
  }

  /**
   * Tests for project members.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testProjectMembers(AcceptanceTester $i) {

    // Create the title to be used.
    $title = $i->uwRandomString();

    // Create a project node.
    $this->nodesUsed[$title] = $i->createCtNode(
      $this->contentType,
      $title,
      TRUE
    );

    // Get the path of the webpage.
    $path = $i->getWebPagePath($this->nodesUsed[$title]);

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Go to the node edit page.
    $i->amOnPage($path . '/edit');

    // Create two members.
    $members = [
      $i->uwRandomString(),
      $i->uwRandomString(),
    ];

    // Click the add project member button.
    $i->click('Add Project member');
    $i->waitForElement('textarea[id*="edit-field-uw-project-members-0-subform-field-uw-project-name-0-value"]');

    // Fill the first project member.
    $i->fillField(
      'textarea[id*="edit-field-uw-project-members-0-subform-field-uw-project-name-0-value"]',
      $members[0]
    );

    // Click the add project member button.
    $i->click('Add Project member');
    $i->waitForElement('textarea[id*="edit-field-uw-project-members-1-subform-field-uw-project-name-0-value"]');

    // Fill in the second project member.
    $i->fillField(
      'textarea[id*="edit-field-uw-project-members-1-subform-field-uw-project-name-0-value"]',
      $members[1]
    );

    // Click the save button.
    $i->click('Save');
    $i->see('Project ' . $title . ' has been updated.');

    // Check for only one project member.
    foreach ($members as $member) {
      $i->seeNumberOfElements(
        Locator::contains('.card__project-member', $member),
        1
      );
    }
  }

  // phpcs:disable
  /**
   * Function to run after the test fails.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _after(AcceptanceTester $i): void {
    // phpcs:enable

    // Delete the terms we created.
    foreach ($this->tidsUsed as $tid) {
      $term = Term::load($tid);
      if ($term) {
        $term->delete();
      }
    }

    // If there are nodes then delete them.
    if (!empty($this->nodesUsed)) {
      foreach ($this->nodesUsed as $node) {
        $node->delete();
      }
    }

    // Clear out the nodes used array.
    $this->nodesUsed = [];

    // Delete all the blocks.
    $i->deleteAllBlocks();
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i): void {
    // phpcs:enable

    // Delete the terms we created.
    foreach ($this->tidsUsed as $tid) {
      $term = Term::load($tid);
      if ($term) {
        $term->delete();
      }
    }

    // If there are nodes then delete them.
    if (!empty($this->nodesUsed)) {
      foreach ($this->nodesUsed as $node) {
        $node->delete();
      }
    }

    // Clear out the nodes used array.
    $this->nodesUsed = [];

    // Delete all the blocks.
    $i->deleteAllBlocks();
  }

}
