<?php

use Codeception\Util\Locator;
use Drupal\taxonomy\Entity\Term;

/**
 * Class WcmsTestsMediaAboveCest.
 *
 * Tests for media above.
 */
class WcmsTestsMediaAboveCest {

  /**
   * Array of nodes used.
   *
   * @var array
   */
  private array $nodesUsed = [];

  /**
   * Array of term ids used.
   *
   * @var array
   */
  private array $tidsUsed = [];

  /**
   * Test media above settings.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testSettingsMediaAbove(AcceptanceTester $i) {

    // Get the content types with flags about media.
    $content_types = $this->getMediaAboveContentTypes();

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Step through each content type and check if media settings are correct.
    foreach ($content_types as $content_type => $both_media_flag) {

      // Create nodes for all content types.
      // If this a catalog add a catalog term first.
      if ($content_type == 'uw_ct_catalog_item') {

        // Get a term title.
        $term_title = $i->uwRandomString();

        // Create a catalog term.
        $this->tidsUsed[$term_title] = $i->createTerm(
          'uw_vocab_catalogs',
          $term_title
        );
      }

      // If this a service add a service term first.
      if ($content_type == 'uw_ct_service') {

        // Get a term title.
        $term_title = $i->uwRandomString();

        // Create a catalog term.
        $this->tidsUsed[$term_title] = $i->createTerm(
          'uw_vocab_service_categories',
          $term_title
        );
      }

      // Get the title for the node.
      $node_title = $i->uwRandomString();

      // Create the node.
      $this->nodesUsed[$node_title] = $i->createCtNode(
        $content_type,
        $node_title,
        TRUE
      );

      // Get the path of the webpage.
      $path = $i->getWebPagePath($this->nodesUsed[$node_title]);

      // Go to this node page.
      $i->amOnPage($path);

      // Get the actual path (node/{nid}).
      $path2 = \Drupal::service('path_alias.manager')->getPathByAlias($i->getCurrentUrl());

      // Get nid from the actual path.
      if (preg_match('/node\/(\d+)/', $path2, $matches)) {
        $nid = $matches[1];
      }

      // Check type of media options for all paths of all content types.
      $paths = [
        'node/add/' . $content_type,
        $i->getWebPagePath($this->nodesUsed[$node_title]) . '/edit',
        'clone/' . $nid . '/quick_clone',
      ];

      foreach ($paths as $path) {
        // Go to content type create, edit and clone page.
        $i->amOnPage($path);

        // Check for none and banner options.
        $i->seeElement('#edit-field-uw-type-of-media option[value="_none"]');
        $i->seeElement('#edit-field-uw-type-of-media option[value="banner"]');

        // If the flag for both media is set, check for image option.
        if ($both_media_flag) {
          $i->seeElement('#edit-field-uw-type-of-media option[value="image"]');
        }
        else {
          $i->dontSeeElement('#edit-field-uw-type-of-media option[value="image"]');
        }
      }
    }
  }

  /**
   * Function to test the banners above.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testBannersAbove(AcceptanceTester $i) {

    // Get the image styles.
    $image_styles = $this->getImageStyles();

    // The images that are to be used.
    $images = $this->getImagesForBanners($i);

    // Get the content types with flags about media.
    $content_types = $this->getMediaAboveContentTypes();

    // Get the types of banners and their associated classes.
    $banner_types = $this->getBannerTypes();

    // Login as administrator.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');

    // Set the images for media above.
    $this->setImagesForMediaAbove($i, $images);

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Step through each content type and add the image banner.
    foreach (array_keys($content_types) as $content_type) {

      // If this a catalog add a catalog term first.
      if ($content_type == 'uw_ct_catalog_item') {

        // Get a term title.
        $term_title = $i->uwRandomString();

        // Create a catalog term.
        $this->tidsUsed[$term_title] = $i->createTerm(
          'uw_vocab_catalogs',
          $term_title
        );
      }

      // If this a service add a service term first.
      if ($content_type == 'uw_ct_service') {

        // Get a term title.
        $term_title = $i->uwRandomString();

        // Create a catalog term.
        $this->tidsUsed[$term_title] = $i->createTerm(
          'uw_vocab_service_categories',
          $term_title
        );
      }

      // Get the title for the node.
      $node_title = $i->uwRandomString();

      // Create the node.
      $this->nodesUsed[$node_title] = $i->createCtNode(
        $content_type,
        $node_title,
        TRUE
      );

      // Get the path of the webpage.
      $path = $i->getWebPagePath($this->nodesUsed[$node_title]);

      // Go to content type edit page.
      $i->amOnPage($path . '/edit');

      // Select the option for banners.
      $i->selectOption('#edit-field-uw-type-of-media', 'banner');
      $i->waitForElement(Locator::contains('span', 'Banner'));

      // Add banner images to the node.
      $this->addBannerImagesToNode($i, $images);

      // Click the save button and ensure that the node is saved.
      $i->click('#edit-submit');
      $i->see($node_title . ' has been updated');

      // Counter for full image testing.
      $counter = 0;

      // Step through each banner type and test the images and class.
      foreach ($banner_types as $banner_type => $banner_class) {

        // Go to content type edit page.
        $i->amOnPage($path . '/edit');

        // Change the banner type and save the node.
        $i->selectOption('#edit-field-uw-text-overlay-style', $banner_type);
        $i->click('#edit-submit');
        $i->see($node_title . ' has been updated');

        // Test the banner images and class on the node.
        // We only need to test the actual images on the
        // node once, since the images will be on the
        // other banner types, hence why the counter is
        // here in the parameters and checking for 0, so
        // we only do that the on the first banner type.
        $this->testNodeBannerImages(
          $i,
          $images,
          $image_styles,
          $banner_class,
          $counter == 0
        );

        // Increment the counter for the test image flag.
        $counter++;
      }

      // Delete banner image.
      $i->amOnPage('admin/content/media');
      $i->checkOption('input[id="edit-media-bulk-form-0"]');
      $i->click('#edit-submit');
      $i->waitForText('Are you sure you want to delete this media item?');
      $i->click('#edit-submit');

      // Return to content page.
      $i->amOnPage($path);

      // Check title still displays.
      $i->waitForText($node_title);
      $i->seeElement(Locator::contains('h1', $node_title));

      // Re-upload image to media.
      $this->setImageForMedia($i, $images[array_key_last($images)]);
    }
  }

  /**
   * Function to test the banners above.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testImageAbove(AcceptanceTester $i) {

    // Get the content types.
    $content_types = $this->getMediaAboveContentTypes();

    // Get the images and remove the second and third, since
    // we only need one image for this test.
    $images = $this->getImagesForBanners($i);
    unset($images[1]);
    unset($images[2]);

    // Login as administrator.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');

    // Set the images to be used.
    $this->setImagesForMediaAbove($i, $images);

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Step through each content type and if it has hero images,
    // then test the hero images.
    foreach ($content_types as $content_type => $both_media_flag) {

      // If the both media flag is set, test the hero image.
      if ($both_media_flag) {

        // Get the node title and create the node.
        $node_title = $i->uwRandomString();
        $this->nodesUsed[$node_title] = $i->createCtNode(
          $content_type,
          $node_title,
          TRUE
        );

        // Get the path to the node.
        $path = $i->getWebPagePath($this->nodesUsed[$node_title]);

        // Go to the node edit page.
        $i->amOnPage($path . '/edit');
        $i->waitForText($node_title);

        // Select the hero image option.
        $i->selectOption('#edit-field-uw-type-of-media', 'image');

        // Ensure that the Add media button shows up.
        $i->waitForElementClickable('#edit-field-uw-hero-image-open-button');

        // Click the Add media button.
        $i->click('#edit-field-uw-hero-image-open-button');
        $i->waitForText('Add or select media');
        $i->waitForElementClickable(Locator::contains('button', 'Insert selected'));

        // Check the image to be used.
        $for = $i->grabAttributeFrom(
          Locator::contains(
            'label',
            'Select ' . $images[0]['title']
          ),
          'for'
        );
        $i->checkOption('#' . $for);

        // Click the insert selected button.
        $i->click(Locator::contains('button', 'Insert selected'));

        // Ensure that the image is on the page.
        $i->waitForElement('.media-library-item__preview');
        $i->seeElement('.media-library-item__preview');
        $i->waitForElement('.media-library-item__name');
        $i->see($images[0]['title']);
        $i->waitForText('The maximum number');
        $i->see('The maximum number of media items have been selected.');

        // Click save and ensure that node is updated.
        $i->click('Save');
        $i->see($node_title . ' has been updated');

        // No easy way to test the background image, so just test that there
        // is a style tag and the title is correct.
        $i->seeElementInDOM('.card__media style');
        $i->seeElement('.card__featured-image');
        $i->seeElement(Locator::contains('.card__featured-image .card__title', $node_title));

        // Delete hero image.
        $i->amOnPage('admin/content/media');
        $i->checkOption('input[id="edit-media-bulk-form-0"]');
        $i->click('#edit-submit');
        $i->waitForText('Are you sure you want to delete this media item?');
        $i->click('#edit-submit');

        // Return to content page.
        $i->amOnPage($path);

        // Check title still displays.
        $i->waitForText($node_title);
        $i->seeElement(Locator::contains('h1', $node_title));

        // Re-upload image to media.
        $this->setImageForMedia($i, $images[0]);
      }
    }
  }

  /**
   * Function to set the images to be used in banners.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   * @param array $images
   *   The array of images.
   */
  private function setImagesForMediaAbove(AcceptanceTester $i, array $images): void {

    // Step through all the images and add them.
    foreach ($images as $image) {

      // Go to the add image page.
      $i->amOnPage('media/add/uw_mt_image');
      $i->see('Add image');

      // Add remote image.
      $i->click('Remote URL');
      $i->fillField(
        '#edit-name-0-value',
        $image['title'],
      );
      $i->fillField(
        '#edit-field-media-image-0-filefield-remote-url',
        $image['url'],
      );

      // Click and transger and ensure that it uploads.
      $i->click('Transfer');
      $i->waitForText('Alternative text');

      // Fill in the alt text field.
      $i->fillField('input[id*="edit-field-media-image-0-alt"]', $image['title']);

      // Click save and ensure that the image is added.
      $i->click('Save');
      $i->see('Image ' . $image['title'] . ' has been created');
    }
  }

  /**
   * Get the content types with certain info.
   *
   * @return array
   *   Array of content types with flags about media.
   */
  private function getMediaAboveContentTypes(): array {

    return [
      'uw_ct_blog' => TRUE,
      'uw_ct_catalog_item' => FALSE,
      'uw_ct_contact' => FALSE,
      'uw_ct_event' => TRUE,
      'uw_ct_news_item' => TRUE,
      'uw_ct_opportunity' => FALSE,
      'uw_ct_profile' => FALSE,
      'uw_ct_project' => FALSE,
      'uw_ct_service' => FALSE,
      'uw_ct_web_page' => FALSE,
    ];
  }

  /**
   * Function to get the images for banners.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   *
   * @return array[]
   *   Array of images for banners.
   */
  private function getImagesForBanners(AcceptanceTester $i): array {

    // The images that are to be used.
    return [
      [
        'url' => 'https://uwaterloo.ca/building-the-next-wcms/sites/default/files/uploads/images/exception-experience-wcms3-banner.png',
        'title' => $i->uwRandomString(),
      ],
      [
        'url' => 'https://uwaterloo.ca/building-the-next-wcms/sites/default/files/uploads/images/drupalcon-group-photo-1280.jpg',
        'title' => $i->uwRandomString(),
      ],
      [
        'url' => 'https://uwaterloo.ca/building-the-next-wcms/sites/default/files/uploads/images/cloud-services-compared-to-transportation.jpg',
        'title' => $i->uwRandomString(),
      ],
    ];
  }

  /**
   * Function to set the image to be used in hero image.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   * @param array $image
   *   The image.
   */
  private function setImageForMedia(AcceptanceTester $i, array $image): void {

    // Go to the add image page.
    $i->amOnPage('media/add/uw_mt_image');
    $i->see('Add Image');

    // Add remote image.
    $i->click('Remote URL');
    $i->fillField(
      '#edit-name-0-value',
      $image['title'],
    );
    $i->fillField(
      '#edit-field-media-image-0-filefield-remote-url',
      $image['url'],
    );

    // Click and transger and ensure that it uploads.
    $i->click('Transfer');
    $i->waitForText('Alternative text');

    // Fill in the alt text field.
    $i->fillField('input[id*="edit-field-media-image-0-alt"]', $image['title']);

    // Click save and ensure that the image is added.
    $i->click('Save');
    $i->see('Image ' . $image['title'] . ' has been created');
  }

  /**
   * Function to get the image styles.
   *
   * @return string[]
   *   Array of image styles.
   */
  private function getImageStyles(): array {

    return [
      'uw_is_media_x_large',
      'uw_is_media_large',
      'uw_is_media_medium',
      'uw_is_media_small',
      'uw_is_media_x_small',
    ];
  }

  /**
   * Function to add the banner images to the node.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   * @param array $images
   *   The array of images.
   */
  private function addBannerImagesToNode(AcceptanceTester $i, array $images): void {

    // Add the three images from above.
    for ($j = 0; $j < 3; $j++) {

      // Click on the add image banner button.
      $i->click('Add Image banner');
      $image_banner_id = 'edit-field-uw-banner-' . $j . '-subform-field-uw-ban-image-open-button';

      // Ensure the element is visible and clickable.
      $i->waitForElementVisible('input[id*="' . $image_banner_id . '"]', 30);
      $i->waitForElementClickable('input[id*="' . $image_banner_id . '"]', 30);

      // Use JavaScript to scroll to the element.
      $i->executeJS("document.querySelector('input[id*=\"" . $image_banner_id . "\"]').scrollIntoView();");

      // Click on the Add media button.
      $i->click('input[id*="' . $image_banner_id . '"]');

      $i->waitForText('Add or select media');
      $i->waitForElementClickable(Locator::contains('button', 'Insert selected'));

      // Check the image to be used.
      $for = $i->grabAttributeFrom(
        Locator::contains(
          'label',
          'Select ' . $images[$j]['title']
        ),
        'for'
      );
      $i->checkOption('#' . $for);

      // Click the insert selected button.
      $i->click(Locator::contains('button', 'Insert selected'));

      // Ensure that the image is on the page.
      $i->waitForElement('.media-library-item__preview');
      $i->see('A minimum banner size of 1010x300 is recommended. ');

      // Ensure that the link field is on the page.
      $i->waitForText('Provide an optional link for this banner. ');

      // Fill in the link field.
      $uriname = 'field_uw_banner[' . $j . '][subform][field_uw_ban_link][' . $j . '][uri]';
      $i->grabMultiple(Locator::find('input', ['name' => $uriname]));
      $i->fillField(
        'input[id*="edit-field-uw-banner-' . $j . '-subform-field-uw-ban-link-0-uri"]',
        'https://google.ca'
      );

      // Ensure that the big text field is on the page.
      $i->waitForText('Big text appears at a larger size, and, if both are set, above the small text.');

      // Fill in the big text field.
      $bigname = 'field_uw_banner[' . $j . '][subform][field_uw_ban_big_text][' . $j . '][value]';
      $i->grabMultiple(Locator::find('input', ['name' => $bigname]));
      $i->fillField(
        'input[id*="edit-field-uw-banner-' . $j . '-subform-field-uw-ban-big-text-0-value"]',
        'Big text ' . ($j + 1)
      );

      // Ensure that the small text field is on the page.
      $i->waitForText('Small text appears at a smaller size, and, if both are set, below the big text.');

      // Fill in the small text field.
      $smallname = 'field_uw_banner[' . $j . '][subform][field_uw_ban_small_text][' . $j . '][value]';
      $i->grabMultiple(Locator::find('input', ['name' => $smallname]));
      $i->fillField(
        'input[id*="edit-field-uw-banner-' . $j . '-subform-field-uw-ban-small-text-0-value"]',
        'Small text ' . ($j + 1)
      );
    }
  }

  /**
   * Function to test the banner images on the node.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   * @param array $images
   *   The array of images.
   * @param array $image_styles
   *   The array of image styles.
   * @param string $banner_class
   *   The banner class to test for.
   * @param bool $test_images_flag
   *   Flag for testing the images on the node.
   */
  private function testNodeBannerImages(
    AcceptanceTester $i,
    array $images,
    array $image_styles,
    string $banner_class,
    bool $test_images_flag,
  ): void {

    // Ensure that the associated class with the banner type is on the page.
    $i->seeElement('.' . $banner_class);

    // Test the actual images if the flag is set.
    if ($test_images_flag) {

      // Step through three times for each banner.
      for ($j = 1; $j <= 3; $j++) {

        // Ensure that we can see the link in the banner.
        $i->seeElementInDOM('#banner' . $j . '-');

        // Ensure that we can see the big text.
        $i->seeElementInDOM(
          Locator::contains(
            '#banner' . $j . '- .card__banner--title',
            'Big text ' . $j
          )
        );

        // Ensure that we can see the small text.
        $i->seeElementInDOM(
          Locator::contains(
            '#banner' . $j . '- .card__banner--sub-title',
            'Small text ' . $j
          )
        );

        // Get the parts of the image so that we can get
        // the full name of the image.
        $image_parts = explode('/', $images[$j - 1]['url']);
        $image_name = end($image_parts);
        $image_name = explode('.', $image_name);
        $image_name = $image_name[0];

        // Step through each of the image styles and ensure that
        // the srcset is being set correctly.
        foreach ($image_styles as $image_style) {

          // Get the partial url to the image with the image style.
          $image_style_url = $image_style;
          $image_style_url .= '/public/uploads/images/';
          $image_style_url .= $image_name;

          // Ensure that we see the image url in the srcset.
          $i->seeElementInDOM('source[srcset*="' . $image_style_url . '"]');
        }
      }
    }
  }

  /**
   * Function to get the banner types and their associated classes.
   *
   * @return string[]
   *   Array of banner types and their associated classes.
   */
  private function getBannerTypes(): array {

    return [
      'full-width' => 'uw-text-overlay--full-width',
      'left-dark' => 'uw-text-overlay--left-dark',
      'left-light' => 'uw-text-overlay--left-light',
      'split' => 'uw-text-overlay--split',
      'full-overlay' => 'uw-text-overlay--full-overlay',
    ];
  }

  // phpcs:disable

  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i): void {
    // phpcs:enable

    // Step through each of the nodes used and delete them.
    foreach ($this->nodesUsed as $node) {
      $node->delete();
    }

    // Reset the nodes used array.
    $this->nodesUsed = [];

    // If there are tids, delete them and reset array.
    if (!empty($this->tidsUsed)) {
      foreach ($this->tidsUsed as $tid) {
        $term = Term::load($tid);
        $term->delete();
      }
      $this->tidsUsed = [];
    }
  }

  // phpcs:disable

  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _after(AcceptanceTester $i): void {
    // phpcs:enable

    // Step through each of the nodes used and delete them.
    foreach ($this->nodesUsed as $node) {
      $node->delete();
    }

    // Reset the nodes used array.
    $this->nodesUsed = [];

    // If there are tids, delete them and reset array.
    if (!empty($this->tidsUsed)) {
      foreach ($this->tidsUsed as $tid) {
        $term = Term::load($tid);
        $term->delete();
      }
      $this->tidsUsed = [];
    }
  }

}
