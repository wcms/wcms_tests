<?php

use Step\Acceptance\ContentType;
use Facebook\WebDriver\WebDriverKeys;

/**
 * Class WcmsTestsContentTypeProfileCest.
 *
 * Tests for profiles.
 */
class WcmsTestsContentTypeProfileCest {

  /**
   * The content type.
   *
   * @var string
   */
  private string $contentType = 'uw_ct_profile';

  /**
   * Array of nodes used.
   *
   * @var array
   */
  private array $nodesUsed = [];

  /**
   * Tests for profiles.
   *
   * @param Step\Acceptance\ContentType $i
   *   Acceptance test variable.
   */
  public function testProfileContentType(ContentType $i): void {

    // Test the content type.
    $i->testContentType(
      'Profile',
      $i->getContentTypeFields($this->contentType)
    );

    // Test the content type edits.
    $i->testContentTypeEdits(
      $this->contentType,
      $i->getContentTypeEdits($this->contentType)
    );
  }

  /**
   * Tests for profiles synchronization with contacts.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testProfileContactSynchronization(AcceptanceTester $i) {
    // Login as site manager user.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Set random values for contact fields.
    $contact_values = [
      'contactName' => $i->uwRandomString(),
      'contactSortName' => $i->uwRandomString(),
      'contactAffiliation' => $i->uwRandomString(),
      'contactTitle' => $i->uwRandomString(),
    ];

    // Contact fields that sync with profile.
    $contact_fields = [
      'title' => &$contact_values['contactName'],
      'field_uw_ct_contact_sort_name' => &$contact_values['contactSortName'],
      'field_uw_ct_contact_affiliation' => &$contact_values['contactAffiliation'],
      'field_uw_ct_contact_title' => &$contact_values['contactTitle'],
    ];

    // Profile fields that sync with contact.
    $profile_fields = [
      'title' => &$contact_values['contactName'],
      'field_uw_ct_profile_sort_name' => &$contact_values['contactSortName'],
      'field_uw_ct_profile_affiliation' => &$contact_values['contactAffiliation'],
      'field_uw_ct_profile_title' => &$contact_values['contactTitle'],
    ];

    // Create a contact.
    $this->nodesUsed[$contact_values['contactName']] = $i->createCtNode(
      'uw_ct_contact',
      $contact_values['contactName'],
      TRUE,
      $contact_fields,
    );

    // Go to the profile add page.
    $i->amOnPage('node/add');
    $i->click('Profile');

    // Wait for contact sync field to load.
    $i->waitForElement('input[name="field_uw_ct_profile_contact[0][target_id]"]');

    // Fill contact sync field.
    $i->fillField('input[name="field_uw_ct_profile_contact[0][target_id]"]', $contact_values['contactName']);
    $i->pressKey('input[name="field_uw_ct_profile_contact[0][target_id]"]', WebDriverKeys::ENTER);

    // Wait briefly for fields to sync.
    $i->waitForElement('input[name="title[0][value]"][value="' . $contact_values['contactName'] . '"]');

    // Check synced fields.
    foreach ($profile_fields as $key => $val) {
      $i->seeElement('input[name="' . $key . '[0][value]"][value="' . $val . '"]');
    }

    // Fill in other required profile fields.
    $i->checkOption('input[name="field_uw_blank_summary[value]"]');
    $i->fillField('textarea[name="field_uw_meta_description[0][value]"]', $i->uwRandomString());
    $i->selectOption('select[name="moderation_state[0][state]"]', 'Published');

    // Create profile.
    $i->click('input[id^="edit-submit"]');

    // Goto contact edit page.
    $i->amOnPage('contacts/' . strtolower($contact_values['contactName']) . '/edit');

    // Set new contact values.
    foreach (array_keys($contact_values) as $key) {
      $contact_values[$key] = $i->uwRandomString();
    }

    // Update contact.
    foreach ($contact_fields as $key => $val) {
      $i->fillField('input[name="' . $key . '[0][value]"]', $val);
    }
    $i->click('input[id^="edit-submit"]');

    // Check profile for synced changes.
    $i->amOnPage('profiles/' . strtolower($contact_values['contactName']) . '/edit');
    foreach ($profile_fields as $key => $val) {
      $i->seeElement('input[name="' . $key . '[0][value]"][value="' . $val . '"]');
    }
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _after(AcceptanceTester $i): void {
    // phpcs:enable

    // Step through each of the nodes used and delete them.
    foreach ($this->nodesUsed as $node) {
      $node->delete();
    }

    // Delete all the blocks.
    $i->deleteAllBlocks();
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i): void {
    // phpcs:enable

    // Step through each of the nodes used and delete them.
    foreach ($this->nodesUsed as $node) {
      $node->delete();
    }

    // Delete all the blocks.
    $i->deleteAllBlocks();
  }

}
