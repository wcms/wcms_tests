<?php

use Drupal\webform\Entity\Webform;

/**
 * Class WebformElementsCest.
 *
 * Tests for web forms elements.
 */
class WcmsTestsWebformElementsCest {

  /**
   * The web form entity.
   *
   * @var \Drupal\webform\Entity\Webform
   */
  private $webform;

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _before(AcceptanceTester $i) {
    // phpcs:enable

    // Get any web forms with title Test Form.
    $webforms = \Drupal::entityTypeManager()
      ->getStorage('webform')
      ->loadByProperties(['title' => 'Test Form']);

    // If there are any web forms delete them.
    if (count($webforms) > 0) {
      foreach ($webforms as $webform) {
        $webform->delete();
      }
    }

    // Create a web form.
    $this->webform = Webform::create([
      'id' => 'test_form',
      'title' => 'Test Form',
    ]);

    // Save the newly created web form.
    $this->webform->save();
  }

  /**
   * Test for web form basic elements.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   *
   * @throws Exception
   */
  public function testElementsBasic(AcceptanceTester $i) {

    // Login as form editor.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_form_editor');

    $this->testElements($i, 'basic');
  }

  /**
   * Test for web form options elements.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   *
   * @throws Exception
   */
  public function testElementsOptions(AcceptanceTester $i) {

    // Login as form editor.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_form_editor');

    $this->testElements($i, 'options');
  }

  /**
   * Test for web form advanced elements.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   *
   * @throws Exception
   */
  public function testElementsAdvanced(AcceptanceTester $i) {

    // Login as form editor.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_form_editor');

    $this->testElements($i, 'advanced');
  }

  /**
   * Test for web form composite elements.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   *
   * @throws Exception
   */
  public function testElementsComposite(AcceptanceTester $i) {

    // Login as form editor.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_form_editor');

    $this->testElements($i, 'composite');
  }

  /**
   * Test for web form date elements.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   *
   * @throws Exception
   */
  public function testElementsDate(AcceptanceTester $i) {

    // Login as form editor.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_form_editor');

    $this->testElements($i, 'date');
  }

  /**
   * Test for web form entity elements.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   *
   * @throws Exception
   */
  public function testElementsEntity(AcceptanceTester $i) {

    // Login as form editor.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_form_editor');

    $this->testElements($i, 'entity');
  }

  /**
   * Test for web form containers elements.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   *
   * @throws Exception
   */
  public function testElementsContainers(AcceptanceTester $i) {

    // Login as form editor.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_form_editor');

    $this->testElements($i, 'containers');
  }

  /**
   * Function to test the elements of a web form.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   * @param string $type
   *   The type of elements to test.
   *
   * @throws Exception
   */
  private function testElements(AcceptanceTester $i, string $type): void {

    // Get the elements to test.
    $elements = $this->getWebformElements($type);

    // Get the elements that we are not to see, mainly
    // the attributes.
    $webform_elements_not_to_see = $this->getWebformElementsNotToSee();

    // Make sure 'Wrapper attributes' does not exist for all elements.
    foreach ($elements as $element => $machine_name) {

      // The elements that we are not going to add
      // programmatically, only click to add.
      $elements_click_only = [
        'Custom composite',
        'Radios',
        'Radios other',
        'Select',
        'Select other',
        'Table select',
        'Entity autocomplete',
        'Entity checkboxes',
        'Entity radios',
        'Entity select',
        'Media library',
        'Term checkboxes',
        'Term select',
      ];

      // If this is a field to add programmatically, then
      // add it.  If not add using clicks.
      if (!in_array($element, $elements_click_only)) {

        // Add the new field with the element to test.
        // Much faster to add field programmatically.
        $new_field = [
          $element => [
            '#type' => $machine_name,
            '#title' => $element,
          ],
        ];

        // If there is no element, add it to the web form.
        if (!in_array($element, $this->webform->getElementsDecoded())) {
          $new_element_arrangment = array_merge($this->webform->getElementsDecoded(), $new_field);
          $this->webform->setElements($new_element_arrangment)->save();
        }

        // Go to the build of the web form.
        $i->amOnPage('/admin/structure/webform/manage/test_form');

        // Ensure that we see the element and then click on it.
        $i->see($element);

        // The css selector for the edit element button.
        $css_selector = '.webform-ui-element-type-' . $machine_name . ' .dropbutton-action a';

        // Click the edit element button.
        $i->click($css_selector);
      }
      else {

        // Go to the build of the webform.
        $i->amOnPage('/admin/structure/webform/manage/test_form');

        // Click on the Add element button.
        $i->click('Add element');
        $i->waitForText('Select an element');

        // Wait for the element to appear and click on it.
        $i->waitForText($element);
        $i->click($element);

        // Wait for element to appear and that we see it.
        $i->waitForText($element);
        $i->see($element);
      }

      // Wait for Advanced tab to appear, then click on it.
      $i->waitForText('Advanced');
      $i->click('Advanced');
      $i->waitForText('Browse available tokens.');

      // If this is not an item, ensure that we can see
      // the default values tab.
      if ($element !== 'Item') {
        $i->seeElement('details[id*="edit-default"]');
      }

      // For some reason this is needed as the General
      // tab gets clicked.
      $i->click('Advanced');
      $i->waitForText('Browse available tokens.');

      // Step through each of the elements not to see and ensure
      // that we do not see them.
      foreach ($webform_elements_not_to_see as $webform_element_not_to_see) {

        // For some reason this is needed as the General
        // tab gets clicked.
        $i->click('Advanced');
        $i->waitForText('Browse available tokens.');
        $i->dontSee($webform_element_not_to_see);
      }

      if ($element !== 'Item') {

        // Click on the Submission display tab and ensure that
        // we do not see the display wrapper attributes.
        $i->click('details[id*="edit-display"]');

        // For some reason this is needed as the General
        // tab gets clicked.
        $i->click('Advanced');
        $i->waitForText('Browse available tokens.');

        $i->dontSee('DISPLAY WRAPPER ATTRIBUTES');
      }
    }
  }

  /**
   * Function to get the elements in web forms to not see.
   *
   * @return string[]
   *   Array of elements not to see in web forms.
   */
  private function getWebformElementsNotToSee(): array {

    return [
      'ELEMENT ATTRIBUTES',
      'LABEL ATTRIBUTES',
      'SUMMARY ATTRIBUTES',
      'TITLE ATTRIBUTES',
      'WRAPPER ATTRIBUTES',
    ];
  }

  /**
   * Function to get the web form elements to check.
   *
   * @param string $type
   *   The type of elements to get.
   *
   * @return string[]
   *   Array of web form elements to check.
   */
  private function getWebformElements(string $type): array {

    // Return at least a blank array.
    $elements = [];

    switch ($type) {
      case 'basic':
        $elements = [
          'Checkbox' => 'checkbox',
          'Textarea' => 'textarea',
          'Text field' => 'textfield',
        ];
        break;

      case 'advanced':
        $elements = [
          'Autocomplete' => 'webform_autocomplete',
          'Color' => 'color',
          'Email' => 'email',
          'Email confirm' => 'webform_email_confirm',
          'Email multiple' => 'webform_email_multiple',
          'Height' => 'webform_height',
          'Mapping' => 'webform_mapping',
          'Number' => 'number',
          'Range' => 'range',
          'Same as' => 'webform_same',
          'Scale' => 'webform_scale',
          'Telephone' => 'tel',
          'Terms of service' => 'webform_terms_of_service',
          'URL' => 'url',
        ];
        break;

      case 'composite':
        $elements = [
          'Basic address' => 'webform_address',
          'Contact' => 'webform_contact',
          'Custom composite' => 'webform_custom_composite',
          'Link' => 'webform_link',
          'Name' => 'webform_name',
          'Telephone advanced' => 'webform_telephone',
        ];
        break;

      case 'date':
        $elements = [
          'Date' => 'date',
          'Date Time' => 'datetime',
          'Date list' => 'datelist',
          'Time' => 'webform_time',
        ];
        break;

      case 'options':
        $elements = [
          'Checkboxes' => 'checkboxes',
          'Checkboxes other' => 'webform_checkboxes_other',
          // @todo Fix this later to work
          // 'Likert' => 'webform_likert',
          'Radios' => 'radios',
          'Radios other' => 'webform_radio_others',
          'Select' => 'select',
          'Select other' => 'webform_select_other',
          'Table select' => 'tableselect',
        ];
        break;

      case 'entity':
        $elements = [
          'Entity autocomplete' => 'entity_autocomplete',
          'Entity checkboxes' => 'webform_entity_checkboxes',
          'Entity radios' => 'webform_entity_radios',
          'Entity select' => 'webform_entity_select',
          'Media library' => 'media_library',
          'Term checkboxes' => 'webform_term_checkboxes',
          'Term select' => 'webform_term_select',
        ];
        break;

      case 'containers':
        $elements = [
          'Item' => 'item',
          'Table' => 'webform_table',
        ];
        break;
    }

    return $elements;
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _passed(AcceptanceTester $i) {

    $this->webform->delete();
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i) {
    // phpcs:enable

    $this->webform->delete();
  }

}
