<?php

/**
 * Class ServiceHoursCest.
 *
 * Tests for service hours.
 */
class WcmsTestsServiceHoursCest {

  /**
   * Array used for any tids that we created.
   *
   * @var array
   */
  private $tidsUsed = [];

  /**
   * Tests for service hours.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testServiceHours(AcceptanceTester $i) {

    // Create a new term and get tid.
    $this->tidsUsed[] = $i->createTerm('uw_vocab_service_categories', $i->uwRandomString());

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Go to add service node page amd ensure page loads.
    $i->amOnPage('/node/add/uw_ct_service');
    $i->see('Create Service');

    // The field value.
    $title = 'Service1';
    $summary = 'Summary for service 1';
    $status = 'Future';

    // Fill in the fields.
    $i->fillField('title[0][value]', $title);
    $i->fillCkEditor($summary, '#edit-field-uw-service-summary-0-value');
    $i->selectOption('#edit-field-uw-service-status', $status);
    $i->checkOption('#edit-field-uw-service-category-0-' . $this->tidsUsed[0] . '-' . $this->tidsUsed[0]);

    // The field values for exception.
    $reg_date = date('m/d/Y');
    $exp_date = date('m/d/Y', strtotime("tomorrow"));
    $start_time = '10:00AM';
    $end_time = '15:00:00';

    // Get the correct day of the week for the id of
    // the start and end time.
    $day_of_week = date('N');
    if ($day_of_week == 7) {
      $day_of_week = 0;
    }
    else {
      $day_of_week = $day_of_week * 2;
    }

    // Fill in the fields.
    $i->fillField('field_uw_service_hours[office_hours][0][value][' . $day_of_week . '][starthours][time]', $start_time);
    $i->fillField('field_uw_service_hours[office_hours][0][value][' . $day_of_week . '][endhours][time]', $end_time);

    // Click the add exception button.
    $i->click('Add exception');
    $i->waitForElement('input[id^="edit-field-uw-service-hours-office-hours-exceptions-0-value-0-day"]');

    // The field values for exception.
    $date = date('m/d/Y', strtotime("tomorrow"));
    $start_time = '10:00AM';
    $end_time = '15:00:00';

    // Fill the fields.
    $i->fillField('field_uw_service_hours[office_hours_exceptions][0][value][0][day]', $date);
    $i->fillField('field_uw_service_hours[office_hours_exceptions][0][value][0][starthours][time]', $start_time);
    $i->fillField('field_uw_service_hours[office_hours_exceptions][0][value][0][endhours][time]', $end_time);

    // Click the save button and ensure service is created.
    $i->click('Save');
    $i->see('Service ' . $title . ' has been created');

    // Ensure that the time for Sunday is 10am to 3pm.
    $day = date('l', strtotime($reg_date));
    $i->see($day . ': 10:00 am-3:00 pm');

    // Ensure that the exception date appears
    // in the correct format with closed.
    $date_with_format = date('l, F j, Y', strtotime($exp_date)) . ': 10:00 am-3:00 pm';
    $i->see($date_with_format);
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _after(AcceptanceTester $i): void {
    // phpcs:enable

    // If we used any terms, delete them.
    if (!empty($this->tidsUsed)) {
      $controller = \Drupal::entityTypeManager()
        ->getStorage('taxonomy_term');
      $entities = $controller->loadMultiple($this->tidsUsed);
      $controller->delete($entities);
    }
  }

  // phpcs:disable
  /**
   * Function to run if the test fails.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i): void {
    // phpcs:enable

    // If we used any terms, delete them.
    if (!empty($this->tidsUsed)) {
      $controller = \Drupal::entityTypeManager()
        ->getStorage('taxonomy_term');
      $entities = $controller->loadMultiple($this->tidsUsed);
      $controller->delete($entities);
    }
  }

}
