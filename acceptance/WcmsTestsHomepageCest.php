<?php

/**
 * Class WcmsTestsHomepageCest.
 *
 * Tests for homepage.
 */
class WcmsTestsHomepageCest {

  /**
   * Function to test the page elements of the homepage.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testHomepageElements(AcceptanceTester $i): void {

    // Login with site manager and go to node edit page.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');
    $i->amOnPage('home/edit');

    // Ensure that we can not see config that is on all
    // other pages other than the homepage.
    $i->dontSeeElement('#edit-path-0');
    $i->dontSeeElement('#edit-promote-wrapper');
    $i->dontSeeElement('#edit-sticky-wrapper');

    // Cannot test for #edit-menu because an empty 'div' with that exists.
    $i->dontSeeElement('#edit-menu-enabled');
    $i->dontSeeElement('#edit-menu-place-in-menu');

    // Test that the home page cannot be the parent of any item.
    $i->dontSeeElement('option[value="main:uw_base_profile.front_page"]');

    // No delete link.
    $i->dontSeeElement('a#edit-delete');

    // No access to delete page.
    $i->amOnPage('home/delete');
    $i->see('You are not authorized to access this page.');
  }

  /**
   * Function to test the home page link.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testHomePageMenuLink(AcceptanceTester $i): void {

    // Logout just in case we are in as another user.
    $i->amOnPage('user/logout');

    // Login as administrator.
    $i->logInWithRole('administrator');

    // Go the main menu management page.
    $i->amOnPage('admin/structure/menu/manage/main');

    // Check that home page menu entry can be edited by admin.
    $i->seeElement('#edit-links-menu-plugin-iduw-base-profilefront-page-enabled');
    $i->seeElementInDOM('#edit-links-menu-plugin-iduw-base-profilefront-page-weight');
    $i->seeElement('a[href="/admin/structure/menu/link/uw_base_profile.front_page/edit"]');

    // Logout as administrator.
    $i->amOnPage('user/logout');

    // Login as content author.
    $i->logInWithRole('uw_role_content_author');

    // Go the main menu management page.
    $i->amOnPage('admin/structure/menu/manage/main');

    // Check that home page menu entry can not be edited
    // by content author.
    $i->dontSeeElement('#edit-links-menu-plugin-iduw-base-profilefront-page-enabled');
    $i->dontSeeElementInDOM('#edit-links-menu-plugin-iduw-base-profilefront-page-weight');
    $i->dontSeeElement('a[href="/admin/structure/menu/link/uw_base_profile.front_page/edit"]');
  }

  /**
   * Function to test home page and protection.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testHomePageConfig(AcceptanceTester $i): void {

    // Login as administrator.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');

    // Go the home page edit page.
    $i->amOnPage('home/edit');

    // Click on the advanced configuration details tab.
    $i->click('#edit-field-uw-meta-tags-0');

    // Ensure that the basic tag has a default value.
    $i->seeElement('.form-item-field-uw-meta-tags-0-basic-title input[value="Home | [site:name]"]');

    // Login as administrator.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Go the home page edit page.
    $i->amOnPage('home/edit');

    // Click on the advanced configuration details tab.
    $i->click('#edit-field-uw-meta-tags-0');

    // Ensure that this user can not edit the basic title tag.
    $i->dontSeeElement('.form-item-field-uw-meta-tags-0-basic-title input[value="Home | [site:name]"]');
  }

}
