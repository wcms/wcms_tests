<?php

/**
 * Class ImgInlineCest.
 *
 * Tests that images are not block elements.
 */
class WcmsTestsImgInlineCest {

  /**
   * Array of nodes used.
   *
   * @var array
   */
  private array $nodesUsed;

  /**
   * Tests that images are not block elements.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testImgInline(AcceptanceTester $i) {

    // Login as administrator.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');

    // Step through each of the block elements and test them.
    foreach ($this->getBlocks() as $block) {

      $title = $i->uwRandomString();
      // Create a webpage.
      $this->nodesUsed[$title] = $i->createWebPage($title);

      // Get the path of the webpage.
      $path = $i->getWebPagePath($this->nodesUsed[$title]);

      // Go to the node layout page.
      $i->amOnPage($path . '/layout');

      // Add a block with each tag.
      $i->waitForElementClickable('.layout-builder__add-block');
      $i->click('.layout-builder__add-block');
      $i->waitForText('Choose a block');
      $i->click('Copy text');
      $i->waitForText('Block description');

      // Fill the title.
      $i->fillField('input[name="settings[label]"]', $i->uwRandomString());

      // Select the full text format.
      $i->selectOption('settings[block_form][field_uw_copy_text][0][format]', 'uw_tf_full_html');
      $i->waitForText('Full HTML');
      $i->waitForElement('.cke_toolbox');

      // Click on the source button.
      $i->click('Source');

      // Fill the ckeditor field with the block element.
      $i->fillCkEditor(
        '<' . $block . '> Here is an image. <img alt="Inserted image in ' . $block . ' block" src="https://uwaterloo.ca/building-the-next-wcms/sites/default/files/uploads/images/icons-2021_laptop.png" /> More text. </' . $block . '>',
      'textarea[name="settings[block_form][field_uw_copy_text][0][value]"]'
      );

      // Click the submit button.
      $i->waitForElementClickable('input[id*="edit-actions-submit"]');
      $i->click('input[id*="edit-actions-submit"]');

      // Ensure that the block is on the page.
      $i->waitForElement('.uw-copy-text div ' . $block . ' img');

      // Check that image's parent is a block.
      $i->seeElement('.uw-copy-text div ' . $block . ' img', ['alt' => 'Inserted image in ' . $block . ' block']);

      // Remove added block to avoid ambiguity in finding image.
      $i->amOnPage($path . '/layout/discard-changes');
      $i->click('#edit-submit');
      $i->waitForText('The changes to the layout have been discarded.');
    }
  }

  /**
   * Function to get the block elements to test.
   *
   * @return string[]
   *   Array of block elements to test.
   */
  private function getBlocks(): array {

    return [
      'a',
      'address',
      'article',
      'aside',
      'blockquote',
      'dd',
      'div',
      'dl',
      'dt',
      'fieldset',
      'figcaption',
      'figure',
      'footer',
      'form',
      'h1',
      'h2',
      'h3',
      'h4',
      'h5',
      'h6',
      'header',
      'li',
      'main',
      'nav',
      'ol',
      'p',
      'pre',
      'section',
    ];
  }

  // phpcs:disable

  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _after(AcceptanceTester $i): void {
    // phpcs:enable

    // Step through each of the nodes used and delete them.
    foreach ($this->nodesUsed as $node) {
      $node->delete();
    }
  }

  // phpcs:disable

  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i): void {
    // phpcs:enable

    // Step through each of the nodes used and delete them.
    foreach ($this->nodesUsed as $node) {
      $node->delete();
    }
  }

}
