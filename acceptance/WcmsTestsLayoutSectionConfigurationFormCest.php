<?php

/**
 * Class LayoutSectionConfigurationFormCest.
 *
 * Tests for section layouts.
 */
class WcmsTestsLayoutSectionConfigurationFormCest {

  /**
   * Array of nodes used.
   *
   * @var array
   */
  private array $nodesUsed;

  /**
   * Tests that images are not block elements.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function layoutConfigurationFormTest(AcceptanceTester $i) {

    // There are six layouts.
    $listLayout = [
      'One column',
      'Two columns',
      'Three columns',
      'Four columns',
      'Inverted "L" - right',
      'Inverted "L" - left',
    ];

    // Log in as admin.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');

    // Loop through each layout type to test the config forms.
    foreach ($listLayout as $section) {

      // Title for the webpage.
      $title = $i->uwRandomString();

      // Create a webpage.
      $this->nodesUsed[$section] = $i->createWebPage($title);

      // Get the path of the webpage.
      $path = $i->getWebPagePath($this->nodesUsed[$section]);

      // Ensure that we are on the correct page.
      $i->amOnPage($path . '/layout');
      $i->see('Edit layout for ' . $title);

      // Test step 2: Look on the layout edit page to click 'Add section'.
      $i->click('Add section');
      $i->waitForText('Choose a layout for this section');

      // Test step 3: Click section name link.
      $i->click($section);
      $i->waitForText('Configure section');

      // Verify all configuration form elements.
      // Note: Javascription is off so no conditional form elements are hidden.
      $i->see('Administrative label');
      $i->see('Section ID');

      // Array of options for section widths.
      $sectionWidths = [
        'uw_lbs_contained_width',
        'uw_lbs_contained_width_wide',
        'uw_lbs_contained_width_narrow',
        'uw_lbs_full_width',
      ];

      $i->see('Section width');

      // Loop to assert every option is there and default is selected.
      foreach ($sectionWidths as $width) {

        if ($width == 'uw_lbs_contained_width') {
          $i->seeElement('option[value="uw_lbs_contained_width"][selected="selected"]');
        }
        else {
          $i->seeElement('option[value="' . $width . '"]');
        }
      }

      // Array of options for section spacing.
      $sectionSpacings = [
        'uw_lbs_section_spacing_default',
        'uw_lbs_section_spacing_75',
        'uw_lbs_section_spacing_50',
        'uw_lbs_section_spacing_25',
        'uw_lbs_section_spacing_none',
      ];

      $i->see('Section spacing');

      // Loop to assert every option is there and default is selected.
      foreach ($sectionSpacings as $spacing) {
        // Default value.
        if ($spacing == 'uw_lbs_section_spacing_default') {
          $i->seeElement('option[value="uw_lbs_section_spacing_default"][selected="selected"]');
        }
        else {
          $i->seeElement('option[value="' . $spacing . '"]');
        }
      }

      // Array of options for section spacing.
      $sectionSeparators = [
        'uw_lbs_section_separator_none',
        'uw_lbs_section_separator_bottom',
        'uw_lbs_section_separator_narrow',
      ];

      $i->see('Section separator');

      // Loop to assert every option is there and default is selected.
      foreach ($sectionSeparators as $separator) {
        // Default value.
        if ($separator == 'uw_lbs_section_separator_none') {
          $i->seeElement('option[value="uw_lbs_section_separator_none"][selected="selected"]');
        }
        else {
          $i->seeElement('option[value="' . $separator . '"]');
        }
      }

      // No column separator and section alignment on one column.
      if ($section !== 'One column') {
        $i->see('Column widths');

        // Array of options for column separators.
        $columnSeparators = [
          'uw_lbs_column_separator_none',
          'uw_lbs_column_separator_between',
          'uw_lbs_column_separator_narrow',
        ];

        $i->see('Column separator');

        // Loop to assert every option is there and default is selected.
        foreach ($columnSeparators as $separator) {
          // Default value.
          if ($separator == 'uw_lbs_column_separator_none') {
            $i->seeElement('option[value="uw_lbs_column_separator_none"][selected="selected"]');
          }
          else {
            $i->seeElement('option[value="' . $separator . '"]');
          }
        }

        // Test section alignment field.
        $sectionAlignments = [
          'uw_lbs_section_alignment_top_align_content',
          'uw_lbs_section_alignment_bottom_align_content',
          'uw_lbs_section_alignment_center_align_content',
        ];

        $i->see('Section alignment');

        // Loop to assert every option is there and default is selected.
        foreach ($sectionAlignments as $alignment) {
          // Default value.
          if ($alignment == 'uw_lbs_section_alignment_top_align_content') {
            $i->seeElement('option[value="uw_lbs_section_alignment_top_align_content"][selected="selected"]');
          }
          else {
            $i->seeElement('option[value="' . $alignment . '"]');
          }
        }
      }

      // Assert we don't see column separator.
      else {
        $i->dontSee('Column widths');
        $i->dontSeeElement('#edit-layout-settings-column-class');

        $i->dontSee('Column separator');
        $i->dontSeeElement('#edit-layout-builder-style-column-separator');
      }

      $columnWidths = [];
      $default = '';

      // Check column width options.
      switch ($section) {
        case 'Two columns':
        case 'Inverted "L" - right':
        case 'Inverted "L" - left':
          $columnWidths = [
            'even-split',
            'larger-left',
            'larger-right',
          ];

          $default = 'even-split';
          break;

        case 'Three column':
          $columnWidths = [
            'even-split',
            'larger-left',
            'larger-middle',
            'larger-right',
            'legacy-38-38-24',
            'legacy-24-38-38',
          ];

          $default = 'even-split';
          break;

        case 'Four columns':
          $columnWidths = [
            'even-split',
            'larger-left',
            'larger-second',
            'larger-third',
            'larger-right',
            'legacy-23-27-27-23',
          ];

          $default = 'even-split';
          break;
      }

      // Loop to assert every option is there and default is selected.
      foreach ($columnWidths as $width) {
        // Default value.
        if ($width == $default) {
          $i->seeElement('option[value="' . $width . '"][selected="selected"]');
        }
        else {
          $i->seeElement('option[value="' . $width . '"]');
        }
      }

      // Test cases for checking css for section spacing,
      // section separator, and column separator.
      $sectionSpacingsCSS = [
        'Default' => 'uw-section-spacing--default',
        '75%' => 'uw-section-spacing--75',
        '50%' => 'uw-section-spacing--50',
        '25%' => 'uw-section-spacing--25',
        'None' => 'uw-section-spacing--none',
      ];

      $sectionSeparatorsCSS = [
        'None' => 'uw-section-separator--none',
        'Bottom' => 'uw-section-separator--bottom',
        'Bottom when narrow' => 'uw-section-separator--narrow',
      ];

      $columnSeparatorsCSS = [
        'None' => 'uw-column-separator--none',
        'Between' => 'uw-column-separator--between',
        'Between when narrow' => 'uw-column-separator--narrow',
      ];

      $sectionAlignmentCSS = [
        'Top align content' => 'uw-section-alignment--top-align-content',
        'Bottom align content' => 'uw-section-alignment--bottom-align-content',
        'Center align content' => 'uw-section-alignment--center-align-content',
      ];

      if ($section == 'One column') {

        // Section separator css test for one column sections.
        foreach ($sectionSeparatorsCSS as $form => $css) {

          // Name for section label.
          $label = $i->uwRandomString();

          // First add the section.
          $i->amOnPage($path . '/layout');
          $i->click('Add section');
          $i->waitForText('Choose a layout for this section');
          $i->click($section);
          $i->waitForText('Configure section');

          // Fill in the section label and select section separator option.
          $i->fillField('Administrative label', $label);
          $i->selectOption(' Section separator', $form);
          $i->click('Add section');
          $i->waitForText('Configure ' . $label);

          // Assert that we see the section separator.
          $i->seeElement('section[class*="' . $css . '"]');
        }
      }

      else {

        // Section + column separator css test for multi-column sections.
        $sections = array_keys($sectionSeparatorsCSS);
        $columns = array_keys($columnSeparatorsCSS);

        foreach (array_combine($sections, $columns) as $secForm => $colForm) {

          // Name for section label.
          $label = $i->uwRandomString();

          // First add the section.
          $i->amOnPage($path . '/layout');
          $i->click('Add section');
          $i->waitForText('Choose a layout for this section');
          $i->click($section);
          $i->waitForText('Configure section');

          // Fill section label and select section and column separator option.
          $i->fillField('Administrative label', $label);
          $i->selectOption('Section separator', $secForm);
          $i->selectOption('Column separator', $colForm);
          $i->click('Add section');
          $i->waitForText('Configure ' . $label);

          // Assert that we see the section and column separator.
          $i->seeElement('section[class*="' . $sectionSeparatorsCSS[$secForm] . ' ' . $columnSeparatorsCSS[$colForm] . '"]');
        }
      }

      // Section spacing test.
      foreach ($sectionSpacingsCSS as $form => $css) {

        // Name for section label.
        $label = $i->uwRandomString();

        // First add the section.
        $i->amOnPage($path . '/layout');
        $i->click('Add section');
        $i->waitForText('Choose a layout for this section');
        $i->click($section);
        $i->waitForText('Configure section');

        // Fill in the section label and select section spacing option.
        $i->fillField('Administrative label', $label);
        $i->selectOption('Section spacing', $form);
        $i->click('Add section');
        $i->waitForText('Configure ' . $label);

        // Assert that we see the section spacing.
        $i->seeElement('section[class*="' . $css . '"]');
      }

      if ($section !== 'One column') {
        // Section Alignment test.
        foreach ($sectionAlignmentCSS as $form => $css) {

          // Name for section label.
          $label = $i->uwRandomString();

          // First add the section.
          $i->amOnPage($path . '/layout');
          $i->click('Add section');
          $i->waitForText('Choose a layout for this section');
          $i->click($section);
          $i->waitForText('Configure section');

          // Fill in the section label and select section spacing option.
          $i->fillField('Administrative label', $label);
          $i->selectOption('Section alignment', $form);
          $i->click('Add section');
          $i->waitForText('Configure ' . $label);

          // Assert that we see the section spacing.
          $i->seeElement('section[class*="' . $css . '"]');
        }
      }
    }
  }

  // phpcs:disable

  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _after(AcceptanceTester $i): void {
    // phpcs:enable

    // Step through each of the nodes used and delete them.
    foreach ($this->nodesUsed as $node) {
      $node->delete();
    }

    // Delete all the blocks.
    $i->deleteAllBlocks();

  }

  // phpcs:disable

  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i): void {
    // phpcs:enable

    // Step through each of the nodes used and delete them.
    foreach ($this->nodesUsed as $node) {
      $node->delete();
    }

    // Delete all the blocks.
    $i->deleteAllBlocks();
  }

}
