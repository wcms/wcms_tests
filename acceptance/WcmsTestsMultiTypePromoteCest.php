<?php

use Codeception\Util\Locator;

/**
 * Class MultiTypePromoteCest.
 *
 * Tests for menus.
 */
class WcmsTestsMultiTypePromoteCest {

  /**
   * Array of nodes used.
   *
   * @var array
   */
  private array $nodesUsed;

  /**
   * Tests if promoted items are displayed in multi-type lists.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testMultiTypePromote(AcceptanceTester $i) {

    // Login as admin.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');

    // Creating promoted and unpromoted nodes for the Multi type list.
    $this->nodesUsed['Unpromoted Opportunities'] = $i->createCtNode(
      'uw_ct_opportunity',
      'Unpromoted Opportunities',
      TRUE
    );
    $this->nodesUsed['Promoted Opportunities'] = $i->createCtNode(
      'uw_ct_opportunity',
      'Promoted Opportunities',
      TRUE,
      ['promoted' => 1]
    );
    $this->nodesUsed['Unpromoted Events'] = $i->createCtNode(
      'uw_ct_event',
      'Unpromoted Events',
      TRUE
    );
    $this->nodesUsed['Promoted Events'] = $i->createCtNode(
      'uw_ct_event',
      'Promoted Events',
      TRUE,
      ['promote' => 1]
    );
    $this->nodesUsed['Unpromoted Blog'] = $i->createCtNode(
      'uw_ct_blog',
      'Unpromoted Blog',
      TRUE
    );
    $this->nodesUsed['Promoted Blog'] = $i->createCtNode(
      'uw_ct_blog',
      'Promoted Blog',
      TRUE,
      ['promote' => 1]
    );
    $this->nodesUsed['Unpromoted News'] = $i->createCtNode(
      'uw_ct_news_item',
      'Unpromoted News',
      TRUE
    );
    $this->nodesUsed['Promoted News'] = $i->createCtNode(
      'uw_ct_news_item',
      'Promoted News',
      TRUE,
      ['promote' => 1]
    );
    $this->nodesUsed['Promoted Webpage'] = $i->createCtNode(
      'uw_ct_web_page',
      'Promoted Webpage',
      TRUE
    );

    // Creating a webpage to test the Mult type list.
    $this->nodesUsed['All Webpage'] = $i->createCtNode(
      'uw_ct_web_page',
      'All Webpage',
      TRUE
    );

    // Manually promoting the opportunity node.
    $i->amOnPage('/opportunities/promoted-opportunities/edit');
    $i->click('#edit-options');
    $i->checkOption('#edit-promote-value');
    $i->click('Save');

    // Manually unpromoting the other 3 nodes.
    $links = [
      '/news/unpromoted-news/edit',
      '/blog/unpromoted-blog/edit',
      '/events/unpromoted-events/edit',
    ];

    // Step through each links and ensure unpromote it.
    foreach ($links as $pages) {
      $i->amOnPage($pages);
      $i->click('#edit-options');
      $i->uncheckOption('#edit-promote-value');
      $i->click('Save');
    }

    // Add a multi-type list that only displays promoted items.
    $path = $i->getWebPagePath($this->nodesUsed['Promoted Webpage']);
    $i->amOnPage($path . '/layout');
    $i->click('Add block');
    $i->waitForText('Multi-type list');
    $i->click('Multi-type list');
    $i->waitforText('Configure block');
    $i->click('Add block');
    $i->click('Save layout');

    $content = [
      'News' => 'button[id="tab-1"]',
      'Events' => 'button[id="tab-2"]',
      'Blog' => 'button[id="tab-3"]',
      'Opportunities' => 'button[id="tab-4"]',
    ];

    // Check that tabs and only the promoted items are displayed.
    foreach ($content as $headers => $tabs) {
      $i->seeElement(Locator::contains($tabs, $headers));
      $i->click(Locator::contains($tabs, $headers));
      $i->see('Promoted ' . $headers);
      $i->dontSee('Unpromoted ' . $headers);
    }

    // Add a multi-type list that displays all items.
    $path = $i->getWebPagePath($this->nodesUsed['All Webpage']);
    $i->amOnPage($path . '/layout');
    $i->click('Add block');
    $i->waitForText('Multi-type list');
    $i->click('Multi-type list');
    $i->waitforText('Configure block');
    $i->checkOption('input[id*="edit-settings-show-all-items"]');
    $i->click('Add block');
    $i->click('Save layout');

    // Check that tabs and both promoted and unpromoted items are displayed.
    foreach ($content as $headers => $tabs) {
      $i->seeElement(Locator::contains($tabs, $headers));
      $i->click(Locator::contains($tabs, $headers));
      $i->see('Promoted ' . $headers);
      $i->see('Unpromoted ' . $headers);
    }
  }

  // phpcs:disable

  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _after(AcceptanceTester $i): void {
    // phpcs:enable

    // Step through all the nodes used and delete them.
    foreach ($this->nodesUsed as $node) {
      $node->delete();
    }

    // Reset the nodes used array.
    $this->nodesUsed = [];

    // Delete all the blocks.
    $i->deleteAllBlocks();
  }

  // phpcs:disable

  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i): void {
    // phpcs:enable

    // Step through all the nodes used and delete them.
    foreach ($this->nodesUsed as $node) {
      $node->delete();
    }

    // Reset the nodes used array.
    $this->nodesUsed = [];

    // Delete all the blocks.
    $i->deleteAllBlocks();
  }

}
