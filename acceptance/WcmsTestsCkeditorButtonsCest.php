<?php

use Codeception\Util\Locator;

/**
 * Class CkeditorButtonsCest.
 *
 * Tests for ck editor buttons.
 */
class WcmsTestsCkeditorButtonsCest {

  /**
   * Array of nodes used.
   *
   * @var array
   */
  private array $nodesUsed = [];

  /**
   * Tests for ck editor buttons.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testCkeditorButtons(AcceptanceTester $i) {

    // Login as administrator.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');

    $text_formats = [
      'uw_tf_standard' => 'Standard',
      'uw_tf_full_html' => 'Full HTML',
      'uw_tf_basic' => 'Basic',
    ];

    foreach ($text_formats as $tf_machine_name => $tf_name) {
      $i->amOnPage('admin/config/content/formats/manage/' . $tf_machine_name);
      $i->see($tf_name);
      $i->seeElement('li[data-drupal-ckeditor-button-name="Anchor"][class="ckeditor-button"] a[href="#"][role="button"][title="Anchor"][aria-label="Anchor"]');

      if ($tf_machine_name === 'uw_tf_standard') {
        // Checking if abbr icon is part of available plugins (on toolbar).
        // Takes into account button group too. Moving abbr to a new group
        // will fail the test.
        $i->seeElement('ul[aria-labelledby="ckeditor-toolbar-group-aria-label-for-styles"][role="toolbar"][data-drupal-ckeditor-button-sorting="target"] li[data-drupal-ckeditor-button-name="abbr"][class="ckeditor-button"] a[href="#"][role="button"][title="Abbreviation"][aria-label="Abbreviation"]');
      }
      elseif ($tf_machine_name === 'uw_tf_basic') {
        // Checking if abbr plugin is in the list of available (not enabled)
        // plugins.
        $i->seeElement('ul[id="ckeditor-available-buttons"] li[data-drupal-ckeditor-button-name="abbr"][class="ckeditor-button"] a[href="#"][role="button"][title="Abbreviation"][aria-label="Abbreviation"]');
      }
    }

  }

  /**
   * Tests for ckeditor language support.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testLanguageSupport(AcceptanceTester $i) {
    // Html tags to test.
    $languages = [
      'ar' => 'Arabic',
      'zh' => 'Chinese',
      'hr' => 'Croatian',
      'xnd' => 'Dene languages',
      'nl' => 'Dutch',
      'en' => 'English',
      'fr' => 'French',
      'de' => 'German',
      'grc' => 'Greek (ancient)',
      'el' => 'Greek (modern)',
      'gwl' => 'Gwich\u0027in',
      'iu' => 'Inuktitut',
      'it' => 'Italian',
      'ja' => 'Japanese',
      'ko' => 'Korean',
      'la' => 'Latin',
      'moh' => 'Mohawk',
      'scs' => 'North Slavey',
      'oj' => 'Ojibwa',
      'pt' => 'Portuguese',
      'ru' => 'Russian',
      'es' => 'Spanish',
    ];

    // Create a webpage.
    $this->nodesUsed['Copy Text Block'] = $i->createWebPage('Copy Text Block');

    // Get the path of the webpage.
    $path = $i->getWebPagePath($this->nodesUsed['Copy Text Block']);

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Test each option.
    foreach ($languages as $code => $language) {

      // Go to the layout page for the webpage we created.
      $i->amOnPage($path . '/layout');
      $i->see('Edit layout for ' . $this->nodesUsed['Copy Text Block']->getTitle());

      // Add a block.
      $i->click('Add block');
      $i->waitForText('Choose a block');

      // Click on the copy text block.
      $i->click('Copy text');
      $i->waitForText('Configure block');

      // Adds tags to text and xpath selector.
      $tagged_text = '<span dir="ltr" lang="' . $code . '">' . $language . '</span>';

      // Fill the title field.
      $i->fillField('settings[label]', $language);

      // Click on source and fill in the ck editor field with the tagged text.
      $i->click('Source');
      $i->fillCkEditor($tagged_text, 'textarea[name="settings[block_form][field_uw_copy_text][0][value]"]');

      // Submit form.
      $i->click('input[id*="edit-actions-submit"]');

      // Ensure that the tag is on the page.
      $i->waitForElement(Locator::contains('span[lang="' . $code . '"]', $language));
      $i->seeElement('span[lang="' . $code . '"]');

      // Remove added block to start again.
      $i->amOnPage($path . '/layout/discard-changes');
      $i->click('#edit-submit');
      $i->waitForText('The changes to the layout have been discarded.');
    }
  }

  // phpcs:disable

  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _after(AcceptanceTester $i): void {
    // Step through each of the nodes used and delete them.
    foreach ($this->nodesUsed as $node) {
      $node->delete();
    }
  }

  // phpcs:disable

  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i): void {
    // Step through each of the nodes used and delete them.
    foreach ($this->nodesUsed as $node) {
      $node->delete();
    }
  }

}
