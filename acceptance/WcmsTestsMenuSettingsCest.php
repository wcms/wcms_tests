<?php

use Drupal\node\Entity\Node;
use Drupal\uw_cfg_common\Service\UWService;

/**
 * Class WcmsTestsMenuSettingsCest.
 *
 * Tests for menu settings.
 */
class WcmsTestsMenuSettingsCest {

  /**
   * The web page node.
   *
   * @var \Drupal\node\Entity\Node
   */
  private $webpage;

  /**
   * The title of the webpage.
   *
   * @var string
   */
  private string $title;

  // phpcs:disable
  /**
   * Function to run before the test starts.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _before(AcceptanceTester $i) {
    // phpcs:enable

    // Set the title for the webpage.
    $this->title = 'Test Menu Settings';

    // Create a webpage.
    $this->webpage = Node::create([
      'type'        => 'uw_ct_web_page',
      'title'       => $this->title,
      'field_uw_meta_description' => $this->title,
    ]);
    $this->webpage->save();
  }

  /**
   * Test "Menu Settings".
   *
   * "Place in site hierarchy" and "Add menu link".
   * This is custom WCMS functionality in uw_sites_all.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testMenuSettings(AcceptanceTester $i): void {

    // Get the path and url to the newly created page.
    $path = \Drupal::service('path_alias.manager')
      ->getAliasByPath('/node/' . $this->webpage->id());
    $url = $path . '/edit';

    // This part done as admin.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');

    // By default, "Place in site hierarchy" is unchecked. There should be
    // no menu item.
    $i->amOnPage('admin/structure/menu/manage/main');
    $i->dontSee($this->title);

    // Go the edit page of the content type, we
    // saved this url above.
    $i->amOnPage($url);

    // Add the menu to the site hierarchy.
    $i->click('#edit-menu');
    $i->click('input[name="menu[enabled]"]');
    $i->click('#edit-submit');

    // Go back to the main menu management page.
    $i->amOnPage('admin/structure/menu/manage/main');

    // Ensure that the menu link for our newly created
    // content is enabled.
    $i->see($this->title);
    $i->dontSee($this->title . ' (disabled)');

    // Go back to the content type edit page.
    $i->amOnPage($url);

    // Uncheck the add to menu button.
    $i->click('#edit-menu');
    $i->click('input[name="menu[place_in_menu]"]');
    $i->click('#edit-submit');

    // Go back to the main menu management page and
    // ensure that the menu link is disabled.
    $i->amOnPage('admin/structure/menu/manage/main');
    $i->see($this->title . ' (disabled)');

    // Login as administrator.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');

    // Delete the menu links, so that the site
    // continues to work as all content created
    // during the tests gets deleted causing WSOD
    // when visiting the main menu management page.
    $i->amOnPage($url);
    $i->click('input[name="menu[enabled]"]');
    $i->click('#edit-submit');

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Ensure that this node is not the homepage.
    $i->assertTrue(
      !UWService::nodeIsHomePage($this->webpage->id()),
      'UWService::nodeIsHomePage() correctly identifies page as not the home page.'
    );

    // Test the page elements as a regular page.
    $this->testPageElements(
      $i,
      $this->webpage->id(),
      'regular'
    );

    // Make the page the home page.
    \Drupal::service('config.factory')
      ->getEditable('system.site')
      ->set('page.front', $path)
      ->save();

    // Ensure that this node is now the homepage.
    $i->assertTrue(
      UWService::nodeIsHomePage($this->webpage->id()),
      'UWService::nodeIsHomePage() correctly identifies page as the home page.'
    );

    // Test the page elements as home page.
    $this->testPageElements(
      $i,
      $this->webpage->id(),
      'home'
    );

    // Make the page the home page node 1 again.
    $path = \Drupal::service('path_alias.manager')
      ->getAliasByPath('/node/1');
    \Drupal::service('config.factory')
      ->getEditable('system.site')
      ->set('page.front', $path)
      ->save();
  }

  /**
   * Function to test the page elements.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   * @param int $nid
   *   The node id.
   * @param string $type
   *   The type of page.
   */
  private function testPageElements(
    AcceptanceTester $i,
    int $nid,
    string $type = 'regular',
  ): void {

    // Tests for home page.
    if ($type == 'home') {

      // Login with site manager and go to node edit page.
      $i->amOnPage('user/logout');
      $i->logInWithRole('uw_role_site_manager');
      $i->amOnPage('node/' . $nid . '/edit');

      // Ensure that we can not see config that is on all
      // other pages other than the homepage.
      $i->dontSeeElement('#edit-path-0');
      $i->dontSeeElement('#edit-promote-wrapper');
      $i->dontSeeElement('#edit-sticky-wrapper');

      // Cannot test for #edit-menu because an empty 'div' with that exists.
      $i->dontSeeElement('#edit-menu-enabled');
      $i->dontSeeElement('#edit-menu-place-in-menu');

      // Test that the home page cannot be the parent of any item.
      $i->dontSeeElement('option[value="main:uw_base_profile.front_page"]');

      // No delete link.
      $i->dontSeeElement('a#edit-delete');

      // No access to delete page.
      $i->amOnPage('node/' . $nid . '/delete');
      $i->see('You are not authorized to access this page.');
    }
    else {

      // Login with administrator and go to node edit page.
      $i->amOnPage('user/logout');
      $i->logInWithRole('administrator');
      $i->amOnPage('node/' . $nid . '/edit');

      // Ensure that we can not see config that is on all
      // other pages other than the homepage.
      $i->seeElement('#edit-path-0');
      $i->seeElement('#edit-options');
      $i->click('#edit-options');
      $i->seeElement('#edit-promote-wrapper');
      $i->seeElement('#edit-sticky-wrapper');

      // Login with site manager and go to node edit page.
      $i->amOnPage('user/logout');
      $i->logInWithRole('uw_role_site_manager');
      $i->amOnPage('node/' . $nid . '/edit');

      // Cannot test for #edit-menu because an empty 'div' with that exists.
      $i->seeElement('#edit-menu');
      $i->click('#edit-menu');
      $i->seeElement('#edit-menu-enabled');
      $i->checkOption('#edit-menu-enabled');
      $i->seeElement('#edit-menu-place-in-menu');

      // Test that the page can not be part of home page.
      $i->dontSeeElement('option[value="main:uw_base_profile.front_page"]');

      // No delete link.
      $i->seeElement('a#edit-delete');

      // No access to delete page.
      $i->amOnPage('node/' . $nid . '/delete');
      $i->see('Are you sure you want to delete the content item');
    }
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _passed(AcceptanceTester $i) {

    $this->webpage->delete();
  }

  // phpcs:disable
  /**
   * Function to run if the test fails.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i) {
    // phpcs:enable

    $this->webpage->delete();
  }

}
