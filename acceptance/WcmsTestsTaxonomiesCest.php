<?php

use Codeception\Util\Locator;

/**
 * Class TaxonomiesCest.
 *
 * Tests for taxonomies.
 */
class WcmsTestsTaxonomiesCest {

  /**
   * Taxonomy test.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function taxonomiesTest(AcceptanceTester $i) {

    // Login as authenticated user.
    // Login as administrator.
    $i->logInWithRole('administrator');

    // Pages to test that exists.
    $pages = [
      'admin/structure/taxonomy/manage/uw_vocab_faculties_and_schools/overview',
      'admin/structure/taxonomy/manage/uw_vocab_audience/overview',
      'admin/structure/taxonomy/manage/uw_vocab_catalogs/overview',
      'admin/structure/taxonomy/manage/uw_vocab_catalog_categories/overview',
    ];

    // Step through each page and ensure that it exists.
    foreach ($pages as $page) {
      $i->amOnPage($page);
    }

    // Look for search paths.
    $i->amOnPage('admin/config/search/path');
    $i->seeElementInDOM('a[href^="/catalogs/faculties-and-schools/faculty-"]');
    $i->seeElementInDOM('a[href^="/audience/current-"]');
    $i->seeElementInDOM(Locator::contains('a', '/events/types'));

    // Look for search patterns.
    $i->amOnPage('admin/config/search/path/patterns');
    $i->see('Audience path pattern');
    $i->see('Vocabulary is uw_vocab_audience');
    $i->see('Catalogs path pattern');
    $i->see('Vocabulary is uw_vocab_catalogs');
    $i->see('Catalog categories path pattern');
    $i->see('Vocabulary is uw_vocab_catalog_categories');
    $i->see('Faculties and schools path pattern');
    $i->see('Vocabulary is uw_vocab_faculties_and_schools');

    // Make sure URL alias fields off of taxonomy forms for all vocabularies.
    $vocabs = [
      'uw_vocab_audience',
      'uw_vocab_blog_tags',
      'uw_vocab_catalog_categories',
      'uw_vocab_catalogs',
      'uw_tax_event_tags',
      'uw_tax_event_type',
      'uw_vocab_faculties_and_schools',
      'uw_vocab_news_tags',
    ];

    // Step through each of the vocabs and ensure that
    // we do not have the URL alias.
    foreach ($vocabs as $vocab) {
      $i->amOnPage('admin/structure/taxonomy/manage/' . $vocab . '/add');
      $i->dontSee('Generate automatic URL alias');
      $i->dontSeeElement('#edit-path-0-pathauto');
      $i->dontSee('URL alias');
      $i->dontSeeElement('#edit-path-0-alias');
    }

    // The default terms that should be installed.
    $terms = [
      'uw_tax_event_type' => [
        'Conference',
        'Information session',
        'Lecture',
        'Open house',
        'Performance',
        'Reception',
        'Reunion',
        'Seminar',
        'Thesis defence',
        'Workshop',
      ],
      'uw_vocab_audience' => [
        'Current students',
        'Current undergraduate students',
        'Current graduate students',
        'Future students',
        'Future undergraduate students',
        'Future graduate students',
        'Faculty',
        'Staff',
        'Alumni',
        'Parents',
        'Donors | Friends | Supporters',
        'Employers',
        'International',
        'Media',
      ],
      'uw_vocab_faculties_and_schools' => [
        'Conrad Grebel University College',
        'Faculty of Arts',
        'Faculty of Engineering',
        'Faculty of Environment',
        'Faculty of Health',
        'Faculty of Mathematics',
        'Faculty of Science',
        'Renison University College',
        'St. Jerome',
        'United College',
      ],
      'uw_vocab_opportunity_employment' => [
        'Full time',
        'Part time',
      ],
      'uw_vocab_opportunity_payrate' => [
        'Honorarium',
        'Hourly',
        'Salary',
        'USG',
      ],
      'uw_vocab_opportunity_type' => [
        'Paid',
        'Research participant',
        'Volunteer',
      ],
      'uw_vocab_project_status' => [
        'Current projects',
        'Proposed projects',
        'Other initiatives',
        'Completed projects',
        'Current demonstrators',
      ],
    ];

    // Step through each of the terms and ensure
    // that they are installed.
    foreach ($terms as $vocab => $term) {
      $i->amOnPage('admin/structure/taxonomy/manage/' . $vocab . '/overview');
      foreach ($term as $item) {
        $i->see($item);
      }
    }
  }

}
