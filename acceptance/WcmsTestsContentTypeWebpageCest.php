<?php

use Step\Acceptance\ContentType;

/**
 * Class WcmsTestsContentTypeWebpageCest.
 *
 * Tests for web pages.
 */
class WcmsTestsContentTypeWebpageCest {

  /**
   * The content type.
   *
   * @var string
   */
  private string $contentType = 'uw_ct_web_page';

  /**
   * Tests for blog content type.
   *
   * @param Step\Acceptance\ContentType $i
   *   Acceptance test variable.
   */
  public function testWebpageContentType(ContentType $i): void {

    // Test the fields in the content type.
    $i->testContentType(
      'Web page',
      $i->getContentTypeFields($this->contentType)
    );

    // Test the content type edits.
    $i->testContentTypeEdits(
      $this->contentType,
      $i->getContentTypeEdits($this->contentType)
    );
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _after(AcceptanceTester $i): void {
    // phpcs:enable

    // Delete all the blocks.
    $i->deleteAllBlocks();
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i): void {
    // phpcs:enable

    // Delete all the blocks.
    $i->deleteAllBlocks();
  }

}
