<?php

/**
 * Class MediaMenuItemsCest.
 *
 * Tests for media menu items.
 */
class WcmsTestsMediaMenuItemsCest {

  /**
   * Tests for media authorized.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testMediaMenuAuthorized(AcceptanceTester $i) {

    // The roles to test.
    $roles = [
      'administrator',
      'uw_role_site_manager',
      'uw_role_content_author',
      'uw_role_content_editor',
    ];

    // The media types to test.
    $media_types = $this->getMediaTypes();

    // Step through each of the roles and test media items.
    foreach ($roles as $role) {

      // Login as user.
      $i->amOnPage('user/logout');
      $i->logInWithRole($role);

      // Step through each media type and test.
      foreach ($media_types as $machine_id => $media_type) {

        // Go to add a file page and ensure it loads.
        $i->amOnPage('media/add/' . $machine_id);
        $i->see('Add ' . $media_type);

        // Go to media list page with media type as filter
        // and ensure it loads and is selected.
        $i->amOnPage('admin/content/media?type=' . $machine_id);
        $i->see('Media');
        $i->seeElement('option[value="' . $machine_id . '"][selected="selected"]');
      }

      // Test media on dashboard.
      $i->amOnPage('dashboard/my_dashboard');
      $i->see('Media');
      $i->seeLink('Manage all media');
    }
  }

  /**
   * Tests for media unauthorized.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testMediaMenuUnauthorized(AcceptanceTester $i) {

    // Roles to test.
    $roles = [
      'uw_role_site_owner',
      'uw_role_form_editor',
      'uw_role_form_results_access',
    ];

    // The media types to test.
    $media_types = $this->getMediaTypes();

    // Step through each of the roles and test media items.
    foreach ($roles as $role) {

      // Login as user.
      $i->amOnPage('user/logout');
      $i->logInWithRole($role);

      // Step through each media type and test.
      foreach ($media_types as $machine_id => $media_type) {

        // Go to add a file page and ensure it does not load.
        $i->amOnPage('media/add/' . $machine_id);
        $i->see('You are not authorized to access this page.');
      }

      // Go to the dashboard and ensure there is no manage media.
      $i->amOnPage('dashboard/my_dashboard');
      $i->see('My Dashboard');
      $i->dontSee('Manage all media');
    }

  }

  /**
   * Function to get the media types.
   *
   * @return string[]
   *   Array of media types.
   */
  private function getMediaTypes(): array {

    // The media types to test.
    return [
      'uw_mt_file' => 'File',
      'uw_mt_icon' => 'Icon',
      'uw_mt_image' => 'Image',
      'uw_mt_local_video' => 'Local video',
      'uw_mt_remote_video' => 'Remote video',
      'uw_mt_vimeo_banner_video' => 'Vimeo banner video',
    ];
  }

}
