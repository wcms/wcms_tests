<?php

use Drupal\block\Entity\Block;

/**
 * Class SpecialAlertConfigCest.
 *
 * Tests for special-alert site.
 */
class WcmsTestsSpecialAlertCest {

  /**
   * Tests for special alert config.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testSpecialAlertConfig(AcceptanceTester $i) {

    // Login as administrator.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');

    // Go to special alert config page.
    $i->amOnPage('admin/config/content/special-alert');

    // Display this alert is not checked by default.
    $i->see('Display this alert');
    $i->seeElement('input[id^="edit-display-option"][type="checkbox"]');
    $i->dontSeeCheckboxIsChecked('display_option');

    // Special alert is in Title field by default.
    $i->see('Title');
    $i->seeElement('input[id^="edit-label"][type="text"][value="Special alert"]');
    $i->see('Optionally enter a title to display with this alert');

    // Display title is not checked by default.
    $i->see('Display title');
    $i->seeElement('input[id^="edit-display-option"][type="checkbox"]');
    $i->dontSeeCheckboxIsChecked('label_display');

    // Display style has two options.
    $i->see('Display style');
    $i->seeElement('option[value="default"]');
    $i->seeElement('option[value="priority"]');

    // Check Date.
    $i->see('Date');
    $i->seeElement('input[id^="edit-date-date"]');

    // Check Time.
    $i->see('Time');
    $i->seeElement('input[id^="edit-time-time"]');
    $i->see('Optionally enter a date and time to display with this alert.');

    // Special Alert message is required field.
    $i->see('Special alert message');
    $i->seeInCkEditor(
      $i,
      'Replace this with your Special alert message.',
      '#edit-message-value'
    );

    // Pages field with two.
    $i->see('Pages');
    $i->see('Show for the listed pages');
    $i->see('Hide for the listed pages');

    // Two radio button.
    $i->seeElement('input[id^="edit-negate-0"][type="radio"][checked="checked"]');
    $i->seeElement('input[id^="edit-negate-1"][type="radio"]');
  }

  /**
   * Test the actual special alert block.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testSpecialAlertBlock(AcceptanceTester $i) {

    // Login as administrator.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');

    // Test case 1:
    // Admin just go to settings page, click save button.
    // Home page shows nothing about special alert.
    $i->amOnPage('/admin/config/content/special-alert');
    $i->click('Save configuration');
    $i->see('The configuration options have been saved.');
    $i->amOnPage('home');
    $i->dontSeeElement('.uw-special-alert');

    // Test case 2:
    // Go back to settings page, only check 'Display this alert'
    // Home page shows special alert block with default message.
    $i->amOnPage('/admin/config/content/special-alert');
    $i->click('#edit-display-option');
    $i->seeCheckboxIsChecked('#edit-display-option');
    $i->click('Save configuration');
    $i->see('The configuration options have been saved.');
    $i->amOnPage('home');
    $i->seeElement('.uw-special-alert');
    $i->see('Replace this with your Special alert message.');

    // Test case 3:
    // Go back to settings page, only check 'Display title'
    // Home page shows special alert block with title.
    $i->amOnPage('/admin/config/content/special-alert');
    $i->click('#edit-label-display');
    $i->seeCheckboxIsChecked('#edit-label-display');
    $i->click('Save configuration');
    $i->see('The configuration options have been saved.');
    $i->amOnPage('home');
    $i->seeElement('.uw-special-alert .special-alert__heading');
    $i->see('Special alert');

    // Test case 4:
    // Go back to settings page, add date only.
    // Home page shows special alert block with date entered.
    $i->amOnPage('/admin/config/content/special-alert');
    $date = date(
      'm/d/Y',
      strtotime(
        '+' . rand(1, 30) . ' days'
      )
    );
    $i->fillField(
      'input[id^="edit-date-date"]',
      $date
    );
    $date = date('l, F j, Y', strtotime($date));
    $i->click('Save configuration');
    $i->see('The configuration options have been saved.');
    $i->amOnPage('home');
    $i->seeElement('.uw-special-alert .special-alert__date');
    $i->see($date);

    // Test case 5:
    // Go back to settings page, add time only.
    // Home page shows special alert block with time entered.
    $i->amOnPage('/admin/config/content/special-alert');
    $time_hour = mt_rand(1, 12);
    $enter_hour = str_pad($time_hour, 2, "0", STR_PAD_LEFT);
    $time_min = str_pad(mt_rand(0, 59), 2, "0", STR_PAD_LEFT);
    $time_sec = str_pad(mt_rand(0, 59), 2, "0", STR_PAD_LEFT);
    $display_time = $time_hour . ':' . $time_min;
    $enter_time = $enter_hour . ':' . $time_min . ':' . $time_sec;
    if (rand(0, 1)) {
      $display_time .= ' AM';
      $enter_time .= 'AM';
    }
    else {
      $display_time .= ' PM';
      $enter_time .= 'PM';
    }
    $i->fillField(
      'input[id^="edit-time-time"]',
      $enter_time
    );
    $i->click('Save configuration');
    $i->see('The configuration options have been saved.');
    $i->amOnPage('home');
    $i->seeElement('.uw-special-alert .special-alert__date');
    $i->see($display_time);

    // Test case 6:
    // Go back to settings page, only change custom message.
    // Home page shows special alert block with custom message.
    $i->amOnPage('/admin/config/content/special-alert');
    $message = $i->uwRandomString();
    $i->fillCkEditor(
      $message,
      'textarea[name="message[value]"]'
    );
    $i->click('Save configuration');
    $i->see('The configuration options have been saved.');
    $i->amOnPage('home');
    $i->seeElement('.uw-special-alert');
    $i->see($message);
    // Make sure it is default display of special alert block.
    $i->dontSeeElement('.uw-special-alert .uw-icon');

    // Test case 7:
    // Go back to settings page, only change 'Display style'
    // from Default to Priority.
    // Home page show special alert block with style priority.
    $i->amOnPage('/admin/config/content/special-alert');
    $i->selectOption('#edit-display-style', 'priority');
    $i->click('Save configuration');
    $i->see('The configuration options have been saved.');
    $i->amOnPage('home');
    $i->seeElement('.uw-special-alert .uw-icon');
  }

  /**
   * Test for roles to edit special alerts.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testSpecialAlertsRoles(AcceptanceTester $i) {

    // Get the wcms roles.
    $roles = $i->getRoles();

    // The roles that can view special alerts.
    $allowed_roles = [
      'administrator',
      'uw_role_site_manager',
    ];

    // Step through each role and test if it can or
    // can not see the special alerts config.
    foreach ($roles as $role) {

      // Login with the role.
      $i->amOnPage('user/logout');
      $i->logInWithRole($role);

      // Go to special alerts page.
      $i->amOnPage('/admin/config/content/special-alert');

      // Check the access for the role.
      if (in_array($role, $allowed_roles)) {
        $i->see('uWaterloo Special alert configuration');
      }
      else {
        $i->see('Access denied');
        $i->see('You are not authorized to access this page.');
      }
    }
  }

  /**
   * Function for when tests are completed.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  private function testComplete(AcceptanceTester $i) {

    // Load the special alert block.
    $block = Block::load('specialalert');

    // The default settings for special alert block.
    $settings = [
      'label' => 'Special alert',
      'label_display' => 0,
      'provider' => 'uw_custom_blocks',
      'display_option' => 0,
      'message' => [
        'value' => '<p>Replace this with your Special alert message.</p>',
        'format' => 'uw_tf_basic',
      ],
    ];

    // Set the settings for special alert block.
    $block->set('settings', $settings);

    $block->save();
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _after(AcceptanceTester $i): void {
    // phpcs:enable

    // Ensure that the special alert block returns
    // to its default settings.
    $this->testComplete($i);
  }

  // phpcs:disable
  /**
   * Function to run if the tests fail.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i): void {
    // phpcs:enable

    // Ensure that the special alert block returns
    // to its default settings.
    $this->testComplete($i);
  }

}
