<?php

use Step\Acceptance\ContentType;
use Step\Acceptance\TaxonomyTest;

/**
 * Class WcmsTestsContentTypeEventCest.
 *
 * Tests for events.
 */
class WcmsTestsContentTypeEventCest {

  /**
   * The content type.
   *
   * @var string
   */
  private string $contentType = 'uw_ct_event';

  /**
   * Tests for blog content type.
   *
   * @param Step\Acceptance\ContentType $i
   *   Acceptance test variable.
   */
  public function testEventContentType(ContentType $i): void {

    // Test the fields in the content type.
    $i->testContentType(
      'Event',
      $i->getContentTypeFields($this->contentType)
    );

    // Test the content type edits.
    $i->testContentTypeEdits(
      $this->contentType,
      $i->getContentTypeEdits($this->contentType)
    );
  }

  /**
   * Function to test taxonomies.
   *
   * @param Step\Acceptance\TaxonomyTest $i
   *   The acceptance tester.
   */
  public function testEventTaxonomies(TaxonomyTest $i): void {

    // Test the taxonomy terms.
    $i->testTaxonomies(
      $this->contentType,
      'event',
      [
        'uw_vocab_audience',
        'uw_tax_event_type',
      ]
    );
  }

  /**
   * Function to test location info on events.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testEventLocationInfo(AcceptanceTester $i): void {

    // Login with the content author role.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_content_author');

    // Go to the add event content type page.
    $i->amOnPage('node/add/uw_ct_event');

    // Ensure that location stuff is present.
    $i->seeElement('#edit-location-presets-select');
    $i->seeElement('#edit-field-uw-event-map-0-uri');
    $i->see('Optional: provide a link to a map with the event location (e.g. https://uwaterloo.ca/map/)');
  }

  /**
   * Test event content type with dates.
   *
   * @param Step\Acceptance\ContentType $i
   *   The acceptance tester.
   */
  public function testEventDates(ContentType $i): void {

    // Get the dates to be used.
    $dates = $i->getEarlyMiddleLateDates();

    // Step through each of the dates and create content.
    foreach ($dates as $date) {

      // Get the edits for the content type.
      $edits = $i->getContentTypeEdits('uw_ct_event');

      // Set the default edit date to one of the generated dates.
      $edits['field']['edit-field-uw-event-date-0-time-wrapper-value-date'] = $date;

      // Test the edits with the new date, not running the full
      // tests like we do when testing the content type.
      $i->testContentTypeEdits(
        'uw_ct_event',
        $edits,
        FALSE
      );

      // Click on submit and ensure that the node has been created.
      $i->click('#edit-submit');
      $i->see('Event ' . $edits['field']['edit-title-0-value'] . ' has been created');
    }

    // Test the blog listing page.
    $i->testListingPage($dates, 'events');
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _after(AcceptanceTester $i): void {
    // phpcs:enable

    // Delete all the blocks.
    $i->deleteAllBlocks();
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i): void {
    // phpcs:enable

    // Delete all the blocks.
    $i->deleteAllBlocks();
  }

}
