<?php

use Codeception\Util\Locator;
use Facebook\WebDriver\WebDriverKeys;

/**
 * Class WcmsTestsBlocksListingsCest.
 *
 * Tests for listing blocks.
 */
class WcmsTestsBlocksListingsCest {

  /**
   * Array used for any tids that we created.
   *
   * @var array
   */
  private $tidsUsed = [];

  /**
   * Array of nodes used.
   *
   * @var array
   */
  private array $nodesUsed = [];

  /**
   * Function to test the multi type list block.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testMultiTypeListBlock(AcceptanceTester $i) {

    // Create a webpage.
    $this->nodesUsed['Multi Type List Block'] = $i->createWebPage('Multi Type List Block');

    // Get the path of the webpage.
    $path = $i->getWebPagePath($this->nodesUsed['Multi Type List Block']);

    // List of content types used in multi-type list.
    $content_types = [
      'uw_ct_news_item' => 'news',
      'uw_ct_event' => 'events',
      'uw_ct_blog' => 'blog',
      'uw_ct_opportunity' => 'opportunity',
    ];

    // Create content types for testing in multi-type list.
    foreach ($content_types as $type => $label) {
      $title = $label . 'list1';
      $this->nodesUsed[$title] = $i->createCtNode(
        $type,
        $title,
        TRUE
      );
    }

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Go to the layout page for the webpage we created.
    $i->amOnPage($path . '/layout');
    $i->see('Edit layout for ' . $this->nodesUsed['Multi Type List Block']->getTitle());

    // Add a block.
    $i->click('Add block');
    $i->waitForText('Choose a block');

    // Click on the multi type list block.
    $i->click('Multi-type list');
    $i->waitForText('Configure block');

    // Ensure title options are present.
    $i->see('Title');
    $i->seeElement('input[id^="edit-settings-label"]');
    $i->see('Display title');
    $i->seeElement('input[id^="edit-settings-label-display"]');

    // Ensure heading level option is present.
    $i->see('Heading level');
    $i->seeElement('select[id^="edit-settings-heading-level"] option[value="h2"][selected="selected"]');

    // Enable item weight options.
    $i->click('Show row weights');
    $i->see('Hide row weights');

    // Step through each node type and test elements.
    foreach ($content_types as $type => $label) {
      $i->see(ucfirst($label));
      $i->seeElement('select[id^="edit-settings-items-' . $label . '-number-of-nodes"] option[value="3"][selected="selected"]');
      $i->seeElement('select[id^="edit-settings-items-' . $label . '-shown"] option[value="1"][selected="selected"]');
      $i->seeElement('select[id^="edit-settings-items-' . $label . '-weight"]');
    }

    // Ensure checkboxes are present.
    $i->seeElement('input[id^="edit-settings-show-all-items"]');
    $i->seeElement('input[id^="edit-settings-show-view-all"]');

    // Enable checkboxes.
    $i->checkOption('input[id^="edit-settings-show-all-items"]');
    $i->checkOption('input[id^="edit-settings-show-view-all"]');

    // Click on add block and ensure it is on the screen.
    $i->click('Add block');
    $i->waitForElement(Locator::contains('div[class="uw-admin-label"]', 'Multi-type list'));
    $i->seeElement(Locator::contains('div[class="uw-admin-label"]', 'Multi-type list'));

    // Click on save layout (List tabs cannot be navigated in layout editor).
    $i->click('Save layout');

    // Ensure that we see elements for list tabs.
    $i->seeElement('div[class="uw-tabs"]');
    $i->seeElement('div[class="uw-tab"]');
    $i->seeElement(Locator::contains('button[id*="tab-"]', 'News'));
    $i->seeElement(Locator::contains('button[id*="tab-"]', 'Events'));
    $i->seeElement(Locator::contains('button[id*="tab-"]', 'Blog'));
    $i->seeElement(Locator::contains('button[id*="tab-"]', 'Opportunities'));

    // Navigate to 'News' tab and check news elements.
    $i->click('News', '.uw-tab');
    $i->see('newslist1');
    $i->see('Read all news');

    // Navigate to 'Events' tab and check event elements.
    $i->click('Events', '.uw-tab');
    $i->see('eventslist1');
    $i->see('All upcoming events');

    // Navigate to 'Blog' tab and check blog elements.
    $i->click('Blog', '.uw-tab');
    $i->see(strtoupper(date('l, F j, Y')));
    $i->see('bloglist1');
    $i->see('View all blog posts');

    // Navigate to 'Opportunities' tab and check opportunities elements.
    $i->click('Opportunities', '.uw-tab');
    $i->see('opportunitylist1');
    $i->see('View all opportunities');
  }

  /**
   * Function to test the automatic list block config.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testAutomaticListBlockConfig(AcceptanceTester $i) {

    // Get the content types for the automatic list block.
    $content_types = $this->getAutomaticContentTypes();

    // Create a webpage.
    $this->nodesUsed['Automatic List Block'] = $i->createWebPage('Automatic List Block');

    // Get the path of the webpage.
    $path = $i->getWebPagePath($this->nodesUsed['Automatic List Block']);

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Go to the layout page for the webpage we created.
    $i->amOnPage($path . '/layout');
    $i->see('Edit layout for ' . $this->nodesUsed['Automatic List Block']->getTitle());

    // Add a block.
    $i->click('Add block');
    $i->waitForText('Choose a block');

    // Click on the automatic list block.
    $i->click('Automatic list');
    $i->waitForText('Configure block');

    // Step through each content type and test that it is in
    // the select list.
    foreach (array_keys($content_types) as $machine_name) {
      $i->seeElement('option[value="' . $machine_name . '"]');
    }
  }

  /**
   * Function to test the manual list block config.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testManualListBlockConfig(AcceptanceTester $i) {

    // Get the content types for the manual list block.
    $content_types = $this->getManualContentTypes();

    // Create a webpage.
    $this->nodesUsed['Manual List Block'] = $i->createWebPage('Manual List Block');

    // Get the path of the webpage.
    $path = $i->getWebPagePath($this->nodesUsed['Manual List Block']);

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Go to the layout page for the webpage we created.
    $i->amOnPage($path . '/layout');
    $i->see('Edit layout for ' . $this->nodesUsed['Manual List Block']->getTitle());

    // Add a block.
    $i->click('Add block');
    $i->waitForText('Choose a block');

    // Click on the Manual list block.
    $i->click('Manual list');
    $i->waitForText('Configure block');

    // Step through each content type and test that it is in
    // the select list.
    foreach (array_keys($content_types) as $machine_name) {
      $i->seeElement('option[value="' . $machine_name . '"]');
    }
  }

  /**
   * Function to test the automatic list block.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testAutomaticListBlock(AcceptanceTester $i) {

    // Get the content types for the automatic list block.
    $content_types = $this->getAutomaticContentTypes();

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Create a keyword for publication references.
    $i->amOnPage('bibcite/keyword/add');
    $i->fillField('Name', 'testkey');
    $i->click('Save');

    // Step through each content type and test the block.
    foreach ($content_types as $machine_name => $content_type) {

      // Create nodes to be used in the test.
      $this->createContentForBlocks(
        $i,
        $content_type
      );

      // Get the name of webpage based on block.
      $block_page_name = $content_type . ' List Block';

      // Create a webpage.
      $this->nodesUsed[$block_page_name] = $i->createWebPage($block_page_name);

      // Get the path of the webpage.
      $path = $i->getWebPagePath($this->nodesUsed[$block_page_name]);

      // Go to the layout page for the webpage we created.
      $i->amOnPage($path . '/layout');
      $i->see('Edit layout for ' . $this->nodesUsed[$block_page_name]->getTitle());

      // Test the automatic block config.
      $this->testAutomaticBlockContentTypeConfig(
        $i,
        $machine_name
      );

      // Test the actual settings of the block.
      $this->testAutomaticListBlockSettings(
        $i,
        $machine_name
      );
    }
  }

  /**
   * Function to test the manual list block.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testManualListBlock(AcceptanceTester $i) {

    // Get the content types for the manual list block.
    $content_types = $this->getManualContentTypes();

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Step through each content type and test the block.
    foreach ($content_types as $machine_name => $content_type) {

      // Create nodes to be used in the test.
      $this->createContentForBlocks(
        $i,
        $content_type
      );

      // Get the name of webpage based on block.
      $block_page_name = $content_type . ' List Block';

      // Create a webpage.
      $this->nodesUsed[$block_page_name] = $i->createWebPage($block_page_name);

      // Get the path of the webpage.
      $path = $i->getWebPagePath($this->nodesUsed[$block_page_name]);

      // Go to the layout page for the webpage we created.
      $i->amOnPage($path . '/layout');
      $i->see('Edit layout for ' . $this->nodesUsed[$block_page_name]->getTitle());

      // Test the automatic block config.
      $this->testManualBlockContentTypeConfig(
        $i,
        $machine_name,
        $content_type
      );

      // Test the actual settings of the block.
      $this->testManualListBlockSettings(
        $i,
        $content_type
      );
    }
  }

  /**
   * Function to test the Waterloo Events Block.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testWaterlooEventsBlock(AcceptanceTester $i) {

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Get upcoming Waterloo Event urls.
    $events = $this->getWaterlooEvents($i, 3);

    // Create a webpage.
    $this->nodesUsed['Waterloo Events Block'] = $i->createWebPage('Waterloo Events Block');

    // Get the path of the webpage.
    $path = $i->getWebPagePath($this->nodesUsed['Waterloo Events Block']);

    // Go to the layout page for the webpage we created.
    $i->amOnPage($path . '/layout');
    $i->see('Edit layout for ' . $this->nodesUsed['Waterloo Events Block']->getTitle());

    // Add a block.
    $i->click('Add block');
    $i->waitForText('Choose a block');

    // Click on the Waterloo Events block.
    $i->click('Waterloo Events');
    $i->waitForText('Configure block');

    // Step through and add all Waterloo Events urls.
    foreach ($events as $index => $eventUrls) {

      // Add a Waterloo Events link.
      $i->waitForElement('input[name="settings[items_fieldset][items][' . $index . '][url]"]');
      $i->fillField('input[name="settings[items_fieldset][items][' . $index . '][url]"]', $eventUrls);

      // Add a Waterloo Events Item.
      $i->click('input[value="Add Waterloo Events item"]');
    }

    // Add the block.
    $i->click('Add block');
    $i->waitForElement(Locator::contains('div[class="uw-admin-label"]', 'Waterloo Events'));

    // Navigate to link are validate that url is not repeated.
    $i->click('Save layout');
    $i->waitForText('Waterloo Events');

    // Step through and click on each Waterloo Event.
    foreach ($events as $eventUrl) {

      // Click on the Event.
      $i->click('a[href="' . $eventUrl . '"]');
      $i->waitForText('Waterloo Events');

      // Check that correct url is displayed.
      $i->assertEquals($i->getCurrentUrl(), substr($eventUrl, 20, NULL));

      // Navigate back to Waterloo Events webpage.
      $i->amOnPage($path);
    }
  }

  /**
   * Function to test list parity.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testListParity(AcceptanceTester $i) {

    // Login as admin.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');

    // Create the webpage to test on.
    $this->nodesUsed['page'] = $i->createCtNode('uw_ct_web_page', 'testpage', TRUE);

    // Get content types that work for both list blocks and loop through.
    $content_types = $this->getManualContentTypes();
    foreach ($content_types as $machine_name => $content_type) {

      // Create content for each type.
      switch ($content_type) {

        case 'Catalogs':
          $this->tidsUsed['catalog'] = $i->createTerm('uw_vocab_catalogs', 'testcatalog');
          break;

        // Use the catalog taxonomy for the catalog item.
        case 'Catalog items':
          $this->nodesUsed['catalog_item'] = $i->createCtNode('uw_ct_catalog_item', 'testcatalog_item', TRUE, ['catalog' => $this->tidsUsed['catalog']]);
          break;

        default:
          $this->nodesUsed[$machine_name] = $i->createCtNode('uw_ct_' . $machine_name, 'test' . $machine_name, TRUE);
          break;
      }

      // Navigate to layout builder.
      $i->amOnPage('/testpage/layout');
      $i->waitforText('Edit layout for testpage');

      // Add an automatic list block.
      $i->click('Add block');
      $i->waitForText('Choose a block');
      $i->click('Automatic list');
      $i->waitForText('Configure block');
      $i->fillField('input[name="settings[label]"]', 'title');
      $i->selectOption('settings[content_type]', $content_type);

      // Set config to all events for event type.
      if ($content_type == 'Events') {
        $i->selectOption('settings[event][date]', 'All events regardless of date');
      }

      // Create a block id and add the block.
      $i->fillField('input[name="settings[layout_builder_id]"]', 'automatic' . $machine_name);
      $i->click('Add block');
      $i->waitForElement('.uw-admin-label');

      // Add an manual list block.
      $i->click('Add block');
      $i->waitForText('Choose a block');
      $i->click('Manual list');
      $i->waitForText('Configure block');
      $i->fillField('input[name="settings[label]"]', 'title');
      $i->selectOption('settings[content_type]', $content_type);
      $i->fillField('input[name="settings[' . $machine_name . '][items_fieldset][' . $machine_name . '_ids][0][id]"]', 'test' . $machine_name);

      // Create a block id and add the block.
      $i->fillField('input[name="settings[layout_builder_id]"]', 'manual' . $machine_name);
      $i->click('Add block');
      $i->waitForText('Edit layout for testpage');

      // Save the layout.
      $i->click('Save layout');
      $i->waitForText('The layout override has been saved.');

      // Create a reference image using the automatic list.
      // Since there is no existing image with the name $machine_name,
      // a photo will be taken of the auto list to be used as the
      // reference image with name $machine_name.
      $i->dontSeeVisualChanges($machine_name, '#automatic' . str_replace('_', '-', $machine_name));

      // Compare the reference image to the manual list.
      // Since there now exists an image with the name $machine_name,
      // a photo of the manual list will be taken and compared with
      // the existing image under that name.
      $i->dontSeeVisualChanges($machine_name, '#manual' . str_replace('_', '-', $machine_name));

      // Remove the section.
      $i->amOnPage('/testpage/layout');
      $this->removeAddFirstSection($i);
    }
  }

  // phpcs:disable

  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _after(AcceptanceTester $i): void {
    // phpcs:enable

    // Compelte the block tests.
    $i->completeBlockTests($this->tidsUsed, $this->nodesUsed);
  }

  // phpcs:disable

  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i): void {
    // phpcs:enable

    // Compelte the block tests.
    $i->completeBlockTests($this->tidsUsed, $this->nodesUsed);
  }

  /**
   * Function to get the content types for automatic listing block.
   *
   * @return string[]
   *   Array of content types for automatic listing block.
   */
  private function getAutomaticContentTypes(): array {

    return [
      'blog' => 'Blog posts',
      'catalog_item' => 'Catalog items',
      'catalog' => 'Catalogs',
      'contact' => 'Contacts',
      'event' => 'Events',
      'news_item' => 'News items',
      'opportunity' => 'Opportunities',
      'profile' => 'Profiles',
      'project' => 'Projects',
      'publication_reference' => 'Publication references',
      'service' => 'Services',
    ];
  }

  /**
   * Function to get the content types for manual listing block.
   *
   * @return string[]
   *   Array of content types for manual listing block.
   */
  private function getManualContentTypes(): array {

    return [
      'blog' => 'Blog posts',
      'catalog' => 'Catalogs',
      'catalog_item' => 'Catalog items',
      'contact' => 'Contacts',
      'event' => 'Events',
      'news_item' => 'News items',
      'opportunity' => 'Opportunities',
      'profile' => 'Profiles',
      'project' => 'Projects',
      'service' => 'Services',
    ];
  }

  /**
   * Function to create the content to test the blocks.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   * @param string $content_type
   *   The content type.
   */
  private function createContentForBlocks(
    AcceptanceTester $i,
    string $content_type,
  ) {

    // Create content based on the content type.
    switch ($content_type) {

      case 'Blog posts':
        // Create a term to be used with the blog.
        $this->tidsUsed['blogtag'] = $i->createTerm('uw_vocab_blog_tags', 'bt1');

        // Create a bunch of blog nodes.
        for ($j = 1; $j <= 4; $j++) {
          $this->nodesUsed[$content_type . $j] = $i->createCtNode(
            'uw_ct_blog',
            $content_type . $j,
            TRUE,
            $j <= 2 ? ['field_uw_blog_tags' => [$this->tidsUsed['blogtag']]] : []
          );
        }
        break;

      case 'Catalogs':
        // Use the catalog created in 'Catalog items'.
        break;

      case 'Catalog items':
        // Create terms to be used with catalog items.
        $this->tidsUsed['catalog'] = $i->createTerm('uw_vocab_catalogs', 'c1');

        // Create a bunch of catalog item nodes.
        for ($j = 1; $j <= 4; $j++) {
          $this->nodesUsed[$content_type . $j] = $i->createCtNode(
            'uw_ct_catalog_item',
            $content_type . $j,
            TRUE,
            $j <= 2 ? ['field_uw_catalog_catalog' => ['target_id' => $this->tidsUsed['catalog']]] : []
          );
        }
        break;

      case 'Contacts':
        // Create a term to be used with contacts.
        $this->tidsUsed['contactgroup'] = $i->createTerm('uw_vocab_contact_group', 'cg1');

        // Create a bunch of contact nodes.
        for ($j = 1; $j <= 4; $j++) {
          $this->nodesUsed[$content_type . $j] = $i->createCtNode(
            'uw_ct_contact',
            $content_type . $j,
            TRUE,
            $j <= 2 ? ['field_uw_ct_contact_group' => [$this->tidsUsed['contactgroup']]] : []
          );
        }
        break;

      case 'News items':
        // Create a term to be used with news.
        $this->tidsUsed['newstag'] = $i->createTerm('uw_vocab_news_tags', 'nt1');

        // Create a bunch of news item nodes.
        for ($j = 1; $j <= 4; $j++) {
          $this->nodesUsed[$content_type . $j] = $i->createCtNode(
            'uw_ct_news_item',
            $content_type . $j,
            TRUE,
            $j <= 2 ? ['field_uw_news_tags' => [$this->tidsUsed['newstag']]] : []
          );
        }
        break;

      case 'Events':
        // Create terms to be used with events.
        $this->tidsUsed['eventtags'] = $i->createTerm('uw_tax_event_tags', 'et1');

        // Create a bunch of event nodes.
        for ($j = 1; $j <= 4; $j++) {
          $this->nodesUsed[$content_type . $j] = $i->createCtNode(
            'uw_ct_event',
            $content_type . $j,
            TRUE,
            $j <= 2 ? [
              'field_uw_event_tags' => [$this->tidsUsed['eventtags']],
              // Event_type id 25 = Conference.
              'field_uw_event_type' => 25,
            ] : []
          );
        }
        break;

      case 'Opportunities':
        // Create a bunch of opportunity nodes.
        for ($j = 1; $j <= 4; $j++) {
          $this->nodesUsed[$content_type . $j] = $i->createCtNode(
            'uw_ct_opportunity',
            $content_type . $j,
            TRUE,
            $j <= 2 ? [
              // Opportunity_type id 35 = Paid.
              'field_uw_opportunity_type' => 35,
              // Opportunity_employment id 38 = Full time.
              'field_uw_opportunity_employment' => 38,
              // Opportunity_pay_type id 41 = Hourly.
              'field_uw_opportunity_pay_type' => 41,
            ] : []
          );
        }
        break;

      case 'Profiles':
        // Create a term to be used with profiles.
        $this->tidsUsed['profiletype'] = $i->createTerm('uw_vocab_profile_type', 'ptype1');

        // Create a bunch of profile nodes.
        for ($j = 1; $j <= 4; $j++) {
          $this->nodesUsed[$content_type . $j] = $i->createCtNode(
            'uw_ct_profile',
            $content_type . $j,
            TRUE,
            $j <= 2 ? ['field_uw_ct_profile_type' => [$this->tidsUsed['profiletype']]] : []
          );
        }
        break;

      case 'Projects':
        // Create terms to be used with projects.
        $this->tidsUsed['projecttopic'] = $i->createTerm('uw_vocab_project_topic', 'pt1');

        // Create a bunch of project nodes.
        for ($j = 1; $j <= 4; $j++) {
          $this->nodesUsed[$content_type . $j] = $i->createCtNode(
            'uw_ct_project',
            $content_type . $j,
            TRUE,
            $j <= 2 ? [
              // Project_status id 44 = Current projects.
              'field_uw_project_status' => 44,
              'field_uw_project_topics' => [$this->tidsUsed['projecttopic']],
            ] : []
          );
        }
        break;

      case 'Publication references':
        // Create a bunch of bibcite items.
        for ($j = 1; $j <= 4; $j++) {
          // @todo Create pubref.
        }
        break;

      case 'Services':
        // Create a term to be used with services.
        $this->tidsUsed['servicecategory'] = $i->createTerm('uw_vocab_service_categories', 'sc1');

        // Create a bunch of service nodes.
        for ($j = 1; $j <= 4; $j++) {
          $this->nodesUsed[$content_type . $j] = $i->createCtNode(
            'uw_ct_service',
            $content_type . $j,
            TRUE,
            $j <= 2 ? ['field_uw_service_category' => $this->tidsUsed['servicecategory']] : []
          );
        }
        break;

    }
  }

  /**
   * Function to test for the settings of the automatic list block.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   * @param string $content_type
   *   The content type.
   */
  private function testAutomaticListBlockSettings(
    AcceptanceTester $i,
    string $content_type,
  ) {

    // Test the settings of the automatic block based on
    // the content type.
    switch ($content_type) {

      case 'blog':
        $i->selectOption('settings[blog][limit]', '6');
        $i->click('Add block');
        $i->waitForText('Automatic list');
        $i->waitForElement('.card__teaser--blog');
        $i->seeNumberOfElements('.card__teaser--blog', 4);
        $this->removeAddFirstSection($i);
        $this->addListingBlockWithContentType(
          $i,
          'Automatic list',
          $content_type,
          TRUE,
          2
        );
        $i->waitForElement('.card__teaser--blog');
        $i->seeNumberOfElements('.card__teaser--blog', 2);
        $this->removeAddFirstSection($i);
        $this->addListingBlockWithContentType(
          $i,
          'Automatic list',
          $content_type,
          FALSE,
          10
        );
        $i->fillField('input[name="settings[blog][tags]"]', 'bt1');
        $i->pressKey('input[name="settings[blog][tags]"]', WebDriverKeys::ARROW_DOWN);
        $i->pressKey('input[name="settings[blog][tags]"]', WebDriverKeys::ENTER);
        $i->click('Add block');
        $i->waitForElement('.card__teaser--blog');
        $i->seeNumberOfElements('.card__teaser--blog', 2);
        break;

      case 'catalog':
        $i->click('Add block');
        $i->waitForText('Automatic list');
        $i->waitForElement('.views-field-nid');
        $i->seeNumberOfElements('.views-field-nid', 3);
        break;

      case 'catalog_item':
        $i->click('Add block');
        $i->waitForText('Automatic list');
        $i->waitForElement('.views-field-title');
        $i->seeNumberOfElements('.views-field-title', 3);
        $this->removeAddFirstSection($i);
        $this->addListingBlockWithContentType(
          $i,
          'Automatic list',
          $content_type,
          FALSE,
          10
        );
        $i->fillField('input[name="settings[catalog_item][catalog]"]', 'c1');
        $i->click('Add block');
        $i->waitForElement('.views-field-title');
        $i->seeNumberOfElements('.views-field-title', 2);
        break;

      case 'contact':
        $i->click('Add block');
        $i->waitForText('Automatic list');
        $i->waitForElement('.uw-contact');
        $i->seeNumberOfElements('.uw-contact', 3);
        $this->removeAddFirstSection($i);
        $this->addListingBlockWithContentType(
          $i,
          'Automatic list',
          $content_type,
          FALSE,
          10
        );
        $i->fillField('input[name="settings[contact][groups]"]', 'cg1');
        $i->click('Add block');
        $i->waitForElement('.uw-contact');
        $i->seeNumberOfElements('.uw-contact', 2);
        break;

      case 'event':
        $i->click('Add block');
        $i->waitForText('Automatic list');
        $i->waitForElement('.card__teaser--event');
        $i->seeNumberOfElements('.card__teaser--event', 3);
        $this->removeAddFirstSection($i);
        $this->addListingBlockWithContentType(
          $i,
          'Automatic list',
          $content_type,
          FALSE,
          10
        );
        $i->fillField('input[name="settings[event][tags]"]', 'et1');
        $i->click('Add block');
        $i->waitForElement('.card__teaser--event');
        $i->seeNumberOfElements('.card__teaser--event', 2);
        break;

      case 'news_item':
        $i->click('Add block');
        $i->waitForText('Automatic list');
        $i->waitForElement('.card__teaser--news-item');
        $i->seeNumberOfElements('.card__teaser--news-item', 3);
        $this->removeAddFirstSection($i);
        $this->addListingBlockWithContentType(
          $i,
          'Automatic list',
          $content_type,
          FALSE,
          10
        );
        $i->fillField('input[name="settings[news_item][tags]"]', 'nt1');
        $i->click('Add block');
        $i->waitForElement('.card__teaser--news-item');
        $i->seeNumberOfElements('.card__teaser--news-item', 2);
        break;

      case 'opportunity':
        $i->click('Add block');
        $i->waitForText('Automatic list');
        $i->waitForElement('.card__teaser--opportunity');
        $i->seeNumberOfElements('.card__teaser--opportunity', 3);
        $this->removeAddFirstSection($i);
        $this->addListingBlockWithContentType(
          $i,
          'Automatic list',
          $content_type,
          FALSE,
          10
        );
        $i->selectOption('select[name="settings[opportunity][type]"]', 35);
        $i->selectOption('select[name="settings[opportunity][employment]"]', 38);
        $i->selectOption('select[name="settings[opportunity][rate_of_pay]"]', 41);
        $i->click('Add block');
        $i->waitForElement('.card__teaser--opportunity');
        $i->seeNumberOfElements('.card__teaser--opportunity', 2);
        break;

      case 'profile':
        $i->click('Add block');
        $i->waitForText('Automatic list');
        $i->waitForElement('.card__teaser--profile');
        $i->seeNumberOfElements('.card__teaser--profile', 3);
        $this->removeAddFirstSection($i);
        $this->addListingBlockWithContentType(
          $i,
          'Automatic list',
          $content_type,
          FALSE,
          10
        );
        $i->fillField('input[name="settings[profile][types]"]', 'ptype1');
        $i->click('Add block');
        $i->waitForElement('.card__teaser--profile');
        $i->seeNumberOfElements('.card__teaser--profile', 2);
        break;

      case 'project':
        $i->click('Add block');
        $i->waitForText('Automatic list');
        $i->waitForElement('.card__teaser--project');
        $i->seeNumberOfElements('.card__teaser--project', 3);
        $this->removeAddFirstSection($i);
        $this->addListingBlockWithContentType(
          $i,
          'Automatic list',
          $content_type,
          FALSE,
          10
        );
        $i->selectOption('select[name="settings[project][status]"]', 44);
        $i->selectOption('select[name="settings[project][topic]"]', 'pt1');
        $i->click('Add block');
        $i->waitForElement('.card__teaser--project');
        $i->seeNumberOfElements('.card__teaser--project', 2);
        break;

      case 'publication_reference':
        // @todo Add test for publication reference block.
        break;

      case 'service':
        $i->click('Add block');
        $i->waitForText('Automatic list');
        $i->waitForElement('.views-field-field-uw-service-summary');
        $i->seeNumberOfElements('.views-field-field-uw-service-summary', 4);
        $this->removeAddFirstSection($i);
        $i->click('Add block');
        $i->waitForText('Choose a block');
        $i->click('Automatic list');
        $i->waitForText('Automatic list');
        $i->waitForElement('.js-form-item-settings-content-type');
        $i->selectOption('settings[content_type]', 'service');
        $i->fillField('input[name="settings[service][categories]"]', 'sc1');
        $i->click('Add block');
        $i->waitForElement('.views-field-field-uw-service-summary');
        $i->seeNumberOfElements('.views-field-field-uw-service-summary', 2);
        break;

    }
  }

  /**
   * Function to test for the settings of the manual list block.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   * @param string $content_type
   *   The content type.
   */
  private function testManualListBlockSettings(
    AcceptanceTester $i,
    string $content_type,
  ) {

    // Test the settings of the Manual block based on
    // the content type.
    switch ($content_type) {

      case 'Events':
        $i->fillField('input[name="settings[event][items_fieldset][event_ids][0][id]"]', 'Events1');
        $i->selectOption('settings[event][style]', 'agenda');
        $i->click('Add block');
        $i->waitForText('Manual list');
        $i->waitForElement('.uw-agenda');
        $this->removeAddFirstSection($i);
        $this->addListingBlockWithContentType(
          $i,
          'Manual list',
          'event',
          FALSE,
        );
        $i->fillField('input[name="settings[event][items_fieldset][event_ids][0][id]"]', 'Events1');
        $i->selectOption('settings[event][style]', 'default');
        $i->click('Add block');
        $i->waitForText('Manual list');
        $i->dontSeeElement('.uw-agenda');
        break;
    }
  }

  /**
   * Function to test the automatic block config.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   * @param string $content_type
   *   The content type.
   */
  private function testAutomaticBlockContentTypeConfig(
    AcceptanceTester $i,
    string $content_type,
  ) {

    // Add the automatic list block, without saving the block.
    if ($content_type != 'catalog' && $content_type != 'service') {
      $this->addListingBlockWithContentType(
        $i,
        'Automatic list',
        $content_type,
        FALSE,
        3
      );
    }

    // Ensure that the config is on the block based
    // on the content type.
    switch ($content_type) {

      case 'blog':
        $i->see('Blog list settings');
        $i->see('Items per block');
        $i->see('Blog tag(s)');
        $i->see('Include all eligible items, not just those promoted to the home page');
        $i->seeElement('input[id*="show-all-items"][value="1"]');
        break;

      case 'catalog':
        $i->click('Add block');
        $i->waitForText('Choose a block');
        $i->click('Automatic list');
        $i->waitForText('Automatic list');
        $i->waitForElement('.js-form-item-settings-content-type');
        $i->selectOption('settings[content_type]', 'catalog');
        $i->see('Catalog list settings');
        $i->see('Heading selector');
        break;

      case 'catalog_item':
        $i->see('Catalog item list settings');
        $i->see('Items per block');
        $i->see('Catalog(s)');
        $i->see('Categories');
        $i->see('Include all eligible items, not just those promoted to the home page');
        $i->seeElement('input[id*="show-all-items"][value="1"]');
        break;

      case 'contact':
        $i->see('Contact list settings');
        $i->see('Items per block');
        $i->see('Show contact image?');
        $i->see('Group(s)');
        $i->see('Sort');
        $i->seeElement('option[value="alphabetic"]');
        $i->seeElement('option[value="global"]');
        $i->see('Alphabetic sort would use the name for sorting purposes field; global arrangement would use the weights from the rearrange contact view.');
        $i->see('Include all eligible items, not just those promoted to the home page');
        $i->seeElement('input[id*="show-all-items"][value="1"]');
        break;

      case 'event':
        $i->see('Event list settings');
        $i->see('Items per block');
        $i->see('Event type(s)');
        $i->see('Event tag(s)');
        $i->see('Time period');
        $i->see('Style');
        $i->see('Include all eligible items, not just those promoted to the home page');
        $i->seeElement('input[id*="show-all-items"][value="1"]');
        break;

      case 'news_item':
        $i->see('News item list settings');
        $i->see('Items per block');
        $i->see('News tag(s)');
        $i->see('Include all eligible items, not just those promoted to the home page');
        $i->seeElement('input[id*="show-all-items"][value="1"]');
        break;

      case 'opportunity':
        $i->see('Opportunity list settings');
        $i->see('Items per block');
        $i->see('Opportunity type');
        $i->see('Employment type');
        $i->see('Rate of pay type');
        $i->see('Include all eligible items, not just those promoted to the home page');
        $i->seeElement('input[id*="show-all-items"][value="1"]');
        break;

      case 'profile':
        $i->see('Profile list settings');
        $i->see('Items per block');
        $i->see('Show profile image?');
        $i->see('Type(s)');
        $i->see('Sort');
        $i->seeElement('option[value="alphabetic"]');
        $i->seeElement('option[value="global"]');
        $i->see('Alphabetic sort would use the name for sorting purposes field; global arrangement would use the weights from the rearrange profile view.');
        $i->see('Include all eligible items, not just those promoted to the home page');
        $i->seeElement('input[id*="show-all-items"][value="1"]');
        break;

      case 'project':
        $i->see('Project list settings');
        $i->see('Role');
        $i->see('Status');
        $i->see('Topic');
        $i->see('Include all eligible items, not just those promoted to the home page');
        $i->seeElement('input[id*="show-all-items"][value="1"]');
        break;

      case 'publication_reference':
        $i->see('Publication reference list settings');
        $i->see('Heading level');
        $i->see('Items per block');
        $i->see('Search');
        $i->see('Keyword(s)');
        $i->see('Type');
        $i->see('Year');
        break;

      case 'service':
        $i->click('Add block');
        $i->waitForText('Choose a block');
        $i->click('Automatic list');
        $i->waitForText('Automatic list');
        $i->waitForElement('.js-form-item-settings-content-type');
        $i->selectOption('settings[content_type]', 'service');
        $i->see('Service list settings');
        $i->see('Categories');
        $i->see('Include all eligible items, not just those promoted to the home page');
        $i->seeElement('input[id*="show-all-items"][value="1"]');
        break;
    }
  }

  /**
   * Function to test the manual block config.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   * @param string $machine_name
   *   The machine name.
   * @param string $content_type
   *   The content type.
   */
  private function testManualBlockContentTypeConfig(
    AcceptanceTester $i,
    string $machine_name,
    string $content_type,
  ) {

    // Add the Manual list block, without saving the block.
    $this->addListingBlockWithContentType(
      $i,
      'Manual list',
      $content_type,
      FALSE
    );

    // Ensure that the config is on the block based
    // on the content type.
    switch ($machine_name) {

      case 'event':
        $i->see('Event list settings');
        $i->see('Events');
        $i->see('Style');
        break;
    }
  }

  /**
   * Function to remove then add first section on a layout.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  private function removeAddFirstSection(AcceptanceTester $i) {

    $i->click(Locator::contains('a', 'Remove Section 1'));
    $i->waitForText('Are you sure you want to remove section 1?');
    $i->click('Remove');
    $i->waitForElementNotVisible('.layout-builder__region');
    $i->click('.layout-builder__link--add');
    $i->waitForText('One column');
    $i->click('One column');
    $i->waitForText('Configure section');
    $i->waitForText('Add section');
    $i->click('input[value="Add section"]');
    $i->waitForText('Add block');
  }

  /**
   * Function to add an automatic listing block with content type to the page.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   * @param string $block_type
   *   The block type.
   * @param string $content_type
   *   The content type.
   * @param bool $save_block
   *   Flag to save the block.
   * @param int $limit
   *   The number of items (limit).
   */
  private function addListingBlockWithContentType(
    AcceptanceTester $i,
    string $block_type,
    string $content_type,
    bool $save_block = TRUE,
    int $limit = 3,
  ) {

    $i->click('Add block');
    $i->waitForText('Choose a block');
    $i->click($block_type);
    $i->waitForText($block_type);
    $i->waitForElement('.js-form-item-settings-content-type');
    $i->selectOption('settings[content_type]', $content_type);
    if ($block_type == 'Automatic list'
        && $content_type != 'services'
        && $block_type != 'catalogs') {
      $i->selectOption('settings[' . $content_type . '][limit]', $limit);
    }
    if ($save_block) {
      $i->click('Add block');
      $i->waitForText($block_type);
    }
  }

  /**
   * Function to get upcoming Waterloo events urls.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   * @param int $num
   *   Number of events to return.
   *
   * @return string[]
   *   Array of Waterloo Events urls.
   */
  private function getWaterlooEvents(AcceptanceTester $i, int $num) {

    // Initialize events array.
    $events = [];

    // Initiatlize current data variable.
    $date = date('Y-m-d');

    // Add Event urls to array up until desired amount.
    while (count($events) < $num) {

      // Send api request and decode response.
      $i->sendGet('https://uwaterloo.ca/events/api/v3.0/event?start_date=' . $date);
      $data = json_decode($i->seeResponse(), TRUE);

      // Add event paths from response to the array.
      foreach ($data['data'] as $event) {

        // Stop if desired number of events has been reached.
        if (count($events) >= $num) {
          continue;
        }
        $events[] = $event['path'];
      }

      // Increment the day.
      $date = date('Y-m-d', strtotime($date . ' +1 day'));
    }
    return $events;
  }

}
