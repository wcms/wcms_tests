<?php

/**
 * Class LayoutBuilderBrowserCest.
 *
 * Tests for layout builder browser.
 */
class WcmsTestsLayoutBuilderBrowserCest {

  /**
   * Tests for layout builder browser.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testLayoutBuilderBrowser(AcceptanceTester $i) {

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');

    // Searches, Teasers and Webforms, and blocks related.
    $items = [
      'tr[data-drupal-selector="edit-categories-category-uw-bc-content"]',
      'tr[data-drupal-selector="edit-categories-uw-cbl-banner-images"]',
      'tr[data-drupal-selector="edit-categories-inline-blockuw-cbl-blockquote"]',
      'tr[data-drupal-selector="edit-categories-inline-blockuw-cbl-call-to-action"]',
      'tr[data-drupal-selector="edit-categories-inline-blockuw-cbl-copy-text"]',
      'tr[data-drupal-selector="edit-categories-uw-lbb-expand-collapse"]',
      'tr[data-drupal-selector="edit-categories-inline-blockuw-cbl-facts-and-figures"]',
      'tr[data-drupal-selector="edit-categories-uw-lbb-image"]',
      'tr[data-drupal-selector="edit-categories-inline-blockuw-cbl-image-gallery"]',
      'tr[data-drupal-selector="edit-categories-inline-blockuw-cbl-mailman"]',
      'tr[data-drupal-selector="edit-categories-inline-blockuw-cbl-related-links"]',
      'tr[data-drupal-selector="edit-categories-uw-lbb-tags"]',
      'tr[data-drupal-selector="edit-categories-inline-blockuw-cbl-timeline"]',
      'tr[data-drupal-selector="edit-categories-category-uw-bc-external-embeds"]',
      'tr[data-drupal-selector="edit-categories-inline-blockuw-cbl-codepen"]',
      'tr[data-drupal-selector="edit-categories-uw-lbb-facebook"]',
      'tr[data-drupal-selector="edit-categories-inline-blockuw-cbl-google-maps"]',
      'tr[data-drupal-selector="edit-categories-inline-blockuw-cbl-instagram"]',
      'tr[data-drupal-selector="edit-categories-inline-blockuw-cbl-mailchimp"]',
      'tr[data-drupal-selector="edit-categories-inline-blockuw-cbl-powerbi"]',
      'tr[data-drupal-selector="edit-categories-inline-blockuw-cbl-remote-video"]',
      'tr[data-drupal-selector="edit-categories-inline-blockuw-cbl-tableau"]',
      'tr[data-drupal-selector="edit-categories-uw-lbb-twitter"]',
      'tr[data-drupal-selector="edit-categories-category-uw-bc-listings"]',
      'tr[data-drupal-selector="edit-categories-uw-lbb-automatic-list"]',
      'tr[data-drupal-selector="edit-categories-uw-lbb-manual-list"]',
      'tr[data-drupal-selector="edit-categories-uw-cbl-multi-type-list"]',
      'tr[data-drupal-selector="edit-categories-uw-lbb-waterloo-events"]',
      'tr[data-drupal-selector="edit-categories-waterloo-news"]',
      'tr[data-drupal-selector="edit-categories-category-uw-bc-searches"]',
      'tr[data-drupal-selector="edit-categories-uw-lbb-catalog-search"]',
      'tr[data-drupal-selector="edit-categories-uw-lbb-project-search"]',
      'tr[data-drupal-selector="edit-categories-uw-lbb-publication-search"]',
      'tr[data-drupal-selector="edit-categories-uw-lbb-services-search"]',
      'tr[data-drupal-selector="edit-categories-category-uw-bc-teasers"]',
      'tr[data-drupal-selector="edit-categories-uw-lbb-teaser"]',
      'tr[data-drupal-selector="edit-categories-category-uw-bc-webforms"]',
      'tr[data-drupal-selector="edit-categories-webform-block"]',
    ];

    // Go to the layout builder browser config page.
    $i->amOnPage('admin/config/content/layout-builder-browser');
    $i->see('Layout Builder Browser configuration');

    // Step through each item and ensure on the page.
    foreach ($items as $item) {
      $i->seeElement($item);
    }
  }

}
