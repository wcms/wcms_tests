<?php

use Codeception\Util\Locator;

/**
 * Class GlobalFooterCest.
 *
 * Tests for global footer.
 */
class WcmsTestsGlobalFooterCest {

  /**
   * Tests for global footer.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testGlobalFooter(AcceptanceTester $i) {

    // Go to the home page.
    $i->amOnPage('');
    $i->see('Home');

    // Find all 'News' links, we should have 2.
    $i->seeNumberOfElements(Locator::contains('span', 'News'), 2);

    // Test all the links.
    $i->seeElement(Locator::contains('span', 'Contact Waterloo'));
    $i->seeElement(Locator::contains('span', 'Maps & directions'));
    $i->seeElement(Locator::contains('span', 'Emergency notifications'));
    $i->seeElement(Locator::contains('span', 'Accessibility'));
    $i->seeElement(Locator::contains('span', 'Privacy'));
    $i->seeElement(Locator::contains('span', 'Copyright'));
    $i->seeElement(Locator::contains('span', 'News'));
    $i->seeElement(Locator::contains('span', 'Careers'));
    $i->seeElement(Locator::contains('span', 'Feedback'));

    // Test the social media.
    $i->seeElement('a[href="https://www.instagram.com/uofwaterloo/"]');
    $i->seeElement('a[href="https://twitter.com/UWaterloo"]');
    $i->seeElement('a[href="https://www.linkedin.com/school/uwaterloo/"]');
    $i->seeElement('a[href="https://www.facebook.com/university.waterloo"]');
    $i->seeElement('a[href="https://www.youtube.com/user/uwaterloo"]');
    $i->seeElement(Locator::contains('a[href="https://uwaterloo.ca/social-media/"]', '@uwaterloo social directory'));

    // Test Indigenous message.
    $i->see('The University of Waterloo acknowledges that much of our work takes place on the traditional territory of the Neutral, Anishinaabeg, and Haudenosaunee peoples. Our main campus is situated on the Haldimand Tract, the land granted to the Six Nations that includes six miles on each side of the Grand River. Our active work toward reconciliation takes place across our campuses through research, learning, teaching, and community building, and is co-ordinated within the Office of Indigenous Relations.');
  }

}
