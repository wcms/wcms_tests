<?php

use Drupal\uw_cfg_common\Service\UWLdap;

/**
 * Class UwLdapCest.
 *
 * Tests for uw ldap.
 */
class WcmsTestsUwLdapCest {

  /**
   * Tests for uw ldap.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testUwLdap(AcceptanceTester $i) {

    // Test LDAP view page.
    $test_path = 'uw-ldap/wcmsadmi';

    // Go to the ldap page.
    $i->amOnPage($test_path);
    $i->see('You are not authorized to access this page.');

    // Login as administrator.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');

    // Go to the ldap page and ensure that it loads.
    $i->amOnPage($test_path);
    $i->see('LDAP View: wcmsadmi');

    // Create a new authenticated account.
    $i->amOnPage('/user/logout');
    $i->logInWithRole('authenticated');

    // The config factor service.
    $config_factory = \Drupal::service('config.factory');

    // Get the current user.
    $current_user = \Drupal::currentUser();

    // Create a new ldap entry.
    $ldap = new UWLdap($config_factory, $current_user);

    // Test formatResults().
    $test_in = ['objectguid' => ['ABCDEFGHIJKLMNOP']];
    $test_in = $ldap->formatResults($test_in);
    $test_out = ['objectguid' => ['44434241-4645-4847-494A-4B4C4D4E4F50']];
    $i->assertEquals($test_in, $test_out, 'formatResults() gives correct results.');
  }

}
