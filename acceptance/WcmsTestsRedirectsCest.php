<?php

use Drupal\redirect\Entity\Redirect;
use Drupal\user\Entity\Role;

/**
 * Class RedirectsCest.
 *
 * Tests for redirects.
 */
class WcmsTestsRedirectsCest {

  /**
   * The web page object.
   *
   * @var array
   */
  private $webpages;

  /**
   * Tests for redirects.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testRedirectsPermissions(AcceptanceTester $i) {

    // Load all roles and check 'administer redirects' permissions.
    // Only site manager should have it. This approach can be used to test
    // all permissions, so there is no need to load permissions page which
    // can cause timeouts.
    foreach (Role::loadMultiple() as $test_role) {

      // Bypass tests for administrator.
      if ($test_role->id() === 'administrator') {
        continue;
      }

      if ($test_role->id() === 'uw_role_site_manager') {
        $i->assertTrue(
          $test_role->hasPermission('administer redirects'),
          'Role: ' . $test_role->label() . ' is missing redirect permission.'
        );
      }
      else {
        $i->assertFalse(
          $test_role->hasPermission('administer redirects'),
          'Role: ' . $test_role->label() . ' has redirect permission.',
        );
      }
    }
  }

  /**
   * Function to test the redirects settings.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testRedirectsSettings(AcceptanceTester $i) {

    // Login as administrator.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');

    // Check admin/config/search/redirect/settings.
    $i->amOnPage('admin/config/search/redirect/settings');
    $i->see('Settings');
    $i->dontSeeCheckboxIsChecked('#edit-redirect-auto-redirect');
    $i->seeCheckboxIsChecked('#edit-redirect-passthrough-querystring');
    $i->seeOptionIsSelected('#edit-redirect-default-status-code', '301 Moved Permanently');
    $i->seeCheckboxIsChecked('#edit-redirect-route-normalizer-enabled');
    $i->dontSeeCheckboxIsChecked('#edit-redirect-ignore-admin-path');
    $i->dontSeeCheckboxIsChecked('#edit-redirect-access-check');
    $i->seeOptionIsSelected('#edit-row-limit', '10000');
    $i->seeElement('#edit-ignore-pages');
    $i->dontSeeCheckboxIsChecked('#edit-suppress-404');
  }

  /**
   * Function to test redirects.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testRedirectsUsage(AcceptanceTester $i) {

    // Login as administrator.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');

    // Webpages to create.
    $webpages = [
      'webpage1',
      'webpage2',
      'webpage3',
      'webpage4',
      'webpage5',
      'webpage6',
    ];

    // Step through each webpage and create it.
    foreach ($webpages as $webpage) {
      $this->webpages[] = $i->createWebPage($webpage);
    }

    // Test case 1: Internal node redirects to external url.
    // Test case 2: Internal node redirects to internal view page.
    // Test case 3: Internal node redirects to internal node.
    // Test case 4: Internal node redirects to internal term.
    // Test case 5: Internal term redirects to external url.
    // Test case 6: Internal term redirects to internal view page.
    // Test case 7: Internal term redirects to internal term.
    // Test case 8: Internal term redirects to internal node.
    $redirects = [
      'webpage1' => 'https://google.com',
      'webpage2' => '/blog',
      'webpage3' => '/webpage5',
      'webpage4' => '/audience/staff',
      'audience/faculty' => 'https://google.com',
      'audience/alumni' => '/events',
      'audience/parents' => '/audience/employers',
      'audience/international' => '/webpage6',
    ];

    foreach ($redirects as $source => $redirect) {

      // Add redirects for the above four cases.
      $i->amOnPage('admin/config/search/redirect/add');
      $i->see('Add URL redirect');

      // Fill in the fields.
      $i->fillField('redirect_source[0][path]', $source);
      $i->fillField('redirect_redirect[0][uri]', $redirect);

      // Due to the sources are existing valid path, so the first submission
      // will show a warning message. Then we need to save again.
      $i->click('Save');
      $i->see('The source path ' . $source . ' appears to be a valid path. It is preferred to create URL aliases for existing paths rather than redirects.');
      $i->click('Save');
      $i->see('The redirect has been saved.');

      // Load source page to see if the redirect is working.
      $i->amOnPage($source);
      if ($redirect == 'https://google.com') {
        $i->see('Google');
      }
      else {
        $i->assertEquals($redirect, $i->getCurrentUrl());
      }
    }
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _after(AcceptanceTester $i): void {
    // phpcs:enable

    // If we created webpages, delete them.
    if (!empty($this->webpages)) {
      foreach ($this->webpages as $webpage) {
        $webpage->delete();
      }
    }

    // Get all redirects.
    $redirects = Redirect::loadMultiple();

    // Step through the redirects and delete them.
    foreach ($redirects as $redirect) {
      $redirect->delete();
    }
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i): void {
    // phpcs:enable

    // If we created webpages, delete them.
    if (!empty($this->webpages)) {
      foreach ($this->webpages as $webpage) {
        $webpage->delete();
      }
    }

    // Get all redirects.
    $redirects = Redirect::loadMultiple();

    // Step through the redirects and delete them.
    foreach ($redirects as $redirect) {
      $redirect->delete();
    }
  }

}
