<?php

use Drupal\user\Entity\User;

/**
 * Class WcmsTestsUsersCest.
 *
 * Tests for all kinds of user things.
 */
class WcmsTestsUsersCest {

  /**
   * Function to test that certain roles can not create uwaterloo users.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   *
   * @throws Exception
   */
  public function testAddUwaterlooUser(AcceptanceTester $i) {

    // Login as site owner.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_owner');

    // Go to the people page.
    $i->amOnPage('admin/people');

    // Check if add uwaterloo users button on the page.
    $i->see('Add UWaterloo user(s)');

    // Click the add uwaterloo users button.
    $i->click('Add UWaterloo user(s)');

    // Ensure that text about uwaterloo users is on the page.
    $i->see('Add UWaterloo user(s)');
    $i->see('WatIAM user ID(s): (maximum 8 characters per ID)');

    // Ensure that the users textarea is on the page.
    $i->seeElement('#edit-users');

    // Click the submit button.
    $i->click('Create new account(s)');

    // Ensure that error message about required field displays.
    $i->see('WatIAM user ID(s): (maximum 8 characters per ID) field is required.');

    // Fill the users field with at least one user.
    $i->fillField('#edit-users', 'testid');

    // Click the submit button.
    $i->click('Create new account(s)');

    // Ensure that the success message appears.
    $i->see('Finished creating users.');
  }

  /**
   * Function to test that blocks are present.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   *
   * @throws Exception
   */
  public function testCreateUwaterlooUserByRole(AcceptanceTester $i) {

    // Roles that don't have access to user creation.
    $roles = [
      'authenticated',
      'uw_role_site_manager',
      'uw_role_content_editor',
      'uw_role_content_author',
      'uw_role_form_editor',
      'uw_role_form_results_access',
    ];

    // Step through each role and test that there is no access.
    foreach ($roles as $role) {

      // Login with the role.
      $i->amOnPage('user/logout');
      $i->logInWithRole($role);

      // Go to the add uwaterloo users page.
      $i->amOnPage('admin/add-uwaterloo-users');

      // Ensure that there is no access.
      $i->see('Access denied');
      $i->see('You are not authorized to access this page.');
    }
  }

  // phpcs:disable

  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _after(AcceptanceTester $i): void {
    // phpcs:enable

    // Load the user.
    $user = User::load('testid');

    // If there is a user, delete it.
    if ($user) {
      $user->delete();
    }
  }

  // phpcs:disable

  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i): void {
    // phpcs:enable

    // Load the user.
    $user = User::load('testid');

    // If there is a user, delete it.
    if ($user) {
      $user->delete();
    }
  }

}
