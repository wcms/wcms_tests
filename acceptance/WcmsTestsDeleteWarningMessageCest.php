<?php

/**
 * Class DeleteWarningMessageCest.
 *
 * Tests for delete warning message.
 */
class WcmsTestsDeleteWarningMessageCest {

  /**
   * Array used for any nodes that we created.
   *
   * @var array
   */
  private $nodesUsed = [];

  /**
   * Array used for any tids that we created.
   *
   * @var array
   */
  private $tidsUsed = [];

  /**
   * Tests for delete warning message.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testDeleteWarningMessage(AcceptanceTester $i) {

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // The message we are looking for.
    $message = 'CAUTION. This will permanently delete this piece of content; this action cannot be undone. If anything references this content, it may cause visual or structural issues on that page. Make sure you have removed or updated all references before deleting.';

    // The content types to test.
    $content_types = [
      'uw_ct_blog' => 'Blog',
      'uw_ct_catalog_item' => 'Catalog',
      'uw_ct_contact' => 'Contact',
      'uw_ct_news_item' => 'News',
      'uw_ct_event' => 'Event',
      'uw_ct_profile' => 'Profile',
      'uw_ct_project' => 'Project',
      'uw_ct_service' => 'Service',
      'uw_ct_site_footer' => 'Site footer',
      'uw_ct_web_page' => 'Web page',
    ];

    // Check delete node message for the given content type.
    foreach ($content_types as $content_type => $name) {
      $this->nodesUsed[$content_type] = $i->createCtNode($content_type, $name, TRUE);
      $i->amOnPage('node/' . $this->nodesUsed[$content_type]->id() . '/delete');
      $i->see('Are you sure you want to delete the content item ' . $this->nodesUsed[$content_type]->getTitle());
      $i->see($message);
    }

    // The vocabs to be tested.
    $vocabs = [
      'uw_vocab_blog_tags' => 'Blog tag',
      'uw_vocab_catalog_categories' => 'Catalog category',
      'uw_vocab_catalogs' => 'Catalog',
      'uw_vocab_contact_group' => 'Contact group',
      'uw_tax_event_tags' => 'Event tag',
      'uw_tax_event_type' => 'Event type',
      'uw_vocab_news_tags' => 'News tag',
      'uw_vocab_profile_type' => 'Profile type',
      'uw_vocab_service_categories' => 'Service category',
    ];

    // Check delete term message for the given vocab.
    foreach ($vocabs as $vocab => $name) {
      $this->tidsUsed[$vocab] = $i->createTerm($vocab, $name);
      $i->amOnPage('taxonomy/term/' . $this->tidsUsed[$vocab] . '/delete');
      $i->see('Are you sure you want to delete the taxonomy term');
      $i->see($message);
    }

  }

  /**
   * Tests for prevent deleting catalog being used.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testPreventDeletingCatalogBeingUsed(AcceptanceTester $i) {
    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');

    // Create a catalog term.
    $catalog_term = $i->createTerm('uw_vocab_catalogs', 'Animal');

    // Put tid in an array which can be removed after testing.
    $this->tidsUsed[] = $catalog_term;

    // Go to add a catalog item node using the above catalog term.
    $i->amOnPage('node/add/uw_ct_catalog_item');
    $i->waitForText('Create Catalog item');
    $title = $i->uwRandomString();
    $i->fillField('title[0][value]', $title);
    $i->click('label[for="edit-field-uw-catalog-catalog-' . $catalog_term . '"]');
    $i->fillField('field_uw_meta_description[0][value]', $i->uwRandomString());
    $i->selectOption('moderation_state[0][state]', 'published');
    $i->click('Save');
    $i->see('Catalog item ' . $title . ' has been created.');

    // Try to delete the catalog term being used.
    $i->amOnPage('taxonomy/term/' . $catalog_term . '/delete');
    $i->see('You cannot delete this catalog as it is in use. Delete or reassign any catalog items assigned to this catalog in order to be able to remove it.');
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _passed(AcceptanceTester $i) {

    // If we used any terms, delete them.
    if (!empty($this->tidsUsed)) {
      $controller = \Drupal::entityTypeManager()
        ->getStorage('taxonomy_term');
      $entities = $controller->loadMultiple($this->tidsUsed);
      $controller->delete($entities);
    }

    // Delete the nodes we created.
    if (!empty($this->nodesUsed)) {
      foreach ($this->nodesUsed as $node) {
        $node->delete();
      }
    }
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i) {
    // phpcs:enable

    // If we used any terms, delete them.
    if (!empty($this->tidsUsed)) {
      $controller = \Drupal::entityTypeManager()
        ->getStorage('taxonomy_term');
      $entities = $controller->loadMultiple($this->tidsUsed);
      $controller->delete($entities);
    }

    // Delete the nodes we created.
    if (!empty($this->nodesUsed)) {
      foreach ($this->nodesUsed as $node) {
        $node->delete();
      }
    }
  }

}
