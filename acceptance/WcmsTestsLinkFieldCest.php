<?php

use Step\Acceptance\ContentType;

/**
 * Class WcmsTestsLinkFieldCest.
 *
 * Tests for link field restrictions.
 */
class WcmsTestsLinkFieldCest {

  /**
   * Roles to test.
   *
   * @var array
   */
  private $roles = [
    'uw_role_site_manager',
    'uw_role_content_author',
    'uw_role_content_editor',
  ];

  /**
   * Link texts.
   *
   * @var array
   */
  private $linkTexts = [
    '<button>',
    '<nolink>',
  ];

  /**
   * Array of nodes used.
   *
   * @var array
   */
  private array $nodesUsed = [];

  /**
   * Tests that <nolink> and <button> are not accepted where they shouldn't be.
   *
   * @param Step\Acceptance\ContentType $i
   *   Content Type test variable.
   */
  public function testLinkField(ContentType $i) {

    // Login as administrator.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');

    // Check linkit default matchers.
    $i->amOnPage('admin/config/content/linkit/manage/default/matchers');
    $matchers = [
      'Disallowed URLs: https://staging.uwaterloo.ca https://pilots.uwaterloo.ca',
      'Error message: Linking to a staging/pilots site is not allowed as these are temporary sites not intended for public use. For links within a site, please use an internal path.',
    ];
    foreach ($matchers as $matcher) {
      $i->see($matcher);
    }

    // Check linkit add matchers.
    $i->amOnPage('admin/config/content/linkit/manage/default/matchers/add');
    $i->checkOption('#edit-plugin-prevent-url');
    $i->click('Save and continue');

    // Check default 'Disallowed URLs' form.
    $disallowed_urls = [
      'prevent-urls' => [
        'label' => 'Disallow linking to these URLs',
        'help text' => 'Separate multiple URLs with a space. Linking will be disallowed for any URLs that start with these values.',
      ],
      'error-message' => [
        'label' => 'Disallow linking to these URLs',
        'help text' => "Include '@url' to show the URL entered by the user.",
      ],
    ];

    // Step through each of the disallowed and ensure that
    // they are on the page.
    foreach ($disallowed_urls as $field) {
      foreach ($field as $item) {
        $i->see($item);
      }
    }

    // Ensure that the linking to thr url is not allowed.
    $i->seeElement('input[id^="edit-error-message"][value="Linking to the URL @url is not allowed."]');

    // Fill the error message field as blank, click on the
    // save changes and ensure that the url field is required.
    $i->fillField('#edit-error-message', '');
    $i->click('Save changes');
    $i->see('Disallow linking to these URLs field is required.');
    $i->see('Error message field is required.');

    // Fill the prevent urls and error message, click on the
    // save change and check that there is an error message
    // about valid url.
    $i->fillField('#edit-prevent-urls', $i->uwRandomString());
    $i->fillField('#edit-error-message', $i->uwRandomString());
    $i->click('Save changes');
    $i->see('1 error has been found');
    $i->see('Please enter a valid URL.');

    // The content types to test.
    $ct_to_test = [
      'uw_ct_blog',
      'uw_ct_profile',
      'uw_ct_event',
      'uw_ct_opportunity',
      'uw_ct_project',
    ];

    // Get all the content types.
    $content_types = $i->getContentTypes();

    // Step through each of the content types and
    // test the links.
    foreach ($content_types as $content_type) {

      // If the content type is in the list of content types
      // to test then actually test it.
      if (in_array($content_type, $ct_to_test)) {

        // Get the edits for the content type.
        $edits = $i->getContentTypeEdits($content_type);

        // Step through each of the roles and test with
        // that role.
        foreach ($this->roles as $role) {

          // Step through the different link types and test them.
          foreach ($this->linkTexts as $link_text) {

            // Based on the content type add fields to the edits.
            switch ($content_type) {

              case 'uw_ct_blog':
                $edits['field']['edit-field-author-0-value'] = $i->uwRandomString();
                $edits['field']['edit-field-uw-author-link-0-uri'] = $link_text;
                break;

              case 'uw_ct_profile':
                $edits['field']['edit-field-uw-ct-profile-info-link-0-uri'] = $link_text;
                $edits['field']['edit-field-uw-ct-profile-info-link-0-title'] = $i->uwRandomString();
                $edits['field']['edit-field-uw-ct-profile-link-persona-0-uri'] = $link_text;
                $edits['field']['edit-field-uw-ct-profile-link-contact-0-uri'] = $link_text;
                break;

              case 'uw_ct_event':
                $edits['field']['edit-field-uw-event-website-0-uri'] = $link_text;
                break;

              case 'uw_ct_opportunity':
                $edits['field']['edit-field-uw-opportunity-link-0-uri'] = $link_text;
                $edits['field']['edit-field-uw-opportunity-additional-0-uri'] = $link_text;
                $edits['field']['edit-field-uw-opportunity-contact-0-uri'] = $link_text;
                break;

              case 'uw_ct_project':
                $edits['field']['edit-field-uw-project-members-0-subform-field-uw-project-members-link-0-uri'] = $link_text;
                break;
            }

            // Test the content type, using the extra fields from above.
            $i->testContentTypeEdits(
              $content_type,
              $edits,
              FALSE,
              $role
            );

            // Click on the submit button.
            $i->click('#edit-submit');

            // Ensure that the type of link is not supported.
            $i->see('The ' . $link_text . ' value is not supported');
          }
        }
      }
    }

    // Step through each of the roles and test some of the extra
    // content types.
    foreach ($this->roles as $role) {

      // Create an event  ode with the host as nolink.
      $edits = $i->getContentTypeEdits('uw_ct_event');
      $edits['field']['edit-field-uw-event-host-0-uri'] = '<nolink>';
      $i->testContentTypeEdits(
        'uw_ct_event',
        $edits,
        FALSE,
        $role
      );

      // Click on the submit button.
      $i->click('#edit-submit');

      // Blank link text when using <nolink>, show error message.
      $i->see('You must provide link text when using <nolink> as the URL.');

      // Get a title and set it in the edits.
      $event_title = $i->uwRandomString();

      // Create a new event node.
      $this->nodesUsed[$event_title] = $i->createCtNode(
        'uw_ct_event',
        $event_title,
        FALSE,
      );

      // Get the path to the newly created event node.
      $path = $i->getWebPagePath($this->nodesUsed[$event_title]);

      // Click on the submit button.
      $i->click('#edit-submit');

      // Go to the node edit page.
      $i->amOnPage($path . '/edit');

      // Get the expected text.
      $expected_text = $i->uwRandomString();

      // Fill in the host fields and click save.
      $i->fillField('#edit-field-uw-event-host-0-uri', '<nolink>');
      $i->fillField('#edit-field-uw-event-host-0-title', $expected_text);
      $i->click('Save');

      // Go to the layout page.
      $i->amOnPage($path . '/layout');
      $i->see('You are editing the layout for this Event content item.');

      // Ensure that the nolink message appears.
      $i->dontSee('The <nolink> value is not supported');

      // Ensure that there is the host and expected text on the page.
      // Also, that you don't see the expected text link.
      $i->see('Host:');
      $i->see($expected_text);
      $i->dontSeeLink($expected_text);
    }
  }

  /**
   * Test that appropriate content block errors appear.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testContentBlocks(AcceptanceTester $i) {

    // Create a webpage.
    $this->nodesUsed['Cta block'] = $i->createWebPage('Cta block', TRUE);

    // Get the path of the webpage.
    $path = $i->getWebPagePath($this->nodesUsed['Cta block']);

    // Step through each of the roles and try the blocks.
    foreach ($this->roles as $role) {

      // Login with the role.
      $i->amOnPage('user/logout');
      $i->logInWithRole($role);

      // Get todays date.
      $date = date('m/d/Y');

      // Step through each link texts and fill the field.
      foreach ($this->linkTexts as $text) {

        // Set the block types and fields.
        $block_types = [
          'Call to action' => [
            'Fields' => [
              'settings[label]' => $i->uwRandomString(),
              'settings[block_form][field_uw_cta_details][0][subform][field_uw_cta_link][0][uri]' => $text,
              'settings[block_form][field_uw_cta_details][0][subform][field_uw_cta_text_details][0][text_value]' => $i->uwRandomString(),
            ],
          ],
          'Related links' => [
            'Fields' => [
              'settings[label]' => $i->uwRandomString(),
              'settings[block_form][field_uw_rl_related_link][0][uri]' => $text,
              'settings[block_form][field_uw_rl_related_link][0][title]' => $i->uwRandomString(),
            ],
          ],
          'Timeline' => [
            'Fields' => [
              'settings[label]' => $i->uwRandomString(),
              'settings[block_form][field_uw_timeline][0][subform][field_uw_timeline_date][0][value][date]' => $date,
              'settings[block_form][field_uw_timeline][0][subform][field_uw_timeline_headline][0][value]' => $i->uwRandomString(),
              'settings[block_form][field_uw_timeline][0][subform][field_uw_timeline_link][0][uri]' => $text,
            ],
            'Options' => [
              'settings[block_form][field_uw_timeline_style]' => 'vertical-month',
              'settings[block_form][field_uw_timeline_sort]' => 'asc',
            ],
          ],
        ];

        // Step through each of the block types and check that
        // the appropriate error message appears.
        foreach ($block_types as $block => $fields) {

          // Go to the layout page for the webpage we created.
          $i->amOnPage($path . '/layout');
          $i->see('Edit layout for ' . $this->nodesUsed['Cta block']->getTitle());

          // Click the add block.
          $i->click('Add block');
          $i->waitForText($block);
          $i->click($block);
          $i->waitForText('Configure block');

          // Fill the block fields.
          $this->fillBlockFields($i, $fields, 'Add block');

          // Ensure that proper message appears.
          $i->waitForText('The ' . $text . ' value is not supported');
          $i->see('The ' . $text . ' value is not supported');
        }
      }
    }
  }

  /**
   * Fill in and submit form.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   * @param array $edit
   *   Array of information.
   */
  private function fillBlockFields(AcceptanceTester $i, array $edit) {

    // Step through each of the block edits and fill them.
    foreach ($edit as $form_type => $information) {

      // Step through each type of fields and fill it.
      foreach ($information as $id => $value) {

        // Fill in fields.
        if ($form_type == 'Fields') {
          // However fields when adding a block must be found by name.
          $i->fillField($id, $value);
        }

        // Fill in CK Editors.
        if ($form_type == 'CkEditors') {
          $i->fillCkEditor($value, $id);
        }

        // Select options.
        if ($form_type == 'Options') {
          $i->selectOption($id, $value);
        }
      }
    }

    // Click the add block button.
    $i->click('Add block');
  }

  // phpcs:disable

  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _after(AcceptanceTester $i): void {
    // phpcs:enable

    // Step through each of the nodes used and delete them.
    foreach ($this->nodesUsed as $node) {
      $node->delete();
    }

    // Delete all the blocks.
    $i->deleteAllBlocks();

  }

  // phpcs:disable

  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i): void {
    // phpcs:enable

    // Step through each of the nodes used and delete them.
    foreach ($this->nodesUsed as $node) {
      $node->delete();
    }

    // Delete all the blocks.
    $i->deleteAllBlocks();
  }

}
