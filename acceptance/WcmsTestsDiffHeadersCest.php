<?php

use Drupal\node\Entity\Node;

/**
 * Class DiffHeadersCest.
 *
 * Tests for diff headers.
 */
class WcmsTestsDiffHeadersCest {

  /**
   * The web page object.
   *
   * @var Drupal\node\Entity\Node
   */
  private $webpage;

  /**
   * The title of the webpage.
   *
   * @var string
   */
  private $webPageTitle;

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _before(AcceptanceTester $i) {
    // phpcs:enable

    // Get a title for the web page.
    $this->webPageTitle = $i->uwRandomString();

    // Create the web page.
    $this->webpage = Node::create([
      'type'        => 'uw_ct_web_page',
      'title'       => $this->webPageTitle,
      'field_uw_meta_description' => $this->webPageTitle,
    ]);
    $this->webpage->save();
  }

  /**
   * Tests for diff headers.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function diffHeadersTest(AcceptanceTester $i) {

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Get the path to the web page.
    $path = \Drupal::service('path_alias.manager')
      ->getAliasByPath('/node/' . $this->webpage->id());

    // Go to the web page edit page.
    $i->amOnPage($path . '/edit');

    // Set the new web page title.
    $this->webPageTitle = $i->uwRandomString();
    $i->fillField('title[0][value]', $this->webPageTitle);
    $i->fillField('field_uw_meta_description[0][value]', $this->webPageTitle);
    $i->selectOption('moderation_state[0][state]', 'published');

    // Click on the save web page.
    $i->click('Save');

    // Ensure that the page has been saved.
    $i->waitForText('Web page ' . $this->webPageTitle . ' has been updated.');

    // Get the path to the web page.
    $path = \Drupal::service('path_alias.manager')
      ->getAliasByPath('/node/' . $this->webpage->id());

    // Wait for revisions link to be visible and click it.
    $i->waitForText('Revisions');
    $i->click('Revisions');

    // Ensure that we see the diff revisions.
    $i->see('SELECT REVISION A');
    $i->see('SELECT REVISION B');
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _passed(AcceptanceTester $i) {

    // Delete the webpage we created.
    $this->webpage->delete();
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i) {
    // phpcs:enable

    // Delete the webpage we created.
    $this->webpage->delete();
  }

}
