<?php

use Codeception\Util\Locator;

/**
 * Class DashboardCest.
 *
 * Tests for dashboard.
 */
class WcmsTestsDashboardCest {

  /**
   * Tests for dashboard access.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testAccess(AcceptanceTester $i) {

    // Roles that do not have access to layout.
    $layout_link_no_access_roles = [
      'uw_role_site_owner',
      'uw_role_form_editor',
      'uw_role_form_results_access',
    ];

    // The roles to test.
    $roles = [
      'anonymous',
      'authenticated',
      // Disable testing for these roles. In testing only for these roles, the
      // request to dashboard/my_dashboard returns 404; it behaves properly
      // otherwise. We have not been able to figure out why. This started
      // happening with the creation of UWDropbutton in uw_cfg_common. If
      // UWDropbutton does not have an access check, then everything works
      // correctly.
      // @code
      // 'uw_role_site_owner',
      // 'uw_role_site_manager',
      // 'uw_role_content_editor',
      // 'uw_role_content_author',
      // 'uw_role_form_editor',
      // 'uw_role_form_results_access',
      // @endcode
      'administrator',
    ];

    // Step through each of the roles and test access.
    foreach ($roles as $role) {

      // Logout current user.
      $i->amOnPage('user/logout');

      // If not anonymous, login as that role.
      if ($role !== 'anonymous') {
        $i->logInWithRole($role);
      }

      // Go to the dashboard page.
      $i->amOnPage('dashboard/my_dashboard');

      // If not authenticate ensure access. If authenticated,
      // ensure no access.
      if ($role !== 'authenticated' && $role !== 'anonymous') {
        $i->see('My Dashboard');
        $i->see("Who's online");

        // If admin or site owner, ensure that can see people
        // and reports.  If other role ensure that they do not
        // see the people and reports.
        if ($role == 'administrator' || $role == 'uw_role_site_owner') {
          $i->seeElementInDOM(Locator::contains('ul[class="toolbar-menu"] li a[href="/dashboard/my_dashboard"]', 'My Dashboard'));
          $i->seeElementInDOM(Locator::contains('ul[class="toolbar-menu"] li a[href="/admin/people"]', 'People'));
          $i->seeElementInDOM(Locator::contains('ul[class="toolbar-menu"] li a[href="/admin/reports"]', 'Reports'));
        }
        else {
          $i->dontSeeElementInDOM(Locator::contains('ul[class="toolbar-menu"] li a', 'People'));
          $i->dontSeeElementInDOM(Locator::contains('ul[class="toolbar-menu"] li a', 'Reports'));
        }

        // Check for access on the layout link.
        if (in_array($role, $layout_link_no_access_roles)) {
          $i->seeLink('ul[class="dropbutton"] li a[href="/home/layout"]');
        }
        else {
          $i->dontSeeLink('ul[class="dropbutton"] li a[href="/home/layout"]');
        }
      }
      else {
        $i->see('You are not authorized to access this page.');
      }
    }
  }

  /**
   * Tests for dashboard elements.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testElements(AcceptanceTester $i) {

    // Login with site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Go to the dashboard page.
    $i->amOnPage('dashboard/my_dashboard');
    $i->see('My Dashboard');

    // Test for site footer.
    $i->seeLink('Add/edit');
    $i->seeElement('a[href="/node/add/uw_ct_site_footer"]');

    // Get the default sections.
    $default_sections = $this->getDefaultSections();

    // Step through each of the default sections.
    foreach ($default_sections as $block => $classes) {
      $class = explode(' ', $classes);
      foreach ($class as $c) {
        $i->seeElement('section[class*="' . $c . '"] ' . $block);
      }
    }

    // Get the non-default blocks.
    $non_default_blocks = $this->getNonDefaultBlocks();

    // Adds non-default blocks.
    $this->addDashboardBlocks($non_default_blocks, $i);

    // Get the non-default sections.
    $non_default_sections = $this->getNonDefaultSections();

    // Test elements of each non-default section.
    foreach ($non_default_sections as $block => $elements) {
      // Check the block was successfully added.
      $i->seeElement($block);

      // Check block elements.
      foreach ($elements as $label => $field) {
        // Check label for non-buttons.
        $i->see($label);

        // Check field element.
        $i->seeElement($block . ' ' . $field);
      }
    }
  }

  /**
   * Tests for layout builder.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testLayoutBuilder(AcceptanceTester $i) {

    // Login with site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_owner');

    // Go to the dashboard page.
    $i->amOnPage('dashboard/my_dashboard');
    $i->see('My Dashboard');

    // Check that 'Override' button exists and click it.
    $i->seeLink('Personalize');
    $i->click('Personalize');

    // Check that 'Add block' link exists and click it.
    $i->waitForElementClickable('.layout-builder__link--add');
    $i->click('Add block');
    $i->waitForText('Choose a block');

    // Check that UW Custom Blocks panel appears on page.
    $i->seeElement(Locator::contains('summary[class="seven-details__summary"] span', 'UW Custom Blocks'));

    // Since this is always changing, check that only some custom blocks appear.
    $i->seeLink('Catalog search');
    $i->seeLink('Multi-type list');
    $i->seeLink('Project search');

    // Check the three bibcite blocks in UW Dashboard Items panel.
    $i->seeLink('Publication reference authors');
    $i->seeLink('Publication reference keywords');
    $i->seeLink('Publication references');

    // Following blocks should not be available on dashboard.
    $i->dontSeeLink('Special alert');
    $i->dontSeeLink('Blog author');
    $i->dontSeeLink('UW content moderation');
  }

  /**
   * Tests for cancel button of taxonomy terms.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testTaxonomyTermCancelButton(AcceptanceTester $i) {

    // Login with site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');

    // Taxonomy terms.
    $terms = [
      'uw_vocab_audience',
      'uw_vocab_blog_tags',
      'uw_vocab_catalog_categories',
      'uw_vocab_contact_group',
      'uw_tax_event_tags',
      'uw_tax_event_type',
      'uw_vocab_news_tags',
      'uw_vocab_opportunity_employment',
      'uw_vocab_opportunity_payrate',
      'uw_vocab_opportunity_type',
      'uw_vocab_profile_type',
      'uw_vocab_project_role',
      'uw_vocab_project_status',
      'uw_vocab_project_topic',
      'uw_vocab_service_categories',
    ];

    // Loop adding terms.
    foreach ($terms as $term) {
      // Case 1: Click cancel button after landing the page.
      $i->amOnPage('admin/structure/taxonomy/manage/' . $term . '/add');
      $i->see('Add term');
      $i->click('Cancel');
      $i->see('My Dashboard');

      // Case 2: Click cancel button after creating a term.
      $i->amOnPage('admin/structure/taxonomy/manage/' . $term . '/add');
      $i->see('Add term');
      $i->fillField('#edit-name-0-value', $i->uwRandomString());
      $i->click('Save');
      $i->waitForText('Created new term');
      $i->click('Cancel');
      $i->see('My Dashboard');
    }
  }

  /**
   * Adds dashboard blocks through site actions.
   *
   * @param array $blocks
   *   List of blocks to be added.
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  private function addDashboardBlocks(array $blocks, AcceptanceTester $i) {
    // Navigates to override page.
    $i->amOnPage('dashboard/my_dashboard/override');

    foreach ($blocks as $block => $class) {
      // Clicks 'Add block' button.
      $i->waitForElementClickable('.layout-builder__link--add');
      $i->click('Add block');
      $i->waitForText('Choose a block');

      // Selects the block to be added.
      $i->click($block);
      $i->waitForText('Configure block');

      // Adds the block.
      $i->click('input[value="Add block"]');
      $i->waitForElement($class);
    }

    // Saves the dashboard layout.
    $i->click('Save layout');

  }

  /**
   * Function to get ids and classes of default sections.
   *
   * @return array
   *   Array of ids and classes for default sections.
   */
  private function getDefaultSections(): array {

    return [
      '#web-resources-news' => 'uw-full-width layout layout--uw-3-col even-split',
      '#recent-content' => 'uw-full-width layout layout--uw-3-col even-split',
      '#whos-online' => 'uw-full-width layout layout--uw-3-col even-split',
      '#content-list' => 'uw-full-width layout layout--uw-2-col larger-left',
      '#forms-list' => 'uw-full-width layout layout--uw-2-col larger-left',
      // Publication references block has no id, so searching by class instead.
      '.block-uw-publication-reference-block' => 'uw-full-width layout layout--uw-2-col larger-left',
      '#content-management-menu' => 'uw-full-width layout layout--uw-2-col larger-left',
      '#site-management-menu' => 'uw-full-width layout layout--uw-2-col larger-left',
    ];
  }

  /**
   * Function to get ids/classes and elements of non-default sections.
   *
   * @return array
   *   Array of ids/classes and elements for non-default sections.
   */
  private function getNonDefaultSections(): array {
    // Currently only returns publication reference dashboard blocks.
    return [
      '.block-uw-publication-authors-block' =>
      [
        'Name' => '#edit-combine',
        'Sort by' => '#edit-sort-by',
        'Order' => '#edit-sort-order',
        'Filter' => '#edit-submit-uw-view-pub-authors',
      ],
      '.block-uw-publication-keywords-block' =>
      [
        'Name' => '#edit-name',
        'Filter' => '#edit-submit-uw-view-pub-keywords',
      ],
    ];
  }

  /**
   * Returns names and classes of blocks not on the default dashboard page.
   *
   * @return array
   *   Array of names and classes of blocks not on the default dashboard page.
   */
  private function getNonDefaultBlocks(): array {
    // Currently only returns publication reference dashboard blocks.
    return [
      'Publication reference authors' => '.block-uw-publication-authors-block',
      'Publication reference keywords' => '.block-uw-publication-keywords-block',
    ];
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _after(AcceptanceTester $i): void {
    // phpcs:enable

    // Delete all the blocks.
    $i->deleteAllBlocks();
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i): void {
    // phpcs:enable

    // Delete all the blocks.
    $i->deleteAllBlocks();
  }

}
