<?php

use Step\Acceptance\ContentType;

/**
 * Class WcmsTestsContentTypeExpandCollapseCest.
 *
 * Tests for blogs.
 */
class WcmsTestsContentTypeExpandCollapseCest {

  /**
   * The content type.
   *
   * @var string
   */
  private string $contentType = 'uw_ct_expand_collapse_group';

  /**
   * Tests for expand collapse content type.
   *
   * @param Step\Acceptance\ContentType $i
   *   Acceptance test variable.
   */
  public function testExpandCollapseContentType(ContentType $i): void {

    // Test the fields in the content type.
    $i->testContentType(
      'Expand/collapse group',
      $i->getContentTypeFields($this->contentType)
    );

    // Test the content type edits.
    $i->testContentTypeEdits(
      $this->contentType,
      $i->getContentTypeEdits($this->contentType)
    );
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _after(AcceptanceTester $i): void {
    // phpcs:enable

    // Delete all the blocks.
    $i->deleteAllBlocks();
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i): void {
    // phpcs:enable

    // Delete all the blocks.
    $i->deleteAllBlocks();
  }

}
