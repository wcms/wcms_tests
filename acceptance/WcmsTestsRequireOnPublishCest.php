<?php

use Drupal\taxonomy\Entity\Term;

/**
 * Class RequireOnPublishCest.
 *
 * Tests for require on publish.
 */
class WcmsTestsRequireOnPublishCest {

  /**
   * Array used for any nodes that we created.
   *
   * @var array
   */
  private $nodesUsed = [];

  /**
   * Array used for any taxonomies that we created.
   *
   * @var array
   */
  private $tidsUsed = [];

  /**
   * Tests for require on publish.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testRequireOnPublish(AcceptanceTester $i) {

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // The content types to test.
    $content_types = [
      'uw_ct_blog',
      'uw_ct_catalog_item',
      'uw_ct_contact',
      'uw_ct_news_item',
      'uw_ct_event',
      'uw_ct_profile',
      'uw_ct_service',
      'uw_ct_web_page',
    ];

    // Create a node for the given content type.
    foreach ($content_types as $content_type) {

      // Reset the fields and term settings to null so that
      // they are only used when required.
      $fields = NULL;
      $term_settings = NULL;

      // Get term settings if required by content type.
      switch ($content_type) {

        case 'uw_ct_catalog_item':
          $term_settings = [
            'vocab' => 'uw_vocab_catalogs',
            'field_name' => 'catalog',
          ];
          break;

        case 'uw_ct_service':
          $term_settings = [
            'vocab' => 'uw_vocab_service_categories',
            'field_name' => 'service',
          ];
          break;
      }

      if (isset($term_settings)) {

        // Create a term to be used for the catalog.
        $this->tidsUsed[] = $i->createTerm(
          $term_settings['vocab'],
          $i->uwRandomString()
        );

        // Add the catalog term to the fields.
        $fields[$term_settings['field_name']] = end($this->tidsUsed);
      }

      // Get the title for the node.
      $title = $i->uwRandomString();

      // Crete a node based on the content type.
      $this->nodesUsed[$content_type] = $i->createCtNode(
        $content_type,
        $title,
        FALSE,
        $fields
      );

      // Get the path to the web page.
      $path = \Drupal::service('path_alias.manager')
        ->getAliasByPath('/node/' . $this->nodesUsed[$content_type]->id());

      $i->amOnPage($path . '/layout');

      $i->selectOption('#edit-moderation-state-0-state', 'Published');

      // Save the layout and test for require for publishing.
      $i->waitForElementClickable('#edit-actions #edit-submit');
      $i->click('#edit-actions #edit-submit');
      $i->see('is required when publishing.');
    }
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _passed(AcceptanceTester $i) {

    // Delete the nodes we created.
    if (!empty($this->nodesUsed)) {
      foreach ($this->nodesUsed as $node) {
        $node->delete();
      }
    }

    // If there are terms used, delete them.
    if (!empty($this->tidsUsed)) {
      foreach ($this->tidsUsed as $tid) {
        $term = Term::load($tid);
        $term->delete();
      }
    }
  }

  // phpcs:disable
  /**
   * Function to run if the tests fail.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i) {
    // phpcs:enable

    // Delete the nodes we created.
    if (!empty($this->nodesUsed)) {
      foreach ($this->nodesUsed as $node) {
        $node->delete();
      }
    }

    // If there are terms used, delete them.
    if (!empty($this->tidsUsed)) {
      foreach ($this->tidsUsed as $tid) {
        $term = Term::load($tid);
        $term->delete();
      }
    }
  }

}
