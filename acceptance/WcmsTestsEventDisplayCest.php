<?php

/**
 * Class EventDisplayCest.
 *
 * Tests for event display.
 */
class WcmsTestsEventDisplayCest {

  /**
   * Tests for event display.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testEventDisplay(AcceptanceTester $i) {

    // @todo Figure out how to test when JS not enabled all location fields.
    // Field values.
    $title = $i->uwRandomString();
    $host_title = $i->uwRandomString();
    $host_url = 'https://uwaterloo.ca/';
    $web_title = $i->uwRandomString();
    $web_url = 'https://google.ca';
    $cost = '$10.00';
    $lat = '43.470763';
    $long = '-80.543813';
    $map_url = 'https://uwaterloo.ca/map/';
    $description = $i->uwRandomString();
    $date = date('m/d/Y');
    $start_time = '10:00am';
    $end_time = '11:00am';
    $todays_date = date("F j, Y") . ' 10:00 AM - 11:00 AM';

    // Fill in all edit fields, omitting location and event tags.
    $edits = [
      'edit-title-0-value' => $title,
      'edit-field-uw-event-date-0-time-wrapper-value-date' => $date,
      'edit-field-uw-event-date-0-time-wrapper-value-time' => $start_time,
      'edit-field-uw-event-date-0-time-wrapper-end-value-time' => $end_time,
      'edit-field-uw-event-summary-0-value' => $i->uwRandomString(),
      'edit-field-uw-event-host-0-title' => $host_title,
      'edit-field-uw-event-host-0-uri' => $host_url,
      'edit-field-uw-event-website-0-title' => $web_title,
      'edit-field-uw-event-website-0-uri' => $web_url,
      'edit-field-uw-event-cost-0-value' => $cost,
      'edit-field-uw-event-location-coord-0-value-lat' => $lat,
      'edit-field-uw-event-location-coord-0-value-lon' => $long,
      'edit-field-uw-event-map-0-uri' => $map_url,
      'edit-field-uw-audience-0-1-1-children-2-2' => 'checked',
      'edit-field-uw-event-type-25' => 'checked',
      'edit-field-uw-meta-description-0-value' => $description,
      'edit-moderation-state-0-state' => 'Published',
    ];

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Go to the add event node page.
    $i->amOnPage('node/add/uw_ct_event');
    $i->see('Create Event');

    // Fill in the fields.
    $this->fillInEventFields($i, $edits);

    // Click save and ensure that event was created.
    $i->click('Save');
    $i->see('Event ' . $edits['edit-title-0-value'] . ' has been created.');

    // The expected display.
    $expected_display = [
      'Title' => $title,
      'Date' => $todays_date,
      'Location coordinates' => 'Location coordinates',
      'Link to map' => $map_url,
      'Host' => $host_title,
      'Event website' => $web_title,
      'Cost' => '$10',
      'Term1' => 'Current students',
      'Term2' => 'Current undergraduate students',
      'Term3' => 'Conference',
    ];

    // Create an array of all front end users.
    $roles = [
      'uw_role_site_manager',
      'uw_role_content_editor',
      'uw_role_content_author',
    ];

    // Step through each of the roles and test display.
    foreach ($roles as $role) {

      // Set a new title for the event node.
      $edits['edit-title-0-value'] = $i->uwRandomString();
      $expected_display['Title'] = $edits['edit-title-0-value'];

      // Login with the role.
      $i->amOnPage('user/logout');
      $i->logInWithRole($role);

      // Go to add Event Page Node and ensure the page status is OK.
      $i->amOnPage('/node/add/uw_ct_event');
      $i->see('Create event');

      // Ensure Content Author cannot publish a Service.
      // Set moderation states for different users.
      if ($role == 'uw_role_content_author') {
        $i->dontSee('option[id="edit-moderation-state-0-state"][value="published"]');
        $edits['edit-moderation-state-0-state'] = 'Needs Review';
      }
      else {
        $edits['edit-moderation-state-0-state'] = 'Published';
      }

      // Fill in Event Node edit page. Save the Event Node.
      $this->fillInEventFields($i, $edits);

      // Click save and ensure that event was created.
      $i->click('Save');
      $i->see('Event ' . $edits['edit-title-0-value'] . ' has been created.');

      // Click on the save layout.
      $i->click('Save layout');
      $i->waitForText('The layout override has been saved.');

      // Step through all the expected display and ensure that
      // they appear correctly.
      foreach ($expected_display as $label => $field_value) {

        // Check the display of various field values.
        if (
          $label == 'Title' ||
          $label == 'Date' ||
          $label == 'Cost'
        ) {

          $i->see($field_value);
        }

        // Check the display of 'Location coordinates' label.
        elseif ($label == 'Location coordinates') {
          $i->see($label);
        }

        // Check the display of the Terms.
        elseif (substr($label, 0, -1) == 'Term') {
          $i->seeLink($field_value);
        }

        // Check the display of remaining labels and their field values.
        elseif (
          $label == 'Link to map' ||
          $label == 'Host' ||
          $label == 'Event website'
        ) {

          $i->see($label);
          $i->seeLink($field_value);
        }

      }
    }
  }

  /**
   * Function to fill in the fields on the event node.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   * @param array $edits
   *   The array of fields.
   */
  private function fillInEventFields(AcceptanceTester $i, array $edits): void {

    // Step through and fill in all the fields.
    foreach ($edits as $id => $value) {

      // Fill in the field based on the type of field.
      if ($id == 'edit-moderation-state-0-state') {
        $i->selectOption('#' . $id, $value);
      }
      elseif ($value == 'checked') {
        $i->checkOption('#' . $id);
      }
      elseif ($id == 'edit-field-uw-event-summary-0-value') {
        $i->fillCkEditor($value, '#' . $id);
      }
      else {
        $i->fillField('#' . $id, $value);
      }
    }
  }

}
