<?php

use Drupal\webform\Entity\Webform;

/**
 * Class WebformSettingsCest.
 *
 * Tests for web forms settings.
 */
class WcmsTestsWebformSettingsCest {

  /**
   * The web form entity.
   *
   * @var \Drupal\webform\Entity\Webform
   */
  private $webform;

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _before(AcceptanceTester $i) {
    // phpcs:enable

    // Get any web forms with title Test Form.
    $webforms = \Drupal::entityTypeManager()
      ->getStorage('webform')
      ->loadByProperties(['title' => 'Test Form']);

    // If there are any web forms delete them.
    if (count($webforms) > 0) {
      foreach ($webforms as $webform) {
        $webform->delete();
      }
    }

    // Create a web form.
    $this->webform = Webform::create([
      'id' => 'test_form',
      'title' => 'Test Form',
    ]);

    // Save the newly created web form.
    $this->webform->save();
  }

  /**
   * Test for captcha element.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testCaptcha(AcceptanceTester $i) {

    // Login as form editor.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_form_editor');

    // Go to test form manage page.
    $i->amOnPage('/admin/structure/webform/manage/test_form');

    // Ensure that the captcha is present.
    $i->see('[captcha]');
  }

  /**
   * Function to test that captcha is present on form.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testCaptchaPresent(AcceptanceTester $i) {

    // Logout of the site, so we are anonymous.
    $i->amOnPage('/user/logout');

    // Visit the form.
    $i->amOnPage('form/test-form');

    // Ensure the description is on the form.
    $i->see('This question is for testing whether or not you are a human visitor and to prevent automated spam submissions.');
  }

  /**
   * Test for web form settings.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   *
   * @throws Exception
   */
  public function testSettings(AcceptanceTester $i) {

    // Login as form editor.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_form_editor');

    // Go to the build of the webform.
    $i->amOnPage('/admin/structure/webform/manage/test_form');

    // Check help text to "send from" in webform email handler.
    // Based on the above new added form, click settings.
    $i->click('Settings');

    // Click Emails / Handlers.
    $i->click('Emails / Handlers');

    // Click Add email.
    $i->click('Add email');

    // Wait for the email settings to load.
    $i->waitForText('Sends a webform submission via an email.');

    // The help text should be there.
    $i->see('This must be an @uwaterloo.ca email address for sending to succeed. Please consider using the reply-to email option instead when emails are not limited to campus accounts.');

    // Click on the confirmation link.
    $i->click('Confirmation');

    // Ensure that the inline is checked by default.
    $i->seeCheckboxIsChecked('input[id="edit-confirmation-type-inline"]');

  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _passed(AcceptanceTester $i) {

    $this->webform->delete();
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i) {
    // phpcs:enable

    $this->webform->delete();
  }

}
