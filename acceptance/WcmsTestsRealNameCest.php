<?php

/**
 * Class RealNameCest.
 *
 * Tests for real name.
 */
class WcmsTestsRealNameCest {

  /**
   * Tests for real name settings.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testRealNameSettings(AcceptanceTester $i) {

    // Login as administrator.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');

    // Go to the rela name page.
    $i->amOnPage('admin/config/people/realname');
    $i->see('Real name');

    // Ensure that pattern is correct.
    $i->seeElement('input[value="[user:field_uw_first_name] [user:field_uw_last_name]"]');
  }

  /**
   * Function to test real name in user.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testRealNameUser(AcceptanceTester $i) {

    // Login as administrator.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');

    // Add a new user.
    $i->amOnPage('admin/people/create');
    $i->see('Add user');

    // Settings used for user.
    $first_name = $i->uwRandomString();
    $last_name = $i->uwRandomString();
    $pass = strtolower($i->uwRandomString()) . 'W#3h';
    $email = 'wcmstest@uwaterloo.ca';
    $name = 'wcmstest';

    // Fill in the fields.
    $i->fillField('field_uw_first_name[0][value]', $first_name);
    $i->fillField('field_uw_last_name[0][value]', $last_name);
    $i->fillField('mail', $email);
    $i->fillField('#edit-name', $name);
    $i->fillField('pass[pass1]', $pass);
    $i->waitForElement('.password-strength__text');
    $i->seeElement('.password-strength__text');
    $i->fillField('pass[pass2]', $pass);
    $i->waitForText('yes');
    $i->see('yes');

    // Create the new user.
    $i->click('Create new account');
    $i->waitForText('Created a new user account');
    $i->see('Created a new user account for ' . $name . '. No email has been sent.');

    // Go to the people page.
    $i->amOnPage('admin/people');
    $i->see('People');

    // Ensure that we see the real name.
    $i->see($first_name . ' ' . $last_name);

    // field_uw_first_name and field_uw_last_name are not displayed.
    $i->amOnPage('users/wcmstest');
    $i->dontSee('First name');
    $i->dontSee('Last name');
  }

  // phpcs:disable
  /**
   * Function to run after the test fails.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _after(AcceptanceTester $i): void {
    // phpcs:enable

    // Get the user that we created.
    $users = \Drupal::entityTypeManager()
      ->getStorage('user')
      ->loadByProperties(['name' => 'wcmstest']);

    // Delete the user we created.
    foreach ($users as $user) {
      $user->delete();
    }
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i): void {
    // phpcs:enable

    // Get the user that we created.
    $users = \Drupal::entityTypeManager()
      ->getStorage('user')
      ->loadByProperties(['name' => 'wcmstest']);

    // Delete the user we created.
    foreach ($users as $user) {
      $user->delete();
    }
  }

}
