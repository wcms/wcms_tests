<?php

use Step\Acceptance\ContentType;
use Step\Acceptance\TaxonomyTest;

/**
 * Class WcmsTestsContentTypeBlogCest.
 *
 * Tests for blogs.
 */
class WcmsTestsContentTypeBlogCest {

  /**
   * The content type.
   *
   * @var string
   */
  private string $contentType = 'uw_ct_blog';

  /**
   * Tests for blog content type.
   *
   * @param Step\Acceptance\ContentType $i
   *   Acceptance test variable.
   */
  public function testBlogContentType(ContentType $i): void {

    // Test the fields in the content type.
    $i->testContentType(
      'Blog',
      $i->getContentTypeFields($this->contentType)
    );

    // Test the content type edits.
    $i->testContentTypeEdits(
      $this->contentType,
      $i->getContentTypeEdits($this->contentType)
    );
  }

  /**
   * Function to test taxonomies.
   *
   * @param Step\Acceptance\TaxonomyTest $i
   *   The acceptance tester.
   */
  public function testBlogTaxonomies(TaxonomyTest $i): void {

    // Test the taxonomy terms.
    $i->testTaxonomies(
      $this->contentType,
      'blog',
      ['uw_vocab_audience']
    );
  }

  /**
   * Test blog content type with dates.
   *
   * @param Step\Acceptance\ContentType $i
   *   The acceptance tester.
   */
  public function testBlogDates(ContentType $i): void {

    // Get the dates to be used.
    $dates = $i->getEarlyMiddleLateDates();

    // Step through each of the dates and create content.
    foreach ($dates as $date) {

      // Get the edits for the content type.
      $edits = $i->getContentTypeEdits('uw_ct_blog');

      // Set the default edit date to one of the generated dates.
      $edits['field']['edit-field-uw-blog-date-0-value-date'] = $date;

      // Test the edits with the new date, not running the full
      // tests like we do when testing the content type.
      $i->testContentTypeEdits(
        'uw_ct_blog',
        $edits,
      FALSE
      );

      // Click on submit and ensure that the node has been created.
      $i->click('#edit-submit');
      $i->see('Blog post ' . $edits['field']['edit-title-0-value'] . ' has been created');
    }

    // Test the blog listing page.
    $i->testListingPage($dates, 'blog');
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _after(AcceptanceTester $i): void {
    // phpcs:enable

    // Delete all the blocks.
    $i->deleteAllBlocks();
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i): void {
    // phpcs:enable

    // Delete all the blocks.
    $i->deleteAllBlocks();
  }

}
