<?php

/**
 * Class AuthSiteCest.
 *
 * Tests for auth site.
 */
class WcmsTestsAuthSiteCest {

  /**
   * Tests for auth site.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testAuthSite(AcceptanceTester $i) {

    // Get the test cases.
    $cases = $this->getCases($i);

    // Get the default access denied message.
    $default_message = $this->getDefaultMessage();

    // Loop to check all cases.
    foreach ($cases as $case => $edit) {

      // Login as site manager.
      $i->amOnPage('user/logout');
      $i->logInWithRole('administrator');

      $i->amOnPage('admin/config/system/auth_site');

      $i->checkOption('#edit-enabled');
      $i->waitForText('ACCESS CONTROL SETTINGS');

      foreach ($edit as $id => $value) {
        if ($id == 'edit-uw-access-control-method-group') {
          $i->selectOption('#' . $id, $value);
        }
        else {
          $i->fillField('#' . $id, $value);
        }
      }

      // Click on save config and ensure that it saves.
      $i->click('Save configuration');
      $i->waitForText('The configuration options have been saved.');
      $i->see('The configuration options have been saved.');

      // Admin logout.
      $i->amOnPage('user/logout');

      // Non-logged in user loads home page.
      $i->amOnPage('home');

      // Check log in page.
      switch ($case) {
        // edit-uw-auth-site-access-denied-title is empty,
        // and edit-uw-auth-site-access-denied-excerpt is default.
        case 'case1':
        case 'case5':
        case 'case6':
        case 'case7':
        case 'case8':
        case 'case9':
          $i->see('Log in');
          $i->see($default_message);
          break;

        // edit-uw-auth-site-access-denied-title is empty,
        // and edit-uw-auth-site-access-denied-excerpt is changed.
        case 'case2':
          $i->see('Log in');
          $i->see($edit['edit-uw-auth-site-text']);
          break;

        // edit-uw-auth-site-access-denied-title is filled,
        // and edit-uw-auth-site-access-denied-excerpt is default.
        case 'case3':
          $i->see($edit['edit-uw-auth-site-access-denied-title']);
          $i->see($default_message);
          break;

        // edit-uw-auth-site-access-denied-title is filled,
        // and edit-uw-auth-site-access-denied-excerpt is changed.
        case 'case4':
          $i->see($edit['edit-uw-auth-site-access-denied-title']);
          $i->see($edit['edit-uw-auth-site-access-denied-excerpt']);
          break;
      }

      // Check for reset password and correct redirect url.
      $i->dontSee('Reset your password');
      $i->amOnPage('user/password');
      $i->seeCurrentUrlEquals('/user/login?destination=/user/password');

      // Log in as authenticated user.
      $i->amOnPage('user/logout');
      $i->logInWithRole('authenticated');

      // Access denied message setting in home page.
      $i->amOnPage('home');

      switch ($case) {
        // Authenticated user can view home page for these cases.
        case 'case1':
        case 'case2':
        case 'case3':
        case 'case4':
        case 'case9':
          $i->seeLink('Contacts');
          $i->seeLink('Profiles');
          $i->seeLink('Blog');
          $i->seeLink('Events');
          $i->seeLink('News');
          break;

        // Display custom access denied title and default message.
        case 'case5':
          $i->see($edit['edit-uw-auth-site-access-denied-title-logged-in']);
          $i->see($default_message);
          break;

        // Display 'Access denied' title and custom message.
        case 'case6':
          $i->see('Access denied');
          $i->see($edit['edit-uw-auth-site-access-denied-excerpt-logged-in']);
          break;

        // Display custom access denied title and message.
        case 'case7':
        case 'case8':
          $i->see($edit['edit-uw-auth-site-access-denied-title-logged-in']);
          $i->see($edit['edit-uw-auth-site-access-denied-excerpt-logged-in']);
          break;

      }

      // Check 'admin/config/system/auth_site' as example,
      // got default access denied for all cases.
      $i->amOnPage('admin/config/system/auth_site');
      $i->see('Access denied');
      $i->see('You are not authorized to access this page');

      // Site owner access admin/config/system/auth_site page.
      $i->amOnPage('user/logout');
      $i->logInWithRole('uw_role_site_owner');
      $i->amOnPage('admin/config/system/auth_site');

      // Site owner cannot view 'Authenticated site' settings.
      $i->dontSee('Require authentication to access this site');
    }
  }

  /**
   * Function to get the default access denied message.
   *
   * @return string
   *   The access denied message.
   */
  private function getDefaultMessage(): string {
    return 'Content on this site is restricted to authorized users; you must log in. Note that if you are not an authorized user, you may be able to log in, but still not have access to the content.';
  }

  /**
   * Function to get the test cases for auth site.
   *
   * @return array
   *   Array of test cases for auth site.
   */
  private function getCases(AcceptanceTester $i): array {

    // The default message in edit-uw-auth-site-text field.
    $default_message = $this->getDefaultMessage();

    return [
      'case1' => [
        'edit-uw-access-control-method-group' => 'auth',
        'edit-uw-auth-site-access-denied-title' => '',
        'edit-uw-auth-site-access-denied-excerpt' => '',
        'edit-uw-auth-site-access-denied-title-logged-in' => '',
        'edit-uw-auth-site-access-denied-excerpt-logged-in' => '',
        'edit-uw-auth-site-text' => $default_message,
      ],
      'case2' => [
        'edit-uw-access-control-method-group' => 'auth',
        'edit-uw-auth-site-access-denied-title' => '',
        'edit-uw-auth-site-access-denied-excerpt' => '',
        'edit-uw-auth-site-access-denied-title-logged-in' => '',
        'edit-uw-auth-site-access-denied-excerpt-logged-in' => '',
        'edit-uw-auth-site-text' => $i->uwRandomString(),
      ],
      'case3' => [
        'edit-uw-access-control-method-group' => 'auth',
        'edit-uw-auth-site-access-denied-title' => $i->uwRandomString(),
        'edit-uw-auth-site-access-denied-excerpt' => '',
        'edit-uw-auth-site-access-denied-title-logged-in' => '',
        'edit-uw-auth-site-access-denied-excerpt-logged-in' => '',
        'edit-uw-auth-site-text' => $default_message,
      ],
      'case4' => [
        'edit-uw-access-control-method-group' => 'auth',
        'edit-uw-auth-site-access-denied-title' => $i->uwRandomString(),
        'edit-uw-auth-site-access-denied-excerpt' => $i->uwRandomString(),
        'edit-uw-auth-site-access-denied-title-logged-in' => '',
        'edit-uw-auth-site-access-denied-excerpt-logged-in' => '',
        'edit-uw-auth-site-text' => $default_message,
      ],
      'case5' => [
        'edit-uw-access-control-method-group' => 'group',
        'edit-ad-require-groups' => $i->uwRandomString(),
        'edit-ad-deny-groups' => '',
        'edit-wcms-accounts' => '',
        'edit-uw-auth-site-access-denied-title' => '',
        'edit-uw-auth-site-access-denied-excerpt' => '',
        'edit-uw-auth-site-access-denied-title-logged-in' => $i->uwRandomString(),
        'edit-uw-auth-site-access-denied-excerpt-logged-in' => '',
        'edit-uw-auth-site-text' => $default_message,
      ],
      'case6' => [
        'edit-uw-access-control-method-group' => 'group',
        'edit-ad-require-groups' => $i->uwRandomString(),
        'edit-ad-deny-groups' => '',
        'edit-wcms-accounts' => '',
        'edit-uw-auth-site-access-denied-title' => '',
        'edit-uw-auth-site-access-denied-excerpt' => '',
        'edit-uw-auth-site-access-denied-title-logged-in' => '',
        'edit-uw-auth-site-access-denied-excerpt-logged-in' => $i->uwRandomString(),
        'edit-uw-auth-site-text' => $default_message,
      ],
      'case7' => [
        'edit-uw-access-control-method-group' => 'group',
        'edit-ad-require-groups' => $i->uwRandomString(),
        'edit-ad-deny-groups' => '',
        'edit-wcms-accounts' => '',
        'edit-uw-auth-site-access-denied-title' => '',
        'edit-uw-auth-site-access-denied-excerpt' => '',
        'edit-uw-auth-site-access-denied-title-logged-in' => $i->uwRandomString(),
        'edit-uw-auth-site-access-denied-excerpt-logged-in' => $i->uwRandomString(),
        'edit-uw-auth-site-text' => $default_message,
      ],
      'case8' => [
        'edit-uw-access-control-method-group' => 'group',
        'edit-ad-require-groups' => '',
        'edit-ad-deny-groups' => '',
        'edit-wcms-accounts' => '',
        'edit-uw-auth-site-access-denied-title' => '',
        'edit-uw-auth-site-access-denied-excerpt' => '',
        'edit-uw-auth-site-access-denied-title-logged-in' => $i->uwRandomString(),
        'edit-uw-auth-site-access-denied-excerpt-logged-in' => $i->uwRandomString(),
        'edit-uw-auth-site-text' => $default_message,
      ],
      'case9' => [
        'edit-uw-access-control-method-group' => 'group',
        'edit-ad-require-groups' => '',
        'edit-ad-deny-groups' => $i->uwRandomString(),
        'edit-wcms-accounts' => '',
        'edit-uw-auth-site-access-denied-title' => '',
        'edit-uw-auth-site-access-denied-excerpt' => '',
        'edit-uw-auth-site-access-denied-title-logged-in' => $i->uwRandomString(),
        'edit-uw-auth-site-access-denied-excerpt-logged-in' => $i->uwRandomString(),
        'edit-uw-auth-site-text' => $default_message,
      ],
    ];
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _passed(AcceptanceTester $i) {

    // Get the config for the auth site.
    $config = \Drupal::configFactory()
      ->getEditable('uw_auth_site.auth_site');

    // Get the actual config from the auth site form.
    $auth_site = $config->get('auth_site');

    // Reset all the values of the config.
    $auth_site['enabled'] = FALSE;
    $auth_site['uw_access_control_method'] = NULL;
    $auth_site['ad_require_groups'][0] = NULL;
    $auth_site['ad_deny_groups'][0] = NULL;
    $auth_site['uw_auth_site_access_denied_title'] = NULL;
    $auth_site['uw_auth_site_access_denied_excerpt'] = NULL;
    $auth_site['uw_auth_site_access_denied_title_logged_in'] = NULL;
    $auth_site['uw_auth_site_access_denied_excerpt_logged_in'] = NULL;

    // Set the auth site with values from above.
    $config->set('auth_site', $auth_site);

    // Save the config.
    $config->save();

    // Run the update permissions so that further tests work.
    shell_exec('drush uwperm');

    // Flush all the caches so that tests can continue.
    drupal_flush_all_caches();
  }

  // phpcs:disable
  /**
   * Function to run if the test fails.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i) {
    // phpcs:enable

    // Get the config for the auth site.
    $config = \Drupal::configFactory()->getEditable('uw_auth_site.auth_site');

    // Get the actual config from the auth site form.
    $auth_site = $config->get('auth_site');

    // Reset all the values of the config.
    $auth_site['enabled'] = FALSE;
    $auth_site['uw_access_control_method'] = NULL;
    $auth_site['ad_require_groups'][0] = NULL;
    $auth_site['ad_deny_groups'][0] = NULL;
    $auth_site['uw_auth_site_access_denied_title'] = NULL;
    $auth_site['uw_auth_site_access_denied_excerpt'] = NULL;
    $auth_site['uw_auth_site_access_denied_title_logged_in'] = NULL;
    $auth_site['uw_auth_site_access_denied_excerpt_logged_in'] = NULL;

    // Set the auth site with values from above.
    $config->set('auth_site', $auth_site);

    // Save the config.
    $config->save();

    // Run the update permissions so that further tests work.
    shell_exec('drush uwperm');

    // Flush all the caches so that tests can continue.
    drupal_flush_all_caches();
  }

}
