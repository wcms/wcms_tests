<?php

use Step\Acceptance\ContentType;

/**
 * Class WcmsTestsContentTypeSidebarCest.
 *
 * Tests for sidebars.
 */
class WcmsTestsContentTypeSidebarCest {

  /**
   * The content type.
   *
   * @var string
   */
  private string $contentType = 'uw_ct_sidebar';

  /**
   * Tests for sidebar content type.
   *
   * @param Step\Acceptance\ContentType $i
   *   Acceptance test variable.
   */
  public function testSidebarContentType(ContentType $i): void {

    // Test the fields in the content type.
    $i->testContentType(
      'Sidebar',
      $i->getContentTypeFields($this->contentType)
    );

    // Test the content type edits.
    $i->testContentTypeEdits(
      $this->contentType,
      $i->getContentTypeEdits($this->contentType)
    );
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _after(AcceptanceTester $i): void {
    // phpcs:enable

    // Delete all the blocks.
    $i->deleteAllBlocks();
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i): void {
    // phpcs:enable

    // Delete all the blocks.
    $i->deleteAllBlocks();
  }

}
