<?php

use Codeception\Util\Locator;

/**
 * Class WcmsTestsBlocksEmbedsCest.
 *
 * Tests for embed blocks.
 */
class WcmsTestsBlocksEmbedsCest {

  /**
   * Array used for any tids that we created.
   *
   * @var array
   */
  private $tidsUsed = [];

  /**
   * Array of nodes used.
   *
   * @var array
   */
  private array $nodesUsed;

  /**
   * Function to test the Google Maps block.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testGoogleMapsBlock(AcceptanceTester $i) {

    // Create a webpage.
    $this->nodesUsed['Google Maps Block'] = $i->createWebPage('Google Maps Block');

    // Get the path of the webpage.
    $path = $i->getWebPagePath($this->nodesUsed['Google Maps Block']);

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Go to the layout page for the webpage we created.
    $i->amOnPage($path . '/layout');
    $i->see('Edit layout for ' . $this->nodesUsed['Google Maps Block']->getTitle());

    // Add a block.
    $i->click('Add block');
    $i->waitForText('Choose a block');

    // Click on the Google Maps block.
    $i->click('Google Maps');
    $i->waitForText('Configure block');

    // Get the settings for Google Maps.
    $title = $i->uwRandomString();
    $gm_url = 'https://www.google.com/maps/embed?pb=' . $i->uwRandomString();
    $gm_height = rand(150, 1000);

    // Fill in required fields.
    $i->fillField('settings[label]', $title);
    $i->fillField('settings[block_form][field_gmaps_embedded_url][0][uri]', $gm_url);
    $i->fillField('settings[block_form][field_gmaps_height][0][value]', $gm_height);

    // Click on add block and ensure it is on the screen.
    $i->click('Add block');
    $i->waitForElement(Locator::contains('div[class="uw-admin-label"]', 'Google Maps'));
    $i->seeElement(Locator::contains('div[class="uw-admin-label"]', 'Google Maps'));

    // Check for elements on the page.
    $i->see($title);
    $i->seeElement('div[class="uw-google-maps"]');
    $i->seeElement('iframe[src="' . $gm_url . '"][height="' . $gm_height . '"]');
  }

  /**
   * Function to test the Power BI block.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testPowerBiBlock(AcceptanceTester $i) {

    // Create a webpage.
    $this->nodesUsed['PowerBI Block'] = $i->createWebPage('PowerBI Block');

    // Get the path of the webpage.
    $path = $i->getWebPagePath($this->nodesUsed['PowerBI Block']);

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Go to the layout page for the webpage we created.
    $i->amOnPage($path . '/layout');
    $i->see('Edit layout for ' . $this->nodesUsed['PowerBI Block']->getTitle());

    // Add a block.
    $i->click('Add block');
    $i->waitForText('Choose a block');

    // Click on the PowerBI block.
    $i->click('PowerBI');
    $i->waitForText('Configure block');

    // Get the settings for the block.
    $title = $i->uwRandomString();
    $url = 'https://app.powerbi.com/' . $i->uwRandomString();

    // Fill in required fields.
    $i->fillField('settings[label]', $title);
    $i->fillField('settings[block_form][field_uw_powerbi_url][0][value]', $url);

    // Click on add block and ensure it is on the screen.
    $i->click('Add block');
    $i->waitForElement(Locator::contains('div[class="uw-admin-label"]', 'PowerBI'));
    $i->seeElement(Locator::contains('div[class="uw-admin-label"]', 'PowerBI'));

    // Ensure elements are on the page.
    $i->see($title);
    $i->seeElement('div[class="uw-powerbi"]');
    $i->seeElement('iframe[src="' . $url . '"]');
  }

  /**
   * Function to test the social intents block.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testSocialIntentsBlock(AcceptanceTester $i) {

    // Create a webpage.
    $this->nodesUsed['Social Intents Block'] = $i->createWebPage('Social Intents Block');

    // Get the path of the webpage.
    $path = $i->getWebPagePath($this->nodesUsed['Social Intents Block']);

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Go to the layout page for the webpage we created.
    $i->amOnPage($path . '/layout');
    $i->see('Edit layout for ' . $this->nodesUsed['Social Intents Block']->getTitle());

    // Add a block.
    $i->click('Add block');
    $i->waitForText('Choose a block');

    // Click on the social intents block.
    $i->click('Social Intents');
    $i->waitForText('Configure block');

    // Get the settings we are going to use.
    $title = $i->uwRandomString();
    $username = 'uwist';

    // Fill in the required fields.
    $i->fillField('settings[label]', $title);
    $i->fillField('settings[block_form][field_uw_si_username][0][value]', $username);

    // Click on add block and ensure it is on the screen.
    $i->click('Add block');
    $i->waitForElement(Locator::contains('div[class="uw-admin-label"]', 'Social Intents'));
    $i->seeElement(Locator::contains('div[class="uw-admin-label"]', 'Social Intents'));

    // Ensure elements appear on the page.
    $i->see($title);
    $i->seeElement('iframe[src="https://chat.socialintents.com/c/' . $username . '"]');
  }

  /**
   * Function to test the Tableau visualization block.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testTableauVisualizationBlock(AcceptanceTester $i) {

    // Create a webpage.
    $this->nodesUsed['Tableau Visualization Block'] = $i->createWebPage('Tableau Visualization Block');

    // Get the path of the webpage.
    $path = $i->getWebPagePath($this->nodesUsed['Tableau Visualization Block']);

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Go to the layout page for the webpage we created.
    $i->amOnPage($path . '/layout');
    $i->see('Edit layout for ' . $this->nodesUsed['Tableau Visualization Block']->getTitle());

    // Add a block.
    $i->click('Add block');
    $i->waitForText('Choose a block');

    // Click on the Tableau visualization block.
    $i->click('Tableau visualization');
    $i->waitForText('Configure block');

    // Get the settings we are going to use.
    $title = $i->uwRandomString();
    $height = rand(100, 1000);
    $site_name = $i->uwRandomString();
    $tableau_name = $i->uwRandomString();

    // Fill in the required fields.
    $i->fillField('settings[label]', $title);
    $i->selectOption('settings[block_form][field_uw_tbl_server]', 'Public');
    $i->fillField('settings[block_form][field_uw_tbl_site_name][0][value]', $site_name);
    $i->fillField('settings[block_form][field_uw_tbl_tableau_name][0][value]', $tableau_name);
    $i->fillField('settings[block_form][field_uw_tbl_tableau_height][0][value]', $height);
    $i->selectOption('settings[block_form][field_uw_tbl_display_tabs]', 'Yes');

    // Click on add block and ensure it is on the screen.
    $i->click('Add block');
    $i->waitForElement(Locator::contains('div[class="uw-admin-label"]', 'Tableau visualization'));
    $i->seeElement(Locator::contains('div[class="uw-admin-label"]', 'Tableau visualization'));

    // Ensure elements appear on the page.
    $i->see($title);
    $i->seeElementInDOM('object[class="tableauViz"]');
    $i->seeElementInDOM('object[class="tableauViz"] param[name="host_url"][value="https://public.tableau.com/"]');
    $i->seeElementInDOM('object[class="tableauViz"] param[name="site_root"][value="' . $site_name . '"]');
    $i->seeElementInDOM('object[class="tableauViz"] param[name="name"][value="' . $tableau_name . '"]');
    $i->seeElementInDOM('object[class="tableauViz"] param[name="tabs"][value="yes"]');
    $i->seeElementInDOM('script[src^="https://public.tableau.com"]');
    $i->seeElementInDOM('object[style*="height: ' . $height . 'px;"]');
  }

  // phpcs:disable

  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _after(AcceptanceTester $i): void {
    // phpcs:enable

    // Compelte the block tests.
    $i->completeBlockTests($this->tidsUsed, $this->nodesUsed);
  }

  // phpcs:disable

  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i): void {
    // phpcs:enable

    // Compelte the block tests.
    $i->completeBlockTests($this->tidsUsed, $this->nodesUsed);
  }

}
