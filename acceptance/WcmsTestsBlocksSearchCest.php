<?php

use Codeception\Util\Locator;
use Drupal\taxonomy\Entity\Term;

/**
 * Class WcmsTestsBlocksSearchCest.
 *
 * Tests for search blocks.
 */
class WcmsTestsBlocksSearchCest {

  /**
   * Array used for any tids that we created.
   *
   * @var array
   */
  private $tidsUsed = [];

  /**
   * Array of nodes used.
   *
   * @var array
   */
  private array $nodesUsed;

  /**
   * Function to test the catalog search block.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testCatalogSearchBlock(AcceptanceTester $i) {

    // Create a webpage.
    $this->nodesUsed['Catalog Search Block'] = $i->createWebPage('Catalog Search Block');

    // Get the path of the webpage.
    $path = $i->getWebPagePath($this->nodesUsed['Catalog Search Block']);

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Create catalog category, get id and get name.
    $tid = $i->createTerm('uw_vocab_catalogs', $i->uwRandomString());

    // Get the name of the term.
    $name = Term::load($tid)->getName();

    // Add to the tids array so we delete after.
    $this->tidsUsed[] = $tid;

    // Create catalog item.
    $catalog_item_title = $i->uwRandomString();
    $this->nodesUsed[$catalog_item_title] = $i->createCtNode(
      'uw_ct_catalog_item',
      $catalog_item_title,
      TRUE,
      ['catalog' => $tid]
    );

    // Go to the layout page for the webpage we created.
    $i->amOnPage($path . '/layout');
    $i->see('Edit layout for ' . $this->nodesUsed['Catalog Search Block']->getTitle());

    // Add a block.
    $i->click('Add block');
    $i->waitForText('Choose a block');

    // Click on the catalog search block.
    $i->click('Catalog search');
    $i->waitForText('Configure block');

    // Ensure that all fields and help text are present.
    $i->seeElement('input[id^="edit-settings-label"][class*="required"][value="Catalog search"]');
    $i->seeElement('input[id^="edit-settings-label-display"][type="checkbox"][checked="checked"]');
    $i->see('Display title');
    $i->seeElement('input[id^="edit-settings-catalog-settings-catalog-0"][type="radio"]');
    $i->see('All');
    $i->seeElement('input[id^="edit-settings-catalog-settings-catalog-' . $tid . '"][type="radio"]');
    $i->see($name);
    $i->seeElement('textarea[id^="edit-settings-catalog-settings-search-description"]');
    $i->see('Enter text for the catalog search description. Leave blank to have no description. (Note: text cannot contain HTML, and line breaks.)');
    $i->seeElement('input[id^="edit-settings-catalog-settings-search-placeholder"][type="text"]');
    $i->see('Enter text for the catalog search placeholder. Leave blank to have no placeholder.');

    // Click save and ensure required fields are required.
    $i->click('Add block');
    $i->waitForText('Catalog to search field is required.');
    $i->see('Catalog to search field is required.');

    // Fill in required fields.
    $i->fillField('settings[label]', $catalog_item_title);
    $i->checkOption('[id^="edit-settings-catalog-settings-catalog-0"]');
    $i->fillField('settings[catalog_settings][search_description]', 'this is the description');
    $i->fillField('settings[catalog_settings][search_placeholder]', 'this is the placeholder');

    // Click the add block.
    $i->click('Add block');
    $i->waitForElement(Locator::contains('div[class="uw-admin-label"]', 'Catalog search'));
    $i->seeElement(Locator::contains('div[class="uw-admin-label"]', 'Catalog search'));

    // Ensure that we see the values entered.
    $i->see($catalog_item_title);
    $i->see('this is the description');

    // Ensure that we see the search button with placeholder.
    $i->seeElement('input[name="search_input"][type="text"][class*="required"][placeholder="this is the placeholder"]');
  }

  /**
   * Function to test the project search block.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testProjectSearchBlock(AcceptanceTester $i) {

    // Create a webpage.
    $this->nodesUsed['Project Search Block'] = $i->createWebPage('Project Search Block');

    // Get the path of the webpage.
    $path = $i->getWebPagePath($this->nodesUsed['Project Search Block']);

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Create a published project node.
    $project_title = $i->uwRandomString();
    $this->nodesUsed[$project_title] = $i->createCtNode(
      'uw_ct_project',
      $project_title,
      TRUE
    );

    // Go to the layout page for the webpage we created.
    $i->amOnPage($path . '/layout');
    $i->see('Edit layout for ' . $this->nodesUsed['Project Search Block']->getTitle());

    // Add a block.
    $i->click('Add block');
    $i->waitForText('Choose a block');

    // Click on the project search block.
    $i->click('Project search');
    $i->waitForText('Configure block');

    // Fill in the required fields.
    $i->selectOption('settings[search_option][option]', 'All options');
    $i->fillField('settings[project_settings][search_description]', 'this is the description');
    $i->fillField('settings[project_settings][search_placeholder]', 'this is the placeholder');

    // Click on add block and ensure it is on the screen.
    $i->click('Add block');
    $i->waitForElement(Locator::contains('div[class="uw-admin-label"]', 'Project search'));
    $i->seeElement(Locator::contains('div[class="uw-admin-label"]', 'Project search'));

    // Test elements are on the page.
    $i->seeElement('.uw-project-form');
    $i->seeElement('input[value="Search"]');
    $i->seeElement(Locator::contains('label', 'Title/summary'));
    $i->click('details[id^="edit-search"]');
    $i->seeElement('input[placeholder="this is the placeholder"]');
    $i->seeElement(Locator::contains('label', 'Status'));
    $i->click('details[id^="edit-status"]');
    $i->seeElement(Locator::contains('div[id^="edit-search"][class="description"]', 'this is the description'));
  }

  /**
   * Function to test the service search block.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testServiceSearchBlock(AcceptanceTester $i) {

    // Create a webpage.
    $this->nodesUsed['Service Search Block'] = $i->createWebPage('Service Search Block');

    // Get the path of the webpage.
    $path = $i->getWebPagePath($this->nodesUsed['Service Search Block']);

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Go to the layout page for the webpage we created.
    $i->amOnPage($path . '/layout');
    $i->see('Edit layout for ' . $this->nodesUsed['Service Search Block']->getTitle());

    // Add a block.
    $i->click('Add block');
    $i->waitForText('Choose a block');

    // Click on the service search block.
    $i->click('Service search');
    $i->waitForText('Configure block');

    // Get the title we are going to use.
    $title = $i->uwRandomString();

    // Fill in the required fields.
    $i->fillField('settings[label]', $title);
    $i->fillField('settings[service_settings][search_description]', 'this is the description');
    $i->fillField('settings[service_settings][search_placeholder]', 'this is the placeholder');

    // Click on add block and ensure it is on the screen.
    $i->click('Add block');
    $i->waitForElement(Locator::contains('div[class="uw-admin-label"]', 'Service search'));
    $i->seeElement(Locator::contains('div[class="uw-admin-label"]', 'Service search'));

    // Test elements are on the page.
    $i->seeElement('.uw-search-form');
    $i->seeElement('input[value="Search"]');
    $i->seeElement('input[placeholder="this is the placeholder"]');
    $i->seeElement(Locator::contains('div[id^="edit-search-input"][class="description"]', 'this is the description'));
  }

  /**
   * Function to test the OFIS search block.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testOfisSearchBlock(AcceptanceTester $i) {

    // Create a webpage.
    $this->nodesUsed['OFIS Search Block'] = $i->createWebPage('OFIS Search Block');

    // Get the path of the webpage.
    $path = $i->getWebPagePath($this->nodesUsed['OFIS Search Block']);

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Go to the layout page for the webpage we created.
    $i->amOnPage($path . '/layout');
    $i->see('Edit layout for ' . $this->nodesUsed['OFIS Search Block']->getTitle());

    // Add a block.
    $i->click('Add block');
    $i->waitForText('Choose a block');

    // Click on the service search block.
    $i->click('OFIS expert search');
    $i->waitForText('Configure block');

    // Get the values for some fields we are going to use.
    $title = $i->uwRandomString();
    $notice = $i->uwRandomString();

    // Fill in the required fields.
    $i->fillField('settings[label]', $title);
    $i->fillField('settings[departmental_search]', 'eng');
    $i->fillField('settings[presearch_notice]', $notice);

    // Click on add block and ensure it is on the screen.
    $i->click('Add block');
    $i->waitForElement(Locator::contains('div[class="uw-admin-label"]', 'OFIS expert search'));
    $i->seeElement(Locator::contains('div[class="uw-admin-label"]', 'OFIS expert search'));

    // Save layout.
    $i->click('Save layout');
    $i->waitForText($title);

    // Test elements are on the page.
    $i->seeElement('input[id^="ofis-search"]');
    $i->seeElement('input[id^="edit-submit"]');
    $i->seeElement('input[placeholder="Search for keyword, name, or research area"]');
    $i->see($notice);

    // Test search with one result.
    $i->fillField('input[id^="ofis-search"]', 'Aagaard');
    $i->click('Submit');
    $i->waitforText('Number of experts found: 1');
    $i->seeNumberofelements('.expert', 1);
    $i->dontSeeElement('.uw-exp-col');

    // Terms for testing. Results shouldn't overlap due to later test.
    $singleWordSearchTerms = [
      'Mark',
      // For case-sensitivity and extra spaces.
      'joHn  ',
    ];

    // Value for multiple word search.
    $combinedExpectedResultCount = 0;
    $combinedSearchTerm = '';

    // Test single word searches.
    foreach ($singleWordSearchTerms as $term) {
      $i->amOnPage($path);
      $i->fillField('input[id^="ofis-search"]', $term);
      $i->click('Submit');
      $i->waitforText('Number of experts found: ');
      $resultCount = $i->grabTextFrom('div[id^="ofis-results"].ofis-results p');
      $resultCount = preg_replace('/[^0-9]/', '', $resultCount);
      $i->see('Number of experts found: ' . $resultCount);
      $i->seeNumberofelements('.expert', $resultCount);
      $i->seeElement('.uw-exp-col');
      $i->dontSee('Show full Profile');
      $i->click('Expand all');
      $i->see('Show full Profile');

      $combinedExpectedResultCount += $resultCount;
      $combinedSearchTerm .= $term . ' ';
    }

    // Test search with multiple words.
    $i->amOnPage($path);
    $i->fillField('input[id^="ofis-search"]', $combinedSearchTerm);
    $i->click('Submit');
    $i->waitforText('Number of experts found: ' . $combinedExpectedResultCount);
    $i->seeNumberofelements('.expert', $combinedExpectedResultCount);
    $i->seeElement('.uw-exp-col');

    // Search terms and messages that return no results.
    $searchTerms = [
      'aaA-' => 'No results. Please try again.',
      'a' => 'You must enter at least 2 characters in order to search.',
      '' => 'You must enter at least 2 characters in order to search.',
    ];

    // Test searches with no results.
    foreach ($searchTerms as $term => $message) {
      $i->amOnPage($path);
      $i->fillField('input[id^="ofis-search"]', $term);
      $i->click('Submit');
      $i->waitforText($message);
      $i->dontSeeElement('.expert');
      $i->dontSeeElement('.uw-exp-col');
    }
  }

  // phpcs:disable

  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _after(AcceptanceTester $i): void {
    // phpcs:enable

    // Compelte the block tests.
    $i->completeBlockTests($this->tidsUsed, $this->nodesUsed);
  }

  // phpcs:disable

  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i): void {
    // phpcs:enable

    // Compelte the block tests.
    $i->completeBlockTests($this->tidsUsed, $this->nodesUsed);
  }

}
