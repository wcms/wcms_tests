<?php

use Codeception\Util\Locator;

/**
 * Class WcmsTestsExcludeAutoListCest.
 *
 * Tests for exclude auto list.
 */
class WcmsTestsExcludeAutoListCest {

  /**
   * The content types.
   *
   * @var array
   */
  private array $contentTypes = [
    'Blog' => 'uw_ct_blog',
    'Catalog item' => 'uw_ct_catalog_item',
    'Contact' => 'uw_ct_contact',
    'Event' => 'uw_ct_event',
    'News' => 'uw_ct_news_item',
    'Opportunity' => 'uw_ct_opportunity',
    'Profile' => 'uw_ct_profile',
    'Project' => 'uw_ct_project',
    'Service' => 'uw_ct_service',
  ];

  /**
   * The listing pages info.
   *
   * @var array
   */
  private array $listingPages = [
    'blog' => '.card__teaser--blog',
    'catalogs' => '.views-field a',
    'contacts' => '.uw-contact',
    'events' => '.card__teaser--event',
    'news' => '.card__teaser--news-item',
    'opportunities' => '.card__teaser--opportunity',
    'profiles' => '.card__teaser--profile',
    'projects' => '.card__teaser--project',
    'services' => '.views-field a',
  ];

  /**
   * The listing block info.
   *
   * @var array
   */
  private array $listingBlocks = [
    'Blog posts' => '.card__teaser--blog',
    'Catalog items' => 'span a[href^="/catalogs/"]',
    'Contacts' => '.card__teaser--contact',
    'Events' => '.card__teaser--event',
    'News items' => '.card__teaser--news-item',
    'Opportunities' => '.card__teaser--opportunity',
    'Profiles' => '.card__teaser--profile',
    'Projects' => '.card__teaser--project',
    'Services' => 'span a[href^="/services/"]',
  ];

  /**
   * Array of nodes used.
   *
   * @var array
   */
  private array $nodesUsed;

  // phpcs:disable
  /**
   * Function to run before the test(s) starts.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _before(AcceptanceTester $i) {
    // phpcs:enable

    // Step through each of the content types and create 4 nodes.
    foreach ($this->contentTypes as $title => $content_type) {

      // Reset the fields array.
      $fields = [];

      // Create 4 nodes.
      for ($j = 0; $j < 4; $j++) {

        // If the counter is even, then set title and not excluded.
        // If the counter is odd, then set title and to be excluded.
        if ($j % 2 == 0) {
          $content_title = $title . ($j + 1);
          $fields['field_uw_exclude_auto'] = FALSE;
        }
        else {
          $content_title = $title . ($j + 1) . ' excluded';
          $fields['field_uw_exclude_auto'] = TRUE;
        }

        // If this is an event, set the date so that we ensure that
        // we have dates in the future.
        if ($content_type == 'uw_ct_event') {

          // Get the next date for the event.
          $next_date = '+' . ($j + 1) . ' day';
          if ($j > 0) {
            $next_date .= 's';
          }

          // Set the event date.
          $fields['field_uw_event_date'] = [
            'value' => date(strtotime($next_date, strtotime('10:00:00'))),
            'end_value' => date(strtotime($next_date, strtotime('15:00:00'))),
            'duration' => '300',
            'rrule' => NULL,
            'rrule_index' => NULL,
            'timezone' => '',
          ];
        }

        // Create the node.
        $this->nodesUsed[$content_title] = $i->createCtNode(
          $content_type,
          $content_title,
          TRUE,
          $fields
        );
      }
    }
  }

  /**
   * Tests for listing pages with excluded content.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testExcludeListingPages(AcceptanceTester $i): void {

    // Step through each of the listing pages and test that
    // the listing pages look correct.
    foreach ($this->listingPages as $page => $class) {

      // Go to the listing page.
      $i->amOnPage($page);

      // Ensure that only two classes are on the page.
      $i->seeNumberOfElements($class, 2);
    }
  }

  /**
   * Function to test the auto list block with excluded content.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testExcludeAutoListBlock(AcceptanceTester $i): void {

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Step through the listing block info and ensure
    // that it displays properly.
    foreach ($this->listingBlocks as $content_type => $class) {

      // Title for the block webpage.
      $title = $content_type . ' auto block';

      // Create the webpage for the block.
      $this->nodesUsed[$title] = $i->createCtNode(
        'uw_ct_web_page',
        $title,
        TRUE
      );

      // Get the path of the webpage.
      $path = $i->getWebPagePath($this->nodesUsed[$title]);

      // Go to the layout page for the webpage we created.
      $i->amOnPage($path . '/layout');
      $i->see('Edit layout for ' . $this->nodesUsed[$title]->getTitle());

      // Add a block.
      $i->click('Add block');
      $i->waitForText('Choose a block');

      // Click on the Automatic list block.
      $i->click('Automatic list');
      $i->waitForText('Configure block');

      // Select the content type.
      $i->selectOption('select[id*="edit-settings-content-type"]', $content_type);
      $i->click('Add block');

      // Wait for the block to load and ensure that the
      // number of elements are on the page.
      $i->waitForElement(Locator::contains('.uw-admin-label', 'Automatic list'));

      // Ensurfe there are only two on the page.
      $i->seeNumberOfElements($class, 2);
    }
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _after(AcceptanceTester $i): void {
    // phpcs:enable

    // Step through all the nodes used and delete them.
    foreach ($this->nodesUsed as $node) {
      $node->delete();
    }

    // Vocabs to delete.
    $vids = [
      'uw_vocab_catalogs',
      'uw_vocab_service_categories',
    ];

    // Step through each of the vocabs and delete the terms.
    foreach ($vids as $vid) {

      // Load the terms.
      $terms = \Drupal::entityTypeManager()
        ->getStorage('taxonomy_term')
        ->loadTree($vid);

      // Step through each term and delete it.
      foreach ($terms as $term) {

        // Load the term entity.
        $entity = \Drupal::entityTypeManager()
          ->getStorage('taxonomy_term')
          ->load($term->tid);

        // Delete the term.
        $entity->delete();
      }
    }
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i): void {
    // phpcs:enable

    // Step through all the nodes used and delete them.
    foreach ($this->nodesUsed as $node) {
      $node->delete();
    }

    // Vocabs to delete.
    $vids = [
      'uw_vocab_catalogs',
      'uw_vocab_service_categories',
    ];

    // Step through each of the vocabs and delete the terms.
    foreach ($vids as $vid) {

      // Load the terms.
      $terms = \Drupal::entityTypeManager()
        ->getStorage('taxonomy_term')
        ->loadTree($vid);

      // Step through each term and delete it.
      foreach ($terms as $term) {

        // Load the term entity.
        $entity = \Drupal::entityTypeManager()
          ->getStorage('taxonomy_term')
          ->load($term->tid);

        // Delete the term.
        $entity->delete();
      }
    }
  }

}
