<?php

/**
 * Class MediaTypesCest.
 *
 * Tests for media types.
 */
class WcmsTestsMediaTypesCest {

  /**
   * Flag to handle module enabled flag.
   *
   * @var bool
   */
  private bool $moduleFlag = FALSE;

  // phpcs:disable
  /**
   * Function to run before the test(s) starts.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _before(AcceptanceTester $i) {
    // phpcs:enable

    // If the field ui is not enabled, then enable it and set
    // flag so that we disable it once this test is completed.
    if (!\Drupal::service('module_handler')->moduleExists('field_ui')) {
      $this->moduleFlag = TRUE;
      \Drupal::service('module_installer')
        ->install(['field_ui']);
    }

    // If the field ui is not enabled, then enable it and set
    // flag so that we disable it once this test is completed.
    if (!\Drupal::service('module_handler')->moduleExists('dblog')) {
      $this->moduleFlag = TRUE;
      \Drupal::service('module_installer')
        ->install(['dblog']);
    }
  }

  /**
   * Tests for media types.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function mediaTypesTest(AcceptanceTester $i) {

    // Login as administrator.
    $i->logInWithRole('administrator');

    // File directory default value for File media type
    // is 'uploads/documents'.
    $i->amOnPage('admin/structure/media/manage/uw_mt_file/fields/media.uw_mt_file.field_media_file');
    $i->seeInField(['name' => 'settings[file_directory]'], 'uploads/documents');
    $i->seeInField(['name' => 'settings[max_filesize]'], '25 MB');
    $i->seeInField(['name' => 'settings[file_extensions]'], 'bib, csv, doc, docx, epub, gz, key, m, mw, mp3, rtf, pdf, pot, potx, pps, ppt, pptx, psd, r, tar, tex, txt, wav, xls, xlsx, zip');

    // File directory default value for Icon media type
    // is 'uploads/icons'.
    $i->amOnPage('admin/structure/media/manage/uw_mt_icon/fields/media.uw_mt_icon.field_media_image_1');
    $i->seeInField(['name' => 'settings[file_directory]'], 'uploads/icons');
    $i->seeInField(['name' => 'settings[max_filesize]'], '25 MB');

    // File directory default value for Image media type
    // is 'uploads/images'.
    $i->amOnPage('admin/structure/media/manage/uw_mt_image/fields/media.uw_mt_image.field_media_image');
    $i->seeInField(['name' => 'settings[file_directory]'], 'uploads/images');
    $i->seeInField(['name' => 'settings[max_filesize]'], '25 MB');

    // File directory default value for Local video
    // is 'uploads/videos'.
    $i->amOnPage('admin/structure/media/manage/uw_mt_local_video/fields/media.uw_mt_local_video.field_media_file');
    $i->seeInField(['name' => 'settings[file_directory]'], 'uploads/videos');
    $i->seeInField(['name' => 'settings[max_filesize]'], '3MB');
  }

  // phpcs:disable
  /**
   * Function to run after the test fails.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _after(AcceptanceTester $i): void {
    // phpcs:enable

    // If the module flag is set, uninstall it.
    if ($this->moduleFlag) {
      if (\Drupal::service('module_handler')->moduleExists('field_ui')) {
        \Drupal::service('module_installer')
          ->uninstall(['field_ui']);
      }
      if (\Drupal::service('module_handler')->moduleExists('dblog')) {
        \Drupal::service('module_installer')
          ->uninstall(['dblog']);
      }
    }
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i): void {
    // phpcs:enable

    // If the module flag is set, uninstall it.
    // If the module flag is set, uninstall it.
    if ($this->moduleFlag) {
      if (\Drupal::service('module_handler')->moduleExists('field_ui')) {
        \Drupal::service('module_installer')
          ->uninstall(['field_ui']);
      }
      if (\Drupal::service('module_handler')->moduleExists('dblog')) {
        \Drupal::service('module_installer')
          ->uninstall(['dblog']);
      }
    }
  }

}
