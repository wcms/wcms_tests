<?php

use Codeception\Util\Locator;

/**
 * Class WcmsTestsLongTitlesCest.
 *
 * Tests for long titles.
 */
class WcmsTestsLongTitlesCest {

  /**
   * The nodes being used.
   *
   * @var Drupal\node\Entity\Node
   */
  private $nodesUsed = [];

  /**
   * The node types to be used.
   *
   * @var string[]
   */
  private $nodeTypes = [
    'uw_ct_web_page',
    'uw_ct_news_item',
    'uw_ct_blog',
    'uw_ct_contact',
  ];

  // phpcs:disable
  /**
   * Function to run before the test starts.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _before(AcceptanceTester $i) {
    // phpcs:enable

    // Step through each of the node types and generate titles.
    foreach ($this->nodeTypes as $content_type) {

      // Get the title based on the node type.
      switch ($content_type) {
        case 'uw_ct_web_page':
          $this->nodesUsed[$content_type]['title'] = $i->uwRandomString(255, TRUE);
          break;

        case 'uw_ct_news_item':
          $this->nodesUsed[$content_type]['title'] = $i->uwRandomString(2);
          break;

        case 'uw_ct_blog':
          $this->nodesUsed[$content_type]['title'] = $i->uwRandomString(20, TRUE);
          break;

        case 'uw_ct_contact':
          $this->nodesUsed[$content_type]['title'] = $i->uwRandomString(150, TRUE);
          break;
      }

      // Create the node to be used.
      $this->nodesUsed[$content_type]['node'] = $i->createCtNode(
        $content_type,
        $this->nodesUsed[$content_type]['title'],
        TRUE
      );

      // Save the node.
      $this->nodesUsed[$content_type]['node']->save();
    }
  }

  /**
   * Tests for long titles.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testLongTitles(AcceptanceTester $i) {

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Go the dashboard page.
    $i->amOnPage('dashboard/my_dashboard');

    // Step through each of the nodes and ensure that it is
    // visible on the dashbaord page.
    foreach ($this->nodesUsed as $value) {
      $i->seeElement(Locator::contains('#content-list a', $value['title']));
    }
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _passed(AcceptanceTester $i) {

    // Step through each of the nodes and delete them.
    foreach ($this->nodesUsed as $value) {
      $value['node']->delete();
    }
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i) {
    // phpcs:enable

    // Step through each of the nodes and delete them.
    foreach ($this->nodesUsed as $value) {
      $value['node']->delete();
    }
  }

}
