<?php

/**
 * Class OfisCest.
 *
 * Tests for ofis.
 */
class WcmsTestsOfisCest {

  /**
   * Tests for ofis.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testOfis(AcceptanceTester $i) {

    // Check if module is enabled with profile install.
    $module_enabled = \Drupal::moduleHandler()->moduleExists('uw_ws_ofis');
    $i->assertTrue($module_enabled, 'Module uw_ws_ofis not enabled.');

    // Login as administrator.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');

    // Login as administrator and check ofis config form.
    $i->amOnPage('admin/config/uw_ws_ofis');
    $i->see('OFIS settings');

    // Validate settings form fields for administrator.
    $i->seeElement('input[id^="edit-clear"][value="Clear OFIS caches"]');
    $i->seeElement('#edit-api-endpoint');
    $i->seeElement('#edit-profiles-enabled');
    $i->dontSeeCheckboxIsChecked('#edit-profiles-enabled');

    // Login as site owner.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_owner');

    // Login as administrator and check ofis config form.
    $i->amOnPage('admin/config/uw_ws_ofis');
    $i->see('OFIS settings');

    // API endpoint and cache clear should not be visible to site owner.
    $i->dontSeeElement('input[id^="edit-clear"][value="Clear OFIS caches"]');
    $i->dontSeeElement('#edit-api-endpoint');
  }

}
