<?php

/**
 * Class WcmsTestsBlockLinksCest.
 *
 * Tests that all blocks exists.
 */
class WcmsTestsBlockLinksCest {

  /**
   * The web page object.
   *
   * @var Drupal\node\Entity\Node
   */
  private $webpage;

  /**
   * Function to test that blocks are present.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   *
   * @throws Exception
   */
  public function testBlockLinks(AcceptanceTester $i) {

    // Create a webpage.
    $this->webpage = $i->createWebPage('Block Links');

    // Get the path of the webpage.
    $path = $i->getWebPagePath($this->webpage);

    // Get all the blocks.
    $blocks = $i->getBlocks();

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Step through each block and test settings.
    foreach ($blocks as $block) {

      // Go to the webpage layout page.
      $i->amOnPage($path . '/layout');

      // Click add block and ensure block list loads.
      $i->click('Add block');
      $i->waitForText('Choose a block');
      $i->waitForText($block);

      // Click on block.
      $i->click($block);
      $i->waitForText('Configure block');
      $i->see('Heading level');

      // The list of headers that are available.
      $headers = [
        'h2',
        'h3',
        'h4',
        'h5',
        'h6',
      ];

      // Step through each of the headers and ensure that
      // they are on the page correctly.
      foreach ($headers as $tag) {

        // If this is an h2 tag, ensure it is selected, if
        // not check that it is an option in the select list.
        if ($tag == 'h2') {
          $i->seeOptionIsSelected('Heading level', $tag);
        }
        else {
          $i->seeElement('option[value="' . $tag . '"]');
        }
      }

      // If this is not an opportunity or project, then ensure
      // that when the display title is unchecked the heading
      // level is not on the page.
      if (
        $block !== 'Opportunity list' &&
        $block !== 'Project list'
      ) {

        $i->uncheckOption('Display title');
        $i->dontSee('Heading level');
      }

      // Ensure that the block id settings are there.
      $i->see('Block ID is an optional setting which is used to support an anchor link to this block');
    }
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _after(AcceptanceTester $i): void {
    // phpcs:enable

    // If we created a webpage, delete it.
    if ($this->webpage) {
      $this->webpage->delete();
    }

    // Delete all the blocks.
    $i->deleteAllBlocks();
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i): void {
    // phpcs:enable

    // If we created a webpage, delete it.
    if ($this->webpage) {
      $this->webpage->delete();
    }

    // Delete all the blocks.
    $i->deleteAllBlocks();
  }

}
