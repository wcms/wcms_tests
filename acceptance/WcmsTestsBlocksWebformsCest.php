<?php

use Codeception\Util\Locator;
use Drupal\webform\Entity\Webform;

/**
 * Class WcmsTestsBlocksWebformsCest.
 *
 * Tests for webform blocks.
 */
class WcmsTestsBlocksWebformsCest {

  /**
   * Array used for any tids that we created.
   *
   * @var array
   */
  private $tidsUsed = [];

  /**
   * Array of nodes used.
   *
   * @var array
   */
  private array $nodesUsed;

  /**
   * Function to test the webform block.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testWebformBlock(AcceptanceTester $i) {

    // Create a webpage.
    $this->nodesUsed['Webform Block'] = $i->createWebPage('Webform Block');

    // Get the path of the webpage.
    $path = $i->getWebPagePath($this->nodesUsed['Webform Block']);

    // Create a web form.
    $this->nodesUsed['Test Form'] = Webform::create([
      'id' => 'test_form',
      'title' => 'Test Form',
    ]);

    // Save the newly created web form.
    $this->nodesUsed['Test Form']->save();

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Go to the layout page for the webpage we created.
    $i->amOnPage($path . '/layout');
    $i->see('Edit layout for ' . $this->nodesUsed['Webform Block']->getTitle());

    // Add a block.
    $i->click('Add block');
    $i->waitForText('Choose a block');

    // Click on the webform block.
    $i->click('Webform');
    $i->waitForText('Configure block');

    // The settings for the webform block.
    $title = $i->uwRandomString();
    $webform_name = 'Test form (test_form)';

    // Fill in the required fields.
    $i->fillField('settings[label]', $title);
    $i->fillField('settings[webform_id]', $webform_name);

    // Click on add block and ensure it is on the screen.
    $i->click('Add block');
    $i->waitForElement(Locator::contains('div[class="uw-admin-label"]', 'Webform'));
    $i->seeElement(Locator::contains('div[class="uw-admin-label"]', 'Webform'));

    // Need to save the layout to continue the tests.
    $i->waitForElementClickable('#edit-actions #edit-submit');
    $i->click('#edit-actions #edit-submit');

    // Ensure elements are on the page.
    $i->see($title);
    $i->click('input[class*="webform-button--submit"][value="Submit"]');

    // Wait for a click the back to form.
    $i->waitForText('Back to form');

    // Ensure that messaging and download webform appear.
    $i->see('New submission added to Test Form.');
  }

  // phpcs:disable

  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _after(AcceptanceTester $i): void {
    // phpcs:enable

    // Compelte the block tests.
    $i->completeBlockTests($this->tidsUsed, $this->nodesUsed);
  }

  // phpcs:disable

  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i): void {
    // phpcs:enable

    // Compelte the block tests.
    $i->completeBlockTests($this->tidsUsed, $this->nodesUsed);
  }

}
