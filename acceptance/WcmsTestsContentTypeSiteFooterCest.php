<?php

use Step\Acceptance\ContentType;

/**
 * Class WcmsTestsContentTypeSiteFooterCest.
 *
 * Tests for sidebars.
 */
class WcmsTestsContentTypeSiteFooterCest {

  /**
   * The content type.
   *
   * @var string
   */
  private string $contentType = 'uw_ct_site_footer';

  /**
   * Tests for site footer content type.
   *
   * @param Step\Acceptance\ContentType $i
   *   Acceptance test variable.
   */
  public function testSiteFooterContentType(ContentType $i): void {

    // Test the fields in the content type.
    $i->testContentType(
      'Site footer',
      $i->getContentTypeFields($this->contentType)
    );

    // Test the content type edits.
    $i->testContentTypeEdits(
      $this->contentType,
      $i->getContentTypeEdits($this->contentType)
    );
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _after(AcceptanceTester $i): void {
    // phpcs:enable

    // Delete all the blocks.
    $i->deleteAllBlocks();
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i): void {
    // phpcs:enable

    // Delete all the blocks.
    $i->deleteAllBlocks();
  }

}
