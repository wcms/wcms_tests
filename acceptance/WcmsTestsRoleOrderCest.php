<?php

/**
 * Class WcmsTestsRoleOrderCest.
 *
 * Tests that roles are displayed in correct order.
 */
class WcmsTestsRoleOrderCest {

  /**
   * Tests that roles are in correct order.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function roleOrderTest(AcceptanceTester $i) {

    // Login as administrator.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');

    // Go to roles page and ensure roles in correct order.
    $i->amOnPage('admin/people/roles');
    $i->click('Show row weights');
    $i->seeOptionIsSelected('#edit-entities-uw-role-content-editor-weight', '4');
    $i->seeOptionIsSelected('#edit-entities-uw-role-content-author-weight', '5');

    // Go to role settings page and ensure roles in correct order.
    $i->amOnPage('admin/people/role-settings');
    $this->testRoleOrder(
      $i,
      $i->grabMultiple('#edit-user-admin-role option')
    );

    // Go to role assign page and ensure roles in correct order.
    $i->amOnPage('admin/people/roleassign');
    $this->testRoleOrder(
      $i,
      $i->grabMultiple('label[class="option"]')
    );

    // Go to the people page and ensure roles are in correct order.
    $i->amOnPage('admin/people');
    $this->testRoleOrder(
      $i,
      $i->grabMultiple('#edit-role option')
    );

    // Go to the user edit page and ensure roles are in correct order.
    $i->amOnPage('user/1/edit');
    $this->testRoleOrder(
      $i,
      $i->grabMultiple('#edit-roles--wrapper label[class="option"]')
    );

    // Go to create user page and ensure roles are in correct order.
    $i->amOnPage('admin/people/create');
    $this->testRoleOrder(
      $i,
      $i->grabMultiple('#edit-roles--wrapper label[class="option"]')
    );

    // Switch to site owner.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_owner');

    // Go to user edit page and ensure roles are in correct order.
    $i->amOnPage('user/1/edit');
    $i->see('administrator');
    $this->testRoleOrder(
      $i,
      $i->grabMultiple('#edit-roles--wrapper label[class="option"]')
    );

    // Go to create user page and ensure roles are in correct order.
    $i->amOnPage('admin/people/create');
    $this->testRoleOrder(
      $i,
      $i->grabMultiple('#edit-roles--wrapper label[class="option"]')
    );
  }

  /**
   * Test that roles are in correct order.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   * @param array $list
   *   List of roles.
   */
  private function testRoleOrder(AcceptanceTester $i, array $list) {

    // Flag that role is present.
    $role_flag = FALSE;

    // Step through each of the supplied elements and ensure
    // that the roles exists and are in correct order.
    foreach ($list as $index => $role) {

      // If this is a content editor role, set the flag that
      // the role was found and check that the role beside
      // is the content author role.
      if ($role == 'Content editor') {

        // Set the role found flag.
        $role_flag = TRUE;

        // Ensure that role is the content editor.
        $i->assertEquals($list[$index], 'Content editor');

        // If there is a role after the content editor, ensure
        // that it is the content author role.
        if (isset($list[$index + 1])) {

          // Check that content author immediately follows content editor.
          $i->assertEquals($list[$index + 1], 'Content author');
        }
        else {

          // Throw error that no role was found after content editor.
          $i->assertEquals($role_flag, TRUE, 'Did not find any role after content editor.');
        }
      }
    }

    // If there is no role flag, throw error that there was no
    // role found.
    if (!$role_flag) {

      // Show error that content editor role was not found.
      $i->assertEquals($role_flag, TRUE, 'Did not find Content editor role');
    }
  }

}
