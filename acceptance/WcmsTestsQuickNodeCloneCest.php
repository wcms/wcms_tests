<?php

/**
 * Class QuickNodeCloneCest.
 *
 * Tests for quick node clone.
 */
class WcmsTestsQuickNodeCloneCest {

  /**
   * Tests for quick node clone.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testQuickNodeClone(AcceptanceTester $i) {

    // Login as administrator.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');

    // Check quick node clone setting for node.
    $i->amOnPage('admin/config/quick-node-clone');
    $i->see('Quick Node Clone Setting');
    $i->seeElement('input[id="edit-text-to-prepend-to-title"][value="Clone of"]');
    $i->dontSeeCheckboxIsChecked('#edit-clone-status');
    $i->dontSeeCheckboxIsChecked('#edit-bundle-names-uw-ct-blog');
    $i->dontSeeCheckboxIsChecked('#edit-bundle-names-uw-ct-catalog-item');
    $i->dontSeeCheckboxIsChecked('#edit-bundle-names-uw-ct-contact');
    $i->dontSeeCheckboxIsChecked('#edit-bundle-names-uw-ct-event');
    $i->dontSeeCheckboxIsChecked('#edit-bundle-names-uw-ct-news-item');
    $i->dontSeeCheckboxIsChecked('#edit-bundle-names-uw-ct-profile');
    $i->seeCheckboxIsChecked('#edit-bundle-names-uw-ct-sidebar');
    $i->dontSeeCheckboxIsChecked('#edit-bundle-names-uw-ct-site-footer');
    $i->dontSeeCheckboxIsChecked('#edit-bundle-names-uw-ct-web-page');
    $i->seeCheckboxIsChecked('#edit-uw-ct-sidebar-field-uw-attach-page');
    $i->dontSeeCheckboxIsChecked('#edit-uw-ct-sidebar-layout-builder-layout');
    $i->seeCheckboxIsChecked('#edit-allowed-bundles-uw-ct-blog');
    $i->seeCheckboxIsChecked('#edit-allowed-bundles-uw-ct-catalog-item');
    $i->seeCheckboxIsChecked('#edit-allowed-bundles-uw-ct-contact');
    $i->seeCheckboxIsChecked('#edit-allowed-bundles-uw-ct-event');
    $i->seeCheckboxIsChecked('#edit-allowed-bundles-uw-ct-news-item');
    $i->seeCheckboxIsChecked('#edit-allowed-bundles-uw-ct-profile');
    $i->seeCheckboxIsChecked('#edit-allowed-bundles-uw-ct-sidebar');
    $i->dontSeeCheckboxIsChecked('#edit-allowed-bundles-uw-ct-site-footer');
    $i->seeCheckboxIsChecked('#edit-allowed-bundles-uw-ct-web-page');

    // Check quick node clone setting for paragraph.
    $i->amOnPage('admin/config/quick-node-clone-paragraph');
    $i->see('Quick Node Paragraph Clone Setting');
    $i->dontSeeCheckboxIsChecked('#edit-bundle-names-uw-para-call-to-action');
    $i->dontSeeCheckboxIsChecked('#edit-bundle-names-uw-para-fact-figure');
    $i->dontSeeCheckboxIsChecked('#edit-bundle-names-uw-para-ff');
    $i->dontSeeCheckboxIsChecked('#edit-bundle-names-uw-para-image-banner');
    $i->dontSeeCheckboxIsChecked('#edit-bundle-names-uw-para-local-video-banner');
    $i->dontSeeCheckboxIsChecked('#edit-bundle-names-uw-para-timeline');
    $i->dontSeeCheckboxIsChecked('#edit-bundle-names-uw-para-vimeo-video-banner');
    $i->dontSeeCheckboxIsChecked('#edit-allowed-bundles-uw-para-call-to-action');
    $i->dontSeeCheckboxIsChecked('#edit-allowed-bundles-uw-para-fact-figure');
    $i->dontSeeCheckboxIsChecked('#edit-allowed-bundles-uw-para-ff');
    $i->dontSeeCheckboxIsChecked('#edit-allowed-bundles-uw-para-image-banner');
    $i->dontSeeCheckboxIsChecked('#edit-allowed-bundles-uw-para-local-video-banner');
    $i->dontSeeCheckboxIsChecked('#edit-allowed-bundles-uw-para-timeline');
    $i->dontSeeCheckboxIsChecked('#edit-allowed-bundles-uw-para-vimeo-video-banner');
  }

}
