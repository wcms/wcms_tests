<?php

use Codeception\Util\Locator;
use Step\Acceptance\ContentType;

/**
 * Class WcmsTestsContentTypeOpportunityCest.
 *
 * Tests for opportunity content type.
 */
class WcmsTestsContentTypeOpportunityCest {

  /**
   * The content type.
   *
   * @var string
   */
  private string $contentType = 'uw_ct_opportunity';

  /**
   * Tests for blog content type.
   *
   * @param Step\Acceptance\ContentType $i
   *   Acceptance test variable.
   */
  public function testOpportunityContentType(ContentType $i): void {

    // Test the fields in the content type.
    $i->testContentType(
      'Opportunity',
      $i->getContentTypeFields($this->contentType)
    );

    // Test the content type edits.
    $i->testContentTypeEdits(
      $this->contentType,
      $i->getContentTypeEdits($this->contentType)
    );
  }

  /**
   * Tests for taxonomies for opportunity content type.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testTaxonomies(AcceptanceTester $i) {

    // Login as administrator.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');

    // The list of vocabularies to test.
    $vocabs = [
      'uw_vocab_opportunity_employment' => 'edit-entities-uw-path-opportunity-employment-type',
      'uw_vocab_opportunity_payrate' => 'edit-entities-uw-path-opportunity-pay-type',
      'uw_vocab_opportunity_type' => 'edit-entities-uw-path-opportunity-type',
    ];

    // Setup the expected elements and options for the vocab.
    foreach ($vocabs as $id => $vocab) {
      if ($id == 'uw_vocab_opportunity_employment') {
        $expected_items = [
          'Opportunity: employment type path pattern',
          'opportunities/employment_types/[term:name]',
          'Taxonomy term',
          'Vocabulary is uw_vocab_opportunity_employment',
        ];
        $options = [
          'Full time',
          'Part time',
        ];
      }
      elseif ($id == 'uw_vocab_opportunity_payrate') {
        $expected_items = [
          'Opportunity: rate of pay type path pattern',
          'opportunities/pay_types/[term:name]',
          'Taxonomy term',
          'Vocabulary is uw_vocab_opportunity_payrate',
        ];
        $options = [
          'Honorarium',
          'Hourly',
          'Salary',
          'USG',
        ];
      }
      elseif ($id == 'uw_vocab_opportunity_type') {
        $expected_items = [
          'Opportunity: type path pattern',
          'opportunities/types/[term:name]',
          'Taxonomy term',
          'Vocabulary is uw_vocab_opportunity_type',
        ];
        $options = [
          'Paid',
          'Research participant',
          'Volunteer',
        ];
      }

      // Check admin/config/search/path/patterns.
      $i->amOnPage('admin/config/search/path/patterns');
      $i->see('Patterns');

      // Check that elements are correct for the taxonomy.
      foreach ($expected_items as $value) {
        $selector = 'div[id="block-uw-theme-admin-mainpagecontent"]';
        $selector .= ' table tbody';
        $selector .= ' tr[data-drupal-selector="' . $vocab . '"] td';
        $i->seeElement(Locator::contains($selector, $value));
      }

      // Check admin/structure/taxonomy/manage/{vocab}/overview.
      $i->amOnPage('admin/structure/taxonomy/manage/' . $id . '/overview');
      $i->see('Opportunity:');

      // Step through each option and ensure exists.
      foreach ($options as $option) {
        $i->see($option);
      }
    }
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _after(AcceptanceTester $i): void {
    // phpcs:enable

    // Delete all the blocks.
    $i->deleteAllBlocks();
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i): void {
    // phpcs:enable

    // Delete all the blocks.
    $i->deleteAllBlocks();
  }

}
