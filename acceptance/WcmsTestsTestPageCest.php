<?php

/**
 * Class TestPageCest.
 *
 * Tests for front page.
 */
class WcmsTestsTestPageCest {

  /**
   * Array used for any tids that we created.
   *
   * @var array
   */
  private array $tidsUsed = [];

  /**
   * Array of nodes used.
   *
   * @var array
   */
  private array $nodesUsed = [];

  /**
   * Front page test.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function frontpageWorks(AcceptanceTester $i) {

    $i->amOnPage('/');
    $i->see('Home');
    $i->see('200 University Avenue West');
  }

  /**
   * Test only one h1 tag.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testOneH1Tag(AcceptanceTester $i) {

    // Go to homepage, ensure only one h1 tag.
    $i->amOnPage('/');
    $i->seeNumberOfElements('h1', 1);

    // Get all the UW content types.
    $content_types = $i->getContentTypes();

    // Step through each of the content types and ensure
    // that there is only one h1 tag.
    foreach ($content_types as $content_type) {

      // Reset the fields to null so that we do not always
      // add fields.
      $fields = NULL;

      // Can not test sidebar at this time.
      if ($content_type !== 'uw_ct_sidebar') {

        // If this is catalog or sidebar we need to create a
        // term for each to enter.
        if (
          $content_type == 'uw_ct_catalog' ||
          $content_type == 'uw_ct_service'
        ) {

          // Create a term based on the content type.
          switch ($content_type) {

            // Create catalog category.
            case 'uw_ct_catalog':
              $tid = $i->createTerm('uw_vocab_catalogs', $i->uwRandomString());
              $fields['catalog'] = $tid;
              break;

            // Create service category.
            case 'uw_ct_service':
              $tid = $i->createTerm('uw_vocab_service_categories', $i->uwRandomString());
              $fields['service'] = $tid;
              break;
          }

          // Add to the tids array so we delete after.
          $this->tidsUsed[] = $tid;
        }

        // Create a node with the content type.
        $node_title = $i->uwRandomString();
        $this->nodesUsed[$node_title] = $i->createCtNode(
          $content_type,
          $node_title,
          TRUE,
          $fields
        );

        // Get the path to the node.
        $path = $i->getWebPagePath($this->nodesUsed[$node_title]);

        // Go to the node.
        $i->amOnPage($path);

        // Ensure that there is only one h1 tag.
        $i->seeNumberOfElements('h1', 1);
      }
    }
  }

  // phpcs:disable

  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _after(AcceptanceTester $i): void {
    // phpcs:enable

    // If there were terms used, delete them.
    if (!empty($this->tidsUsed)) {

      // Step through each term and delete it.
      foreach ($this->tidsUsed as $tid) {

        // Load the term.
        $term = Drupal::entityTypeManager()
          ->getStorage('taxonomy_term')
          ->load($tid);

        if ($term) {
          // Delete the term.
          $term->delete();
        }
      }
    }

    // Step through each of the nodes used and delete them.
    foreach ($this->nodesUsed as $node) {
      $node->delete();
    }
  }

  // phpcs:disable

  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i): void {
    // phpcs:enable

    // If there were terms used, delete them.
    if (!empty($this->tidsUsed)) {

      // Step through each term and delete it.
      foreach ($this->tidsUsed as $tid) {

        // Load the term.
        $term = Drupal::entityTypeManager()
          ->getStorage('taxonomy_term')
          ->load($tid);

        if ($term) {
          // Delete the term.
          $term->delete();
        }
      }
    }

    // Step through each of the nodes used and delete them.
    foreach ($this->nodesUsed as $node) {
      $node->delete();
    }
  }

}
