<?php

use Codeception\Util\Locator;
use Facebook\WebDriver\WebDriverKeys;

/**
 * Class WcmsTestsBlocksTeaserCest.
 *
 * Tests for teaser blocks.
 */
class WcmsTestsBlocksTeaserCest {

  /**
   * Array used for any tids that we created.
   *
   * @var array
   */
  private $tidsUsed = [];

  /**
   * Array of nodes used.
   *
   * @var array
   */
  private array $nodesUsed = [];

  /**
   * Array of content types.
   *
   * @var array
   */
  private array $contentTypes = [];

  // phpcs:disable
  /**
   * Function to run before the test(s) starts.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _before(AcceptanceTester $i) {
    // phpcs:enable

    // Get the content types for the teaser block.
    $this->contentTypes = $this->getTeaserContentTypes();

    // Get the teaser block content types with machine names.
    $teaser_cts = $this->getTeaserContentTypeMachineNames();

    // Step through each of the teaser content types and
    // create nodes for them.
    foreach ($teaser_cts as $machine_name => $content_type) {

      // Array of any extra fields, need to reset on loop.
      $fields = [];

      // If this is a catalog item, then we need to create
      // a catalog first and track it so that we can delete
      // it once the tests are completed.
      if ($machine_name == 'uw_ct_catalog_item') {

        // Create the catalog term.
        $this->tidsUsed['Catalog'] = $i->createTerm(
          'uw_vocab_catalogs',
          'Catalog1',
        );

        // Add to the extra fields for the node creation.
        $fields['catalog'] = $this->tidsUsed['Catalog'];
      }

      // Create the node.
      $this->nodesUsed[$content_type] = $i->createCtNode(
        $machine_name,
        $content_type,
        TRUE,
        count($fields) > 0 ? $fields : NULL,
      );
    }
  }

  /**
   * Function to test the multi type list block.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testTeaserBlockConfig(AcceptanceTester $i) {

    // Create a webpage.
    $this->nodesUsed['Teaser block'] = $i->createWebPage('Teaser block');

    // Get the path of the webpage.
    $path = $i->getWebPagePath($this->nodesUsed['Teaser block']);

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Go to the layout page for the webpage we created.
    $i->amOnPage($path . '/layout');
    $i->see('Edit layout for ' . $this->nodesUsed['Teaser block']->getTitle());

    // Add a block.
    $i->click('Add block');
    $i->waitForText('Choose a block');

    // Click on the content teaser block.
    $i->click('Content teaser');
    $i->waitForText('Configure block');

    // Test the config for the teaser block.
    $i->see('Heading level');
    $i->seeElement('option[value="h2"][selected="selected"]');
    $i->seeElement('option[value="h3"]');
    $i->seeElement('option[value="h4"]');
    $i->seeElement('option[value="h5"]');
    $i->seeElement('option[value="h6"]');
    $i->see('Type of content');

    // Ensure that all the content types are options and have
    // the correct settings when selected.
    foreach ($this->contentTypes as $machine_name => $content_type) {

      // Ensure that the content type is an option.
      $i->seeElement('option[value="' . $machine_name . '"]');

      // Select the content type and ensure that settings are present.
      $i->selectOption('select[name="settings[content_type]"]', $machine_name);
      $i->see($content_type . ' settings');
      $i->see($content_type);
      $i->seeElement('input[name="settings[' . $machine_name . '][nid]"]');
    }
  }

  /**
   * Function to test the multi type list block.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testTeaserBlock(AcceptanceTester $i) {

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Step through each of the content types and ensure that
    // the teaser block works as expected.
    foreach ($this->contentTypes as $machine_name => $content_type) {

      // Get the title of the teaser block.
      $title = $content_type . ' teaser block';

      // Create a web page for the teaser block to be placed on.
      $this->nodesUsed[$title] = $i->createWebPage(
        $title,
      );

      // Get the path to the webpage.
      $path = $i->getWebPagePath($this->nodesUsed[$title]);

      // Go to the layout page for the webpage we created.
      $i->amOnPage($path . '/layout');
      $i->see('Edit layout for ' . $this->nodesUsed[$title]->getTitle());

      // Add a block.
      $i->click('Add block');
      $i->waitForText('Choose a block');

      // Click on the content teaser block.
      $i->click('Content teaser');
      $i->waitForText('Configure block');

      $i->selectOption('select[name="settings[content_type]"]', $content_type);
      $i->waitForText($content_type . ' settings');

      // Get the field name to fill in.
      $field_name = 'input[name="settings[' . $machine_name . '][nid]"]';

      // Fill the node field with the field name and ensure that
      // the autocomplete works and selects it.
      $i->fillField(
        $field_name,
        $this->nodesUsed[$content_type]->getTitle()
      );
      $i->waitForElement('.ui-menu-item');
      $i->pressKey(
        $field_name,
        WebDriverKeys::ARROW_DOWN
      );
      $i->pressKey(
        $field_name,
        WebDriverKeys::ENTER
      );

      // Click on the add block and ensure that it saves.
      $i->click('Add block');
      $i->waitForText('Content teaser');

      // Ensure that the teaser block is on the page.
      $i->waitForElement('.block-uw-cbl-teaser');

      // Check that the teaser block is on the page.
      $i->seeElement('.block-uw-cbl-teaser');

      // Check that the teaser block title element is
      // on the page.
      $i->seeElement('.block-uw-cbl-teaser h2.card__title a');

      // Ensure that the node title is on the page.
      $i->seeElement(
        Locator::contains(
          'a',
          $this->nodesUsed[$content_type]->getTitle()
        )
      );
    }
  }

  /**
   * Function to get the content types for teaser block.
   *
   * @return string[]
   *   Array of content types for teaser block.
   */
  private function getTeaserContentTypes(): array {

    return [
      'blog' => 'Blog',
      'catalog_item' => 'Catalog item',
      'contact' => 'Contact',
      'event' => 'Event',
      'news_item' => 'News item',
      'opportunity' => 'Opportunity',
      'profile' => 'Profile',
      'project' => 'Project',
      'service' => 'Service',
    ];
  }

  /**
   * Function to get the teaser content type machine names.
   *
   * @return string[]
   *   Array of content types for teaser blocks with machine names.
   */
  private function getTeaserContentTypeMachineNames(): array {

    return [
      'uw_ct_blog' => 'Blog',
      'uw_ct_catalog_item' => 'Catalog item',
      'uw_ct_contact' => 'Contact',
      'uw_ct_event' => 'Event',
      'uw_ct_news_item' => 'News item',
      'uw_ct_opportunity' => 'Opportunity',
      'uw_ct_profile' => 'Profile',
      'uw_ct_project' => 'Project',
      'uw_ct_service' => 'Service',
    ];
  }

  // phpcs:disable

  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _after(AcceptanceTester $i): void {
    // phpcs:enable

    // Complete the block tests.
    $i->completeBlockTests($this->tidsUsed, $this->nodesUsed);
  }

  // phpcs:disable

  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i): void {
    // phpcs:enable

    // Complete the block tests.
    $i->completeBlockTests($this->tidsUsed, $this->nodesUsed);
  }

}
