<?php

use Step\Acceptance\ContentType;

/**
 * Class WcmsTestsContentTypeServiceCest.
 *
 * Tests for service.
 */
class WcmsTestsContentTypeServiceCest {

  /**
   * The content type.
   *
   * @var string
   */
  private string $contentType = 'uw_ct_service';

  /**
   * Array of tids that were used in the test.
   *
   * @var array
   */
  private $tidsUsed = [];

  /**
   * Tests for service content type.
   *
   * @param Step\Acceptance\ContentType $i
   *   Acceptance test variable.
   */
  public function testServiceContentType(ContentType $i): void {

    // The service category taxonomy term.
    $service_category = $i->uwRandomString();

    // Create a catalog term and get tid for later use.
    $this->tidsUsed[] = $i->createTerm(
      'uw_vocab_service_categories',
      $service_category
    );

    // Test the fields in the content type.
    $i->testContentType(
      'Service',
      $i->getContentTypeFields($this->contentType)
    );

    // Get the edits for the content type.
    $edits = $i->getContentTypeEdits($this->contentType);

    // Selector for service category.
    $selector = 'edit-field-uw-service-category-0-' . $this->tidsUsed[0] . '-' . $this->tidsUsed[0];

    // Adding the service category checkbox to the edits.
    $edits['checkbox'] = [
      $selector => $service_category,
    ];

    // Test the content type edits.
    $i->testContentTypeEdits(
      $this->contentType,
      $edits
    );
  }

  // phpcs:disable
  /**
   * Function to run if the test fails.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i): void {
    // phpcs:enable

    // If there were terms used, delete them.
    if (!empty($this->tidsUsed)) {

      // Step through each term and delete it.
      foreach ($this->tidsUsed as $tid) {

        // Load the term.
        $term = Drupal::entityTypeManager()
          ->getStorage('taxonomy_term')
          ->load($tid);

        if ($term) {
          // Delete the term.
          $term->delete();
        }
      }
    }

    // Delete all the blocks.
    $i->deleteAllBlocks();
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _after(AcceptanceTester $i): void {
    // phpcs:enable

    // If there were terms used, delete them.
    if (!empty($this->tidsUsed)) {

      // Step through each term and delete it.
      foreach ($this->tidsUsed as $tid) {

        // Load the term.
        $term = Drupal::entityTypeManager()
          ->getStorage('taxonomy_term')
          ->load($tid);

        if ($term) {
          // Delete the term.
          $term->delete();
        }
      }
    }

    // Delete all the blocks.
    $i->deleteAllBlocks();
  }

}
