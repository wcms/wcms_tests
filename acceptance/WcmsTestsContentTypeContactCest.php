<?php

use Codeception\Util\Locator;
use Step\Acceptance\ContentType;

/**
 * Class WcmsTestsContentTypeContactCest.
 *
 * Tests for contact content type.
 */
class WcmsTestsContentTypeContactCest {

  /**
   * The content type.
   *
   * @var string
   */
  private string $contentType = 'uw_ct_contact';

  /**
   * Array used for any tids that we created.
   *
   * @var array
   */
  private $tidsUsed = [];

  /**
   * Array of nodes used.
   *
   * @var array
   */
  private array $nodesUsed = [];

  /**
   * Tests for contact content type.
   *
   * @param Step\Acceptance\ContentType $i
   *   Acceptance test variable.
   */
  public function testContactContentType(ContentType $i) {

    // Test the content type.
    $i->testContentType(
      'Contact',
      $i->getContentTypeFields($this->contentType)
    );

    // Test the content type edits.
    $i->testContentTypeEdits(
      $this->contentType,
      $i->getContentTypeEdits($this->contentType)
    );
  }

  /**
   * Function to test contact content type settings.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testContactSettings(AcceptanceTester $i) {

    // Login as administrator.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');

    // Check rearrange contacts.
    $i->amOnPage('admin/rearrange-contacts');
    $i->see('Rearrange contacts');
    $i->see('Published');
    $i->seeElement('option[value="All"][selected="selected"]');
    $i->seeElement('input[value="Apply filters"]');

    // Check dashboard contact rearrange link.
    $i->amOnPage('dashboard/my_dashboard');
    $i->see('My Dashboard');
    $i->seeElementInDom(Locator::contains('ul.dropbutton a[href="/admin/rearrange-contacts"]', 'Rearrange'));

    // Make sure site manager's rearrange is checked.
    $i->amOnPage('admin/content-access');
    $i->see('Content access');
    $i->seeCheckboxIsChecked('#edit-permissions-contact-rearrange-site-manager');
    $i->dontSeeCheckboxIsChecked('#edit-permissions-contact-rearrange-content-editor');
    $i->dontSeeCheckboxIsChecked('#edit-permissions-contact-rearrange-content-author');

    // Check pathauto pattern to make sure they are in same row.
    $i->amOnPage('admin/config/search/path/patterns');
    $i->see('Patterns');
    $elements = 'div[id="block-uw-theme-admin-mainpagecontent"] table tbody tr[data-drupal-selector="edit-entities-uw-path-contact"] td';
    $expected_items = [
      'Contact path pattern',
      'contacts/[node:title]',
      'Content',
      'Content type is uw_ct_contact',
    ];
    foreach ($expected_items as $expected_item) {
      $i->seeElement(Locator::contains($elements, $expected_item));
    }
  }

  /**
   * Function to test contact reordering.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testContactReorder(AcceptanceTester $i) {

    // Create 5 contacts.
    for ($x = 1; $x <= 5; $x++) {
      $this->nodesUsed['Contact' . $x] = $i->createCtNode(
        $this->contentType,
        'Contact' . $x,
        TRUE,
        ['field_uw_ct_contact_sort_name' => 'Contact' . $x]
      );
    }

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Go to rearange contacts page.
    $i->amOnPage('admin/rearrange-contacts');
    $i->see('Rearrange contacts');

    // Check contacts are in correct order.
    for ($x = 1; $x <= 5; $x++) {
      $i->see('Contact' . $x, Locator::elementAt('//table/tbody/tr', $x));
    }

    // Rearrange contacts.
    $newOrder = [
      1 => 5,
      2 => 4,
      3 => 2,
      4 => 3,
      5 => 1,
    ];

    $i->click('Show row weights');

    // Set weights for all contacts.
    foreach ($newOrder as $contactNum => $newPos) {
      $x = $contactNum - 1;
      $i->fillField('#edit-draggableviews-' . $x . '-weight', $newPos);
    }

    $i->click('#edit-save-order');
    $i->click('Hide row weights');

    // Check contacts are in new order.
    foreach ($newOrder as $contactNum => $newPos) {
      $i->see('Contact' . $contactNum, Locator::elementAt('//table/tbody/tr', $newPos));
    }

    // Reset Order.
    $i->click('#edit-reset-order');

    // Check contacts are in original order.
    for ($x = 1; $x <= 5; $x++) {
      $i->see('Contact' . $x, Locator::elementAt('//table/tbody/tr', $x));
    }
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _after(AcceptanceTester $i): void {
    // phpcs:enable

    // Compelte the block tests.
    $i->completeBlockTests($this->tidsUsed, $this->nodesUsed);
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i): void {
    // phpcs:enable

    // Compelte the block tests.
    $i->completeBlockTests($this->tidsUsed, $this->nodesUsed);
  }

}
