<?php

/**
 * Class MenusCest.
 *
 * Tests for menus.
 */
class WcmsTestsMenusCest {

  /**
   * Tests for content management menu.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testContentManagementMenu(AcceptanceTester $i) {

    // Login as administrator.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');

    // Go the content menu links edit page.
    $i->amOnPage('admin/structure/menu/manage/uw-menu-content-management');
    $i->see('Edit menu Content management');

    // Get the content menu links.
    $content_links = $this->getContentMenuLinks();

    // Step through and ensure the links exist.
    foreach ($content_links as $key => $item) {
      $i->seeLink($item);
      $i->seeElement('a[href$="' . $key . '"]');
    }
  }

  /**
   * Tests for site information menu.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testSiteInformationMenu(AcceptanceTester $i) {

    // Login as administrator.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');

    // Go the site information menu links edit page.
    $i->amOnPage('admin/structure/menu/manage/uw-menu-site-management');
    $i->see('Edit menu Site information');

    // Get the site information menu links.
    $site_links = $this->getSiteInformationMenuLinks();

    // Step through and ensure the links exist.
    foreach ($site_links as $key => $item) {
      $i->seeLink($item);
      $i->seeElement('a[href$="' . $key . '"]');
    }
  }

  /**
   * Tests adding menu links with various roles.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testAddingMenuLinks(AcceptanceTester $i) {

    // Roles to test with.
    $roles = [
      'uw_role_site_owner',
      'uw_role_site_manager',
      'uw_role_content_editor',
      'uw_role_content_author',
    ];

    // Step through each of the roles and test adding menu links.
    foreach ($roles as $role) {

      // Login with role.
      $i->amOnPage('user/logout');
      $i->logInWithRole($role);

      // Check can't add menu link through list of menus.
      $i->amOnPage('admin/structure/menu');
      $i->dontSeeElement('ul[class="dropbutton"] li a[href="admin/structure/menu/manage/main/add"]');
      $i->dontSeeElement('ul[class="dropbutton"] li a[href="admin/structure/menu/manage/uw-menu-audience-menu/add"]');

      // Path to 'Main navigation' menu.
      $i->amOnPage('admin/structure/menu/manage/main');
      $i->dontSeeLink('Add menu');

      // Path to add menu for 'Main navigation' menu.
      $i->amOnPage('admin/structure/menu/manage/main/add');
      $i->see('You are not authorized to access this page.');

      // Path to add link for 'Information for' menu.
      $i->amOnPage('admin/structure/menu/manage/uw-menu-audience-menu/add');
      $i->see('You are not authorized to access this page.');
    }
  }

  /**
   * Tests for home menu link weight.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testHomeMenuLinkWeight(AcceptanceTester $i) {

    // Login as administrator.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');

    // Go the home.
    $i->amOnPage('admin/structure/menu/link/uw_base_profile.front_page/edit');
    $i->see('Edit menu link Home');

    // Test for weight of -51.
    $i->seeElement('input#edit-weight[value="-51"]');
  }

  /**
   * Tests for main menu.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testMenuMain(AcceptanceTester $i) {

    // Path for the home page.
    $path = '/admin/structure/menu/link/uw_base_profile.front_page/edit';

    // Login as administrator.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');

    // Go the home.
    $i->amOnPage('admin/structure/menu/manage/main');
    $i->see('Edit menu Main navigation');

    // Test that administrator can edit home link.
    $i->seeElement('ul[class="dropbutton"] li a[href="' . $path . '"]');

    // Check that catalogs is disabled.
    $i->see('Catalogs (disabled)');

    // Load the catalog menu link.
    $menu_link = current(\Drupal::entityTypeManager()
      ->getStorage('menu_link_content')
      ->loadByProperties(['title' => 'Catalogs']));

    // Get the selector for the enabled checkbox for catalogs
    // menu link.
    $selector = '#edit-links-menu-plugin-idmenu-link-content';
    $selector .= $menu_link->uuid();
    $selector .= '-enabled';

    // Ensure that catalogs is not enabled.
    $i->dontSeeCheckboxIsChecked($selector);
  }

  /**
   * Tests for home main menu access.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testMenuMainAccess(AcceptanceTester $i) {

    // Path for the home page.
    $path = 'admin/structure/menu/link/uw_base_profile.front_page/edit';

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Go the home.
    $i->amOnPage('admin/structure/menu/manage/main');
    $i->see('Edit menu Main navigation');

    // Ensure that other roles can not edit home link.
    $i->dontSeeElement('ul[class="dropbutton"] li a[href="' . $path . '"]');

    // Try to edit the home link.
    $i->amOnPage($path);

    // Ensure that they can not access the home page link edit.
    $i->see('You are not authorized to access this page.');

    // Menu add item link.
    $i->amOnPage('admin/structure/menu');

    // Path to check for.
    $path = 'admin/structure/menu/manage/main/add';

    // Ensure that other roles can not add menu links.
    $i->dontSeeElement('ul[class="dropbutton"] li a[href="' . $path . '"]');

    // Try to add a menu link.
    $i->amOnPage($path);

    // Ensure that they can not access the add menu link.
    $i->see('You are not authorized to access this page.');
  }

  /**
   * Function to get the content menu links.
   *
   * @return string[]
   *   Array of content menu links.
   */
  private function getContentMenuLinks(): array {

    return [
      'node/add/uw_ct_blog' => 'Blog post',
      'admin/structure/taxonomy/manage/uw_vocab_blog_tags/add' => 'Add blog tag',
      'admin/structure/taxonomy/manage/uw_vocab_blog_tags/overview' => 'List blog tags',
      'node/add/uw_ct_catalog_item' => 'Catalog item',
      'admin/structure/taxonomy/manage/uw_vocab_catalog_categories/add' => 'Add catalog category',
      'admin/structure/taxonomy/manage/uw_vocab_catalogs/add' => 'Add catalog',
      'admin/structure/taxonomy/manage/uw_vocab_catalog_categories/overview' => 'List catalog categories',
      'admin/structure/taxonomy/manage/uw_vocab_catalogs/overview' => 'List catalogs',
      'node/add/uw_ct_event' => 'Event',
      'admin/structure/taxonomy/manage/uw_tax_event_tags/add' => 'Add event tag',
      'admin/structure/taxonomy/manage/uw_tax_event_type/add' => 'Add event type',
      'admin/structure/taxonomy/manage/uw_tax_event_tags/overview' => 'List event tags',
      'admin/structure/taxonomy/manage/uw_tax_event_type/overview' => 'List event types',
      'node/add/uw_ct_news_item' => 'News item',
      'admin/structure/taxonomy/manage/uw_vocab_news_tags/add' => 'Add news tag',
      'admin/structure/taxonomy/manage/uw_vocab_news_tags/overview' => 'List news tags',
      'node/add/uw_ct_sidebar' => 'Sidebar',
      'node/add/uw_ct_site_footer' => 'Site footer',
      'node/add/uw_ct_web_page' => 'Web page',
      'admin/structure/webform/add' => 'Form',
      'admin/structure/webform/submissions/manage' => 'Manage submissions',
      'admin/structure/taxonomy/manage/uw_vocab_audience/add' => 'Audience',
      'admin/structure/taxonomy/manage/uw_vocab_audience/overview' => 'List terms',
      /* Conflicting links Manage, there is one found in admin top bar.
      'admin/structure/taxonomy/manage/uw_vocab_audience' => 'Manage',
       */
    ];
  }

  /**
   * Function to get the site information menu links.
   *
   * @return string[]
   *   Array of site information menu links.
   */
  private function getSiteInformationMenuLinks(): array {

    return [
      'admin/content-access' => 'Content access',
      'admin/config/search/redirect/404' => 'Fix 404 pages',
      'admin/structure/menu' => 'Menus',
      'admin/config/user-interface/shortcut' => 'Shortcuts',
      'admin/config/search/redirect' => 'Redirects',
      'https://uwaterloo.atlassian.net/servicedesk/customer/portal/117' => 'Submit a ticket',
      'https://uwaterloo.atlassian.net/wiki/spaces/WCMSKB/pages/43447124426/WCMS+3+how-to+documents' => 'WCMS how-to documents',
    ];

  }

}
