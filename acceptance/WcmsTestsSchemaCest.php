<?php

/**
 * Class SchemaCest.
 *
 * Tests for schemas.
 */
class WcmsTestsSchemaCest {

  /**
   * Tests for schema modules.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function schemaModulesTest(AcceptanceTester $i) {

    // Make sure the below modules are enabled.
    $modules = [
      'schema_article' => 'Schema.org Article',
      'schema_event' => 'Schema.org Event',
      'schema_metatag' => 'Schema.org Metatag',
      'schema_organization' => 'Schema.org Organization',
      'schema_service' => 'Schema.org Service',
      'schema_web_page' => 'Schema.org WebPage',
    ];

    // Step through each module and ensure that
    // it is enabled.
    foreach ($modules as $key => $module) {
      $enabled = \Drupal::moduleHandler()->moduleExists($key);
      $i->assertTrue($enabled, $module . ' module is not enabled.');
    }
  }

  /**
   * Tests for schema settings.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function schemaSettingsTest(AcceptanceTester $i) {

    // Login as administrator.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');

    // Go to the metatag config page.
    $i->amOnPage('/admin/config/search/metatag/settings');
    $i->see('Configure the Metatag module');

    // Get the settings for metatag.
    $settings = $this->getMetaSettings();

    // Step through the content types and settings
    // and test all the settings.
    foreach ($settings as $content_type => $content_type_settings) {

      // Get the selector for the details tab.
      $selector = '#edit-entity-type-groups-node-' . str_replace('_', '-', $content_type);

      // Scroll to the details tab and open it.
      $i->scrollTo($selector, 0, -100);
      $i->click($selector);

      foreach ($content_type_settings as $setting => $checked) {

        // Get the name of the checkbox.
        $name = 'entity_type_groups[node][';
        $name .= $content_type;
        $name .= '][0][';
        $name .= $setting;
        $name .= ']';

        // Test if checkbox checked or not based
        // on the settings.
        if ($checked) {
          $i->seeCheckboxIsChecked($name);
        }
        else {
          $i->dontSeeCheckboxIsChecked($name);
        }
      }

      // Close the details tab.
      $i->click($selector);
    }
  }

  /**
   * Tests for schema content type fields.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function schemaContentTypeFieldsTest(AcceptanceTester $i) {

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Make sure meta tag info fields exist when
    // add a node of the given content type.
    $types = $i->getContentTypes(TRUE);

    // Step through each content type and check
    // for meta tab fields.
    foreach ($types as $type) {
      $i->amOnPage('node/add/' . $type);
      $i->see('Create ');
      $i->seeElement('#edit-field-uw-meta-description-0-value');
      $i->seeElement('#edit-field-uw-meta-image-open-button');
    }
  }

  /**
   * Function to get the content type meta tag settings.
   *
   * @return array[]
   *   Array of content type meta tag settings.
   */
  private function getMetaSettings(): array {

    // The settings for meta tags per content type.
    $settings = [
      'uw_ct_blog' => [
        'advanced' => TRUE,
        'basic' => TRUE,
        'open_graph' => TRUE,
        'schema_article' => TRUE,
        'schema_event' => FALSE,
        'schema_job_posting' => FALSE,
        'schema_organization' => FALSE,
        'schema_person' => FALSE,
        'schema_service' => FALSE,
        'schema_web_page' => FALSE,
        'twitter_cards' => TRUE,
      ],
      'uw_ct_catalog_item' => [
        'advanced' => TRUE,
        'basic' => TRUE,
        'open_graph' => TRUE,
        'schema_article' => FALSE,
        'schema_event' => FALSE,
        'schema_job_posting' => FALSE,
        'schema_organization' => FALSE,
        'schema_person' => FALSE,
        'schema_service' => FALSE,
        'schema_web_page' => TRUE,
        'twitter_cards' => TRUE,
      ],
      'uw_ct_contact' => [
        'advanced' => TRUE,
        'basic' => TRUE,
        'open_graph' => TRUE,
        'schema_article' => FALSE,
        'schema_event' => FALSE,
        'schema_job_posting' => FALSE,
        'schema_organization' => FALSE,
        'schema_person' => TRUE,
        'schema_service' => FALSE,
        'schema_web_page' => FALSE,
        'twitter_cards' => TRUE,
      ],
      'uw_ct_event' => [
        'advanced' => TRUE,
        'basic' => TRUE,
        'open_graph' => TRUE,
        'schema_article' => FALSE,
        'schema_event' => TRUE,
        'schema_job_posting' => FALSE,
        'schema_organization' => FALSE,
        'schema_person' => FALSE,
        'schema_service' => FALSE,
        'schema_web_page' => FALSE,
        'twitter_cards' => TRUE,
      ],
      'uw_ct_news_item' => [
        'advanced' => TRUE,
        'basic' => TRUE,
        'open_graph' => TRUE,
        'schema_article' => TRUE,
        'schema_event' => FALSE,
        'schema_job_posting' => FALSE,
        'schema_organization' => FALSE,
        'schema_person' => FALSE,
        'schema_service' => FALSE,
        'schema_web_page' => FALSE,
        'twitter_cards' => TRUE,
      ],
      'uw_ct_expand_collapse_group' => [
        'advanced' => FALSE,
        'basic' => FALSE,
        'open_graph' => FALSE,
        'schema_article' => FALSE,
        'schema_event' => FALSE,
        'schema_job_posting' => FALSE,
        'schema_organization' => FALSE,
        'schema_person' => FALSE,
        'schema_service' => FALSE,
        'schema_web_page' => FALSE,
        'twitter_cards' => FALSE,
      ],
      'uw_ct_opportunity' => [
        'advanced' => TRUE,
        'basic' => TRUE,
        'open_graph' => TRUE,
        'schema_article' => FALSE,
        'schema_event' => FALSE,
        'schema_job_posting' => TRUE,
        'schema_organization' => FALSE,
        'schema_person' => FALSE,
        'schema_service' => FALSE,
        'schema_web_page' => FALSE,
        'twitter_cards' => TRUE,
      ],
      'uw_ct_profile' => [
        'advanced' => TRUE,
        'basic' => TRUE,
        'open_graph' => TRUE,
        'schema_article' => FALSE,
        'schema_event' => FALSE,
        'schema_job_posting' => FALSE,
        'schema_organization' => FALSE,
        'schema_person' => TRUE,
        'schema_service' => FALSE,
        'schema_web_page' => FALSE,
        'twitter_cards' => TRUE,
      ],
      'uw_ct_project' => [
        'advanced' => TRUE,
        'basic' => TRUE,
        'open_graph' => TRUE,
        'schema_article' => FALSE,
        'schema_event' => FALSE,
        'schema_job_posting' => FALSE,
        'schema_organization' => FALSE,
        'schema_person' => FALSE,
        'schema_service' => FALSE,
        'schema_web_page' => TRUE,
        'twitter_cards' => TRUE,
      ],
      'uw_ct_service' => [
        'advanced' => TRUE,
        'basic' => TRUE,
        'open_graph' => TRUE,
        'schema_article' => FALSE,
        'schema_event' => FALSE,
        'schema_job_posting' => FALSE,
        'schema_organization' => FALSE,
        'schema_person' => FALSE,
        'schema_service' => TRUE,
        'schema_web_page' => TRUE,
        'twitter_cards' => TRUE,
      ],
      'uw_ct_sidebar' => [
        'advanced' => FALSE,
        'basic' => FALSE,
        'open_graph' => FALSE,
        'schema_article' => FALSE,
        'schema_event' => FALSE,
        'schema_job_posting' => FALSE,
        'schema_organization' => FALSE,
        'schema_person' => FALSE,
        'schema_service' => FALSE,
        'schema_web_page' => FALSE,
        'twitter_cards' => FALSE,
      ],
      'uw_ct_site_footer' => [
        'advanced' => FALSE,
        'basic' => FALSE,
        'open_graph' => FALSE,
        'schema_article' => FALSE,
        'schema_event' => FALSE,
        'schema_job_posting' => FALSE,
        'schema_organization' => FALSE,
        'schema_person' => FALSE,
        'schema_service' => FALSE,
        'schema_web_page' => FALSE,
        'twitter_cards' => FALSE,
      ],
      'uw_ct_web_page' => [
        'advanced' => TRUE,
        'basic' => TRUE,
        'open_graph' => TRUE,
        'schema_article' => FALSE,
        'schema_event' => FALSE,
        'schema_job_posting' => FALSE,
        'schema_organization' => FALSE,
        'schema_person' => FALSE,
        'schema_service' => FALSE,
        'schema_web_page' => TRUE,
        'twitter_cards' => TRUE,
      ],
    ];

    return $settings;
  }

}
