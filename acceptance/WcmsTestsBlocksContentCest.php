<?php

use Codeception\Util\Locator;

/**
 * Class WcmsTestsBlocksContentCest.
 *
 * Tests for content blocks.
 */
class WcmsTestsBlocksContentCest {

  /**
   * Array used for any tids that we created.
   *
   * @var array
   */
  private $tidsUsed = [];

  /**
   * Array of nodes used.
   *
   * @var array
   */
  private array $nodesUsed;

  /**
   * Tests for banner block.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testBannerImagesBlock(AcceptanceTester $i) {

    // Create a webpage.
    $this->nodesUsed['Banner images block'] = $i->createWebPage('Banner images block');

    // Get the path of the webpage.
    $path = $i->getWebPagePath($this->nodesUsed['Banner images block']);

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Go to the layout page for the webpage we created.
    $i->amOnPage($path . '/layout');
    $i->see('Edit layout for ' . $this->nodesUsed['Banner images block']->getTitle());

    // Add a block.
    $i->click('Add block');
    $i->waitForText('Choose a block');

    // Click on the banner block.
    $i->click('Banner images');
    $i->waitForText('Configure block');

    // Ensure that banner image required.
    $i->seeElement(Locator::contains('div[id^="edit-settings-block-form-field-uw-banner-item"] strong[class="form-required"]', 'Banner'));

    // Ensure that no banner added yet on the screen.
    $i->see('No Banner added yet.');

    // Check 'Add Image banner', 'Add Local video banner'
    // and 'Add Vimeo video banner'.
    $elements = [
      'Image',
      'Local video',
    ];

    // Step through each of the banner types and ensure
    // that they are present.
    foreach ($elements as $element) {
      $i->seeElementInDOM('input[type="submit"][value="Add ' . $element . ' banner"]');
    }

    // Dont see the Vimeo option.
    $i->dontSeeElementInDOM('input[type="submit"][value="Add Vimeo video banner"]');

    // Check 'Settings' group.
    // 'Settings' is required.
    $i->seeElement(Locator::contains('details[id^="edit-settings-block-form-group-uw-settings"]', 'Settings'));
    $i->seeElement(Locator::contains('details.required-fields', 'Settings'));
    $i->seeElement('details[id^="edit-settings-block-form-group-uw-settings"] summary.form-required');

    // Open the settings details.
    $i->click('details[id^="edit-settings-block-form-group-uw-settings"]');
    $i->waitForText('These settings apply when more than one banner image is used.');

    // Slide speed is required and has correct help text.
    $i->seeElement('label[for^="edit-settings-block-form-field-uw-slide-speed"][class*="form-required"]');
    $i->seeElement('input[id^="edit-settings-block-form-field-uw-slide-speed"][value="7000"]');
    $i->see('Enter the time, in milliseconds, that should elapse between slides when the slideshow is playing.');

    // Transition speed is required and has correct help text.
    $i->seeElement('label[for^="edit-settings-block-form-field-uw-transition-speed"][class*="form-required"]');
    $i->seeElement('input[id^="edit-settings-block-form-field-uw-transition-speed"][value="400"]');
    $i->see('Enter the time, in milliseconds, the banner transition should take to complete.');

    // Autoplay is checked by default.
    $i->seeCheckboxIsChecked('input[id^="edit-settings-block-form-field-uw-autoplay-value"]');
    $i->see('Controls whether or not the slideshow starts immediately on page load.');

    // 'Text overlay style' is required.
    $i->seeElement('label[for^="edit-settings-block-form-field-uw-text-overlay-style"][class*="form-required"]');

    // The selected list match the expected list.
    $expected_options = [
      'full-width' => 'Full banner width, bottom, theme colour background ("FDSU" style)',
      'left-dark' => 'Left side of banner, vertically centered, faded background, white text (for dark images)',
      'left-light' => 'Left side of banner, vertically centered, faded background, black text (for light images)',
      'split' => 'Split top and bottom, black and white backgrounds ("single page" style)',
      'full-overlay' => 'Full black overlay, centered text ("conference" style)',
    ];

    // Step through each of options and ensure on screen.
    foreach ($expected_options as $value => $expected_option) {

      // The selector for the option.
      $option = 'option[value="' . $value . '"]';

      // If full width option, ensure that it is selected.
      if ($value == 'full-width') {
        $option .= '[selected="selected"]';
      }

      // Ensure that element is on the page.
      $i->seeElement($option);
    }

    // Full banner width, bottom, theme colour background
    // ("FDSU" style) is selected as default.
    $i->see('All options other than "conference" style are now supported. "Conference" is provided for future use.');

    $banners = [
      'Image' => 'edit-settings-block-form-field-uw-banner-item-add-more-add-more-button-uw-para-image-banner',
      'Local video' => 'edit-settings-block-form-field-uw-banner-item-add-more-add-more-button-uw-para-local-video-banner',
    ];

    // The fields to check for.
    $fields = [
      'Link' => [
        'help_text' => 'Provide an optional link for this banner.',
        'id' => 'field-uw-ban-link',
      ],
      'Big text' => [
        'help_text' => 'Big text appears at a larger size, and, if both are set, above the small text.',
        'id' => 'uw-ban-big-text',
      ],
      'Small text' => [
        'help_text' => 'Small text appears at a smaller size, and, if both are set, below the big text.',
        'id' => 'field-uw-ban-small-text',
      ],
    ];

    // Steo thorough each banner type and test elements.
    foreach ($banners as $banner_name => $banner_id) {

      // If not on a image banner, click the drop down list button.
      if ($banner_name !== 'Image') {
        $i->click('.dropbutton-multiple button');
      }

      // Click Add banner type button'.
      $i->waitForElement('input[id^="' . $banner_id . '"]');
      $i->click('input[id^="' . $banner_id . '"]');
      $i->waitForText($banner_name . ' banner');

      // Ensure that elements for banner exist.
      if ($banner_name == 'Local video') {
        $i->seeElement(Locator::contains('span[class="fieldset-legend js-form-required form-required"]', 'Video'));
      }
      else {
        $i->seeElement(Locator::contains('span[class="fieldset-legend js-form-required form-required"]', $banner_name));
      }
      $i->see('No media items are selected.');
      $i->seeElement('input[id^="edit-settings-block-form-field-uw-banner-item"][value="Add media"]');

      // Steo through each field and ensure that it is correct.
      foreach ($fields as $field_name => $field) {
        $i->see($field_name);
        foreach ($field as $index => $value) {
          if ($index == 'help_text') {
            if ($field_name == 'Big text' && $banner_name == 'Local video') {
              $i->see('Provide an optional link for this banner.');
            }
            else {
              $i->see($value);
            }
          }
          else {
            $i->seeElement('input[id*="' . $value . '"]');
          }
        }
      }

      // Remove the banner.
      $i->click('input[id^="edit-settings-block-form-field-uw-banner-item"]');
      $i->waitForText('Deleted Banner: ' . $banner_name . ' banner');
      $i->click('input[id^="edit-settings-block-form-field-uw-banner-item"]');
      $i->waitForText('No Banner added yet.');
    }

  }

  /**
   * Function to test for Vimeo banners.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testBannerVimeoBlock(AcceptanceTester $i) {

    // Create a webpage.
    $this->nodesUsed['Banner vimeo block'] = $i->createWebPage('Banner vimeo block');

    // Get the path of the webpage.
    $path = $i->getWebPagePath($this->nodesUsed['Banner vimeo block']);

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');

    // Go to the third party video settings.
    $i->amOnPage('/admin/third-party-video-banner-settings');

    // Ensure that check box is on the page.
    $i->seeElementInDOM('input[id="edit-video-banner-enabled"]');

    // Click on the button to enable vimeo.
    $i->click('input[id="edit-video-banner-enabled"]');
    $i->click('input[id="edit-submit"]');

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Go to the layout page for the webpage we created.
    $i->amOnPage($path . '/layout');
    $i->see('Edit layout for ' . $this->nodesUsed['Banner vimeo block']->getTitle());

    // Add a block.
    $i->click('Add block');
    $i->waitForText('Choose a block');

    // Click on the banner block.
    $i->click('Banner images');
    $i->waitForText('Configure block');

    // Ensure that banner image required.
    $i->seeElement(Locator::contains('div[id^="edit-settings-block-form-field-uw-banner-item"] strong[class="form-required"]', 'Banner'));

    $i->seeElementInDOM('input[type="submit"][value="Add Vimeo video banner"]');

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');

    // Go to the third party video settings.
    $i->amOnPage('/admin/third-party-video-banner-settings');

    // Click on the button to disable vimeo.
    $i->click('input[id="edit-video-banner-enabled"]');
    $i->click('input[id="edit-submit"]');
  }

  /**
   * Function to test the copy text block.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testCopyTextBlock(AcceptanceTester $i) {

    // Html tags to test.
    $htmlTags = [
      'strong' => '</strong>',
      'em' => '</em>',
      's' => '</s>',
      'sup' => '</sup>',
      'sub' => '</sub>',
      'mark' => '</mark>',
      'p' => '</p>',
    ];

    // Create a webpage.
    $this->nodesUsed['Copy Text Block'] = $i->createWebPage('Copy Text Block');

    // Get the path of the webpage.
    $path = $i->getWebPagePath($this->nodesUsed['Copy Text Block']);

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Test each option.
    foreach ($htmlTags as $tag => $close) {

      // Go to the layout page for the webpage we created.
      $i->amOnPage($path . '/layout');
      $i->see('Edit layout for ' . $this->nodesUsed['Copy Text Block']->getTitle());

      // Add a block.
      $i->click('Add block');
      $i->waitForText('Choose a block');

      // Click on the copy text block.
      $i->click('Copy text');
      $i->waitForText('Configure block');

      // Text to be used.
      $copy_text = $i->uwRandomString();

      // Adds tags to text and xpath selector.
      $tagged_text = '<' . $tag . '>' . $copy_text . $close;

      // Check that options exist.
      $i->see('Text alignment');
      $i->seeOptionIsSelected('layout_builder_style_uw_lbs_grp_text_alignment', 'Left');
      $i->dontSeeOptionIsSelected('layout_builder_style_uw_lbs_grp_text_alignment', 'Center');
      $i->dontSeeOptionIsSelected('layout_builder_style_uw_lbs_grp_text_alignment', 'Right');

      // Fill the title field.
      $i->fillField('settings[label]', $copy_text);

      // Click on source and fill in the ck editor field with the tagged text.
      $i->click('Source');
      $i->fillCkEditor($tagged_text, 'textarea[name="settings[block_form][field_uw_copy_text][0][value]"]');

      // Submit form.
      $i->click('input[id*="edit-actions-submit"]');

      // Ensure that the tag is on the page.
      $i->waitForElement('.uw-copy-text div ' . $tag);
      $i->see($copy_text, '.uw-copy-text div ' . $tag);

      // Remove added block to start again.
      $i->amOnPage($path . '/layout/discard-changes');
      $i->click('#edit-submit');
      $i->waitForText('The changes to the layout have been discarded.');
    }
  }

  /**
   * Function to test the CTA block.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   *
   * @throws Exception
   */
  public function testCtaBlock(AcceptanceTester $i) {

    // Create a webpage.
    $this->nodesUsed['CTA Block'] = $i->createWebPage('CTA Block');

    // Get the path of the webpage.
    $path = $i->getWebPagePath($this->nodesUsed['CTA Block']);

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Go to the layout page for the webpage we created.
    $i->amOnPage($path . '/layout');
    $i->see('Edit layout for ' . $this->nodesUsed['CTA Block']->getTitle());

    // Add a block.
    $i->click('Add block');
    $i->waitForText('Choose a block');

    // Click on the call to action block.
    $i->click('Call to action');
    $i->waitForText('Configure block');

    // Check for form elements.
    $i->seeElement(Locator::contains('label', 'Link'));
    $i->seeElement(Locator::contains('label', 'Theme'));

    // Ensure that we see the correct number of required fields.
    $i->seeElement(Locator::contains('label.form-required', 'Title'));
    $i->seeElement(Locator::contains('h4.form-required', 'Details'));
    $i->seeElement(Locator::contains('label.form-required', 'Link'));
    $i->seeElement(Locator::contains('h4.form-required', 'Text details'));
    $i->seeElement(Locator::contains('label.form-required', 'Theme'));

    // Ensure that text size options are available.
    $i->seeElement('option[value="small"]');
    $i->seeElement('option[value="medium"]');
    $i->seeElement('option[value="big"]');

    // Get that settings for this block.
    $title = $i->uwRandomString();
    $cta_text = $i->uwRandomString();
    $cta_link = '/home';
    $theme_option = 'org-default';

    // Fill in the fields.
    $i->fillField('settings[label]', $title);
    $i->fillField('settings[block_form][field_uw_cta_details][0][subform][field_uw_cta_link][0][uri]', $cta_link);
    $i->selectOption('settings[block_form][field_uw_cta_details][0][subform][field_uw_cta_text_details][0][style]', 'Small');
    $i->fillField('settings[block_form][field_uw_cta_details][0][subform][field_uw_cta_text_details][0][text_value]', $cta_text);
    $i->selectOption('settings[block_form][field_uw_cta_details][0][subform][field_uw_cta_theme]', 'Black and gold (uWaterloo)');

    // Click on add block and ensure it is on the screen.
    $i->click('Add block');
    $i->waitForElement(Locator::contains('div[class="uw-admin-label"]', 'Call to action'));
    $i->seeElement(Locator::contains('div[class="uw-admin-label"]', 'Call to action'));

    // Check that elements are on the page.
    $i->see($title);
    $i->seeElement('.uw-cta');
    $i->seeElement('aside.' . $theme_option);
    $i->seeElement('a[href="' . $cta_link . '"]');
    $i->seeElement(Locator::contains('.uw-cta__text--small', $cta_text));
  }

  /**
   * Function to test the expand/collapse block.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testExpandCollapseBlock(AcceptanceTester $i) {

    // Create a webpage.
    $this->nodesUsed['Expand Collapse Block'] = $i->createWebPage('Expand Collapse Block');

    // Get the path of the webpage.
    $path = $i->getWebPagePath($this->nodesUsed['Expand Collapse Block']);

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Create an expand/collapse group node.
    $exp_col_title = $i->uwRandomString();
    $this->nodesUsed[$exp_col_title] = $i->createCtNode(
      'uw_ct_expand_collapse_group',
      $exp_col_title,
      TRUE
    );

    // Go to layout page of e/c.
    $i->amOnPage('node/' . $this->nodesUsed[$exp_col_title]->id() . '/layout');

    // Add a block.
    $i->click('Add block');
    $i->waitForText('Choose a block');

    // Click on the copy text block.
    $i->click('Copy text');
    $i->waitForText('Configure block');

    // Fill in copy text block.
    $i->fillField('settings[label]', 'Test copy text');
    $i->fillCkEditor('This is copy text on expand/collapse', 'textarea[name="settings[block_form][field_uw_copy_text][0][value]"]');

    // Add the block and ensure that it appears.
    $i->click('Add block');
    $i->waitForElement(Locator::contains('div[class="uw-admin-label"]', 'Copy text'));
    $i->see('This is copy text on expand/collapse');

    // Click the save layout and ensure that it saves.
    $i->click('Save layout');
    $i->waitForText('This is copy text on expand/collapse');

    // Go to the layout page for the webpage we created.
    $i->amOnPage($path . '/layout');
    $i->see('Edit layout for ' . $this->nodesUsed['Expand Collapse Block']->getTitle());

    // Add a block.
    $i->click('Add block');
    $i->waitForText('Choose a block');

    // Click on the expand/collapse block.
    $i->click('Expand/collapse');
    $i->waitForText('Configure block');

    // Ensure elements exists.
    $i->seeElement('input[id^="edit-settings-label"][type="text"][class*="required"][value="Expand Collapse"]');
    $i->seeElement('input[id^="edit-settings-label-display"][type="checkbox"][checked="checked"]');
    $i->see('Display title');

    // Test heading selector.
    $i->seeElement('select[id^="edit-settings-items-fieldset-heading-selector"]');
    for ($j = 2; $j < 7; $j++) {
      $i->seeElement('option[value="h' . $j . '"]');
    }

    // Test if group input is required.
    $i->seeElement('input[class*="uw-exp-col__group"]');
    $i->seeElement('.uw-exp-col__group-info div[class="form-required"]');

    // Button for adding additional groups exits.
    $i->seeElement('input[id^="edit-settings-add-group"][type="submit"][value="Add expand/collapse group"]');

    // Get title we are going to use.
    $title = $i->uwRandomString();

    // Fill in required fields.
    $i->fillField('settings[label]', $title);
    $i->selectOption('settings[items_fieldset][heading_selector]', 'h2');
    $i->fillField('settings[items_fieldset][items][0][group_info][group]', $exp_col_title);

    // Click on add block and ensure it is on the screen.
    $i->click('Add block');
    $i->waitForElement(Locator::contains('div[class="uw-admin-label"]', 'Expand Collapse'));
    $i->seeElement(Locator::contains('div[class="uw-admin-label"]', 'Expand Collapse'));

    // Test the expand/collapse block contents.
    $i->see($title);
    $i->see($exp_col_title);

    // Open the details tab and ensure the copy text is there.
    $i->seeElement('details[class*="uw-details"]');
    $i->click('details');
    $i->seeElement('details[class*="uw-details"][open=""]');

    // Check that expand/collapse buttons are not there.
    $i->dontSee('Expand All');
    $i->dontSee('Collapse All');

    // Check that there is h2 with correct text.
    $i->seeElement(Locator::contains('h2', $exp_col_title));

    // Ensure that we see the copy text form the e/c node.
    $i->see('This is copy text on expand/collapse');
  }

  /**
   * Function to test the FF block.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   *
   * @throws Exception
   */
  public function testFactsAndFiguresBlock(AcceptanceTester $i) {

    // Create a webpage.
    $this->nodesUsed['FF Block'] = $i->createWebPage('FF Block');

    // Get the path of the webpage.
    $path = $i->getWebPagePath($this->nodesUsed['FF Block']);

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Go to the layout page for the webpage we created.
    $i->amOnPage($path . '/layout');
    $i->see('Edit layout for ' . $this->nodesUsed['FF Block']->getTitle());

    // Add a block.
    $i->click('Add block');
    $i->waitForText('Choose a block');

    // Click on the FF block.
    $i->click('Facts and figures');
    $i->waitForText('Configure block');

    // Check 'Default background colour' field help text.
    $i->see('When gold is selected, the default colour will always be black.');

    // Check 'Default background colour' field default value.
    $i->seeElement('option[value="grey"][selected="selected"]');
    $i->seeElement('select[id^="edit-settings-block-form-field-uw-ff-fact-figure-0-subform-field-uw-ff-dbg-color"]');

    // Select 'gold', then 'Default background colour' field is gone.
    $i->selectOption('settings[block_form][field_uw_ff_fact_figure][0][subform][field_uw_ff_dbg_color]', 'Gold');
    $i->dontSeeElement('div[id^="edit-settings-block-form-field-uw-ff-fact-figure-0-subform-field-uw-ff-def-color-wrapper"]');

    // Select 'black', then 'Default background colour' field shows.
    $i->selectOption('settings[block_form][field_uw_ff_fact_figure][0][subform][field_uw_ff_dbg_color]', 'Black');
    $i->seeElement('div[id^="edit-settings-block-form-field-uw-ff-fact-figure-0-subform-field-uw-ff-def-color-wrapper"]');

    // The default color is 'Default[uWaterloo]' and not have "none" option.
    $i->dontSeeElement(Locator::contains('select[id^="edit-settings-block-form-field-uw-ff-fact-figure-0-subform-field-uw-ff-def-color"] option', '- None -'));

    // Black and gold selected as default.
    $i->seeElement('option[value="org-default"][selected="selected"]');

    // The type includes 'Small highlight' with value 'small'.
    $i->seeElement(Locator::contains('option[value="small"]', 'Small highlight'));
  }

  /**
   * Function to test the image full width block.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   *
   * @throws Exception
   */
  public function testImageFullWidthBlock(AcceptanceTester $i) {

    // Get the image to be used in the block.
    $image = $this->getImageForBlock($i);

    // Get the image styles.
    $image_styles = $this->getImageStyles();

    // Title for the node.
    $title = 'Image block';

    // Create a webpage.
    $this->nodesUsed[$title] = $i->createWebPage($title);

    // Get the path of the webpage.
    $path = $i->getWebPagePath($this->nodesUsed[$title]);

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Add images to the media.
    $this->addImageToMedia($i, $image);

    // Go to the layout page for the webpage we created.
    $i->amOnPage($path . '/layout');
    $i->see('Edit layout for ' . $this->nodesUsed[$title]->getTitle());

    // Add a block.
    $i->click('Add block');
    $i->waitForText('Choose a block');

    // Click on the image block.
    $i->click('Image');
    $i->waitForText('Configure block');

    // Select the full width image.
    $i->selectOption('[id*="edit-settings-image-settings-type-of-image"]', 'full');

    // Wait for the Add media button and click it and wait for media to show up.
    $i->waitForElement('input[id*="edit-settings-image-media-library-open-button"]');
    $i->click('input[id*="edit-settings-image-media-library-open-button"]');
    $i->waitForText('Add or select media');

    // Check the image to be used.
    $for = $i->grabAttributeFrom(
      Locator::contains(
        'label',
        'Select ' . $image['title']
      ),
      'for'
    );
    $i->checkOption('#' . $for);

    // Click the insert selected button.
    $i->click(Locator::contains('button', 'Insert selected'));

    // Ensure that the image is on the page.
    $i->waitForElement('.media-library-item__remove');
    $i->waitForElement('.form-submit');

    // Click on add block and ensure that block loads.
    $i->click('Add block');
    $i->waitForElement(Locator::contains('.uw-admin-label', 'Image'));

    // Get the parts of the image so that we can get
    // the full name of the image.
    $image_parts = explode('/', $image['url']);
    $image_name = end($image_parts);
    $image_name = explode('.', $image_name);
    $image_name = $image_name[0];

    // Step through each of the image styles and ensure that
    // the srcset is being set correctly.
    foreach ($image_styles as $image_style) {

      // Ensure that we see the image url in the srcset.
      $image_style_url = '/sites/default/files/styles/';
      $image_style_url .= $image_style;
      $image_style_url .= '/public/uploads/images/';
      $image_style_url .= $image_name;
      $i->seeElementInDOM('source[srcset*="' . $image_style_url . '"]');

    }
  }

  /**
   * Function to test the image fixed width block.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   *
   * @throws Exception
   */
  public function testImageFixedWidthBlock(AcceptanceTester $i) {

    // Get the image to be used in the block.
    $image = $this->getImageForBlock($i);

    // Get image name.
    $image_parts = explode('/', $image['url']);
    $image_name = end($image_parts);
    $image_name = explode('.', $image_name);
    $image_name = $image_name[0];

    // Get the width size.
    $fixed_sizes = [
      'width',
      'height',
    ];

    // The alignments to test.
    $alignments = [
      'center',
      'left',
      'right',
    ];

    // Title for the node.
    $title = 'Image block';

    // Create a webpage.
    $this->nodesUsed[$title] = $i->createWebPage($title);

    // Get the path of the webpage.
    $path = $i->getWebPagePath($this->nodesUsed[$title]);

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Add images to the media.
    $this->addImageToMedia($i, $image);

    // Go to the layout page for the webpage we created.
    $i->amOnPage($path . '/layout');
    $i->see('Edit layout for ' . $this->nodesUsed[$title]->getTitle());

    // Add a block.
    $i->click('Add block');
    $i->waitForText('Choose a block');

    // Click on the image block.
    $i->click('Image');
    $i->waitForText('Configure block');

    // Select the full width image.
    $i->selectOption('[id*="edit-settings-image-settings-type-of-image"]', 'sized');

    // Select the original sized image.
    $i->waitForElement('[id*="edit-settings-image-settings-type-of-sized-image"]');
    $i->selectOption('[id*="edit-settings-image-settings-type-of-sized-image"]', 'original');

    // Wait for the Add media button and click it and wait for media to show up.
    $i->waitForElement('input[id*="edit-settings-image-media-library-open-button"]');
    $i->click('input[id*="edit-settings-image-media-library-open-button"]');
    $i->waitForText('Add or select media');

    // Check the image to be used.
    $for = $i->grabAttributeFrom(
      Locator::contains(
        'label',
        'Select ' . $image['title']
      ),
      'for'
    );
    $i->checkOption('#' . $for);

    // Click the insert selected button.
    $i->click(Locator::contains('button', 'Insert selected'));

    // Ensure that the image is on the page.
    $i->waitForElement('.media-library-item__remove');
    $i->waitForElement('.form-submit');

    // Click on add block and ensure that block loads.
    $i->click('Add block');
    $i->waitForElement(Locator::contains('.uw-admin-label', 'Image'));

    // Ensure that picture is on page.
    $i->seeElement('.uw-image__sized-image--original');
    $i->seeInSource($image_name);

    // Step through each of the alignments and test that they work.
    foreach ($alignments as $alignment) {

      // Open the contextual configure menu (pencil icon).
      $i->amOnPage('user/logout');
      $i->logInWithRole('uw_role_site_manager');
      $i->amOnPage($path . '/layout');
      $i->see('Edit layout for ' . $this->nodesUsed[$title]->getTitle());
      $i->moveMouseOver(Locator::contains('button', 'Open Image configuration options'));
      $i->click(Locator::contains('button', 'Open Image configuration options'));

      // Click on the configure link.
      $i->waitForElement('.contextual-links');
      $i->click(Locator::contains('ul li a', 'Configure'));
      $i->waitForText('Configure block');

      // Select the alignement to test.
      $i->selectOption('[id*="edit-settings-image-settings-image-alignment"]', $alignment);

      // Click on add block and ensure that block loads, adding a wait
      // here for image generation, or it will time out waiting for
      // the element.
      $i->click('Update');
      $i->waitForElementNotVisible('.ui-dialog');
      $i->waitForElement(Locator::contains('.uw-admin-label', 'Image'));

      // Test that alignment is on page.
      $i->seeElement('.uw-image__sized-image--' . $alignment);
    }

    // Step through the fixed sizes.
    foreach ($fixed_sizes as $fixed_size) {

      // Step through all the alignments.
      foreach ($alignments as $alignment) {

        // Open the contextual configure menu (pencil icon).
        $i->amOnPage('user/logout');
        $i->logInWithRole('uw_role_site_manager');
        $i->amOnPage($path . '/layout');
        $i->see('Edit layout for ' . $this->nodesUsed[$title]->getTitle());
        $i->moveMouseOver(Locator::contains('button', 'Open Image configuration options'));
        $i->click(Locator::contains('button', 'Open Image configuration options'));

        // Click on the configure link.
        $i->waitForElement('.contextual-links');
        $i->click(Locator::contains('ul li a', 'Configure'));
        $i->waitForText('Configure block');

        // Set the type of sized image to custom.
        $i->waitForElement('[id*="edit-settings-image-settings-type-of-sized-image"]');
        $i->selectOption('[id*="edit-settings-image-settings-type-of-sized-image"]', 'custom');

        // Set the scaled image to the fixed size we are testing.
        $i->waitForElement('[id*="edit-settings-image-settings-scale-by"]');
        $i->selectOption('[id*="edit-settings-image-settings-scale-by"]', $fixed_size);

        // Get the name of the field to fill based on the fixed size.
        if ($fixed_size == 'width') {
          $element_id = 'edit-settings-image-settings-width';
        }
        else {
          $element_id = 'edit-settings-image-settings-height';
        }

        // Set the fixed size to 400.
        $i->waitForElement('[id*="' . $element_id . '"]');
        $i->fillField('[id*="' . $element_id . '"]', '400');

        // Wait for and select the center alignment.
        $i->waitForElement('[id*="edit-settings-image-settings-image-alignment"]');
        $i->selectOption('[id*="edit-settings-image-settings-image-alignment"]', $alignment);

        // Click on add block and ensure that block loads, adding a wait
        // here for image generation, or it will time out waiting for
        // the element.
        $i->click('Update');
        $i->waitForElementNotVisible('.ui-dialog');
        $i->waitForElement(Locator::contains('.uw-admin-label', 'Image'));

        // Ensure that the alignemtn is on the page.
        $i->seeElement('.uw-image__sized-image--' . $alignment);
        $i->seeInSource($image_name);

        // Get the name of the image based on the fixed size and test that the
        // proper attribute is set.
        if ($fixed_size == 'width') {
          $i->seeInSource('400x');
          $i->seeElement('img[width="400"]');
        }
        else {
          $i->seeInSource('x400');
          $i->seeElement('img[height="400"]');
        }
      }
    }
  }

  /**
   * Function to test the related links block.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testRelatedLinksBlock(AcceptanceTester $i) {

    // Create a webpage.
    $this->nodesUsed['Related Links Block'] = $i->createWebPage('Related Links Block');

    // Get the path of the webpage.
    $path = $i->getWebPagePath($this->nodesUsed['Related Links Block']);

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Go to the layout page for the webpage we created.
    $i->amOnPage($path . '/layout');
    $i->see('Edit layout for ' . $this->nodesUsed['Related Links Block']->getTitle());

    // Add a block.
    $i->click('Add block');
    $i->waitForText('Choose a block');

    // Click on the related links block.
    $i->click('Related links');
    $i->waitForText('Configure block');

    // Add a second and third link.
    $i->click('input[id^="edit-settings-block-form-field-uw-rl-related-link-add-more"]');
    $i->waitForElement('input[id^="edit-settings-block-form-field-uw-rl-related-link-1-uri"]');
    $i->click('input[id^="edit-settings-block-form-field-uw-rl-related-link-add-more"]');
    $i->waitForElement('input[id^="edit-settings-block-form-field-uw-rl-related-link-2-uri"]');

    // Get the settings for the related links.
    $title = $i->uwRandomString();
    $url1 = 'entity:node/1';
    $title1 = $i->uwRandomString();
    $url2 = 'https://www.sju.ca/';
    $title2 = $i->uwRandomString();

    // Fill in the fields.
    $i->fillField('settings[label]', $title);
    $i->fillField('settings[block_form][field_uw_rl_related_link][0][uri]', $url1);
    $i->fillField('settings[block_form][field_uw_rl_related_link][0][title]', $title1);
    $i->fillField('settings[block_form][field_uw_rl_related_link][1][uri]', $url2);
    $i->fillField('settings[block_form][field_uw_rl_related_link][1][title]', $title2);
    $i->fillField('settings[block_form][field_uw_rl_related_link][2][uri]', $url1);

    // Click on add block and ensure it is on the screen.
    $i->click('Add block');
    $i->waitForElement(Locator::contains('div[class="uw-admin-label"]', 'Related links'));
    $i->seeElement(Locator::contains('div[class="uw-admin-label"]', 'Related links'));

    // Test that the Related links block markup appears on the page.
    $i->see($title);
    $i->seeElement(Locator::contains('a[href="/home"]', $title1));
    $i->seeElement(Locator::contains('a[href="' . $url2 . '"]', $title2));
    $i->seeElement(Locator::contains('a[href="/home"]', 'Home'));
  }

  /**
   * Function to test the timeline block.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testTimelineBlock(AcceptanceTester $i) {

    // Create a webpage.
    $this->nodesUsed['Timeline Block'] = $i->createWebPage('Timeline Block');

    // Get the path of the webpage.
    $path = $i->getWebPagePath($this->nodesUsed['Timeline Block']);

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Go to the layout page for the webpage we created.
    $i->amOnPage($path . '/layout');
    $i->see('Edit layout for ' . $this->nodesUsed['Timeline Block']->getTitle());

    // Add a block.
    $i->click('Add block');
    $i->waitForText('Choose a block');

    // Click on the timeline block.
    $i->click('Timeline');
    $i->waitForText('Configure block');

    // Ensure that fields are in the block.
    $i->see('Title');
    $i->see('Timeline style');
    $i->seeElement('select[id^="edit-settings-block-form-field-uw-timeline-style"]');
    $i->see('Timeline sort');
    $i->seeElement('select[id^="edit-settings-block-form-field-uw-timeline-sort"]');
    $i->see('Date');
    $i->seeElement('input[id^="edit-settings-block-form-field-uw-timeline-0-subform-field-uw-timeline-date-0-value-date"]');
    $i->see('Headline');
    $i->seeElement('input[id^="edit-settings-block-form-field-uw-timeline-0-subform-field-uw-timeline-headline-0-value"]');
    $i->see('One or both of the "Headline" and "Content" fields must be populated.');
    $i->see('PHOTO');
    $i->seeElement('input[value="Add media"]');
    $i->see('One media item remaining.');
    $i->see('Link');
    $i->see('Links the entire content to a URL. If entered, do not use links inside the content itself.');
    $i->see('Content');
    $i->seeElement('input[value="Add Timeline item"]');
  }

  /**
   * Function to add the image to media.
   *
   * @param AcceptanceTester $i
   *   The Acceptance tester.
   * @param array $image
   *   The image.
   */
  private function addImageToMedia(AcceptanceTester $i, array $image): void {

    // Go to the add image page.
    $i->amOnPage('media/add/uw_mt_image');
    $i->see('Add image');

    // Add remote image.
    $i->click('Remote URL');
    $i->fillField(
      '#edit-name-0-value',
      $image['title'],
    );
    $i->fillField(
      '#edit-field-media-image-0-filefield-remote-url',
      $image['url'],
    );

    // Add remote image.
    $i->click('Remote URL');
    $i->fillField(
      '#edit-name-0-value',
      $image['title'],
    );
    $i->fillField(
      '#edit-field-media-image-0-filefield-remote-url',
      $image['url'],
    );

    // Click and transger and ensure that it uploads.
    $i->click('Transfer');
    $i->waitForText('Alternative text');

    // Fill in the alt text field.
    $i->fillField('input[id*="edit-field-media-image-0-alt"]', $image['title']);

    // Click save and ensure that the image is added.
    $i->click('Save');
    $i->see('Image ' . $image['title'] . ' has been created');
  }

  /**
   * Function to get the images for banners.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   *
   * @return array[]
   *   Array of images for banners.
   */
  private function getImageForBlock(AcceptanceTester $i): array {

    // The images that are to be used.
    return [
      'url' => 'https://uwaterloo.ca/building-the-next-wcms/sites/default/files/uploads/images/exception-experience-wcms3-banner.png',
      'title' => $i->uwRandomString(),
    ];
  }

  /**
   * Function to get the image styles.
   *
   * @return string[]
   *   Array of image styles.
   */
  private function getImageStyles(): array {

    return [
      'uw_is_media_x_large',
      'uw_is_media_large',
      'uw_is_media_medium',
      'uw_is_media_small',
      'uw_is_media_x_small',
    ];
  }

  // phpcs:disable

  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _after(AcceptanceTester $i): void {
    // phpcs:enable

    // Compelte the block tests.
    $i->completeBlockTests($this->tidsUsed, $this->nodesUsed);
  }

  // phpcs:disable

  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i): void {
    // phpcs:enable

    // Compelte the block tests.
    $i->completeBlockTests($this->tidsUsed, $this->nodesUsed);
  }

}
