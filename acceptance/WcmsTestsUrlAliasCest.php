<?php

/**
 * Class UrlAliasCest.
 *
 * Tests for url alias.
 */
class WcmsTestsUrlAliasCest {

  /**
   * Array used for any tids that we created.
   *
   * @var array
   */
  private array $tidsUsed = [];

  /**
   * Array of nodes used.
   *
   * @var array
   */
  private array $nodesUsed = [];

  /**
   * Default generate automatic url alias test.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testDefaultGenerateUrlAlias(AcceptanceTester $i) {

    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    $content_types = $i->getContentTypes(TRUE);

    foreach ($content_types as $content_type) {
      // Go to add node page.
      $i->amOnPage('node/add/' . $content_type);
      $i->see('Create ');

      // Click URL Alias.
      $i->click('#edit-path-0');

      // 'Generate automatic URL alias' is checked as default.
      $i->see('Generate automatic URL alias');
      $i->seeCheckboxIsChecked('#edit-path-0-pathauto');
    }
  }

  /**
   * Generate url alias test.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testGenerateUrlAlias(AcceptanceTester $i) {

    // Get all content types.
    $content_types = $i->getContentTypes(TRUE);

    // Generate nodes for all content types.
    foreach ($content_types as $content_type) {
      $title = $i->uwRandomString();
      $this->nodesUsed[$title] = $i->createCtNode(
        $content_type,
        $title,
        TRUE,
      );
    }

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Loop all nodes.
    foreach ($this->nodesUsed as $node) {
      // Get node path.
      $path = $i->getWebPagePath($node);

      // Go to node edit page.
      $i->amOnPage($path . '/edit');

      // Click URL Alias.
      $i->click('#edit-path-0');
      $i->see('Generate automatic URL alias');

      // Check URL Alias input value.
      $i->seeElement('input[value="' . $path . '"]');

      // Uncheck 'Generate automatic URL alias.
      $i->uncheckOption('#edit-path-0-pathauto');

      // Test double forward slashes.
      $i->fillField(
        '#edit-path-0-alias',
        '/test//page',
      );

      // Click save button.
      $i->click('#edit-submit');

      // Check error message.
      $i->see('The URL must not contain multiple consecutive forward slashes.');

      // Test reserved catalog URLs.
      $reserved_catalogs_urls = [
        'new',
        'updated',
        'popular',
        'category',
        'audience',
        'faculty',
      ];

      foreach ($reserved_catalogs_urls as $url) {
        $i->fillField(
          '#edit-path-0-alias',
          '/catalogs/page/' . $url,
        );

        // Click save button.
        $i->click('#edit-submit');

        // Check error message.
        $i->see('The alias "/catalogs/page/' . $url . '" is a reserved path that cannot be used.');
      }

      // Enter '/test/page' in URL alias field.
      $i->fillField(
        '#edit-path-0-alias',
        '/test/page',
      );

      // Click save button.
      $i->click('#edit-submit');

      // Get current url of this node.
      $current_url = $i->getCurrentUrl();

      // Make sure on this page.
      $i->amOnPage($current_url);

      // Make sure this page url is manually added.
      $i->assertTrue(
        $current_url == '/test/page',
        'The generate url matches',
      );

      // Go to this edit page.
      $i->amOnPage('test/page/edit');

      // Check 'Generate automatic URL alias'.
      $i->checkOption('#edit-path-0-pathauto');

      // Click save button.
      $i->click('#edit-submit');

      // Get current url.
      $current_url = $i->getCurrentUrl();

      // Make sure the url is auto generated.
      $i->assertTrue(
        $current_url == $path,
        'The automatic url matches',
      );

    }
  }

  /**
   * Url alias error message test.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testUrlAliasErrorMessage(AcceptanceTester $i) {

    // Create a webpage (url alias is webpageone).
    $this->nodesUsed['Webpageone'] = $i->createCtNode('uw_ct_web_page', 'Webpageone', TRUE);

    // Logout and log into as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Get webpageone path.
    $path = $i->getWebPagePath($this->nodesUsed['Webpageone']);

    // Prepare test cases.
    $urls = [
      '/web[/][\]' => 'URL alias must be a valid format.',
      'blog' => 'The alias path has to start with a slash.',
      '/blog' => 'The URL must be changed - "/blog" is already in use on this site.',
      '/events' => 'The URL must be changed - "/events" is already in use on this site.',
      '/news' => 'The URL must be changed - "/news" is already in use on this site.',
      '/projects' => 'The URL must be changed - "/projects" is already in use on this site.',
      '/profiles' => 'The URL must be changed - "/profiles" is already in use on this site.',
      '/contacts' => 'The URL must be changed - "/contacts" is already in use on this site.',
      '/services' => 'The URL must be changed - "/services" is already in use on this site.',
      '/opportunities' => 'The URL must be changed - "/opportunities" is already in use on this site.',
      '/dashboard/my_dashboard' => 'The URL must be changed - "/dashboard/my_dashboard" is already in use on this site.',
      '/user' => 'The URL must be changed - "/user" is already in use on this site.',
      '/user/login' => ' The URL must be changed - "/user/login" is already in use on this site.',
      '/users/wcmsadmin/edit' => 'The URL must be changed - "/users/wcmsadmin/edit" is already in use on this site.',
      '/user/logout' => 'The URL must be changed - "/user/logout" is already in use on this site.',
      'admin' => 'The alias path has to start with a slash.',
      '/profile' => 'The alias "/profile" is a reserved path that cannot be used.',
      '/service' => 'The alias "/service" is a reserved path that cannot be used.',
      '/users' => 'The alias "/users" is a reserved path that cannot be used.',
      '/admin/aa' => 'The URL must be changed - "/admin/aa" starts with /admin or /node.',
      '/node' => 'The URL must be changed - "/node" starts with /admin or /node.',
      '/node/55443' => 'The URL must be changed - "/node/55443" starts with /admin or /node.',
      '/node/bb' => 'The URL must be changed - "/node/bb" starts with /admin or /node.',
    ];

    // Entering the url will show error message.
    foreach ($urls as $url => $message) {
      // Go to webpageone edit page.
      $i->amOnPage($path . '/edit');

      // Uncheck 'Generate automatic URL alias'.
      $i->click('#edit-path-0');
      $i->see('Generate automatic URL alias');
      $i->seeElement('input[value="' . $path . '"]');
      $i->uncheckOption('#edit-path-0-pathauto');
      $i->fillField('#edit-path-0-alias', $url);
      $i->click('#edit-submit');
      $i->see($message);
    }

    // Create second webpage (url alias is webpagetwo).
    $this->nodesUsed['Webpagetwo'] = $i->createCtNode('uw_ct_web_page', 'Webpagetwo', TRUE);

    // Get webpagetwo path.
    $path = $i->getWebPagePath($this->nodesUsed['Webpagetwo']);

    // Go to webpagetwo edit page.
    $i->amOnPage($path . '/edit');

    // Uncheck 'Generate automatic URL alias'.
    $i->click('#edit-path-0');
    $i->see('Generate automatic URL alias');
    $i->seeElement('input[value="' . $path . '"]');
    $i->uncheckOption('#edit-path-0-pathauto');

    // Enter url alias which declared by webpageone.
    $i->fillField('#edit-path-0-alias', '/webpageone');
    $i->click('#edit-submit');
    $i->see('The alias /webpageone is already in use in this language.');

  }

  // phpcs:disable

  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _after(AcceptanceTester $i): void {
    // phpcs:enable

    // If there were terms used, delete them.
    if (!empty($this->tidsUsed)) {

      // Step through each term and delete it.
      foreach ($this->tidsUsed as $tid) {

        // Load the term.
        $term = Drupal::entityTypeManager()
          ->getStorage('taxonomy_term')
          ->load($tid);

        if ($term) {
          // Delete the term.
          $term->delete();
        }
      }
    }

    // Step through each of the nodes used and delete them.
    foreach ($this->nodesUsed as $node) {
      $node->delete();
    }
  }

  // phpcs:disable

  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i): void {
    // phpcs:enable

    // If there were terms used, delete them.
    if (!empty($this->tidsUsed)) {

      // Step through each term and delete it.
      foreach ($this->tidsUsed as $tid) {

        // Load the term.
        $term = Drupal::entityTypeManager()
          ->getStorage('taxonomy_term')
          ->load($tid);

        if ($term) {
          // Delete the term.
          $term->delete();
        }
      }
    }

    // Step through each of the nodes used and delete them.
    foreach ($this->nodesUsed as $node) {
      $node->delete();
    }
  }

}
