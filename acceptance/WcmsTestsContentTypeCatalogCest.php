<?php

use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;
use Facebook\WebDriver\WebDriverKeys;
use Step\Acceptance\ContentType;

/**
 * Class WcmsTestsContentTypeCatalogCest.
 *
 * Tests for catalogs.
 */
class WcmsTestsContentTypeCatalogCest {

  /**
   * Array of tids that were used in the test.
   *
   * @var array
   */
  private $tidsUsed = [];

  /**
   * URL to web page that we used for category creation.
   *
   * @var string
   */
  private $webpageUrl;

  /**
   * Array of catalogs created.
   *
   * @var array
   */
  private $catalogsCreated = [];

  /**
   * The content type.
   *
   * @var string
   */
  private string $contentType = 'uw_ct_catalog_item';

  /**
   * Flag to handle module enabled flag.
   *
   * @var bool
   */
  private bool $moduleFlag = FALSE;

  // phpcs:disable
  /**
   * Function to run before the test(s) starts.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _before(AcceptanceTester $i) {
    // phpcs:enable

    // If the field ui is not enabled, then enable it and set
    // flag so that we disable it once this test is completed.
    if (!\Drupal::service('module_handler')->moduleExists('field_ui')) {
      $this->moduleFlag = TRUE;
      \Drupal::service('module_installer')
        ->install(['field_ui']);
    }

  }

  /**
   * Tests for catalog content type.
   *
   * @param Step\Acceptance\ContentType $i
   *   Acceptance test variable.
   */
  public function testCatalogContentType(ContentType $i) {

    // Create a catalog term and get tid for later use.
    $this->tidsUsed[] = $i->createTerm('uw_vocab_catalogs', $i->uwRandomString());

    // Test the fields in the content type.
    $i->testContentType(
      'Catalog',
      $i->getContentTypeFields($this->contentType)
    );

    $edits = $i->getContentTypeEdits('uw_ct_catalog_item');

    // Add the catalog to the list of edits.
    $edits['radio'] = [
      'edit-field-uw-catalog-catalog-' . end($this->tidsUsed)  => end($this->tidsUsed),
    ];

    // Test the content type edits.
    $i->testContentTypeEdits(
      'uw_ct_catalog_item',
      $edits
    );
  }

  /**
   * Tests for catalogs settings.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function catalogSettingsTest(AcceptanceTester $i) {

    // Make sure work flows exist.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');
    $i->amOnPage('admin/config/workflow/workflows/manage/uw_workflow');
    $i->see('Catalog item');

    // Make sure only 'Basic' format is checked.
    $i->amOnPage('admin/structure/types/manage/uw_ct_catalog_item/fields/node.uw_ct_catalog_item.field_uw_catalog_summary');
    $i->dontSeeCheckboxIsChecked('third_party_settings[allowed_formats][allowed_formats][uw_tf_standard]');
    $i->dontSeeCheckboxIsChecked('third_party_settings[allowed_formats][allowed_formats][uw_tf_full_html]');
    $i->dontSeeCheckboxIsChecked('third_party_settings[allowed_formats][allowed_formats][plain_text]');
    $i->seeCheckboxIsChecked('third_party_settings[allowed_formats][allowed_formats][uw_tf_basic]');

    // Make sure catalog term path pattern.
    $i->amOnPage('admin/config/search/path/patterns/uw_path_catalogs');
    $i->see('Edit Catalogs path pattern');
    $i->seeOptionIsSelected('#edit-type', 'Taxonomy term');
    $i->seeInField('pattern', 'catalogs/[term:name]');
    $i->seeCheckboxIsChecked('bundles[uw_vocab_catalogs]');
    $i->seeInField('label', 'Catalogs path pattern');
    $i->seeCheckboxIsChecked('#edit-status');

    // Make sure taxonomy views integrator settings in uw_vocab_catalogs.
    $i->amOnPage('admin/structure/taxonomy/manage/uw_vocab_catalogs');
    $i->see('Edit vocabulary');
    $i->seeElement('#edit-tvi');
    $i->seeCheckboxIsChecked('#edit-tvi-enable-override');
    $i->seeOptionIsSelected('#edit-tvi-view', 'Catalog views show nodes');
    $i->seeOptionIsSelected('#edit-tvi-view-display', 'A-Z Page (page)');
    $i->seeCheckboxIsChecked('#tvi-inherit-check');
    $i->seeCheckboxIsChecked('#tvi-pass-arguments');

    // Make sure 'Tabs to display' field in uw_vocab_catalogs.
    $i->amOnPage('admin/structure/taxonomy/manage/uw_vocab_catalogs/overview/fields');
    $i->see('Manage fields');
    $i->see('Tabs to display');
    $i->see('field_uw_catalog_tabs_display');
    $i->seeLink('List (text)');

    // Make sure 'Tabs to display' field in creating catalog term.
    // all checkboxes are not checked by default.
    $i->amOnPage('admin/structure/taxonomy/manage/uw_vocab_catalogs/add');
    $i->see('Add term');
    $i->see('Tabs to display');
    $keys = [
      'new',
      'popular',
      'category',
      'audience',
      'faculty',
    ];
    foreach ($keys as $key) {
      $selector = '#edit-field-uw-catalog-tabs-display-' . $key;
      $i->seeElement($selector);
      $i->dontSeeCheckboxIsChecked($selector);
    }
  }

  /**
   * Tests for catalogs term and roles.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function catalogTermRolesTest(AcceptanceTester $i) {

    // Create a catalog term and get tid for later use.
    $catalog_term = $i->createTerm('uw_vocab_catalogs', $i->uwRandomString());
    $this->tidsUsed[] = $catalog_term;
    $term = Term::load($catalog_term);
    $catalog_term_label = $term->label();

    // Create a catalog categories term.
    $category_term = $i->createTerm('uw_vocab_catalog_categories', $i->uwRandomString());
    $this->tidsUsed[] = $category_term;

    // Make sure roles at default install are set up as expected.
    $roles = [
      'uw_role_content_author',
      'uw_role_content_editor',
      'uw_role_site_manager',
    ];

    // Step through each role and test catalog item creation.
    foreach ($roles as $role) {

      // Login as the given role.
      $i->amOnPage('user/logout');
      $i->logInWithRole($role);

      // Test catalog item create as role.
      $this->testCreateCatalog($i, $catalog_term);
    }

    // Create the apple catalog category term.
    $apple_term = $i->createTerm('uw_vocab_catalog_categories', 'apple');
    $this->tidsUsed[] = $apple_term;

    // Get the path to the first term we created.
    $first_term_path = \Drupal::service('path_alias.manager')
      ->getAliasByPath('/taxonomy/term/' . $catalog_term);

    // Try to edit the catalog term.
    $i->amOnPage($first_term_path . '/edit');
    $i->see('Edit term');

    // Now click that we can use the category tab.
    $i->checkOption('field_uw_catalog_tabs_display[Category]');

    // Click save and ensure that it was updated.
    $i->click('Save');
    $i->see('Updated term');

    // Go to add catalog item page.
    $i->amOnPage('node/add/uw_ct_catalog_item');
    $i->waitForText('Create Catalog item');

    // Create a new page using apple as category.
    $this->testCreateCatalog($i, $catalog_term, 'apple');

    // Go to the term category page and ensure it loads.
    $i->amOnPage($first_term_path . '/category');
    $i->waitForText('Category');
    $i->waitForText('apple');

    // Ensure that we see the apple category listed.
    $i->seeLink('apple');

    // Unpublish the last catalog item that we
    // created, so that we can test the rest of
    // the settings for the catalogs.
    $i->amOnPage($this->webpageUrl);
    $i->seeLink('Unpublish this content');
    $i->click('Unpublish this content');
    $i->waitForElementClickable('#edit-submit');
    $i->click('#edit-submit');
    $i->see('You have successfully unpublished this content.');

    // Check all paths related to catalog views exist.
    $i->amOnPage('catalogs');
    $i->see('Catalogs');

    // Paths to test for.
    $paths = [
      '',
      'new',
      'popular',
      'audience',
      'category',
      'faculty',
      'audience/staff',
      'faculty/faculty-of-arts',
    ];

    // Load the firs term used.
    $term = Term::load($this->tidsUsed[0]);

    // Step through each path and test that there
    // are no items, there should be nothing
    // published at this point.
    foreach ($paths as $path) {

      // Go to the path.
      $i->amOnPage($first_term_path . '/' . $path);

      // Ensure that the label for the term is present.
      $i->see($catalog_term_label);

      // There is no published catalog item node existing.
      $i->see('There are no items available.');
    }

    // Make sure Metatags display extender is enabled.
    $views_display_extender_settings = \Drupal::config('views.settings')->get('display_extenders');
    $i->assertTrue(in_array('metatag_display_extender', $views_display_extender_settings), 'The metatags display extender is expected to be enabled.');
  }

  /**
   * Function to test creating a catalog item.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   * @param int $catalog_tid
   *   The term id for the catalog.
   * @param string|null $category_text
   *   The label for the category.
   *
   * @throws \Codeception\Exception\ModuleException
   */
  private function testCreateCatalog(AcceptanceTester $i, int $catalog_tid, string $category_text = NULL) {

    // Go to add catalog item page.
    $i->amOnPage('node/add/uw_ct_catalog_item');
    $i->waitForText('Create Catalog item');

    // Get a random title.
    $title = $i->uwRandomString();

    // Fill the fields for the catalog item, here
    // the category should be default, since there
    // is only one.
    $i->fillField('title[0][value]', $title);
    $i->click('label[for="edit-field-uw-catalog-catalog-' . $catalog_tid . '"]');
    $i->fillCkEditor($i->uwRandomString(), '#edit-field-uw-catalog-summary-0-value');

    // If we send a category text, fill the autocomplete
    // deluxe field, fill in meta description and set
    // to published. If not, set moderation to needs review.
    if ($category_text) {

      // Fill the autocomplete deluxe field.
      $i->fillField('field_uw_catalog_category[target_id][textfield]', 'apple');

      // Simulate an enter key press so that autocomplete
      // deluxe does its things and adds term.
      $i->pressKey('#autocomplete-deluxe-input', WebDriverKeys::ENTER);

      // Set to meta description.
      $i->fillField('field_uw_meta_description[0][value]', $i->uwRandomString());

      // Set to published.
      $i->selectOption('moderation_state[0][state]', 'Published');
    }
    else {
      $i->selectOption('moderation_state[0][state]', 'Needs Review');
    }

    // Click save and ensure that catalog item was
    // successfully created.
    $i->click('Save');
    $i->see('Catalog item ' . $title . ' has been created.');

    // Store the catalog item node, so we can delete after tests.
    $path = \Drupal::service('path_alias.manager')->getPathByAlias($i->getCurrentUrl());
    if (preg_match('/node\/(\d+)/', $path, $matches)) {
      $this->catalogsCreated[] = Node::load($matches[1]);
    }

    // If this a category add, test for other things.
    if ($category_text) {

      // Ensure that we do not have the content moderation block.
      $i->dontSeeLink('Publish this content', '/admin/uw-content-moderation/');

      // Ensure that we can not see the revert to defaults button.
      $i->dontSeeElement('input[value="Revert to defaults"]');

      // Ensure that we can see the save layout button.
      $i->seeElement('input[value="Save layout"]');

      // Go to the view page and save the current url,
      // so that we can use it later.
      $i->click('View');
      $this->webpageUrl = $i->getCurrentUrl();
    }
  }

  // phpcs:disable
  /**
   * Function to run if the test fails.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i): void {
    // phpcs:enable

    // If there were terms used, delete them.
    if (!empty($this->tidsUsed)) {

      // Step through each term and delete it.
      foreach ($this->tidsUsed as $tid) {

        // Load the term.
        $term = Drupal::entityTypeManager()
          ->getStorage('taxonomy_term')
          ->load($tid);

        if ($term) {
          // Delete the term.
          $term->delete();
        }
      }
    }

    // If there were catalog items created, delete them.
    if (!empty($this->catalogsCreated)) {
      foreach ($this->catalogsCreated as $node) {
        $node->delete();
      }
    }

    // If the module flag is set, uninstall it.
    if ($this->moduleFlag) {
      \Drupal::service('module_installer')
        ->uninstall(['field_ui']);
    }

    // Delete all the blocks.
    $i->deleteAllBlocks();
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _after(AcceptanceTester $i): void {
    // phpcs:enable

    // If there were terms used, delete them.
    if (!empty($this->tidsUsed)) {

      // Step through each term and delete it.
      foreach ($this->tidsUsed as $tid) {

        // Load the term.
        $term = Drupal::entityTypeManager()
          ->getStorage('taxonomy_term')
          ->load($tid);

        if ($term) {
          // Delete the term.
          $term->delete();
        }
      }
    }

    // If there were catalog items created, delete them.
    if (!empty($this->catalogsCreated)) {
      foreach ($this->catalogsCreated as $node) {
        $node->delete();
      }
    }

    // If the module flag is set, uninstall it.
    if ($this->moduleFlag) {
      \Drupal::service('module_installer')
        ->uninstall(['field_ui']);
    }

    // Delete all the blocks.
    $i->deleteAllBlocks();
  }

}
