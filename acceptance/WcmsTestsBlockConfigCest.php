<?php

use Codeception\Util\Locator;

/**
 * Class WcmsTestsBlockConfigCest.
 *
 * Tests that for any block config.
 */
class WcmsTestsBlockConfigCest {

  /**
   * Flag to handle module enabled flag.
   *
   * @var bool
   */
  private bool $moduleFlag = FALSE;

  // phpcs:disable
  /**
   * Function to run before the test(s) starts.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _before(AcceptanceTester $i) {
    // phpcs:enable

    // If the field ui is not enabled, then enable it and set
    // flag so that we disable it once this test is completed.
    if (!\Drupal::service('module_handler')->moduleExists('field_ui')) {
      $this->moduleFlag = TRUE;
      \Drupal::service('module_installer')
        ->install(['field_ui']);
    }

  }

  /**
   * Tests for cta block fields.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testCtaFields(AcceptanceTester $i) {

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');

    // Go to the cat paragraphs page.
    $i->amOnPage('admin/structure/paragraphs_type/uw_para_call_to_action/fields/paragraph.uw_para_call_to_action.field_uw_cta_text_details');
    $i->see('Text details settings for Call to action');

    // Ensure that text size options are available.
    $i->seeElement('option[value="small"]');
    $i->seeElement('option[value="medium"]');
    $i->seeElement('option[value="big"]');

    // Ensure that the icon/text is not available.
    $i->dontSeeElement(Locator::contains('option', 'icon/text'));

    // Make sure CTA link field required.
    $i->amOnPage('admin/structure/paragraphs_type/uw_para_call_to_action/fields/paragraph.uw_para_call_to_action.field_uw_cta_link');
    $i->see('Link settings for Call to action');
    $i->seeCheckboxIsChecked('#edit-required');

    // Make sure CTA theme field required.
    $i->amOnPage('admin/structure/paragraphs_type/uw_para_call_to_action/fields/paragraph.uw_para_call_to_action.field_uw_cta_theme');
    $i->see('Theme settings for Call to action');
    $i->seeCheckboxIsChecked('#edit-required');
  }

  /**
   * Tests for cta block fields.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testFactsAndFiguresFields(AcceptanceTester $i) {

    // Login as site manager.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');

    // Go to the ff paragraphs page.
    $i->amOnPage('admin/structure/paragraphs_type/uw_para_fact_figure/fields/paragraph.uw_para_fact_figure.field_uw_ff_info');
    $i->see('Fact/figure info settings for Fact/figure');

    // The types of facts and figures.
    $types = [
      'big',
      'medium',
      'small',
      'icon',
      'infographic',
    ];

    // Step through each type and ensure it is an option.
    foreach ($types as $type) {
      $i->seeElement('option[value="' . $type . '"]');
    }
  }

  // phpcs:disable
  /**
   * Function to run after the test fails.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _after(AcceptanceTester $i): void {
    // phpcs:enable

    // If the module flag is set, uninstall it.
    if ($this->moduleFlag) {
      \Drupal::service('module_installer')
        ->uninstall(['field_ui']);
    }
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i): void {
    // phpcs:enable

    // If the module flag is set, uninstall it.
    if ($this->moduleFlag) {
      \Drupal::service('module_installer')
        ->uninstall(['field_ui']);
    }
  }

}
