<?php

use Step\Acceptance\ContentType;
use Step\Acceptance\TaxonomyTest;

/**
 * Class WcmsTestsContentTypeNewsCest.
 *
 * Tests for blogs.
 */
class WcmsTestsContentTypeNewsCest {

  /**
   * The content type.
   *
   * @var string
   */
  private string $contentType = 'uw_ct_news_item';

  /**
   * Tests for expand collapse content type.
   *
   * @param Step\Acceptance\ContentType $i
   *   Acceptance test variable.
   */
  public function testNewsContentType(ContentType $i): void {

    // Test the fields in the content type.
    $i->testContentType(
      'News item',
      $i->getContentTypeFields($this->contentType)
    );

    // Test the content type edits.
    $i->testContentTypeEdits(
      $this->contentType,
      $i->getContentTypeEdits($this->contentType)
    );
  }

  /**
   * Function to test taxonomies.
   *
   * @param Step\Acceptance\TaxonomyTest $i
   *   The acceptance tester.
   */
  public function testNewsTaxonomies(TaxonomyTest $i): void {

    // Test the taxonomy terms.
    $i->testTaxonomies(
      $this->contentType,
      'news-item',
      ['uw_vocab_audience']
    );
  }

  /**
   * Test news content type with dates.
   *
   * @param Step\Acceptance\ContentType $i
   *   The acceptance tester.
   */
  public function testNewsDates(ContentType $i): void {

    // Get the dates to be used.
    $dates = $i->getEarlyMiddleLateDates();

    // Step through each of the dates and create content.
    foreach ($dates as $date) {

      // Get the edits for the content type.
      $edits = $i->getContentTypeEdits('uw_ct_news_item');

      // Set the default edit date to one of the generated dates.
      $edits['field']['edit-field-uw-news-date-0-value-date'] = $date;

      // Test the edits with the new date, not running the full
      // tests like we do when testing the content type.
      $i->testContentTypeEdits(
        'uw_ct_news_item',
        $edits,
        FALSE
      );

      // Click on submit and ensure that the node has been created.
      $i->click('#edit-submit');
      $i->see('News item ' . $edits['field']['edit-title-0-value'] . ' has been created');
    }

    // Test the news listing page.
    $i->testListingPage($dates, 'news');
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _after(AcceptanceTester $i): void {
    // phpcs:enable

    // Delete all the blocks.
    $i->deleteAllBlocks();
  }

  // phpcs:disable
  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i): void {
    // phpcs:enable

    // Delete all the blocks.
    $i->deleteAllBlocks();
  }

}
