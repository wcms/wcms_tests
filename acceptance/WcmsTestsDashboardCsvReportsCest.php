<?php

/**
 * Class DashboardCsvReportsCest.
 *
 * Tests for dashboard csv reports.
 */
class WcmsTestsDashboardCsvReportsCest {

  /**
   * Tests for dashboard csv report.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function testDashboardCsvReport(AcceptanceTester $i) {

    // Roles to test with.
    $roles = [
      'administrator',
      'uw_role_site_owner',
    ];

    // Step through each role and test the report.
    foreach ($roles as $role) {

      // Login with the role.
      $i->amOnPage('user/logout');
      $i->logInWithRole($role);

      // Go to the dashboard.
      $i->amOnPage('dashboard/my_dashboard');
      $i->see('My Dashboard');

      // Scroll to the site management link details.
      $i->scrollTo(['css' => '#site-management-menu'], 100, 0);

      // Check that the content report link exists and click it.
      $i->seeLink('Content report (CSV)');
      $i->click('Content report (CSV)');

      // @todo add checking the PDF.
    }
  }

}
