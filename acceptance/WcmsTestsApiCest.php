<?php

/**
 * Class WcmsTestsApiCest.
 *
 * Tests for listing blocks.
 */
class WcmsTestsApiCest {

  /**
   * Array used for any tids that we created.
   *
   * @var array
   */
  private $tidsUsed = [];

  /**
   * Array of nodes used.
   *
   * @var array
   */
  private array $nodesUsed = [];

  /**
   * Array of random values used.
   *
   * @var array
   */
  private array $valuesUsed = [];

  /**
   * Function to test Api v3.0.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   */
  public function testApi(AcceptanceTester $i) {

    // Login as admin.
    $i->amOnPage('user/logout');
    $i->logInWithRole('administrator');

    // Get the Api node types.
    $content_types = $this->getApiContentTypes();

    // Step through each content type and test the api response.
    foreach ($content_types as $machine_name => $content_type) {

      // Create nodes for specified content type.
      $this->createNodesForApi($i, $machine_name, $content_type);

      // Test root url count is correct.
      $i->amOnPage('api/v3.0/' . $machine_name);
      $this->checkNodeCount($i, $machine_name, 4);

      // Get filters for specified content type.
      $filters = $this->getApiFilters($machine_name);

      // Filters that don't allow multiple filtering.
      $noMultFilters = ['author_first_name', 'author_last_name', 'keywords'];

      // Step through each of the filters and check that the
      // api values are correct.
      foreach ($filters as $filter => $data) {

        // Test each filter count is correct.
        $this->checkNodeCount(
          $i,
          $machine_name . '?' . $filter . '=' . $data[0],
          $data[1]
        );
        $this->checkNodeCount(
          $i,
          $machine_name . '?' . $filter . '=' . $data[0] . '9',
          0
        );

        // Check if current filter doesn't allow multiple filtering.
        if (!in_array($filter, $noMultFilters)) {
          $this->checkNodeCount(
            $i,
            $machine_name . '?' . $filter . '=' . $data[0] . ',' . $data[2],
            $data[3]
          );
        }

        // Step through each of the filters and check if there is a combo
        // filter and if so, check that api is correct.
        foreach ($filters as $combofilter => $combodata) {

          // If the combo filter is the same as the filter, break out of
          // the loop since we do not want to test the same filter combined.
          if ($combofilter == $filter) {
            continue;
          }

          // Test each filter combination count is correct.
          $this->checkNodeCount(
            $i,
            $machine_name . '?' . $filter . '=' . $data[0] . '&' . $combofilter . '=' . $combodata[0],
            min($data[1], $combodata[1])
          );
          $this->checkNodeCount(
            $i,
            $machine_name . '?' . $filter . '=' . $data[0] . '&' . $combofilter . '=' . $combodata[0] . '9',
            0
          );
        }
      }
    }
  }

  /**
   * Function to create content for the Api to test.
   *
   * @param AcceptanceTester $i
   *   The acceptance tester.
   * @param string $machine_name
   *   The content type machine name.
   * @param string $content_type
   *   The content type.
   */
  private function createNodesForApi(AcceptanceTester $i, string $machine_name, string $content_type): void {

    // Have at least and empty array to return.
    $fields = [];

    // Get the correct things to create based on content type.
    switch ($machine_name) {

      case 'blog':

        // Create blog tag.
        $this->tidsUsed['blog_tag'] = $i->createTerm('uw_vocab_blog_tags', 'btag');

        // Create 4 nodes blog content type with different field values.
        for ($j = 1; $j <= 4; $j++) {
          $fields['field_uw_blog_tags'] = $this->tidsUsed['blog_tag'];
          $j <= 1 ? $fields['field_uw_blog_date'] = '2024-05-1' : $fields['field_uw_blog_date'] = '2024-05-21';
          $j <= 2 ? $fields['field_author'] = 'admin1' : $fields['field_author'] = 'admin2';
          $fields['field_uw_audience'] = $j;

          // Create content.
          $this->nodesUsed[$content_type . $j] = $i->createCtNode($content_type, $machine_name . $j, TRUE, $fields,);
        }
        break;

      case 'catalog':

        // Create catalog taxonomies.
        $this->tidsUsed['catalog_tag'] = $i->createTerm('uw_vocab_catalogs', 'ctag');
        $this->tidsUsed['catalog_categories'] = $i->createTerm('uw_vocab_catalog_categories', 'ccategory');

        // Create 4 catalog nodes with different field values.
        for ($j = 1; $j <= 4; $j++) {
          $fields['catalog'] = $this->tidsUsed['catalog_tag'];
          $j <= 1 ? $fields['field_uw_audience'] = 1 : $fields['field_uw_audience'] = 2;
          $j <= 2 ? $fields['field_uw_catalog_category'] = $this->tidsUsed['catalog_categories'] : $fields['field_uw_catalog_category'] = [];
          $j <= 3 ? $fields['field_uw_catalog_faculty'] = 5 : $fields['field_uw_catalog_faculty'] = 6;

          // Create content.
          $this->nodesUsed[$content_type . $j] = $i->createCtNode($content_type, $machine_name . $j, TRUE, $fields,);
        }
        break;

      case 'contact':

        // Create 4 contact nodes with different field values.
        for ($j = 1; $j <= 4; $j++) {
          $j <= 1 ? $fields['field_uw_ct_contact_affiliation'] = 'University of Waterloo' : $fields['field_uw_ct_contact_affiliation'] = 'Waterloo University';
          $j <= 2 ? $fields['field_uw_ct_contact_email'] = 'ist@uwaterloo.ca' : $fields['field_uw_ct_contact_email'] = 'contact@uwaterloo.ca';
          $fields['field_uw_ct_contact_title'] = 'Professor' . $j;

          // Create content.
          $this->nodesUsed[$content_type . $j] = $i->createCtNode($content_type, $machine_name . $j, TRUE, $fields,);
        }
        break;

      case 'event':
        $this->tidsUsed['event_tag'] = $i->createTerm('uw_tax_event_tags', 'etag');

        // Create event 4 nodes with different field values.
        for ($j = 1; $j <= 4; $j++) {
          $fields['field_uw_event_tags'] = $this->tidsUsed['event_tag'];
          $j <= 1 ? $fields['field_uw_event_type'] = 1 : $fields['field_uw_event_type'] = 2;
          $j <= 2 ? $fields['field_uw_event_date'] = [
            'value' => date(strtotime('2024-05-01 10:00:00')),
            'end_value' => date(strtotime('2024-05-01 15:00:00')),
            'duration' => '300',
            'rrule' => NULL,
            'rrule_index' => NULL,
            'timezone' => '',
          ]
          : $fields['field_uw_event_date'] = [
            'value' => date(strtotime('2024-05-01 10:00:00')),
            'end_value' => date(strtotime('2024-05-11 15:00:00')),
            'duration' => '300',
            'rrule' => NULL,
            'rrule_index' => NULL,
            'timezone' => '',
          ];
          $fields['field_uw_audience'] = $j;

          // Create content.
          $this->nodesUsed[$content_type . $j] = $i->createCtNode($content_type, $machine_name . $j, TRUE, $fields,);
        }
        break;

      case 'news':
        $this->tidsUsed['news_tag'] = $i->createTerm('uw_vocab_news_tags', 'ntag');

        // Create 4 news nodes with different field values.
        for ($j = 1; $j <= 4; $j++) {
          $fields['field_uw_news_tags'] = $this->tidsUsed['news_tag'];
          $j <= 2 ? $fields['field_uw_news_date'] = '2024-05-1' : $fields['field_uw_news_date'] = '2024-05-21';
          $fields['field_uw_audience'] = $j;

          // Create content.
          $this->nodesUsed[$content_type . $j] = $i->createCtNode($content_type, $machine_name . $j, TRUE, $fields,);
        }
        break;

      case 'opportunity':

        // Create 4 opportunity nodes with different field values.
        for ($j = 1; $j <= 4; $j++) {
          $fields['field_uw_opportunity_deadline'] = '2024-05-21';
          $j <= 3 ? $fields['field_uw_opportunity_date'] = '2024-05-1' : $fields['field_uw_opportunity_date'] = '2024-05-11';
          $j <= 2 ? $fields['field_uw_opportunity_start_date'] = '2024-05-03' : $fields['field_uw_opportunity_start_date'] = '2024-05-13';
          $j <= 1 ? $fields['field_uw_opportunity_end_date'] = '2024-05-04' : $fields['field_uw_opportunity_end_date'] = '2024-05-14';
          $j <= 3 ? $fields['field_uw_opportunity_employment'] = $i->getTidFromName('Full time') : $fields['field_uw_opportunity_employment'] = $i->getTidFromName('Part time');
          $j <= 2 ? $fields['field_uw_opportunity_type'] = $i->getTidFromName('Paid') : $fields['field_uw_opportunity_type'] = $i->getTidFromName('Research Participant');
          $j <= 1 ? $fields['field_uw_opportunity_pay_type'] = $i->getTidFromName('Hourly') : $fields['field_uw_opportunity_pay_type'] = $i->getTidFromName('Salary');
          $fields['field_uw_opportunity_job_id'] = $j;

          // Create content.
          $this->nodesUsed[$content_type . $j] = $i->createCtNode($content_type, $machine_name . $j, TRUE, $fields,);
        }
        break;

      case 'profile':
        $this->tidsUsed['profile_type'] = $i->createTerm('uw_vocab_profile_type', 'ptype');

        // Create 4 profile nodes with different field values.
        for ($j = 1; $j <= 4; $j++) {
          $j <= 1 ? $fields['field_uw_ct_profile_type'] = $this->tidsUsed['profile_type'] : $fields['field_uw_ct_profile_type'] = [];
          $j <= 2 ? $fields['field_uw_ct_profile_affiliation'] = 'University of Waterloo' : $fields['field_uw_ct_profile_affiliation'] = 'Waterloo University';
          $fields['field_uw_ct_profile_title'] = 'Student' . $j;

          // Create content.
          $this->nodesUsed[$content_type . $j] = $i->createCtNode($content_type, $machine_name . $j, TRUE, $fields,);
        }
        break;

      case 'project':
        $this->tidsUsed['project_topic'] = $i->createTerm('uw_vocab_project_topic', 'ptopic');

        // Create 4 project nodes with different field values.
        for ($j = 1; $j <= 4; $j++) {
          $fields['field_uw_project_topics'] = $this->tidsUsed['project_topic'];
          $j <= 2 ? $fields['field_uw_audience'] = 1 : $fields['field_uw_audience'] = 2;

          // Create content.
          $this->nodesUsed[$content_type . $j] = $i->createCtNode($content_type, $machine_name . $j, TRUE, $fields,);
        }
        break;

      case 'service':
        $this->tidsUsed['service_category'] = $i->createTerm('uw_vocab_service_categories', 'scategory');

        // Create 4 service nodes with different field values.
        for ($j = 1; $j <= 4; $j++) {
          $j <= 1 ? $fields['field_uw_service_audience'] = 1 : $fields['field_uw_service_audience'] = 2;
          $j <= 2 ? $fields['field_uw_service_status'] = 'active' : $fields['field_uw_service_status'] = 'inactive';
          $fields['service'] = $this->tidsUsed['service_category'];

          // Create content.
          $this->nodesUsed[$content_type . $j] = $i->createCtNode($content_type, $machine_name . $j, TRUE, $fields,);
        }
        break;

      case 'webpage':

        // Create 4 webpage nodes with different field values.
        for ($j = 1; $j <= 3; $j++) {

          // Create content.
          $this->nodesUsed[$content_type . $j] = $i->createCtNode($content_type, $machine_name . $j, TRUE, $fields,);
        }
        break;

      case 'publication-reference':

        // Initialize field array.
        $values = [
          'title' => [],
          'bibcite_year' => [],
          'author_first_name' => [],
          'author_last_name' => [],
          'keywords' => [],
        ];

        // Get all the different bibcite types.
        $types = $i->getBibciteTypes();

        // Create 4 publication-references with random field values.
        for ($j = 0; $j <= 3; $j++) {

          // Create a random title and add to field array.
          $title = $i->uwRandomString();
          $fields['title'] = $title;
          $values['title'][$j] = $title;

          // Create a random year and add to field array.
          $year = rand(1924, 2024);

          // Check to make sure year is not already been selected.
          while (in_array($year, $values['bibcite_year'])) {
            $year = rand(1924, 2024);
          }
          $fields['bibcite_year'] = $year;
          $values['bibcite_year'][$j] = $year;

          // Create a contributor and add to field array.
          $firstName = $i->uwRandomString();
          $lastName = $i->uwRandomString();
          $fields['author'] = $i->createBibciteContributor($lastName, $firstName);
          $values['author_first_name'][$j] = $firstName;
          $values['author_last_name'][$j] = $lastName;

          // Create a random keyword and add to field array.
          $keyword = $i->uwRandomString();
          $fields['keywords'] = $i->createBibciteKeyword($keyword);
          $values['keywords'][$j] = $keyword;

          // Set global value array to field array.
          $this->valuesUsed = $values;

          // Create publication reference with a random type.
          $i->createBibcite($types[array_rand($types)], TRUE, $fields);
        }
    }
  }

  /**
   * Function to get the content types for the Api.
   *
   * @return string[]
   *   Array of content types for the Api.
   */
  private function getApiContentTypes(): array {

    return [
      'blog' => 'uw_ct_blog',
      'catalog' => 'uw_ct_catalog_item',
      'contact' => 'uw_ct_contact',
      'event' => 'uw_ct_event',
      'news' => 'uw_ct_news_item',
      'opportunity' => 'uw_ct_opportunity',
      'profile' => 'uw_ct_profile',
      'project' => 'uw_ct_project',
      'service' => 'uw_ct_service',
      'webpage' => 'uw_ct_web_page',
      'publication-reference' => '',
    ];
  }

  /**
   * Function to get the filters for the Api.
   *
   * @return string[]
   *   Array of filters for the Api.
   */
  private function getApiFilters(string $content_type): array {

    // Have at least an empty array to return.
    $filters = [];

    // Get the filters based on the content type.
    switch ($content_type) {

      case 'blog':

        $filters = [
          'title' => [$content_type . '1', 1, $content_type . '2', 2],
          'title_contains' => ['g', 4, 'b', 4],
          'author' => ['admin1', 2, 'admin1', 2],
          'date' => ['2024-05-1', 1, '2024-05-21', 4],
          'audience' => [
            'Current students',
            1,
            'Current undergraduate students',
            2,
          ],
          'tags' => ['btag', 4, 'btag', 4],
        ];
        break;

      case 'catalog':

        $filters = [
          'title' => [$content_type . '1', 1, $content_type . '2', 2],
          'title_contains' => ['t', 4, 'c', 4],
          'audience' => [
            'Current students',
            1,
            'Current undergraduate students',
            4,
          ],
          'catalog' => ['ctag', 4, 'ctag', 4],
          'category' => ['ccategory', 2, 'ccategory2', 2],
          'faculty' => [
            'Future undergraduate students',
            3,
            'Future graduate students',
            4,
          ],
        ];
        break;

      case 'contact':

        $filters = [
          'title' => [$content_type . '1', 1, $content_type . '2', 2],
          'title_contains' => ['o', 4, 'n', 4],
          'affiliation' => [
            'University of Waterloo',
            1,
            'Waterloo University',
            4,
          ],
          'email' => ['ist@uwaterloo.ca', 2, 'contact@uwaterloo.ca', 4],
          'title_position_description' => [
            'Professor1',
            1,
            'Professor3',
            2,
          ],
        ];
        break;

      case 'event':

        $filters = [
          'title' => [$content_type . '1', 1, $content_type . '2', 2],
          'title_contains' => ['e', 4, 'v', 4],
          'end_date' => ['2024-05-01', 2, '2024-05-11', 4],
          'start_date' => ['2024-05-01', 4, '2024-05-10', 4],
          'audience' => [
            'Current students',
            1,
            'Current graduate students',
            2,
          ],
          'tags' => ['etag', 4, 'etag', 4],
          'type' => [
            'Current students',
            1,
            'Current undergraduate students',
            4,
          ],
        ];
        break;

      case 'news':

        $filters = [
          'title' => [$content_type . '1', 1, $content_type . '2', 2],
          'title_contains' => ['w', 4, 's', 4],
          'date' => ['2024-05-1', 2, '2024-05-21', 4],
          'audience' => [
            'Current students',
            1,
            'Current undergraduate students',
            2,
          ],
          'tags' => ['ntag', 4, 'ntag', 4],
        ];
        break;

      case 'opportunity':

        $filters = [
          'title' => [$content_type . '1', 1, $content_type . '2', 2],
          'title_contains' => ['y', 4, 'r', 4],
          'application_deadline' => ['2024-05-21', 4, '2024-05-01', 4],
          'date_posted_application_open' => ['2024-05-1', 3, '2024-05-11', 4],
          'end_date' => ['2024-05-04', 1, '2024-05-14', 4],
          'job_id' => ['1', 1, '2', 2],
          'opportunity_type' => ['Paid', 2, 'Research Participant', 4],
          'rate_of_pay_type' => ['Hourly', 1, 'Salary', 4],
          'start_date' => ['2024-05-03', 2, '2024-05-13', 4],
        ];
        break;

      case 'profile':

        $filters = [
          'title' => [$content_type . '1', 1, $content_type . '2', 2],
          'title_contains' => ['i', 4, 'f', 4],
          'affiliation' => [
            'University of Waterloo',
            2,
            'Waterloo University',
            4,
          ],
          'tags' => ['ptype', 1, '', 1],
          'title_position_description' => ['Student1', 1, 'Student4', 2],
        ];
        break;

      case 'project':

        $filters = [
          'title' => [$content_type . '1', 1, $content_type . '2', 2],
          'title_contains' => ['p', 4, 'j', 4],
          'project_status' => ['Current Projects', 4, 'Future Projects', 4],
          'topics' => ['ptopic', 4, 'ptopic', 4],
          'audience' => [
            'Current students',
            2,
            'Current undergraduate students',
            4,
          ],
        ];
        break;

      case 'service':

        $filters = [
          'title' => [$content_type . '1', 1, $content_type . '2', 2],
          'title_contains' => ['e', 4, 'c', 4],
          'category' => ['scategory', 4, '', 4],
          'status' => ['active', 2, 'inactive', 4],
          'who_can_use' => [
            'Current students',
            1,
            'Current undergraduate students',
            4,
          ],
        ];
        break;

      case 'webpage':

        $filters = [
          'title' => [$content_type . '1', 1, $content_type . '2', 2],
          'title_contains' => ['p', 3, 'e', 4],
        ];
        break;

      case 'publication-reference':

        // Choose two different publication references.
        $rand_keys = array_rand([0, 1, 2, 3], 2);

        // Title, title_contains & year have two filters as they
        // can do multiple filtering.
        // Author and keywords only have one as they
        // cannot do multiple filtering.
        $filters = [
          'title' => [
            $this->valuesUsed['title'][$rand_keys[0]],
            1,
            $this->valuesUsed['title'][$rand_keys[1]],
            2,
          ],
          'title_contains' => [
            substr($this->valuesUsed['title'][$rand_keys[0]], 1),
            1,
            substr($this->valuesUsed['title'][$rand_keys[1]], 1),
            2,
          ],
          'author_first_name' => [
            $this->valuesUsed['author_first_name'][$rand_keys[0]],
            1,
          ],
          'author_last_name' => [
            $this->valuesUsed['author_last_name'][$rand_keys[0]],
            1,
          ],
          'keywords' => [
            $this->valuesUsed['keywords'][$rand_keys[0]],
            1,
          ],
          'year' => [
            $this->valuesUsed['bibcite_year'][$rand_keys[0]],
            1,
            $this->valuesUsed['bibcite_year'][$rand_keys[1]],
            2,
          ],
        ];
    }
    return $filters;
  }

  /**
   * Function to check correct node count.
   *
   * @param AcceptanceTester $i
   *   Checks if 'count' matches $count.
   * @param string $path
   *   The Api path.
   * @param int $count
   *   Expected response count.
   */
  private function checkNodeCount(AcceptanceTester $i, string $path, int $count): void {

    // Sends get request to api and checks response for correct node count.
    $i->sendGet('http://nginx/api/v3.0/' . $path);
    $i->seeResponseContainsJson(['count' => $count]);
  }

  // phpcs:disable

  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _after(AcceptanceTester $i): void {
    // phpcs:enable

    // Compelte the block tests.
    $i->completeBlockTests($this->tidsUsed, $this->nodesUsed);

    // Delete of all bibcite blocks.
    $i->deleteAllBibcite();
  }

  // phpcs:disable

  /**
   * Function to run after the test completes.
   *
   * @param AcceptanceTester $i
   *   Acceptance test variable.
   */
  public function _failed(AcceptanceTester $i): void {
    // phpcs:enable

    // Compelte the block tests.
    $i->completeBlockTests($this->tidsUsed, $this->nodesUsed);

    // Delete of all bibcite blocks.
    $i->deleteAllBibcite();
  }

}
