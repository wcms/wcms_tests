<?php

namespace Helper;

use Codeception\Module;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;

/**
 * Class Acceptance.
 *
 * Here you can define custom actions
 * all public methods declared in helper
 * class will be available in $I.
 */
class Acceptance extends Module {

  /**
   * Function to generate a random string.
   *
   * @param int $length
   *   Length of the random string.
   * @param bool $use_spaces
   *   Flag to use spaces when generating a random string.
   *
   * @return string
   *   The random string.
   */
  public function uwRandomString(int $length = 8, bool $use_spaces = FALSE): string {

    // The characters to be used in the random string.
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

    // The length of the characters that can be in
    // the random string.
    $charactersLength = strlen($characters);

    // At least have something to return as the random string.
    $randomString = '';

    // Step through and get a random string, using the length.
    for ($i = 0; $i < $length; $i++) {

      // If the flag is set to use spaces, and we are not on
      // the first character, and we have at least 6 other
      // characters then add a space, otherwise get a
      // random character.
      if ($use_spaces && $i !== 0 && ($i % 6 == 0)) {
        $randomString .= ' ';
      }
      else {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
    }

    // Return the random string.
    return $randomString;
  }

  /**
   * Fill a CK Editor field.
   *
   * @param string $value
   *   The value to enter into CK Editor.
   * @param string $selector
   *   A CSS (only) selector to identify the <textarea> element.
   */
  public function fillCkEditor(string $value, $selector): void {

    // Get the id of the selector.
    $id = $this->getModule("WebDriver")->executeJs("return document.querySelector('$selector').id");

    // Set the ckeditor field.
    $this->getModule("WebDriver")->executeJs("CKEDITOR.instances['$id'].setData(" . json_encode($value) . ")");
  }

  /**
   * See in a CK Editor field.
   *
   * @param \AcceptanceTester $i
   *   The acceptance tester.
   * @param string $value
   *   The value to enter into CK Editor.
   * @param string $selector
   *   A CSS (only) selector to identify the <textarea> element.
   */
  public function seeInCkEditor(
    \AcceptanceTester $i,
    string $value,
    string $selector,
  ): void {

    // Get the id of the ckeditor field.
    $id = $this->getModule("WebDriver")
      ->executeJs("return document.querySelector('$selector').id");

    // Grab the content from the ckeditor field.
    $content = $this->getModule("WebDriver")
      ->executeJs("return CKEDITOR.instances['$id'].getData()");

    // Asset if value found int ck editor.
    $i->assertTrue(
      strpos($content, $value) !== FALSE,
      'ck editor contains: ' . $value
    );
  }

  /**
   * Function to get the roles.
   *
   * @return string[]
   *   Array of roles.
   */
  public function getRoles(): array {

    return [
      'authenticated',
      'administrator',
      'uw_role_site_owner',
      'uw_role_site_manager',
      'uw_role_content_editor',
      'uw_role_content_author',
      'uw_role_form_editor',
      'uw_role_form_results_access',
    ];

  }

  /**
   * A function to create a term.
   *
   * @param string $vocab
   *   The vocab machine name, such as "uw_vocab_service_categories".
   * @param string $term_name
   *   The name of the term.
   *
   * @return int
   *   Taxonomy term id.
   */
  public function createTerm(string $vocab, string $term_name): int {

    // Create a new term.
    $term = Term::create([
      'name' => $term_name,
      'vid' => $vocab,
    ]);

    // Save the term, do it here so that we still have the
    // term object to be used later.
    $term->save();

    // Get the tid of the term from the object.
    $tid = $term->id();

    return $tid;
  }

  /**
   * Get current url from WebDriver.
   *
   * @return string
   *   The current url.
   *
   * @throws \Codeception\Exception\ModuleException
   */
  public function getCurrentUrl(): string {
    return $this->getModule('WebDriver')->_getCurrentUri();
  }

  /**
   * A function to get all UW content types.
   *
   * @param bool $filter
   *   When TRUE, return only content types that have metatag enabled.
   *
   * @return array
   *   An array of all UW content types filtered by TRUE/FALSE.
   */
  public function getContentTypes(bool $filter = FALSE): array {

    // This is the list of all UW content types, with a TRUE/FALSE, indicating
    // whether or not a metatag tag can be attached to this content type.
    $content_types = [
      'uw_ct_blog' => TRUE,
      'uw_ct_catalog_item' => TRUE,
      'uw_ct_contact' => TRUE,
      'uw_ct_event' => TRUE,
      'uw_ct_expand_collapse_group' => FALSE,
      'uw_ct_news_item' => TRUE,
      'uw_ct_profile' => TRUE,
      'uw_ct_sidebar' => FALSE,
      'uw_ct_site_footer' => FALSE,
      'uw_ct_web_page' => TRUE,
    ];

    $return_content_types = [];
    foreach ($content_types as $key => $value) {
      if (($filter && $value) || !$filter) {
        $return_content_types[] = $key;
      }
    }

    return $return_content_types;
  }

  /**
   * Function to get all the blocks.
   *
   * @return string[]
   *   Array of blocks.
   */
  public function getBlocks(): array {

    return [
      'Banner images',
      'Blockquote',
      'Call to action',
      'Copy text',
      'Expand/collapse',
      'Facebook',
      'Facts and figures',
      'Image',
      'Image gallery',
      'Mailman subscription',
      'Related links',
      'Tags',
      'Timeline',
      'CodePen',
      'Facebook',
      'Google Maps',
      'Instagram',
      'Mailchimp',
      'PowerBI',
      'Remote video',
      'Social Intents',
      'Tableau visualization',
      'X (formerly Twitter)',
      'Automatic list',
      'Manual list',
      'Multi-type list',
      'Waterloo Events',
      'Waterloo News',
      'Catalog search',
      'Project search',
      'Publication reference search',
      'Service search',
      'Content teaser',
      'Webform',
    ];
  }

  /**
   * Function to create a webpage.
   *
   * @param string $title
   *   The title of the webpage.
   *
   * @return \Drupal\node\Entity\Node
   *   The webpage node.
   */
  public function createWebPage(string $title): Node {

    return $this->createCtNode('uw_ct_web_page', $title, TRUE);
  }

  /**
   * Function to get the path of a webpage.
   *
   * @param \Drupal\node\Entity\Node $webpage
   *   The node of the webpage.
   *
   * @return string
   *   The path of the webpage.
   */
  public function getWebPagePath(Node $webpage): string {
    return \Drupal::service('path_alias.manager')
      ->getAliasByPath('/node/' . $webpage->id());
  }

  /**
   * A function to create a node.
   *
   * @param string $content_type
   *   The content type, such as uw_ct_blog.
   * @param string $title
   *   The title for the node.
   * @param bool $set_published
   *   Whether to publish this node.
   * @param array $fields
   *   Array of fields that need to be specified.
   *
   * @return \Drupal\node\Entity\Node
   *   The node.
   */
  public function createCtNode(
    string $content_type,
    string $title = NULL,
    bool $set_published = FALSE,
    array $fields = NULL,
  ): Node {

    // Set the title and summary.
    if (!$title) {
      $title = $this->uwRandomString();
    }
    $summary = $this->uwRandomString();

    // Based on content type, set the fields.
    switch ($content_type) {

      case 'uw_ct_blog':
        $edit = [
          'title' => $title,
          'field_uw_blog_summary' => $summary,
        ];
        break;

      case 'uw_ct_catalog_item':
        $edit = [
          'title' => $title,
          'field_uw_catalog_catalog' => [
            'target_id' => $fields['catalog'] ?? $this->createTerm('uw_vocab_catalogs',
                $this->uwRandomString()),
          ],
        ];
        break;

      case 'uw_ct_contact':
        $edit = [
          'title' => $title,
          'field_uw_ct_contact_sort_name' => $fields['field_uw_ct_contact_sort_name'] ?? $this->uwRandomString(),
          'field_uw_ct_contact_affiliation' => $fields['field_uw_ct_contact_affiliation'] ?? $this->uwRandomString(),
          'field_uw_ct_contact_title' => $fields['field_uw_ct_contact_title'] ?? $this->uwRandomString(),
        ];
        break;

      case 'uw_ct_news_item':
        $edit = [
          'title' => $title,
          'field_uw_news_summary' => $summary,
        ];
        break;

      case 'uw_ct_event':
        $edit = [
          'title' => $title,
          'field_uw_event_summary' => $summary,
          'field_uw_event_date' => $fields['field_uw_event_date'] ?? [
            'value' => date(strtotime('10:00:00')),
            'end_value' => date(strtotime('15:00:00')),
            'duration' => '300',
            'rrule' => NULL,
            'rrule_index' => NULL,
            'timezone' => '',
          ],
        ];
        break;

      case 'uw_ct_opportunity':
        $edit = [
          'title' => $title,
          'field_uw_opportunity_position' => $fields['field_uw_opportunity_position'] ?? $this->uwRandomString(),
          'field_uw_opportunity_type' => $fields['field_uw_opportunity_type'] ?? $this->getTidFromName('Research participant'),
          'field_uw_opportunity_employment' => $fields['field_uw_opportunity_employment'] ?? $this->getTidFromName('Full time'),
          'field_uw_opportunity_post_by' => $fields['field_uw_opportunity_post_by'] ?? $this->uwRandomString(),
          'field_uw_opportunity_pos_number' => $fields['field_uw_opportunity_pos_number'] ?? 1,
          'field_uw_opportunity_date' => $fields['field_uw_opportunity_date'] ?? date('Y-m-d'),
        ];
        break;

      case 'uw_ct_profile':
        $edit = [
          'title' => $title,
          'field_uw_ct_profile_sort_name' => $fields['field_uw_ct_profile_sort_name'] ?? $this->uwRandomString(),
          'field_uw_profile_summary' => $summary,
          'field_uw_ct_profile_affiliation' => $fields['field_uw_ct_profile_affiliation'] ?? $this->uwRandomString(),
          'field_uw_ct_profile_title' => $fields['field_uw_ct_profile_title'] ?? $this->uwRandomString(),
        ];
        break;

      case 'uw_ct_project':
        $storage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
        $terms = $storage->loadByProperties([
          'name' => 'Current projects',
          'vid' => 'uw_vocab_project_status',
        ]);
        $tid = array_keys($terms)[0];

        $edit = [
          'title' => $title,
          'field_uw_project_summary' => $summary,
          'field_uw_project_status' => $tid,
        ];
        break;

      case 'uw_ct_service':
        $edit = [
          'title' => $title,
          'field_uw_service_status' => 'active',
          'field_uw_service_category' => $fields['service'] ?? $this->createTerm('uw_vocab_service_categories', $this->uwRandomString()),
        ];
        break;

      case 'uw_ct_expand_collapse_group':
      case 'uw_ct_site_footer':
      case 'uw_ct_web_page':
        $edit = [
          'title' => $title,
        ];
        break;
    }

    // Add the node type and status.
    $edit['type'] = $content_type;

    // Based on the set published flag, set the status and meta description.
    if ($set_published) {

      // If this is not expand collapse, set the meta description.
      if ($content_type !== 'uw_ct_expand_collapse_group') {

        // If the meta description field is set, use that, if not
        // then use the title.
        if (isset($fields['meta_description'])) {
          $edit['field_uw_meta_description'] = $fields['meta_description'];
        }
        else {
          $edit['field_uw_meta_description'] = $title;
        }
      }

      // Set the status to published.
      $edit['status'] = 1;
    }
    else {

      // If the meta description field is set, then set it in the edits.
      if (isset($fields['meta_description'])) {
        $edit['field_uw_meta_description'] = $fields['meta_description'];
      }

      // Set the status to unpublished.
      $edit['status'] = 0;
    }

    // Set the author to wcmsadmin.
    $edit['uid'] = 1;

    if ($fields) {
      $edit = array_merge($edit, $fields);
    }

    // Create and save the node.
    $node = Node::create($edit);

    // If the set published flag, set the things in the node
    // to ensure that it is published.
    if ($set_published) {
      $node->setPublished(TRUE);
      $node->set('moderation_state', 'published');
    }

    // Save the node.
    $node->save();

    // Get the node id.
    return $node;
  }

  /**
   * Get a term id from a term name.
   *
   * @param string $term_name
   *   The term name.
   *
   * @return int
   *   The tid.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getTidFromName(string $term_name): int {

    // Load the term.
    $term = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadByProperties(['name' => $term_name]);

    // If there are terms, get the first and the tid.
    // If no terms, set tid to 0.
    if (count($term) > 0) {
      $term = current($term);
      $tid = $term->id();
    }
    else {
      $tid = 0;
    }

    return $tid;
  }

  /**
   * Function to get early, middle and late dates.
   *
   * @return array
   *   Array with early, middle and late dates.
   */
  public function getEarlyMiddleLateDates(): array {

    // Get early, middle and late dates.
    $dates['early'] = date('m/d/Y', strtotime("+1 days"));
    $dates['middle'] = date('m/d/Y', strtotime("+1 months"));
    $dates['late'] = date('m/d/Y', strtotime("+2 months"));

    return $dates;
  }

  /**
   * Function to delete all content blocks.
   */
  public function deleteAllBlocks(): void {

    // Get the block content storage.
    $block_storage = \Drupal::service('entity_type.manager')->getStorage('block_content');

    // Get all the content blocks.
    $blocks = $block_storage->loadMultiple();

    // Step through each block and delete it.
    foreach ($blocks as $block) {
      $block->delete();
    }
  }

  /**
   * Function to complete block test.
   *
   * @param array $tidsUsed
   *   The term ids used.
   * @param array $nodesUsed
   *   The nodes used.
   */
  public function completeBlockTests(array $tidsUsed, array $nodesUsed): void {

    // If we used any terms, delete them.
    if (!empty($tidsUsed)) {
      $controller = \Drupal::entityTypeManager()
        ->getStorage('taxonomy_term');
      $entities = $controller->loadMultiple($tidsUsed);
      $controller->delete($entities);
    }

    // Step through each of the nodes used and delete them.
    foreach ($nodesUsed as $node) {
      $node->delete();
    }

    // Delete all the blocks.
    $this->deleteAllBlocks();
  }

  /**
   * Function to see most recent API request response.
   *
   * @return string
   *   Returns response string.
   */
  public function seeResponse(): string {

    // Gets response.
    $response = $this->getModule('REST')->response;
    return $response;
  }

  /**
   * A function to create a Bibcite Keyword.
   *
   * @param string $name
   *   The keyword name.
   */
  public function createBibciteKeyword(string $name) {
    $storage = \Drupal::entityTypeManager()->getStorage('bibcite_keyword');
    $keyw = $storage->create(['name' => $name]);
    $keyw->save();

    return $keyw;
  }

  /**
   * A function to create a Bibcite Contributor.
   *
   * @param string $last_name
   *   The Contributor's last name.
   * @param string $first_name
   *   The Contributor's first name.
   */
  public function createBibciteContributor(string $last_name, string $first_name = NULL) {
    $storage = \Drupal::entityTypeManager()->getStorage('bibcite_contributor');
    $contributor = $storage->create([
      'last_name' => $last_name,
      'first_name' => $first_name,
    ]);
    $contributor->save();

    return $contributor;
  }

  /**
   * A function to get a Bibcite Keyword from the name.
   *
   * @param string $name
   *   The keyword name.
   */
  public function getKeywordFromName(string $name) {
    $keyw = \Drupal::entityTypeManager()->getStorage('bibcite_keyword')->loadByProperties(['name' => $name]);
    if (count($keyw) > 0) {
      $keyw = current($keyw);
      return $keyw;
    }
    else {
      return NULL;
    }
  }

  /**
   * A function to get a Bibcite Contributor from the name.
   *
   * @param string $last_name
   *   The Contributors last name.
   * @param string $first_name
   *   The Contributors first name.
   */
  public function getContributorFromName(string $last_name, string $first_name = NULL) {
    if ($first_name) {
      $contributor = \Drupal::entityTypeManager()
        ->getStorage('bibcite_contributor')
        ->loadByProperties([
          'last_name' => $last_name,
          'first_name' => $first_name,
        ]);
    }
    else {
      $contributor = \Drupal::entityTypeManager()
        ->getStorage('bibcite_contributor')
        ->loadByProperties(['last_name' => $last_name]);
    }

    if (count($contributor) > 0) {
      $contributor = current($contributor);
      return $contributor;
    }
    else {
      return NULL;
    }
  }

  /**
   * A function to create a publication reference.
   *
   * @param string $type
   *   The reference type, such as book.
   * @param bool $set_published
   *   Whether to publish this node.
   * @param array $fields
   *   Array of fields that need to be specified.
   *      eg. String 'title',
   *          String 'bibcite_year',
   *          Int 'bibcite_publisher',
   *          Array 'keywords',
   *          Array 'author'.
   */
  public function createBibcite(
    string $type,
    bool $set_published = FALSE,
    array $fields = NULL,
  ) {
    $storage = \Drupal::entityTypeManager()->getStorage('bibcite_reference');

    // Set type.
    $edit['type'] = $type;

    // Set title.
    $edit['title'] = $fields['title'] ?? $this->uwRandomString();

    // Set publication year.
    if ($type != 'unpublished') {
      $edit['bibcite_year'] = $fields['bibcite_year'] ?? rand(1900, 2024);
    }

    // Set the status to published.
    if ($set_published) {
      $edit['status'] = 1;
      $edit['moderation_state'] = 'published';
    }

    // Set the author to wcmsadmin.
    $edit['uid'] = 1;

    // Set bibcite_publisher.
    switch ($type) {
      case 'book':
      case 'book_chapter':
      case 'conference_paper':
      case 'journal_article':
      case 'thesis':
        $fields['bibcite_publisher'] = $fields['bibcite_publisher'] ?? $this->uwRandomString();
    }

    if ($type != 'unpublished' && array_key_exists('bibcite_publisher', $fields)) {
      $edit['bibcite_publisher'] = $fields['bibcite_publisher'];
    }

    // Set bibcite_notes.
    if ($type == 'unpublished') {
      $edit['bibcite_notes'] = $fields['notes'] ?? $this->uwRandomString();
    }

    if ($fields) {
      $edit = array_merge($edit, $fields);
    }

    // Create and save the reference.
    $bibCite = $storage->create($edit);
    $bibCite->save();

    // If the set published flag, set the things in the reference.
    // to ensure that it is published.
    if ($set_published) {
      $bibCite->setPublished(TRUE);
      $bibCite->set('moderation_state', 'published');
    }

    // Get the id.
    return $bibCite;
  }

  /**
   * Returns an array of all bibcite item types.
   *
   * @return string[]
   *   List of all bibcite types.
   */
  public function getBibciteTypes(): array {
    return [
      'artwork',
      'audiovisual',
      'bill',
      'book',
      'book_chapter',
      'broadcast',
      'case',
      'chart',
      'classical',
      'conference_paper',
      'conference_proceedings',
      'database',
      'film',
      'government_report',
      'hearing',
      'journal',
      'journal_article',
      'legal_ruling',
      'magazine_article',
      'manuscript',
      'map',
      'miscellaneous',
      'miscellaneous_section',
      'newspaper_article',
      'patent',
      'personal',
      'presentation',
      'report',
      'software',
      'statute',
      'thesis',
      'unpublished',
      'website',
      'web_article',
      'web_project_page',
      'web_service',
    ];
  }

  /**
   * Function to generate bibcite items.
   *
   * @param array $bibciteItems
   *   List of bibcite items to generate. If null, generate all.
   * @param array $keywords
   *   List of keyword names.
   * @param bool $randomTitle
   *   Uses random titles if true, otherwise use the content type as title.
   * @param array $submit
   *   All other fields to submit for publication reference creation.
   */
  public function generateBibcite(array $bibciteItems = NULL, array $keywords = NULL, bool $randomTitle = FALSE, array $submit = NULL): void {
    if (!$bibciteItems) {
      // A list of all bibcite item types.
      $bibciteItems = $this->getBibciteTypes();
    }

    if (!$keywords) {
      // Create one of each bibcite item.
      foreach ($bibciteItems as $item) {
        $this->createBibcite($item, TRUE, ['title' => $randomTitle ? $this->uwRandomString() : $item]);
      }
    }
    else {
      // Get length of keyword and bibcite arrays.
      $keywords_count = count($keywords);
      $bibcite_count = count($bibciteItems);

      // Split bibcite item array proportional to the number of keywords.
      $section_length = floor($bibcite_count / $keywords_count);
      $split_bibciteItems = array_chunk($bibciteItems, $section_length);

      foreach ($keywords as $keyword) {
        $this->createBibciteKeyword($keyword);
      }

      // Create bibcite items.
      $i = 0;
      foreach (array_values($split_bibciteItems) as $array) {
        $keyword_id = $this->getKeywordFromName($keywords[$i]);

        foreach (array_values($array) as $item) {
          if ($submit) {
            $submit = array_merge($submit, [
              'title' => $randomTitle ? $this->uwRandomString() : $item,
              'keywords' => $keyword_id,
            ]);
          }
          else {
            $submit = [
              'title' => $randomTitle ? $this->uwRandomString() : $item,
              'keywords' => $keyword_id,
            ];
          }

          $this->createBibcite($item, TRUE, $submit);
        }

        $i++;
      }
    }
  }

  /**
   * A Function to delete all Bibcite content.
   */
  public function deleteAllBibcite(): void {

    // Get all pubrefs.
    $storage = \Drupal::entityTypeManager()->getStorage('bibcite_reference');
    $pubrefs = $storage->loadMultiple();

    // Step through each pubref and delete it.
    foreach ($pubrefs as $pubref) {
      $pubref->delete();
    }

    // Get all keywords.
    $storage = \Drupal::entityTypeManager()->getStorage('bibcite_keyword');
    $keyws = $storage->loadMultiple();

    // Step through each keyword and delete it.
    foreach ($keyws as $keyw) {
      $keyw->delete();
    }

    // Get all contributors.
    $storage = \Drupal::entityTypeManager()->getStorage('bibcite_contributor');
    $contributors = $storage->loadMultiple();

    // Step through each contributor and delete it.
    foreach ($contributors as $contributor) {
      $contributor->delete();
    }
  }

}
