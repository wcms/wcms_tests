<?php

namespace Step\Acceptance;

use Codeception\Util\Locator;

/**
 * Class TestContentType.
 *
 * Actions to test a content type.
 */
class TaxonomyTest extends \AcceptanceTester {

  /**
   * Function to test the taxonomies.
   *
   * @param string $content_type
   *   The content type.
   * @param string $content_type_name
   *   The content type name.
   * @param array $vocabs
   *   Array of vids.
   */
  public function testTaxonomies(
    string $content_type,
    string $content_type_name,
    array $vocabs
  ) {

    // The entity type manager.
    $manager = \Drupal::entityTypeManager()->getStorage('taxonomy_term');

    // Setup empty arrays.
    $parents = [];
    $terms_data = [];

    // Step through each of the vocabs and get the terms.
    foreach ($vocabs as $vid) {

      // Load the taxonomy tree using values.
      $tree = $manager->loadTree(
        $vid,
        0,
        NULL,
        TRUE
      );

      // Step through each of the terms and get the values.
      foreach ($tree as $term) {

        // If there are no children, get the term value into
        // the terms data array.
        if (empty($manager->loadChildren($term->id()))) {
          $terms_data[$vid][] = [
            'id' => $term->id(),
            'name' => $term->getName(),
          ];
        }
        // If there are children, this is a parent term, so
        // get the info into the parents array.
        else {
          $parents[$vid][] = [
            'id' => $term->id(),
            'name' => $term->getName(),
          ];
        }
      }
    }

    // Get the acceptance tester.
    $i = $this;

    // Login with the content author role.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_content_author');

    // Go to the add content type page.
    $i->amOnPage('node/add/' . $content_type);

    // Blog, event, news item have taxonomies fieldset.
    if ($content_type_name !== 'catalog') {

      // Get the selector for the taxonomy group.
      if ($content_type_name == 'project') {
        $selector = '#edit-group-uw-' . $content_type_name . '-taxonomies';
      }
      else {
        $selector = '#edit-group-uw-' . $content_type_name . '-tags';
      }

      // Check for the tags details element.
      $i->seeElement($selector);

      // If there are parents elements, check if on page.
      if (!empty($parents)) {

        // Step through each of the parents.
        foreach ($parents as $terms) {

          // Step through each of the terms in the parents
          // and check if it is on the page.
          foreach ($terms as $term) {

            // Check for the parent term on the page, project has a different
            // setup then the rest of the content types, so need to use a
            // different selector for it.
            if ($content_type_name == 'project') {
              $i->seeElement(Locator::contains('label[class="option"]', $term['name']));
            }
            else {
              $i->seeElement(Locator::contains('div[class="parent-term"]', $term['name']));
            }
          }
        }
      }

      // Step through each of the vocabs.
      foreach ($terms_data as $terms) {

        // Step through all the terms and see if they
        // are on the page.
        foreach ($terms as $term) {

          // Check for the term on the page.
          $i->seeElement(Locator::contains('label[class="option"]', $term['name']));
        }
      }
    }
    // Here we are on a catalog.
    else {

      // Ensure that we see message about creating catalog item.
      $i->see('You must create at least one catalog before adding a catalog item.');

      // Ensure that we see the add catalog link.
      $i->seeLink('Add a catalog');

      // Ensure that the link is correct to the add a catalog.
      $i->seeElement('a[href="/admin/structure/taxonomy/manage/uw_vocab_catalogs/add"]');

      // Remove the permission to create terms for catalogs.
      user_role_revoke_permissions('uw_role_content_author', ['create terms in uw_vocab_catalogs']);

      // Reload the add content type page.
      $i->amOnPage('node/add/' . $content_type);

      // Check that the new message about reaching out to someone
      // who has access is displayed.
      $i->see('Reach out to someone who has access to add a catalog.');

      // Grant the permission back to create terms for catalogs.
      user_role_grant_permissions('uw_role_content_author', ['create terms in uw_vocab_catalogs']);
    }
  }

}
