<?php

namespace Step\Acceptance;

use Codeception\Util\Locator;

/**
 * Class TestContentType.
 *
 * Actions to test a content type.
 */
class ContentType extends \AcceptanceTester {

  /**
   * Function to test the content type.
   *
   * @param string $content_type
   *   The content type.
   * @param array $fields
   *   The fields to look for.
   */
  public function testContentType(
    string $content_type,
    array $fields,
  ) {

    // Get the acceptance tester.
    $i = $this;

    // Login as site manager user.
    $i->amOnPage('user/logout');
    $i->logInWithRole('uw_role_site_manager');

    // Go to the content page.
    $i->amOnPage('node/add');

    // Ensure that we see the link to the content type.
    $i->seeLink($content_type);

    // Click on the content type link to add a new node.
    $i->click($content_type);

    // Wait for at least the first field.
    $i->waitForText($fields['fields'][0]);

    // Step through all the fields and check of the element exists
    // and that the label is on the page.
    foreach ($fields['fields'] as $label) {
      $i->see($label);
    }

    // If there are fields groups to check, then check them.
    if (isset($fields['groups'])) {

      // Step through the list of field groups and check that
      // element exits and that label is on the page.
      foreach ($fields['groups'] as $id => $label) {
        $i->seeElement('#' . $id);
        $i->see($label);
      }
    }

    // If there are required fields, test for them.
    if (isset($fields['required'])) {

      // Step through each of the required fields and test them.
      foreach ($fields['required'] as $required_field) {
        $i->seeElement(Locator::contains('[class*="form-required"]', $required_field));
      }
    }

    // If there are tabs to test, test for them.
    if (isset($fields['tabs'])) {

      foreach ($fields['tabs'] as $tab => $text) {
        $i->click('#' . $tab);
        $i->see($text);
      }
    }
  }

  /**
   * Function to test entering the fields on a content type.
   *
   * @param string $content_type
   *   The content type.
   * @param array $edits
   *   The fields to be used.
   * @param bool $full_test_flag
   *   Flag for running full tests.
   * @param string $role
   *   The role to test as.
   */
  public function testContentTypeEdits(
    string $content_type,
    array $edits,
    bool $full_test_flag = TRUE,
    string $role = 'uw_role_site_manager',
  ): void {

    // Get the acceptance tester.
    $i = $this;

    // Login as site manager user.
    $i->amOnPage('user/logout');
    $i->logInWithRole($role);

    // Go to the add content type page.
    $i->amOnPage('node/add/' . $content_type);

    // If this content type has a blank summary test it.
    if ($this->contentTypeHasBlankSummary($content_type)) {

      // Test that blank summary and description are on page.
      $i->seeElement(Locator::contains('label', 'Intentionally leave summary blank'));
      $i->see('When selected, the summary field will not be used. This may cause usability issues; use only when necessary.');

      // Check the blank summary check box.
      $i->checkOption('#edit-field-uw-blank-summary-value');

      // Get the label name of the summary based on
      // the content type.
      if ($content_type == 'uw_ct_opportunity') {
        $summary_label = 'Position summary';
      }
      else {
        $summary_label = 'Summary';
      }

      // Check that the summary is no longer on the page.
      $i->dontSeeElement(Locator::contains('label[class="form-required"]', $summary_label));

      // Uncheck the blank summary check box.
      $i->uncheckOption('#edit-field-uw-blank-summary-value');

      // Ensure that the summary is back and is required.
      $i->seeElement(Locator::contains('label[class="form-required"]', $summary_label));
    }

    // If the moderation state field is set and the role is content
    // author, need to set the state to needs revuiew since the
    // content author does not have permission to publish.
    if (
      isset($edits['select']['edit-moderation-state-0-state']) &&
      $role == 'uw_role_content_author'
    ) {

      $edits['select']['edit-moderation-state-0-state'] = 'uw_wf_needs_review';
    }

    // Content authors can not set moderation state to publish,
    // so setting that value to needs review when the ole
    // is content author.
    if (
      isset($edits['select']['edit-moderation-state-0-state']) &&
      $role == 'uw_role_content_author'
    ) {

      $edits['option']['edit-moderation-state-0-state'] = 'uw_wf_needs_review';
    }

    // Step through each of the edits and start
    // filling the fields.
    foreach ($edits as $field_type => $values) {

      // Step through each of the values in the edits and
      // fill in the fields.
      foreach ($values as $selector => $value) {

        // Fill in the field based on the field type.
        switch ($field_type) {

          case 'ckeditor':
            $i->fillCkEditor($value, '#' . $selector);
            break;

          case 'field':
            $i->fillField('#' . $selector, $value);
            break;

          case 'checkbox':
            $i->executeJS('document.getElementById("' . $selector . '").checked = true;');
            break;

          case 'select':
            $i->selectOption('#' . $selector, $value);
            break;

          case 'radio':
            $i->checkOption('#' . $selector);
            break;
        }
      }
    }

    // Only run the rest of the tests, if the flag is set.
    if ($full_test_flag) {

      // Click on the submit button.
      $i->click('#edit-submit');

      // Wait for and see the text that the content has
      // been successfully created.  Sidebar has some
      // different messaging.
      if ($content_type == 'uw_ct_sidebar') {

        $i->waitForText('Sidebar Home (sidebar) has been created.');
        $i->see('Sidebar Home (sidebar) has been created. ');
      }
      else {

        $i->waitForText($this->getContentTypeName($content_type) . ' ' . $edits['field']['edit-title-0-value'] . ' has been created');
        $i->see($this->getContentTypeName($content_type) . ' ' . $edits['field']['edit-title-0-value'] . ' has been created');
      }

      // Ensure that revert to defaults button is not on page.
      $i->dontSeeElement('input[value="Revert to defaults"]');

      // If on site footer, ensure there is no clone link.
      // All others, ensure there is a clone link.
      if ($content_type === 'uw_ct_site_footer') {
        $i->dontSeeLink('Clone');
      }
      else {
        $i->seeLink('Clone');
      }

      // Go to layout page and check that there is
      // the edit layout for title text and that
      // the content moderation block does not
      // contain publish this content link.
      $i->seeInCurrentUrl('layout');

      if ($content_type == 'uw_ct_sidebar') {
        $i->see('Edit layout for Home (sidebar)');
      }
      else {
        $i->see('Edit layout for ' . $edits['field']['edit-title-0-value']);
      }
      $i->dontSeeElement('a[href*="/admin/uw-content-moderation/"]');

      // Add a copy text block to the content type.
      $i->click('Layout');
      $i->waitForText('Edit layout for');

      // Click on the add block.
      $i->click('Add block');
      $i->waitForText('Choose a block');

      // Click on add copy text block.
      $i->click('Copy text');
      $i->waitForText('Configure block');

      // FIll in fields.
      $i->fillField('input[name="settings[label]"]', $i->uwRandomString());
      $i->fillCkEditor($i->uwRandomString(), 'textarea[name="settings[block_form][field_uw_copy_text][0][value]"]');

      // Click add block and ensure it appears on page.
      $i->click('Add block');
      $i->waitForElement(Locator::contains('div[class="uw-admin-label"]', 'Copy text'));
      $i->seeElement(Locator::contains('div[class="uw-admin-label"]', 'Copy text'));

      // Save the layout and ensure that new block appears
      // on the page with correct section classes.
      $i->waitForElementClickable('#edit-actions #edit-submit');
      $i->click('#edit-actions #edit-submit');
      $i->waitForElement('section[class*="layout"]');
      $i->seeElement('section[class*="uw-contained-width"]');
      $i->seeElement('section[class*="layout--uw-1-col"]');

      // Get the current edit url, so we can come back to it.
      $edit_url = $i->getCurrentUrl();

      // Check for content moderation block.
      $i->seeElement('.uw-content-moderation__wrapper');
      $i->see('Published status');
      $i->see('Most recent version');
      $i->seeLink('Unpublish this content');
      $i->seeElement('a[href*="/admin/uw-content-moderation/"]');

      // Test the moderation block link works correctly.
      $i->click('Unpublish this content');
      $url = $i->getCurrentUrl();
      $match = preg_match(',/admin/uw-content-moderation/\d+/\d+/\d+$,', $url);
      $i->assertTrue((bool) $match, 'Moderation form URL is in the expected format.');

      // Go back to the content type edit page.
      $i->amOnPage($edit_url);

      // Ensure that the edit tab is on page and click on it.
      $i->seeLink('Edit');
      $i->click('li[class="tabs__tab"] a[href$="edit"]');

      // Click on the save button and ensure that the content
      // was saved and correct messages appear.
      $i->click('Save');
      if ($content_type == 'uw_ct_sidebar') {
        $i->see('Home (sidebar) has been updated');
      }
      else {
        $i->see($edits['field']['edit-title-0-value'] . ' has been updated');
        $i->dontSee('Edit layout for');
      }

      // All content types except footer have moderation form.
      if (
        $content_type !== 'uw_ct_site_footer'
      ) {
        $i->seeLink('Unpublish this content');
      }

      // Specific checks for the footer.
      if ($content_type == 'uw_ct_site_footer') {

        // Check that social media Icons appear for the footer.
        $i->seeElement('.uw-site-footer__social-media');
        $i->seeElement('(//span[@class="off-screen"][normalize-space()="Instagram"])[1]');
        $i->seeElement('(//span[@class="off-screen"][normalize-space()="Instagram"])[1]');
        $i->seeElement('(//span[@class="off-screen"][normalize-space()="X (formerly Twitter)"])[1]');
        $i->seeElement('(//span[@class="off-screen"][normalize-space()="LinkedIn"])[1]');
        $i->seeElement('(//span[@class="off-screen"][normalize-space()="Facebook"])[1]');
        $i->seeElement('(//span[@class="off-screen"][normalize-space()="Youtube"])[1]');
        $i->seeElement('(//span[@class="off-screen"][normalize-space()="Snapchat"])[1]');

        // If we are on the site footer, ensure that when
        // we try to add a new site footer, it goes to the
        // site footer that we have already added.  This
        // ensures that there is only ever one site footer.
        $i->amOnPage('node/add/uw_ct_site_footer');
        $i->see('Edit Site Footer ' . $edits['field']['edit-title-0-value']);
      }

      // Go the view node and get the current url.
      $i->click(Locator::contains('.tabs__tab a', 'View'));
      $view_url = $i->getCurrentUrl();

      // If this content type has page display options, then test it.
      if ($this->contentTypeHasPageDisplayOptions($content_type)) {

        // The page display options.
        $page_display_options = [
          'all',
          'title',
          'none',
        ];

        // The roles that have permissions for page
        // display options.
        $roles_with_page_display_perms = [
          'uw_role_site_manager',
          'uw_role_content_editor',
          'uw_role_content_author',
        ];

        // Step through each of the roles and test teh page
        // display options.
        foreach ($roles_with_page_display_perms as $role_with_page_display_perms) {

          // Login with the role.
          $i->amOnPage('user/logout');
          $i->loginWithRole($role_with_page_display_perms);

          // Step through each of the page display options and test
          // that it works as expected.
          foreach ($page_display_options as $page_display_option) {

            // Go to the edit url.
            $i->amOnPage($edit_url . '/edit');
            $i->waitForElement('#edit-uw-additional-options');

            // Click on the page display option details.
            $i->click('#edit-uw-additional-options');
            $i->waitForElement('#edit-field-uw-option');

            // Select the page display option.
            $i->selectOption('#edit-field-uw-option', $page_display_option);

            // Click on the submit and ensure that it gets updated.
            $i->click('#edit-submit');
            $i->waitForText(' has been updated');

            // Check that the page display option shows correctly
            // on the node page.
            switch ($page_display_option) {

              // All option should have site name and the navigation.
              case 'all':
                $i->seeElement('.uw-site-name__text');
                $i->seeElement('.uw-header__navigation');
                break;

              // Title option should have site name and no navigation.
              case 'title':
                $i->seeElement('.uw-site-name__text');
                $i->dontSeeElement('.uw-header__navigation');
                break;

              // None option should have no site name and no navigation.
              case 'none':
                $i->dontSeeElement('.uw-site-name__text');
                $i->dontSeeElement('.uw-header__navigation');
                break;
            }
          }
        }
      }

      // Roles and whether they can see revisions.
      $roles_to_test = [
        'authenticated',
        'uw_role_site_owner',
        'uw_role_form_editor',
        'uw_role_form_results_access',
      ];

      // Step through each of the roles and test the revisions.
      foreach ($roles_to_test as $role_to_test) {

        // Login with the role.
        $i->amOnPage('user/logout');
        $i->logInWithRole($role_to_test);

        // Go to the revisions page.
        $i->amOnPage($view_url . '/revisions');

        // Ensure that they can not view revisions.
        $i->see('You are not authorized to access this page.');
      }
    }
  }

  /**
   * Function to test the listing page.
   *
   * @param array $dates
   *   Array of dates.
   * @param string $type
   *   The type of listing page to test.
   */
  public function testListingPage(array $dates, string $type): void {

    // Get the acceptance tester.
    $i = $this;

    // Go the listing page.
    $i->amOnPage($type);

    // Step through each of the dates and ensure that
    // they are displayed properly.
    foreach ($dates as $date) {
      if ($type == 'events') {
        $display_date = strtoupper(date('l, F j, Y', strtotime($date))) . ' 1:00 PM - 2:00 PM';
      }
      else {
        $display_date = strtoupper(date('l, F j, Y', strtotime($date)));
      }
      $i->see($display_date);
    }
  }

  /**
   * Function to get all the edit fields for events, news and blogs.
   *
   * @param string $content_type
   *   The content type.
   *
   * @return array
   *   Array of edits to be used.
   */
  public function getContentTypeEdits(string $content_type): array {

    // Get the acceptance tester.
    $i = $this;

    // Get a title that can be used for many things.
    $title = $i->uwRandomString();

    // Get the edits based on the content type.
    switch ($content_type) {

      case 'uw_ct_blog':
        $edits = [
          'field' => [
            'edit-title-0-value' => $title,
            'edit-field-uw-blog-date-0-value-date' => date('m/d/Y'),
            'edit-field-uw-meta-description-0-value' => $title,
          ],
          'ckeditor' => [
            'edit-field-uw-blog-summary-0-value' => $title,
          ],
          'select' => [
            'edit-moderation-state-0-state' => 'published',
          ],
        ];
        break;

      case 'uw_ct_catalog_item':
        $edits = [
          'field' => [
            'edit-title-0-value' => $title,
            'edit-field-uw-meta-description-0-value' => $title,
          ],
          'select' => [
            'edit-moderation-state-0-state' => 'published',
          ],
        ];
        break;

      case 'uw_ct_contact':
        $edits = [
          'field' => [
            'edit-title-0-value' => $title,
            'edit-field-uw-ct-contact-sort-name-0-value' => $title,
            'edit-field-uw-ct-contact-title-0-value' => $i->uwRandomString(),
            'edit-field-uw-meta-description-0-value' => $title,
          ],
          'select' => [
            'edit-moderation-state-0-state' => 'published',
          ],
        ];
        break;

      case 'uw_ct_event':
        $edits = [
          'field' => [
            'edit-title-0-value' => $title,
            'edit-field-uw-event-date-0-time-wrapper-value-date' => date('m/d/Y'),
            'edit-field-uw-event-date-0-time-wrapper-value-time' => '13:00:00',
            'edit-field-uw-event-date-0-time-wrapper-end-value-time' => '14:00:00',
            'edit-field-uw-meta-description-0-value' => $title,
          ],
          'ckeditor' => [
            'edit-field-uw-event-summary-0-value' => $title,
          ],
          'select' => [
            'edit-moderation-state-0-state' => 'published',
          ],
        ];
        break;

      case 'uw_ct_expand_collapse_group':
        $edits = [
          'field' => [
            'edit-title-0-value' => $title,
          ],
          'select' => [
            'edit-moderation-state-0-state' => 'published',
          ],
        ];
        break;

      case 'uw_ct_news_item':
        $edits = [
          'field' => [
            'edit-title-0-value' => $title,
            'edit-field-uw-news-date-0-value-date' => date('m/d/Y'),
            'edit-field-uw-meta-description-0-value' => $title,
          ],
          'ckeditor' => [
            'edit-field-uw-news-summary-0-value' => $title,
          ],
          'select' => [
            'edit-moderation-state-0-state' => 'published',
          ],
        ];
        break;

      case 'uw_ct_opportunity':
        $edits = [
          'field' => [
            'edit-title-0-value' => $title,
            'edit-field-uw-opportunity-post-by-0-value' => $title,
            'edit-field-uw-opportunity-date-0-value-date' => date('m/d/Y'),
            'edit-field-uw-meta-description-0-value' => $title,
          ],
          'ckeditor' => [
            'edit-field-uw-opportunity-position-0-value' => $title,
          ],
          'select' => [
            'edit-field-uw-opportunity-type' => 'Volunteer',
            'edit-field-uw-opportunity-employment' => 'Part time',
            'edit-field-uw-opportunity-pos-number' => 2,
            'edit-moderation-state-0-state' => 'published',
          ],
        ];
        break;

      case 'uw_ct_profile':
        $edits = [
          'field' => [
            'edit-title-0-value' => $title,
            'edit-field-uw-ct-profile-sort-name-0-value' => $title,
            'edit-field-uw-ct-profile-affiliation-0-value' => 'University of Waterloo',
            'edit-field-uw-ct-profile-title-0-value' => $title,
            'edit-field-uw-meta-description-0-value' => $title,
          ],
          'ckeditor' => [
            'edit-field-uw-profile-summary-0-value' => $title,
          ],
          'select' => [
            'edit-moderation-state-0-state' => 'published',
          ],
        ];
        break;

      case 'uw_ct_project':
        $edits = [
          'field' => [
            'edit-title-0-value' => $title,
            'edit-field-uw-meta-description-0-value' => $title,
          ],
          'ckeditor' => [
            'edit-field-uw-project-summary-0-value' => $title,
          ],
          'select' => [
            'edit-field-uw-project-status' => 'Current projects',
            'edit-moderation-state-0-state' => 'published',
          ],
        ];
        break;

      case 'uw_ct_service':
        $edits = [
          'field' => [
            'edit-title-0-value' => $title,
            'edit-field-uw-meta-description-0-value' => $title,
          ],
          'select' => [
            'edit-field-uw-service-status' => 'Active',
            'edit-moderation-state-0-state' => 'published',
          ],
        ];
        break;

      case 'uw_ct_sidebar':
        $edits = [
          'field' => [
            'edit-field-uw-attach-page-0-target-id' => 'Home (1)',
          ],
          'select' => [
            'edit-moderation-state-0-state' => 'published',
          ],
        ];
        break;

      case 'uw_ct_site_footer':
        $edits = [
          'field' => [
            'edit-title-0-value' => $title,
            'edit-field-site-footer-instagram-0-value' => 'uofwaterloo',
            'edit-field-site-footer-twitter-0-value' => 'UWaterloo',
            'edit-field-site-footer-linked-in-0-value' => 'school/uwaterloo',
            'edit-field-site-footer-facebook-0-value' => 'university.waterloo',
            'edit-field-site-footer-you-tube-0-value' => 'user/uwaterloo',
            'edit-field-site-footer-snapchat-0-value' => 'uwaterloo',
          ],
          'select' => [
            'edit-field-site-footer-logo' => 'Faculty of Engineering',
            'edit-moderation-state-0-state' => 'published',
          ],
        ];
        break;

      case 'uw_ct_web_page':
        $edits = [
          'field' => [
            'edit-title-0-value' => $title,
            'edit-field-uw-meta-description-0-value' => $title,
          ],
          'select' => [
            'edit-moderation-state-0-state' => 'published',
          ],
        ];
        break;
    }

    return $edits;
  }

  /**
   * Function to get the fields of the content type.
   *
   * @param string $content_type
   *   The content type.
   *
   * @return array
   *   Array of fields for the content type.
   */
  public function getContentTypeFields(string $content_type): array {

    // Return at least empty fields.
    $fields = [];

    // Get the fields based on the content type.
    switch ($content_type) {

      case 'uw_ct_blog':
        $fields = [
          'fields' => [
            'Title',
            'Date',
            'Type of media',
            'Summary',
            'Intentionally leave summary blank',
            'Author name',
            'Author link',
            'Blog tag(s)',
            'Description of content',
          ],
          'groups' => [
            'field_uw_blog_listing_page_image-media-library-wrapper' => 'Listing page image',
          ],
          'required' => [
            'Title',
            'Date',
            'Summary',
            'Description of content',
          ],
          'tabs' => [
            'edit-uw-additional-options' => 'Exclude from automatically-generated listings',
          ],
        ];
        break;

      case 'uw_ct_catalog_item';
        $fields = [
          'fields' => [
            'Name',
            'Catalog',
            'Type of media',
            'Item summary',
            'Description of content',
          ],
          'required' => [
            'Name',
            'Catalog',
            'Description of content',
          ],
          'tabs' => [
            'edit-uw-additional-options' => 'Exclude from automatically-generated listings',
          ],
        ];
        break;

      case 'uw_ct_contact':
        $fields = [
          'fields' => [
            'WatIAM user ID',
            'Name',
            'Name for sorting purposes',
            'Group(s)',
            'Affiliation',
            'Title, position, or description',
            'Email',
            'Phone #',
            'Location',
            'Contact for',
            'Additional information',
            'Description of content',
          ],
          'groups' => [
            'field_uw_contact_listing_image-media-library-wrapper' => 'Listing image',
            'field_uw_ct_contact_image-media-library-wrapper' => 'Image (portrait)',
            'edit-field-uw-ct-contact-link-profile-0' => 'Link to profile',
            'edit-field-uw-ct-contact-link-persona-0' => 'Link to personal website or CV',
          ],
          'required' => [
            'Name',
            'Name for sorting purposes',
            'Affiliation',
            'Title, position, or description',
            'Description of content',
          ],
          'tabs' => [
            'edit-uw-additional-options' => 'Exclude from automatically-generated listings',
          ],
        ];
        break;

      case 'uw_ct_event':
        $fields = [
          'fields' => [
            'Title',
            'Date and time',
            'Type of media',
            'Summary',
            'Intentionally leave summary blank',
            'Additional information',
            'Event website',
            'Cost',
            'Event location',
            'Map',
            'Description of content',
          ],
          'groups' => [
            'field_uw_event_listing_page_img-media-library-wrapper' => 'Listing page image',
          ],
          'required' => [
            'Title',
            'Date and time',
            'Summary',
            'Description of content',
          ],
          'tabs' => [
            'edit-uw-additional-options' => 'Exclude from automatically-generated listings',
          ],
        ];
        break;

      case 'uw_ct_expand_collapse_group':
        $fields = [
          'fields' => [
            'Title',
          ],
          'required' => [
            'Title',
          ],
        ];
        break;

      case 'uw_ct_news_item':
        $fields = [
          'fields' => [
            'Title',
            'Date',
            'Type of media',
            'Summary',
            'Intentionally leave summary blank',
            'Taxonomies',
            'News Tag(s)',
            'Description of content',
          ],
          'groups' => [
            'field_uw_news_listing_page_image-media-library-wrapper' => 'Listing page image',
          ],
          'required' => [
            'Title',
            'Date',
            'Summary',
            'Description of content',
          ],
          'tabs' => [
            'edit-uw-additional-options' => 'Exclude from automatically-generated listings',
          ],
        ];
        break;

      case 'uw_ct_opportunity':
        $fields = [
          'fields' => [
            'Title',
            'Type of media',
            'Summary',
            'Intentionally leave summary blank',
            'Opportunity type',
            'Employment type',
            'Job ID',
            'Posted by (department/office)',
            'Number of positions',
            'Reports to',
            'Date posted/application open',
            'Start date',
            'End date',
            'Description of content',
          ],
          'groups' => [
            'edit-field-uw-opportunity-deadline-0' => 'Application deadline',
            'edit-field-uw-opportunity-link-0' => 'Link to application',
            'edit-field-uw-opportunity-additional-0' => 'Additional information',
            'edit-field-uw-opportunity-contact-0' => 'Contact for additional information',
          ],
          'required' => [
            'Title',
            'Summary',
            'Opportunity type',
            'Employment type',
            'Posted by (department/office)',
            'Number of positions',
            'Date posted/application open',
            'Description of content',
          ],
          'tabs' => [
            'edit-uw-additional-options' => 'Exclude from automatically-generated listings',
          ],
        ];
        break;

      case 'uw_ct_profile':
        $fields = [
          'fields' => [
            'Synchronize with contact',
            'Name',
            'Name for sorting purposes',
            'Affiliation',
            'Title, position, or description',
            'Link to contact',
            'Profile type(s)',
            'Description of content',
          ],
          'groups' => [
            'field_uw_ct_profile_image-media-library-wrapper' => 'Listing page image',
            'edit-field-uw-ct-profile-link-persona-0' => 'Link to personal website or CV',
            'edit-field-uw-ct-profile-info-link-0' => 'Link to additional information',
            'edit-field-uw-ct-profile-type-wrapper' => 'Profile type(s)',
          ],
          'required' => [
            'Name',
            'Name for sorting purposes',
            'Summary',
            'Affiliation',
            'Title, position, or description',
            'Description of content',
          ],
          'tabs' => [
            'edit-uw-additional-options' => 'Exclude from automatically-generated listings',
          ],
        ];
        break;

      case 'uw_ct_project':
        $fields = [
          'fields' => [
            'Title',
            'Type of media',
            'Summary',
            'Status',
            'Topic(s)',
            'Name',
            'Member\'s link',
            'Project role',
            'Description of content',
          ],
          'groups' => [
            'field_uw_project_listing_image-media-library-wrapper' => 'Listing page image',
            'edit-field-uw-project-timeline-0' => 'Time line',
          ],
          'required' => [
            'Title',
            'Summary',
            'Status',
            'Description of content',
          ],
          'tabs' => [
            'edit-uw-additional-options' => 'Exclude from automatically-generated listings',
          ],
        ];
        break;

      case 'uw_ct_service':
        $fields = [
          'fields' => [
            'Service name',
            'Type of media',
            'Service summary',
            'Service status',
            'Service category',
            'Popularity',
            'Who can use this service',
            'What\'s available',
            'How to request this service',
            'Minimum notice to use service',
            'Average length of time to complete request',
            'Pricing/cost',
            'Support for this service',
            'Service hours',
            'Exceptions',
            'General notes about service hours',
            'Location lookup',
            'Country',
            'Latitude',
            'Longitude',
            'Map',
            'Description of content',
          ],
          'groups' => [
            'edit-field-uw-service-location-coord-0-value' => 'Location coordinates',
          ],
          'required' => [
            'Service name',
            'Service status',
            'Service category',
            'Description of content',
          ],
          'tabs' => [
            'edit-uw-additional-options' => 'Exclude from automatically-generated listings',
          ],
        ];
        break;

      case 'uw_ct_sidebar':
        $fields = [
          'fields' => [
            'Attach to page',
          ],
          'required' => [
            'Attach to page',
          ],
        ];
        break;

      case 'uw_ct_site_footer':
        $fields = [
          'fields' => [
            'Title',
            'Logo',
            'Instagram',
            'X (formerly Twitter)',
            'LinkedIn',
            'Facebook',
            'YouTube',
            'Snapchat',
          ],
          'required' => [
            'Title',
            'Logo',
          ],
        ];
        break;

      case 'uw_ct_web_page':
        $fields = [
          'fields' => [
            'Title',
            'Type of media',
            'Description of content',
          ],
          'required' => [
            'Title',
            'Description of content',
          ],
        ];
        break;
    }

    return $fields;
  }

  /**
   * Get the content type name from the given content type.
   *
   * @param string $content_type
   *   The content type.
   *
   * @return string
   *   The name of the content type.
   */
  private function getContentTypeName(string $content_type): string {

    // Return at least a null name.
    $name = '';

    // Return the name of the content type.
    switch ($content_type) {

      case 'uw_ct_blog':
        $name = 'Blog post';
        break;

      case 'uw_ct_catalog_item':
        $name = 'Catalog item';
        break;

      case 'uw_ct_contact':
        $name = 'Contact';
        break;

      case 'uw_ct_event':
        $name = 'Event';
        break;

      case 'uw_ct_expand_collapse_group':
        $name = 'Expand/collapse group';
        break;

      case 'uw_ct_news_item':
        $name = 'News item';
        break;

      case 'uw_ct_opportunity':
        $name = 'Opportunity';
        break;

      case 'uw_ct_profile':
        $name = 'Profile';
        break;

      case 'uw_ct_project':
        $name = 'Project';
        break;

      case 'uw_ct_service':
        $name = 'Service';
        break;

      case 'uw_ct_sidebar':
        $name = 'Sidebar';
        break;

      case 'uw_ct_site_footer':
        $name = 'Site footer';
        break;

      case 'uw_ct_webpage':
        $name = 'Web page';
        break;
    }

    // Return the name of the content type.
    return $name;
  }

  /**
   * Function to see if content type has a blank summary.
   *
   * @param string $content_type
   *   The content type.
   *
   * @return bool
   *   Flag if content type has blank summary.
   */
  private function contentTypeHasBlankSummary(string $content_type): bool {

    // Flag for blank summary.
    $has_blank_summary_flag = FALSE;

    // Content types that have blank summaries.
    $ct_has_blank_summary = [
      'uw_ct_blog',
      'uw_ct_event',
      'uw_ct_news_item',
      'uw_ct_opportunity',
      'uw_ct_profile',
      'uw_ct_project',
    ];

    // If the supplied content type is in array, set flag.
    if (in_array($content_type, $ct_has_blank_summary)) {
      $has_blank_summary_flag = TRUE;
    }

    return $has_blank_summary_flag;
  }

  /**
   * Function to check if content type has page display options.
   *
   * @param string $content_type
   *   The content type.
   *
   * @return bool
   *   If the content type has page display options.
   */
  private function contentTypeHasPageDisplayOptions(string $content_type): bool {

    // Flag for page display options.
    $has_page_display_options_flag = FALSE;

    // Content type does not have page display options.
    $ct_has_no_page_display_options = [
      'uw_ct_expand_collapse_group',
      'uw_ct_sidebar',
      'uw_ct_site_footer',
    ];

    // If not in the array of content types that do not
    // have page display options, set the flag.
    if (!in_array($content_type, $ct_has_no_page_display_options)) {
      $has_page_display_options_flag = TRUE;
    }

    return $has_page_display_options_flag;
  }

}
